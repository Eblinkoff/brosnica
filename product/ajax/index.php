<?php
if(!isset($_POST['metka']))
{
	h404();//**
	die;
}
require_once('../../env.php');

define('MY_DEBUG', false);//true - показывать ошибки

if(ROOT == '')
// если продакт
{
    $gap = explode('/', $_SERVER['DOCUMENT_ROOT']);
    unset($gap[count($gap) - 1]);
    define('INSIDE_ABSPATH', implode('/',$gap));// для доступа к служебным файлам, недоступным для посетителя сайта
    define('ABSPATH', $_SERVER['DOCUMENT_ROOT']);// для доступа к статике, т.е. к файлам, доступным для посетителей сайта
}
else
// если локалка
{
    define('INSIDE_ABSPATH', $_SERVER['DOCUMENT_ROOT']);// для доступа к служебным файлам, недоступным для посетителя сайта
    define('ABSPATH', $_SERVER['DOCUMENT_ROOT'] . ROOT);// для доступа к статике, т.е. к файлам, доступным для посетителей сайта
}

/** Файлы в кастомной папке у нас называются точно так же, как и файлы ядра. А названия кастомных классов состоят из приставки Custom и названия основного класса, например, class CustomMain. Таким образом, мы можем создать новый кастомный класс, а также можем частично кастомизировать существующие классы при помощи наследования. */
function myAutoload($classname)
{
    if ($classname != 'Custom' && substr($classname,-6) == 'custom') // если вызывается кастомный класс
    {
        // выделяем название основного класса
        $gap = explode('_custom', $classname);
        $rootName = $gap[0];// например, "Main.php"
        // запускаем файл старого класса и кастомного, который ему наследует
        if(is_file(INSIDE_ABSPATH . '/classes/'.$rootName.'.php'))// его может не быть, т.к. мы можем создать новый, кастомизированный класс
        {
            include_once(INSIDE_ABSPATH . '/classes/' . $rootName.'.php');
        }
        if(is_file(INSIDE_ABSPATH . '/custom/classes/'.$rootName.'.php'))
        {
            include_once(INSIDE_ABSPATH . '/custom/classes/'.$rootName.'.php');
        }
    }
    else // если вызывается обыкновенный класс ядра
    {
        include_once(INSIDE_ABSPATH . '/classes/' . $classname.'.php');
    }
}

// регистрируем загрузчик
spl_autoload_register('myAutoload');

$settings = new Settings();

if($_POST['metka'] == 1)
/** checkout
* @param array goods - массив с купленными товарами в формате json, пример одного элемента:
	{
		"thlaspi_id":5568,
		"offer_id":5568,
		"name":"Рудбекия блестящая",
		"price":100,
		"count":"1",
		"max_count":1,
		"discount":0,
		"this_page_url":"/shop/mnogoletniki-p-u/rudbekiya-blestyashchaya.html",
		"img_full_0":"/images/large/rudbekiya-blestyashchaya-rudbeckia-nitida-5568-35651.jpg",
		"img_thumbnail_0":"/images/list/rudbekiya-blestyashchaya-rudbeckia-nitida-5568-35651.jpg",
		"img_icon_0":"/images/icon/rudbekiya-blestyashchaya-rudbeckia-nitida-5568-35651.jpg"
	}
* @param string name - Имя покупателя (обязательный параметр)
* @param string email - email покупателя (необязательный параметр, но должен быть либо телефон, либо email)
* @param string phone - телефон покупателя (необязательный параметр, но должен быть либо телефон, либо email)
* @param string adress - адрес покупателя
* @param string comment - комментарий к заказу (необязательный параметр)
*/
{
	if(!isset($_POST['goods']))
	{
		h404();
		die;
	}
	// товары
	$goods = json_decode($_POST['goods']);
	if(!is_array($goods))
	{
		h404();
		die;
	}

	// создаём массив с данными о товарах для отправки
	$order = array();
	foreach($goods as $key => $good)
	{
		// thlaspi_id
		if(isset($good->thlaspi_id) && check_integer($good->thlaspi_id))
		{
			if(! isset($order[$key]))
			// если у нас ещё не создан соответствующий элемент массива
			{
				$order[$key] = new stdClass();
			}
			$order[$key]->thlaspi_id = $good->thlaspi_id;
		}
		else
		{
			h404('Нет thlaspi_id');
			die;
		}
		
		// offer_id
		if(isset($good->offer_id) && check_integer($good->offer_id))
		{
			$order[$key]->offer_id = $good->offer_id;
		}
		else
		{
			h404('Нет offer_id');
			die;
		}
		
		// count
		if(isset($good->count) && check_integer($good->count))
		{
			$order[$key]->count = $good->count;
		}
		else
		{
			h404('Нет count');
			die;
		}
		
		// discount
		if(isset($good->discount) && check_integer($good->discount))
		{
			$order[$key]->discount = $good->discount;
		}
		else
		{
			h404('Нет discount');
			die;
		}
	}

	// данные покупателя
	if(!($name = _string('name', 100, false)))
	{
		h404();
		die;
	}
	if(!($email = _email('email', true)))
	{
		if($email != '')
		{
			h404();
			die;
		}
	}
	if(!($phone = _string('phone', 15, true)))
	{
		if($phone != '')
		{
			h404();
			die;
		}
	}
	if(!($comment = _string('comment', 5000, true)))
	{
		if($comment != '')
		{
			h404();
			die;
		}
	}
	if(!($adress = _string('adress', 500, true)))
	{
		if($adress != '')
		{
			h404();
			die;
		}
	}
	if($phone == '' && $email == '')
	{
		h404('Empty phone and email');
		die;
	}
	// проверкам конец
	
	$order_table = '';// таблица заказов
	$order_table .= '
		<tr style="font-weight:bold;">
			<th>
				Название товара
			</th>
			<th>
				Количество
			</th>
			<th>
				Цена, р.
			</th>
			<th>
				Сумма, р.
			</th>
		</tr>
	';
	$count_result = 0;// общее количество
	$price_result = 0;// общая стоимость
	foreach($goods as $order_plant)
	{
		$name_of_good = filter_var($order_plant->name, FILTER_SANITIZE_STRING);
		$order_table .= '
			<tr>
				<td>
					'.$name_of_good.'
				</td>
				<td>
					'.$order_plant->count.'
				</td>
				<td>
					'.$order_plant->price.'
				</td>
				<td>
					'.($order_plant->count * $order_plant->price).'
				</td>
			</tr>
		';
		$count_result = $count_result + $order_plant->count;
		$price_result = $price_result + ($order_plant->count * $order_plant->price);
	}
	// строка ИТОГО
	$order_table .= '
		<tr style="font-weight:bold;">
			<td>
				ИТОГО:
			</td>
			<td>
				'.$count_result.'
			</td>
			<td>
			</td>
			<td>
				'.$price_result.'
			</td>
		</tr>
	';

	$order_table = '<table><tbody>'.$order_table.'</tbody></table>';
		
	// отправляем запрос
	$pull_plants = Custom::new_class('Pull_plants');
	$url = 'https://api.thlaspi.com/shop/code/'.$pull_plants->synchr_code.'/checkout';

	$checkout = new stdClass();
	$checkout->sourse = $settings->get_param('shop_name_global');// откуда пришёл заказ
	$checkout->plants = $order;// список купленных растений
	// список контактов покупателя:
	$checkout->contacts = array(
		'name' => $name,
		'email' => $email,
		'phone' => $phone,
		'adress' => $adress,
		'comment' => $comment,
	);
	if(! $order_result = $pull_plants->set_new_order($url, 'checkout', $checkout))
	{
		// Отправляем письмо с растениями в заказе на email администратора в случае, если произошла ошибка передачи
		$zagol = 'Новый заказ возможно не передан и не обработан!';
		$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
		$headers .= 'From: '.EMAIL_CONFIG;
		$message = ' 
			<h1>Новый заказ на "Броснице" возможно не передан по API!</h1> 
			<p>Сделан заказ на сайте <a href="'.$settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'">'.$settings->get_param('shop_name_global').'</a></p>
			<p>Список растений в заказе:</p> 
			'.$order_table.'
			<br><p>Данные покупателя:</p>
			<p><strong>Имя:</strong>'.$name.'</p>
			<p><strong>Электронный адрес:</strong>'.$email.'</p>
			<p><strong>Телефон:</strong>'.$phone.'</p>
			<p><strong>Адрес:</strong>'.$adress.'</p>
			<p><strong>Комментарий:</strong>'.$comment.'</p>
			<p><a href="https://thlaspi.com/sales/" target="_blank">Перейти на thlaspi.com</a></p>
		';
		//mail($email, $zagol, $message,$headers);
		send_mail(PUBLIC_MAIL, $zagol, $message, '', $error_output,$trace_output);
		echo json_decode('{"error" => "'.$pull_plants->error_message.'"}');
		die;
	}
		
	echo json_encode($order_result);
	
	// Надо ещё отправить письма покупателю и продавцу о том, что заказ совершён
	if($settings->get_param('smtp'))
	{
		// формирум письма. Сначала - администратору
		$zagol = 'Сделан заказ № '.$order_result->order_id.' на сайте '.$settings->get_param('shop_name_global');
		$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
		$headers .= 'From: '.EMAIL_CONFIG;
		$message = ' 
			<h1>Новый заказ на "Броснице"!</h1> 
			<p>Сделан заказ номер '.$order_result->order_id.' на сайте <a href="'.$settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'">'.$settings->get_param('shop_name_global').'</a></p>
			<p>Список растений в заказе:</p> 
			'.$order_table.'
			<br><p>Данные покупателя:</p>
			<p><strong>Имя:</strong>'.$name.'</p>
			<p><strong>Электронный адрес:</strong>'.$email.'</p>
			<p><strong>Телефон:</strong>'.$phone.'</p>
			<p><strong>Адрес:</strong>'.$adress.'</p>
			<p><strong>Комментарий:</strong>'.$comment.'</p><br>
			<p><a href="https://thlaspi.com/sales/" target="_blank">Перейти на thlaspi.com</a></p>
		';
		//mail($email, $zagol, $message,$headers);
		send_mail(PUBLIC_MAIL, $zagol, $message, '', $error_output,$trace_output);
		
		// формирум письма. Затем - клиенту
		if($email != '')
		{
			$zagol = 'Заказ № '.$order_result->order_id.' на сайте '.$settings->get_param('shop_name_global');
			$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
			$headers .= 'From: '.EMAIL_CONFIG;
			$message = ' 
				<h1>Спасибо за Ваш заказ!</h1> 
				<p>Сделан заказ номер '.$order_result->order_id.' на сайте <a href="'.$settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'">'.$settings->get_param('shop_name_global').'</a></p>
				<p>Список растений в заказе:</p> 
				'.$order_table.'
				<p>Благодарим Вас за сделанный заказ. Заказ обрабатывается, мы обязательно свяжемся с Вами когда заказ будет сформирован.</p>
			';
			//mail($email, $zagol, $message,$headers);
			send_mail($email, $zagol, $message, '', $error_output,$trace_output);
		}
	}
	// также обновляем товары, т.к. их количество изменилось

	// сначала получаем выгрузку новостей, т.к. они используются на всех страницах сайта в сайдбаре
	$news = $pull_plants->get_news(100);

	// потом получаем выгрузку статей, т.к. они тоже используются на всех страницах сайта в сайдбаре
	$articles = $pull_plants->get_articles(100);
    $plants = Custom::static_class('Main')::get_goods($pull_plants);
    $categories = Custom::static_class('Category')::get_categories($pull_plants);

    // записываем объекты $categories и $plants, $news, $articles в специальной функции для того, чтобы не таскать их через параметры
    Settings::save_object('news', $news);
    Settings::save_object('articles', $articles);
    Settings::save_object('plants', $plants);
    Settings::save_object('categories', $categories);

	// затем - обновляем страницы товаров и категорий
	$main = Custom::new_class('Main');
	$main->update_goods_pages();
}
elseif($_POST['metka'] == 2)
/** Отправить запрос на уведомление, когда растение появится
* @param int good_id - id товара, на который пользователь подписывается
* @param string name - Имя покупателя (обязательный параметр)
* @param string email - email покупателя (необязательный параметр, но должен быть либо телефон, либо email)
* @param string phone - телефон покупателя (необязательный параметр, но должен быть либо телефон, либо email)
*/
{
	// данные покупателя
	if(!($good_id = _integer('good_id', false)))
	{
		h404();
		die;
	}
	if(!($name = _string('name', 100, false)))
	{
		h404();
		die;
	}
	if(!($email = _email('email', true)))
	{
		if($email != '')
		{
			h404();
			die;
		}
	}
	if(!($phone = _string('phone', 15, true)))
	{
		if($phone != '')
		{
			h404();
			die;
		}
	}
	if($phone == '' && $email == '')
	{
		h404('Empty phone and email');
		die;
	}
	// проверкам конец



	// отправляем запрос
	$pull_plants = Custom::new_class('Pull_plants');
	$url = 'https://api.thlaspi.com/shop/code/'.$pull_plants->synchr_code.'/notify';

	$notify = new stdClass();
    $notify->sourse = $settings->get_param('shop_name_global');// откуда пришёл заказ
    $notify->good_id = $good_id;
	// список контактов покупателя:
    $notify->contacts = array(
		'name' => $name,
		'email' => $email,
		'phone' => $phone,
	);
	$order_result = $pull_plants->set_new_order($url, 'notify', $notify);

	echo json_encode($order_result);
}
else
{
	h404('No metka');
}

function h404($message='')
/** Отдаёт 404
*/
{
	header("HTTP/1.0 404 Not Found");
	header("HTTP/1.1 404 Not Found");
	header("Status: 404 Not Found");
	die($message);
}
function ed($v)
{
	// echo '<pre>';
	ob_start();
	var_dump($v);
	$t = ob_get_contents();
	ob_end_clean();
	echo htmlspecialchars($t);
	// echo '</pre>';
	die;
}
function check_integer($value){//проверяет value на то, что он целый, положительный, не дробный
	if($value===0){//ноль тоже включаем
		return true;
	}elseif (is_numeric($value)&&($value = intval($value)) && ($value > 0)){//
		return true;
	}
	return false;
}
function _integer($label,$zero_ok=true){
	/** Проверяет входной параметр вида $_POST[$label] на integer. 
	* @string param $label
	* @boolean param $zero - если true (по умолчанию), то 0 тоже проходит. Если false - ноль не считается проходным и проходят только целые числа от 1 до +бесконечности
	* @int return - проверенное значение $_POST[$label] или false, в случае непрохождения
	*/
	if(isset($_POST[$label])){
		if($zero_ok){
			if($_POST[$label] == 0){
				return 0;
			}
		}else{
			if($_POST[$label] == 0){
				return false;
			}
		}
		if(check_integer($_POST[$label])){
			return $_POST[$label];
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function _string($label,$max_lenght,$may_be_empty=true){
	/** Проверяет входной параметр вида $_POST[$label] на string. 
	* @string param $label
	* @int param $max_lenght - максимальная длина строки
	* @boolean param $may_be_empty - если true (по умолчанию), то '' тоже проходит. Если false - '' не считается проходным и проходят только строки
	* @int return - проверенное значение $_POST[$label] или false, в случае непрохождения
	*/
	if(isset($_POST[$label]) && is_string($_POST[$label])){
		if($may_be_empty){
			if($_POST[$label] == ''){
				return '';
			}
		}
		if($text = filter_var($_POST[$label], FILTER_SANITIZE_STRING)){
			if(mb_strlen($text,'utf-8') > $max_lenght){
				return mb_substr($text,0,$max_lenght,'utf-8');
			}else{
				return $text;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function _url($label,$max_lenght,$may_be_empty=true){
	/** Проверяет входной параметр вида $_POST[$label] на url. 
	* @string param $label
	* @int param $max_lenght - максимальная длина строки
	* @boolean param $may_be_empty - если true (по умолчанию), то '' тоже проходит. Если false - '' не считается проходным и проходят только строки
	* @int return - проверенное значение $_POST[$label] или false, в случае непрохождения
	*/
	if(isset($_POST[$label]) && is_string($_POST[$label])){
		if($may_be_empty){
			if($_POST[$label] == ''){
				return '';
			}
		}
		if($text = filter_var($_POST[$label], FILTER_SANITIZE_URL)){
			if(mb_strlen($text,'utf-8') > $max_lenght){
				return mb_substr($text,0,$max_lenght,'utf-8');
			}else{
				return $text;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function _email($label,$may_be_empty=true){
	/** Проверяет входной параметр вида $_POST[$label] на email. 
	* @string param $label
	* @boolean param $may_be_empty - если true (по умолчанию), то '' тоже проходит. Если false - '' не считается проходным и проходят только строки
	* @int return - проверенное значение $_POST[$label] или false, в случае непрохождения
	*/
	$max_lenght = 129;
	if(isset($_POST[$label]) && is_string($_POST[$label])){
		if($may_be_empty){
			if($_POST[$label] == ''){
				return '';
			}
		}
		if($text = filter_var($_POST[$label], FILTER_SANITIZE_EMAIL)){
			if(mb_strlen($text,'utf-8') > $max_lenght){
				return mb_substr($text,0,$max_lenght,'utf-8');
			}else{
				return $text;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}


/**
 * Отправляет электронное письмо
 *
 * @param string|array $recipient получатель/получатели
 * @param string $subject тема письма
 * @param string $body содержание письма
 * @param string $from адрес отправителя
 * @param string $error_output вывод ошибки
 * @param string $trace_output вывод трассировки
 * @return boolean
 */
function send_mail($recipient, $subject, $body, $from = '', &$error_output = '', &$trace_output = '')
{
	if(defined('SMTP_MAIL') && ! SMTP_MAIL)
	// если smtp отключён, отправляем сообщение без SMTP
	{
		mail($recipient, $subject, $body, $from);
		return true;
	}
	include_once(INSIDE_ABSPATH.'/classes/mail-master/class.phpmailer.php');

	$mail = new PHPMailer();

	if (defined('SMTP_MAIL') && SMTP_MAIL && SMTP_HOST && SMTP_LOGIN && SMTP_PASSWORD)
	{
		$mail->isSMTP(); // telling the class to use SMTP
		$mail->Host       = SMTP_HOST;  // SMTP server
		$mail->SMTPDebug  = MY_DEBUG ? 1 : 0; // enables SMTP debug information (for testing)
										           // 1 = errors and messages
										           // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		if (SMTP_PORT)
		{
			$mail->Port   = SMTP_PORT;          // set the SMTP port for the GMAIL server
		}
		$mail->Username   = SMTP_LOGIN;    // SMTP account username
		$mail->Password   = SMTP_PASSWORD; // SMTP account password

		// TO_DO: Don't mix up these modes; ssl on port 587 or tls on port 465 will not work.
		// TO_DO: PHPMailer 5.2.10 introduced opportunistic TLS - if it sees that the server is advertising TLS encryption (after you have connected to the server), it enables encryption automatically, even if you have not set SMTPSecure. This might cause issues if the server is advertising TLS with an invalid certificate, but you can turn it off with $mail->SMTPAutoTLS = false;.
		$mail->SMTPAutoTLS = false;

		// TO_DO: Failing that, you can allow insecure connections via the SMTPOptions property introduced in PHPMailer 5.2.10 (it's possible to do this by subclassing the SMTP class in earlier versions), though this is not recommended as it defeats much of the point of using a secure transport at all:
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
	}
	else
	{
		return false;
	}
	
	$mail->setFrom($from ? $from : EMAIL_CONFIG, 'Питомник "Бросница"');
	$mail->Subject = $subject;
	$mail->msgHTML($body);

	if (is_array($recipient))
	{
		foreach ($recipient as $to)
		{
			$mail->addAddress($to);
		}
	}
	elseif (strpos($recipient, ',') !== false)
	{
		$recipients = explode(',', $recipient);
		foreach ($recipients as $r)
		{
			$mail->addAddress(trim($r));
		}
	}
	else
	{
		$mail->addAddress($recipient);
	}

	$mailssend = $mail->send();

	$error_output = $mail->ErrorInfo;
	return $mailssend;
}
