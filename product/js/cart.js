// Скрипты для корзины
// document.addEventListener("DOMContentLoaded", function(){
	// console.log('ok')
	// create_goods_list_cart();
	// createWishListCart();
// });
$(document).ready(function() {
	create_goods_list_cart();
	createWishListCart();
});
function create_goods_list_cart()
/** При каждой загрузке страницы обращаемся к localStorage и проверяем - нет ли там товаров в корзине
*/
{
	var cart_array = getLocalStorage('cart_array');
	if(cart_array)
	{
		// и если они есть - выкладываем информацию о товарах в контейнере .goods-list-cart
		var cart_goods_array = JSON.parse(cart_array);
		// console.dir(cart_goods_array)
		// получаем копию шаблона одной строки - товара в корзине
		var empty_good = document.getElementById('set_one_good').firstElementChild;
		// получаем контейнер - куда будем вставлять товары
		var goods_cart_container = document.querySelectorAll('.js_shop-cart-list-products li');
		if(goods_cart_container)
		{
			var i = 0;// счётчик для вставки товаров в корзину
			var count = 0;// считает суммарное количество
			var discount = 0;// считает суммарную скидку
			var price_old = 0;// суммарная цена без скидки
			var price = 0;// суммарная цена со скидкой
			var summ = 0;// суммарная сумма
			// удаляем старые товары
			var ul = document.querySelector('.js_shop-cart-list-products');// parentElement
			for(var k = 0;k < goods_cart_container.length;k++)
			{
				if(goods_cart_container[k].getAttribute('data-is-good') == '1')
				{
					ul.removeChild(goods_cart_container[k]);
				}
			}
			for(var good of cart_goods_array)
			{
				/** good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
				*/
				discount += +good.discount;
				count += +good.count;
				price_old += +good.price;
				price += +good.price - (good.price*good.discount/100);
				summ += good.count*(+good.price - (good.price*good.discount/100));
				// клонируем шаблон строки
				var clone = empty_good.cloneNode(true);
				// заполняем клон актуальной информацией
				if(clone.querySelector('.js_price-for-result'))
				// общая сумма за товар
				{
					clone.querySelector('.js_price-for-result').innerHTML = good.count*(+good.price - (good.price*good.discount/100));
					clone.querySelector('.js_price-for-result').id = 'summ_price_for_good_cart_'+good.offer_id;
				}
				if(clone.querySelector('.js_price_for_one'))
				// цена без скидки
				{
					clone.querySelector('.js_price_for_one').innerHTML = good.price;
				}
				if(clone.querySelector('.js_discount'))
				// скидки
				{
					clone.querySelector('.js_discount').innerHTML = good.discount;
				}
				if(clone.querySelector('.js_discount_price'))
				// цена со скидкой
				{
					clone.querySelector('.js_discount_price').innerHTML = good.price - (good.price*good.discount/100);
				}
				if(clone.querySelector('.js_cart_remove'))
				// кнопка уалить товар из корзины
				{
					clone.querySelector('.js_cart_remove').setAttribute('data-offer-id',good.offer_id);
				}
				if(clone.querySelector('.js_article_cart'))
				// артикул
				{
					clone.querySelector('.js_article_cart').innerHTML = good.thlaspi_id;
				}
				var name = good.name;
				var pribavka = '';

				if(typeof(good.offer_name) != 'undefined')
				{
					if(good.offer_name != '')
					{
						pribavka = ' <br>'+good.offer_name+': '+good.offer_value;
					}
					else
					{
						pribavka = ' <br>'+good.offer_value;
					}
				}
				name += pribavka;

				if(clone.querySelector('.js_name-good-cart'))
				// имя и данные о товаре в корзине
				{
					clone.querySelector('.js_name-good-cart').href = good.this_page_url;
					clone.querySelector('.js_name-good-cart').innerHTML = name;
				}
				if(clone.querySelector('.js_category_in_cart'))
				// категория, к которой относится товар
				{
					clone.querySelector('.js_category_in_cart').href = good.cat_url;
					clone.querySelector('.js_category_in_cart').innerHTML = good.cat_name;
				}
				if(clone.querySelector('.js_inp_gray'))
				// поле для изменения количества товаров
				{
					clone.querySelector('.js_inp_gray').max = good.max_count;
					clone.querySelector('.js_inp_gray').value = good.count;
					clone.querySelector('.js_inp_gray').setAttribute('data-offer-id',good.offer_id);
				}
				if(clone.querySelector('.js_img_in_cart_table'))
				// столбец с фото
				{
					clone.querySelector('.js_img_in_cart_table').style.backgroundImage = 'url('+good.img_icon_0+')';
					clone.querySelector('.js_img_in_cart_table').href = good.this_page_url;
					clone.querySelector('.js_img_in_cart_table').title = good.name;
				}
				// и вставляем в таблицу товаров
				// goods_cart_container.append(clone) // убираем, т.к. не все браузеры поддерживают append
				$(clone).insertAfter(goods_cart_container[0]);
				i++;
			}
			// теперь ещё вставляем total данные в таблицу
			// document.querySelector('.shop-table__cell_discount.cart-total .shop-table__cell-content').innerHTML = discount;// скидка
			document.querySelector('.shop-table__cell_count.cart-total .shop-table__cell-content').innerHTML = count;// количество
			// document.querySelector('.shop-table__cell_price-old.cart-total .shop-table__cell-content').innerHTML = price_old;// суммарная цена без скидки
			// document.querySelector('.shop-table__cell_price.cart-total .shop-table__cell-content').innerHTML = price;// суммарная цена со скидкой
			document.querySelector('.shop-table__cell_sum.cart-total .shop-table__cell-content').innerHTML = summ;// суммарная сумма
			// если скидки нет убираем лишние столбцы с нулями
			if(discount == 0)
			{
				var price_old_elems = document.querySelectorAll('.shop-table__cell_price-old');
				if(price_old_elems.length)
				{
					for(var i = 0; i < price_old_elems.length; i++)
					{
						$(price_old_elems[i]).addClass('del-important');
					}
				}
				var discount_elems = document.querySelectorAll('.shop-table__cell_discount');
				if(discount_elems.length)
				{
					for(var i = 0; i < discount_elems.length; i++)
					{
						$(discount_elems[i]).addClass('del-important');
					}
				}
			}
		}
		cart_calculator();// перерасчитываем сумму
	}
	else
	{
		console.log('Ошибка: нет массива с купленными товарами');
		confirmation('Ошибка: нет массива с купленными товарами', 3);
	}
}

function createWishListCart()
// список хотелок в корзине
{
	var wish_list = getLocalStorage('wish_list');
	if(wish_list)
	{
		var wishes_array = JSON.parse(wish_list);
		// получаем копию шаблона одной строки - товара в списке хотелок
		var empty_good = document.getElementById('set_one_good_in_list').firstElementChild;
		// получаем контейнер - куда будем вставлять товары из списка хотелок
		var goods_cart_container = document.querySelector('.wishes-list-cart');	
		
		var cart_array = getLocalStorage('cart_array');
		if(cart_array)
		{
			var cart_goods_array = JSON.parse(cart_array);
			var k = 0;// считаем прореженные элементы виш-листа
			// и если товары в виш-листе есть - выкладываем информацию о товарах в контейнере .wishes-list-cart
			kangaroo: for(var good of wishes_array)
			{
				// прореживаем массив хотелок - может быть данный товар из хотелок уже в корзине
				if(cart_goods_array.length)
				{
					for(var cart_good of cart_goods_array)// прохоим массив товаров в корзине
					// и проверяем: нет ли соответствия? Если есть - выкидываем из виш-листа
					{
						if(cart_good.thlaspi_id == good[0].thlaspi_id)
						{
							k++;
							continue kangaroo;
						}
					}
				}
				/** good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
				***********************
					
				Шаблон карточек товаров-хотелок wish_good_in_ist.tpl
				*/
				// клонируем шаблон строки
				var clone = linkGoodJs(empty_good, good);
				// и вставляем в таблицу товаров
				// goods_cart_container.append(clone) // убираем, т.к. не все браузеры поддерживают append
				goods_cart_container.appendChild(clone)
			}
			cart_calculator();// перерасчитываем сумму
		
			if(document.getElementById('header-of-full-block-wish') && wishes_array.length && wishes_array.length > k)
			{
				// показываем заголовок виш-листа
				$(document.getElementById('header-of-full-block-wish')).show();
			}
		}
	}
	// else тут нет, т.к. в хотелках может ничего не быть
}

function wishToCart(th)
/** Товар из вишлиста в корзину
* @param obj th - this - кнопка "Добавить в корзину" товара в списке хотелок
*/
{
	var id = th.getAttribute('data-id');
	
	var wish_list = getLocalStorage('wish_list');
	if(wish_list)
	{
		var wishes_array = JSON.parse(wish_list);
		// for(var good of wishes_array)
		// {
			// if(good[0].thlaspi_id == id)
			// {
				// var this_offer = good[0]; // this_offer - объект с выбранным оффером или товаром
				// var index = i;
				// break;
			// }
			// i++;
		// }
		for(var i = 0;i < wishes_array.length;i++)
		{
			if(wishes_array[i][0].thlaspi_id == id)
			{
				if(wishes_array[i].length > 1)
				// это товар с офферами, следовательно надо выяснить какой оффер выбран
				{
					var selectElem = document.getElementById('offers_change_select_'+id);
					if(selectElem)
					{
						var option = selectElem.options[selectElem.selectedIndex];// выбранный option select-та с офферами
						if(option)
						{
							for(var k = 0; k < wishes_array[i].length; k++)
							{
								if(wishes_array[i][k].offer_id == option.value)
								{
									var this_offer = wishes_array[i][k]; // this_offer - объект с выбранным оффером или товаром
									var index = i;
									break;
								}
							}
						}
					}
					break;
				}
				else
				// это не оффер, либо единственный оффер
				{
					var this_offer = wishes_array[i][0]; // this_offer - объект с выбранным оффером или товаром
					var index = i;
					break;
				}
			}
		}

		if(!this_offer)
		{
			console.log('Нет выбранного оффера');
			confirmation('Ошибка! Товар не выбран.', 3);
			return;
		}

		// затем извлекаем из local Storage массив с покупками, если его нет - создаём.
		var cart_array = getLocalStorage('cart_array');
		if(cart_array)
		{
			var localStorageArray = JSON.parse(cart_array);
		}
		else
		{
			localStorageArray = [];
		}
		// выбранное количество товаров
		var count = countInput('count_'+this_offer.offer_id);

		// И записываем в массив купленных товаров наш товар.
		localStorageArray.push({
			thlaspi_id: this_offer.thlaspi_id,
			offer_id: this_offer.offer_id,
			offer_name: this_offer.offer_name,
			offer_value: this_offer.offer_value,
			article: this_offer.article,
			name: this_offer.name,
			price: this_offer.price,
			count: count,
			max_count: this_offer.max_count,
			discount: 0,
			this_page_url: this_offer.this_page_url,
			img_full_0: this_offer.img_full_0,
			img_thumbnail_0: this_offer.img_thumbnail_0,
			img_icon_0: this_offer.img_icon_0,
			cat_url: this_offer.cat_url,
			cat_name: this_offer.cat_name,
		});

		// записываем массив купленных товаров в local Storage
		putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
		
		// удаляем сам товар из dom
		var tot_good = document.getElementById('wish_good_'+wishes_array[index][0].thlaspi_id).parentElement;
		if(tot_good)
		{
			tot_good.parentElement.removeChild(tot_good);
		}
		// удаляем данный товар из хотелок
		wishes_array.splice(index, 1);
		// обновляем список товаров в корзине
		create_goods_list_cart();
		if(!wishes_array.length)
		{
			var header_of_full_block_wish = document.getElementById('header-of-full-block-wish');// заголовок на странице корзины
			if(header_of_full_block_wish)
			{
				$(header_of_full_block_wish).hide();
			}
		}
		// и записываем массив wishes_array в localStorage
		putLocalStorage('wish_list', JSON.stringify(wishes_array));
		// Обновляем виджет "В корзине" в шапке
		reload_cart_widget(localStorageArray);
		confirmation('Товар успешно добавлен в корзину!', 3);
	}
	else
	{
		confirmation('Ошибка!', 3);
		console.log('Нет массива хотелок')
	}
}

// теперь вставляем контакты, если они сохранены
var contacts_array = getLocalStorage('contacts_array');
if(contacts_array)
{
	// и если они есть - выкладываем информацию
	var localStorageContactsArray = JSON.parse(contacts_array);
	if(localStorageContactsArray['name'] !== undefined)
	{
		document.getElementById('inputNameReg').value = localStorageContactsArray['name'];
	}
	if(localStorageContactsArray['phone'] !== undefined)
	{
		document.getElementById('inputPhoneReg').value = localStorageContactsArray['phone'];
	}
	if(localStorageContactsArray['email'] !== undefined)
	{
		document.getElementById('inputEmailReg').value = localStorageContactsArray['email'];
	}
	if(localStorageContactsArray['adress'] !== undefined)
	{
		document.getElementById('inputAdressReg').value = localStorageContactsArray['adress'];
	}
	if(localStorageContactsArray['comment'] !== undefined)
	{
		document.getElementById('inputCommentReg').value = localStorageContactsArray['comment'];
	}
}
ch_delivery_deal();

function cart_calculator()
/** Подсчитывает и обновляет все суммарные показатели в корзине
*/
{
	var cart_array = getLocalStorage('cart_array');
	if(cart_array)
	{
		var localStorageArray = JSON.parse(cart_array);
		var summ_count = 0;
		var summ_price = 0;
		for(var good of localStorageArray)
		{
			summ_count += +good.count;
			summ_price += +good.count*good.price;
		}
		document.getElementById('cart_count').innerHTML = summ_count;
		document.querySelector('.shop-table__cell_count.cart-total .shop-table__cell-content').innerHTML = summ_count;
		document.querySelector('.shop-table__cell_sum.cart-total .shop-table__cell-content').innerHTML = summ_price;
		// теперь учтём ещё способ доставки
		var delivery_array = getLocalStorage('delivery_array');

		if(delivery_array)
		{
			var localStorageDeliveryArray = JSON.parse(delivery_array);
			var deliveryCost = localStorageDeliveryArray.cost;
		}
		else
		{
			deliveryCost = 0;
		}
		document.getElementById('shop_total_block_cost').innerHTML = +summ_price + +deliveryCost;
	}
	else
	{
		console.log('Ошибка: нет массива с купленными товарами');
		confirmation('Ошибка: нет массива с купленными товарами', 3);
	}
}
function delete_good_in_cart(cancel_icon)
/** Удаление товара из корзины
* @param elem cancel_icon - иконка (кнопка) удалить для данного товара
*/
{
	var offer_id = cancel_icon.getAttribute('data-offer-id');
	var cart_array = getLocalStorage('cart_array');
	if(cart_array)
	{
		var localStorageArray = JSON.parse(cart_array);
		var i = 0;
		for(var good of localStorageArray)
		{
			if(good.offer_id == offer_id)
			{
				// удаляем данный элемент массива cart_array(localStorageArray)
				localStorageArray.splice(i, 1);
				// записываем массив купленных товаров в local Storage
				putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
				cart_calculator();
				// Обновляем виджет "В корзине" в шапке
				reload_cart_widget(localStorageArray);
				// удаляем строку с товаром DOM
				var elem = cancel_icon.parentElement.parentElement.parentElement;
				elem.parentNode.removeChild(elem);
				// обновляем вижет с хотелками в корзине
				document.querySelector('.if-unwish-delete').innerHTML = '';
				createWishListCart();
				setTimeout(function(){
					// нажатие на кнопку "В вишлист"
					$('.js_shop_wishlist').each(function(){
						$(this).click(function(){
							var id = $(this).attr('data-id');
							wish(this,id);
						});
					});
					// почему-то не работает
					// selectApp.up('select:not(.decorated)');
				},1000);
				return false;
			}
			i++;
		}
		console.log('Ошибка: Товар не может быть удалён.');
		confirmation('Ошибка: Товар не может быть удалён.', 3);
		return false;
	}
	else
	{
		console.log('Ошибка: нет массива с купленными товарами');
		confirmation('Ошибка: нет массива с купленными товарами', 3);
		return false;
	}
}

function edit_cart_good_count(input)
/** Изменение количества купленных товаров в корзине
* @param elem input - инпут с количеством для данного товара
*/
{
	var offer_id = input.getAttribute('data-offer-id');
	var count = input.value;
	var cart_array = getLocalStorage('cart_array');
	if(cart_array)
	{
		var localStorageArray = JSON.parse(cart_array);
		for(var good of localStorageArray)
		{
			if(good.offer_id == offer_id)
			{
				if(count <= 0)
				{
					count = 1;
					input.value = 1;
					confirmation('Количество товаров не может быть меньше нуля', 3);
				}
				if(count > good.max_count)
				{
					count = good.max_count;
					input.value = good.max_count;
					confirmation('Количество товаров на складе '+good.max_count, 3);
				}
				good.count = count;
				// записываем массив купленных товаров в local Storage
				putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
				// Обновляем общие суммы
				if(document.getElementById('summ_price_for_good_cart_'+offer_id))
				{
					document.getElementById('summ_price_for_good_cart_'+offer_id).innerHTML = +count*good.price;
				}
				cart_calculator();
				// Обновляем виджет "В корзине" в шапке
				reload_cart_widget(localStorageArray)
				return;
			}
		}
		console.log('Ошибка: Количество не может быть изменено');
		confirmation('Ошибка: Количество не может быть изменено', 3);
	}
	else
	{
		console.log('Ошибка: нет массива с купленными товарами');
		confirmation('Ошибка: нет массива с купленными товарами', 3);
	}
}

function checkout()
/** Сделать заказ из корзины
*/
{
	var cart_array = getLocalStorage('cart_array');

	if(cart_array)
	{
		var inputNameReg = document.getElementById('inputNameReg');
		var inputEmailReg = document.getElementById('inputEmailReg');
		var inputPhoneReg = document.getElementById('inputPhoneReg');
		var inputAdressReg = document.getElementById('inputAdressReg');
		var inputCommentReg = document.getElementById('inputCommentReg');
		if(inputNameReg.value == '')
		{
			confirmation('Представьтесь, пожалуйста.', 5);
			inputNameReg.focus();
			return;
		}
		if(inputEmailReg.value == '' && inputPhoneReg.value == '')
		{
			confirmation('Введите, пожалуйста телефон или Email.', 5);
			inputPhoneReg.focus();
			return;
		}
		if(inputEmailReg.value != '' && (inputEmailReg.value.indexOf('@') < 0 || inputEmailReg.value.indexOf('.') < 0))
		{
			confirmation('Ошибка в email', 5);
			inputEmailReg.focus();
			return;
		}

		confirmation('Спасибо за Ваш заказ! Наш менеджер свяжется с Вами в самое ближайшее время!', 5);

		jQuery.ajax({
			url:'/product/ajax/',
			type:'POST',
			// contentType: "application/json; charset=utf-8",
			// dataType: "json",
			data: {metka: 1, goods: cart_array, name: inputNameReg.value, email: inputEmailReg.value, phone: inputPhoneReg.value, adress: inputAdressReg.value, comment: inputCommentReg.value},
			success:function(data){
				console.log(data);
				var result = JSON.parse(data);
				// result.order_id - номер заказа. Кладём его в хранилище
				putLocalStorage('order_id', result.order_id);
				
				setTimeout(function(){
					// записываем в массив последнего заказа содержимое корзины и перехоим на страницу последнего заказа
					if(cart_array)
					{
						var localStorageArray = JSON.parse(cart_array);
						removeLocalStorage('cart_array');
						putLocalStorage('last_order', JSON.stringify(localStorageArray));
					}
					document.location.href = "/product/shop/last_order/";
				},4000);
			},
			error: function() {
				console.log('error');
				// confirmation('Ошибка: Не удалось отправить заказ. Перезагрузите, пожалуйста страницу и попробуйте ещё раз.', 5);
			}    
		});
	}
	else
	{
		console.log('Ошибка: нет массива с купленными товарами');
		confirmation('Ошибка: нет массива с купленными товарами', 3);
	}
}


$(document).on('click', '.js_cart_count_plus', function () {

	var $counter = $(this).closest('.shop-counter').find('input');

	if ($counter.length)
	{
		var vCounter = $counter.val();// количество товаров в инпуте

		if (vCounter/*  && vCounter > 1 */)
		{
			$counter.val(+vCounter + 1);
			$counter.trigger('input');
		}
	}
});


$(document).on('click', '.js_cart_count_minus', function () {

	var $counter = $(this).closest('.shop-counter').find('input');

	if ($counter.length)
	{
		var vCounter = $counter.val();// количество товаров в инпуте

		if (vCounter && vCounter > 1)
		{
			$counter.val(+vCounter - 1);
			$counter.trigger('input');
		}
	}
});

function ch_contacts(th, code)
/** Вносим изменения в массив контактов при редактировании инпута
* @param obj th - this, input с вводимыми данными
* @param string code - код вносимой информации и, соответственно, ключ соответствующего элемента объекта контактов
*/
{
	var contacts_array = getLocalStorage('contacts_array');

	if(contacts_array)
	{
		var localStorageContactsArray = JSON.parse(contacts_array);
	}
	else
	{
		localStorageContactsArray = {};
	}
	localStorageContactsArray[code] = th.value;

	putLocalStorage('contacts_array', JSON.stringify(localStorageContactsArray));	
}

function ch_delivery(ev)
/** Меняем способ доставки по клику
*/
{
	var target = ev.target || ev.srcElement;
	if(target.tagName == 'INPUT')
	{
		setTimeout(function(){
			ch_delivery_deal()
			cart_calculator();// перерасчитываем сумму
		}, 1000);
	}
}
function ch_delivery_deal()
/** Меняем способ доставки
*/
{
	var delivery_array = getLocalStorage('delivery_array');

	if(delivery_array)
	{
		var localStorageDeliveryArray = JSON.parse(delivery_array);
	}
	else
	{
		localStorageDeliveryArray = {};
	}
	var inp = document.querySelectorAll('.shop-cart-list-delivery li input');
	for(var i = 0; i < inp.length; i++)
	{
		if(inp[i].checked == true)
		{
			break;
		}
	}
	// если ни одного пункта не выбрано
	if(inp[i] == undefined)
	{
		if(localStorageDeliveryArray.number != undefined)
		// если массив способов доставки уже есть, выбиаем пункт из этого массива
		{
			i = +localStorageDeliveryArray.number - 1;
		}
		else
		// выбираем первый пункт
		{
			i = 0;
		}
		// inp[i].checked = true;
	}
	else
	// только если способы доставки вообще есть
	{
		localStorageDeliveryArray.number = inp[i].value;
		var li = inp[i].parentElement.parentElement.parentElement.parentElement;

		localStorageDeliveryArray.name = li.querySelector('.shop-table-item__name').innerHTML;
		localStorageDeliveryArray.description = li.querySelector('.shop-table-item__text').innerHTML;
		localStorageDeliveryArray.cost = li.querySelector('.shop-table__cell_sum .shop-table__cell-content').innerHTML;
		// console.log(localStorageDeliveryArray)

		putLocalStorage('delivery_array', JSON.stringify(localStorageDeliveryArray));
	}
}
// нажатие на кнопку "В вишлист"
$('.js_shop_wishlist').each(function(){
	$(this).click(function(){
		var id = $(this).attr('data-id');
		wish(this,id);
	});
});
