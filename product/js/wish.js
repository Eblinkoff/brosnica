// Скрипты для вишлиста (список хотелок)

createWishListCart();
function createWishListCart()
// список хотелок в корзине
{
	var wish_list = getLocalStorage('wish_list');
	
	// получаем контейнер - куда будем вставлять товары из списка хотелок
	var goods_cart_container = document.querySelector('.wishes-list-cart');
	goods_cart_container.innerHTML = '';
	if(wish_list)
	{
		var wishes_array = JSON.parse(wish_list);
		if(! wishes_array.length)
		{
			goods_cart_container.innerHTML = 'Список отложенных товаров пуст.';
			return;
		}
		// получаем копию шаблона одной строки - товара в списке хотелок
		var empty_good = document.getElementById('set_one_good_in_list').firstElementChild;
		
		var cart_array = getLocalStorage('cart_array');
		var cart_goods_array = JSON.parse(cart_array);
		// var k = 0;// считаем прореженные элементы виш-листа
		// и если товары в виш-листе есть - выкладываем информацию о товарах в контейнере .wishes-list-cart
		for(var good of wishes_array)
		{
			// прореживаем массив хотелок - может быть данный товар из хотелок уже в корзине
			// if(cart_goods_array.length)
			// {
				// for(var cart_good of cart_goods_array)// прохоим массив товаров в корзине
				// // и проверяем: нет ли соответствия? Если есть - выкидываем из виш-листа
				// {
					// if(cart_good.thlaspi_id == good[0].thlaspi_id)
					// {
						// k++;
						// continue kangaroo;
					// }
				// }
			// }
			/** good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
			***********************
				
			Шаблон карточек товаров-хотелок wish_good_in_ist.tpl
			*/
			// клонируем шаблон строки
			var clone = linkGoodJs(empty_good, good);
			// и вставляем в таблицу товаров
			// goods_cart_container.append(clone) // убираем, т.к. не все браузеры поддерживают append
			goods_cart_container.appendChild(clone)
		}
		reload_cart_widget(cart_goods_array);// перерасчитываем сумму
	
		if(document.getElementById('header-of-full-block-wish') && wishes_array.length && wishes_array.length > k)
		{
			// показываем заголовок виш-листа
			$(document.getElementById('header-of-full-block-wish')).show();
		}
	}
	else
	// в хотелках ничего нет
	{
		goods_cart_container.innerHTML = 'Список отложенных товаров пуст.';
	}
}

function wishToCart(th)
/** Товар из вишлиста в корзину
* @param obj th - this - кнопка "Добавить в корзину" товара в списке хотелок
*/
{
	var id = th.getAttribute('data-id');
	
	var wish_list = getLocalStorage('wish_list');
	if(wish_list)
	{
		var wishes_array = JSON.parse(wish_list);
		// for(var good of wishes_array)
		// {
			// if(good[0].thlaspi_id == id)
			// {
				// var this_offer = good[0]; // this_offer - объект с выбранным оффером или товаром
				// var index = i;
				// break;
			// }
			// i++;
		// }
		for(var i = 0;i < wishes_array.length;i++)
		{
			if(wishes_array[i][0].thlaspi_id == id)
			{
				if(wishes_array[i].length > 1)
				// это товар с офферами, следовательно надо выяснить какой оффер выбран
				{
					var selectElem = document.getElementById('offers_change_select_'+id);
					if(selectElem)
					{
						var option = selectElem.options[selectElem.selectedIndex];// выбранный option select-та с офферами
						if(option)
						{
							for(var k = 0; k < wishes_array[i].length; k++)
							{
								if(wishes_array[i][k].offer_id == option.value)
								{
									var this_offer = wishes_array[i][k]; // this_offer - объект с выбранным оффером или товаром
									var index = i;
									break;
								}
							}
						}
					}
					break;
				}
				else
				// это не оффер, либо единственный оффер
				{
					var this_offer = wishes_array[i][0]; // this_offer - объект с выбранным оффером или товаром
					var index = i;
					break;
				}
			}
		}

		if(!this_offer)
		{
			console.err('Нет выбранного оффера');
			confirmation('Ошибка! Товар не выбран.', 3);
			return;
		}

		// затем извлекаем из local Storage массив с покупками, если его нет - создаём.
		var cart_array = getLocalStorage('cart_array');
		if(cart_array)
		{
			var localStorageArray = JSON.parse(cart_array);
		}
		else
		{
			localStorageArray = [];
		}
		// выбранное количество товаров
		var count = countInput('count_'+this_offer.offer_id);
		
		// И записываем в массив купленных товаров наш товар.
		localStorageArray.push({
			thlaspi_id: this_offer.thlaspi_id,
			offer_id: this_offer.offer_id,
			offer_name: this_offer.offer_name,
			offer_value: this_offer.offer_value,
			article: this_offer.article,
			name: this_offer.name,
			price: this_offer.price,
			count: count,
			max_count: this_offer.max_count,
			discount: 0,
			this_page_url: this_offer.this_page_url,
			img_full_0: this_offer.img_full_0,
			img_thumbnail_0: this_offer.img_thumbnail_0,
			img_icon_0: this_offer.img_icon_0,
			cat_url: this_offer.cat_url,
			cat_name: this_offer.cat_name,
		});

		// записываем массив купленных товаров в local Storage
		putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
		// удаляем сам товар из dom
		var tot_good = document.getElementById('wish_good_'+wishes_array[index][0].thlaspi_id).parentElement;
		if(tot_good)
		{
			tot_good.parentElement.removeChild(tot_good);
		}
		// удаляем данный товар из хотелок
		wishes_array.splice(index, 1);
		// обновляем список товаров в корзине
		confirmation('Товар успешно добавлен в корзину!', 3);
		
		if(! wishes_array.length)
		{
			var goods_cart_container = document.querySelector('.wishes-list-cart');
			goods_cart_container.innerHTML = 'Список отложенных товаров пуст.';
		}
		// и записываем массив wishes_array в localStorage
		putLocalStorage('wish_list', JSON.stringify(wishes_array));
		// Обновляем виджет "В корзине" в шапке
		reload_cart_widget(localStorageArray);// перерасчитываем сумму
	}
	else
	{
		confirmation('Ошибка!', 3);
		console.log('Нет массива хотелок')
	}
}

$(document).on('click', '.js_cart_count_plus', function () {

	var $counter = $(this).closest('.shop-counter').find('input');

	if ($counter.length)
	{
		var vCounter = $counter.val();// количество товаров в инпуте

		if (vCounter/*  && vCounter > 1 */)
		{
			$counter.val(+vCounter + 1);
			$counter.trigger('input');
		}
	}
});


$(document).on('click', '.js_cart_count_minus', function () {

	var $counter = $(this).closest('.shop-counter').find('input');

	if ($counter.length)
	{
		var vCounter = $counter.val();// количество товаров в инпуте

		if (vCounter && vCounter > 1)
		{
			$counter.val(+vCounter - 1);
			$counter.trigger('input');
		}
	}
});

// нажатие на кнопку "В вишлист"
$('.js_shop_wishlist').each(function(){
	$(this).click(function(){
		var id = $(this).attr('data-id');
		wish(this,id);
	});
});
