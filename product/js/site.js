// Скрипты для всех страниц сайта
/**
Глобальные массивы:
search - массив со всеми товарами для поиска, формируется сервером,
cart_array - массив купленных товаров, формируется js
goods - массив товаров на странице, формируется сервером,
wish_list - массив товаров в хотелках,
*/
/**
 * Скрипт Скроллинга: кнопка наверх
 * css class кнопки: top-link
 отсюда: wp-kama.ru/id_372/strelka-naverh-s-plavnyim-vertikalnyim-prokruchivaniem.html
 */
 
function is_available(good)
/** Есть в наличии или нет
* @param obj good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
* @return bool result - true - есть в наличии и false - нет в наличии
*/
{
	if(!good.length)
	{
		return false;
	}
	if(good.length == 1)
	// если офферов нет
	{
		if(good[0].max_count >= 1 && good[0].price > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	// если есть офферы, наличие цены и количества у основного товара игнорируем
	{
		for(var i = 0; i < good.length; i++)
		{
			if(good[i].max_count >= 1 && good[i].price > 0)
			{
				return true;
			}
		}
		return false;
	}
}
function linkGoodJs(empty_good, good)
/** Собираем товар по шаблону
* @param obj empty_good - копия шаблона одной строки - товара
* @param obj good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
* @return obj clone - собранный товар для вставки куда-либо
*/
{
	var clone = empty_good.cloneNode(true);
	// заполняем клон актуальной информацией
	
	// сам товар
	clone.querySelector('.shop-item-product__box').id = 'wish_good_'+good[0].thlaspi_id;
	
	// хотелки
	clone.querySelector('.js_favorite.js_shop_wishlist').setAttribute('data-id', good[0].thlaspi_id);
	$(clone.querySelector('.js_favorite.js_shop_wishlist')).addClass('favorited');
	
	if(good.length > 1)
	// значит это офферы
	{
		var clone_price_node = clone.querySelector('.js_shop_param_price');// элемент с ценой. Этот - полностью убираем, заменяем его сгенерированными
		var radios = '';
		var options = '';
		var clone_price = '';
		var selected = ' selected="selected"';
		var checked = ' checked="checked"';
		for(var i = 0; i < good.length; i++)
		{
			if(good[i].max_count == 0 || good[i].price == 0) continue;
			
			options += '<option data-count="'+good[i].max_count+'" value="'+good[i].offer_id+'"'+selected+'>'+good[i].offer_value+'</option>';
			
			radios += '<input class="offer-radio-checker" id="m_'+good[i].thlaspi_id+'_offer_'+good[i].offer_id+'" name="show_offer_'+good[i].thlaspi_id+'" value="'+good[i].offer_id+'"'+checked+' type="radio" onclick="check_radio_offer('+good[i].thlaspi_id+','+good[i].offer_id+')">';
			radios += '<label class="js_shop_param_price shop_param_price shop-item-price" for="m_'+good[i].thlaspi_id+'_offer_'+good[i].offer_id+'">\
					<div class="offer-param-name">\
						<strong>'+good[i].offer_value+'</strong>\
					</div>\
					<div class="is_avalible white">\
						<i class="fa fa-check"></i> Есть в наличии <span id="count_goods_'+good[i].max_count+'">'+good[i].max_count+'</span> шт.\
					</div>\
					<div>\
						<span class="shop-counter js_shop_buy">\
							<span class="shop-counter__widget">\
								<a class="shop-counter__reducer js-shop-counter__reducer shop-counter__button far fa-minus-square" href="javascript:void(0)" title="Уменьшить" role="button"></a>\
								<input value="1" name="count" class="number" step="any" min="1" max="'+good[i].max_count+'" id="count_'+good[i].offer_id+'" autocomplete="off" type="text">\
								<a class="shop-counter__adder js-shop-counter__adder shop-counter__button far fa-plus-square" href="javascript:void(0)" title="Добавить" role="button"></a>\
							</span>\
							<span class="product-in-cart"></span>\
						</span>\
						<span class="shop-prices js_shop_param_price" param32="'+good[i].offer_id+'">\
							<span class="shop-price">\
								<span class="js_shop_price" summ="'+good[i].price+'" format_price_1="" format_price_2="" format_price_3="">'+good[i].price+'</span>&nbsp;р\
							</span>\
							<!--span class="shop-price shop-price_old">151990&nbsp;р</span-->\
						</span>\
					</div>\
				</label>';
			// clone_price += '\
				// <span class="shop-prices js_shop_param_price" param32="'+good[i].offer_id+'"'+checked+'>\
					// <span class="shop-price">\
						// <span class="js_shop_price" summ="'+good[i].price+'" format_price_1="" format_price_2="" format_price_3="">'+good[i].price+'</span>&nbsp;р\
					// </span>\
					// <!--span class="shop-price shop-price_old">151990&nbsp;р</span-->\
				// </span>\
			// ';
			
			// после первого раза стираем их:
			if(selected != '')
			{
				selected = '';
				checked = '';
			}
		}
		// содержимое ul class="shop-item-product__options"
		clone.querySelector('.shop-item-product__options .control').innerHTML = '\
			<select id="offers_change_select_'+good[0].thlaspi_id+'" onchange="offer_param_ch(this,'+good[0].thlaspi_id+')" name="param32" class="js_shop_depend_param">'+options+'</select>'+radios;
		// clone_price_node.outerHTML = clone_price;
		clone_price_node.innerHTML = '';
	}
	else
	// значит это товар без офферов
	{
		clone.querySelector('.shop-item-product__options .control').innerHTML = '';
		clone.querySelector('.shop-item-product__numbers').innerHTML = '\
			<span class="shop-prices js_shop_param_price" param32="209">\
				<span class="shop-price">\
					<span class="js_shop_price" summ="'+good[0].price+'" format_price_1="" format_price_2="" format_price_3="">'+good[0].price+'</span>&nbsp;р\
				</span>\
				<!--span class="shop-price shop-price_old">'+good[0].price+'&nbsp;р</span-->\
			</span>\
			<span class="shop-counter js_shop_buy">\
				<span class="shop-counter__widget">\
					<a class="shop-counter__reducer js-shop-counter__reducer shop-counter__button far fa-minus-square" href="javascript:void(0)" title="Уменьшить" role="button"></a>\
					<input type="text" value="1" name="count" class="number" step="any" min="1" max="'+good[0].max_count+'" id="count_'+good[0].offer_id+'" autocomplete="off">\
					<a class="shop-counter__adder js-shop-counter__adder shop-counter__button far fa-plus-square" href="javascript:void(0)" title="Добавить" role="button"></a>\
				</span>\
				<span class="product-in-cart"></span>\
			</span>\
			';
		// цена
		// var js_shop_price = clone.querySelector('.js_shop_price');
		// if(js_shop_price)
		// {
			// clone.querySelector('.js_shop_price').innerHTML = good[0].price;
		// }
		
		// var input = clone.querySelector('.shop-counter__widget input');// поле с количеством товаров
		// input.id = 'count_'+good[0].thlaspi_id;
		// input.setAttribute('max', good[0].max_count);
	}
	
	clone.querySelector('.shop-product__anons.text span').innerHTML = good[0].max_count;// максимальное количество
	clone.querySelector('.shop-product__anons.text span').id = 'count_goods_'+good[0].thlaspi_id;// максимальное количество
		
	// кнопка купить
	var buyButton = clone.querySelector('.shop-item-product__actions.shop-actions input');
	if (buyButton)
	{
		buyButton.setAttribute('data-id', good[0].thlaspi_id)
		buyButton.setAttribute('onclick', 'wishToCart(this)');
		buyButton.setAttribute('data-target', '#modalConfirmation');
	}

	// купить в один клик
	clone.querySelector('.modal.modal_oneClick').id = 'modalOneClick_'+good[0].thlaspi_id;
	$(clone.querySelector('.js_shop_one_click')).hide();
	
	clone.querySelector('.shop-item-product__name a').innerHTML = good[0].name;
	clone.querySelector('.shop-item-product__article').innerHTML = 'Артикул: '+good[0].article;
	clone.querySelectorAll('a')[0].href = good[0].this_page_url;
	clone.querySelectorAll('a')[1].href = good[0].this_page_url;			

	// картинка
	clone.querySelector('.shop-item-product__image.js_shop_img').setAttribute('style', 'background-image: url('+good[0].img_thumbnail_0+');');
	clone.querySelector('.shop-item-product__image.js_shop_img').setAttribute('title', good[0].name);
	
	// характеристики
	var params = '';
	if (typeof good[0].param !== "undefined")// характеристик может и не быть
	{
		for(var i = 0; i < good[0].param.length; i++)
		{
			// console.log(good[0].param[i].name)
			// console.log(good[0].param[i].value)
			// console.log(good[0].param[i].unit)
			var unit = '';
			if(good[0].param[i].unit != '')
			{
				unit = ' '+good[0].param[i].unit;
			}
			params += '<b>'+good[0].param[i].name+':</b><span>&nbsp;'+good[0].param[i].value+'&nbsp;</span>'+unit+'<br>';
		}
	}
	clone.querySelector('.shop-item-product__anons').innerHTML = '<p class="like-h4">'+params+'</p>';
	
	// если товара нет в наличии
	if(! is_available(good))
	{
		// берём контейнер со всякими кнопками, ценами итд и получаем коллекцию его детей
		var cont = clone.querySelector('.shop-item-product__offer').children;
		for(var j = 0; j < cont.length; j++)
		{
			$(cont[j]).hide();// скрываем всех детей
		}
		// и затем создаём и вставляем элемент с надписью "Нет в наличии"
		clone.querySelector('.shop-item-product__offer').innerHTML = clone.querySelector('.shop-item-product__offer').innerHTML + '<!--span class="not-available-string">Нет в наличии</span-->' +
			' <div class="shop-item-product__actions shop-actions">\n' +
			'     <input class="button notify-when-available" action="notify-when-available" value="Сообщить когда появится" type="button" data-target="#modalNotifyWhenAvailable" data-toggle="modal" data-id="'+good[0].thlaspi_id+'">\n' +
			'</div>';
	}

	return clone;
}

function countInput(countInputId)
/** Отдаёт выбранное пользователем количество товаров при покупке
* @param string countInputId - id инпута с выбранным количеством
*/
{
	var count = 1;
	var count_input = document.getElementById(countInputId);
	if(count_input)
	{
		count = count_input.value;
	}
	return count;
}

// при каждой загрузке страницы обращаемся к localStorage и проверяем - нет ли там товаров в корзине и в вишлисте
var cart_array = getLocalStorage('cart_array');
// и если они есть -пишем об этом в шапке сайта
var localStorageArray = JSON.parse(cart_array);
reload_cart_widget(localStorageArray);
// console.dir(goods)// массив товаров на данной странице

function isIE(){
	var ie=!-[1,];
	if(ie){
		return true;
	}else{
		return false;
	}
}
function reload_cart_widget(cart_array)
/** Обновляем виджет корзины в шапке сайта
* @param array cart_array - массив купленных товаров. Каждый элемент этого массива - объект, в котором записаны все данные о купленном товаре
*/
{
	var count = 0;// общее количество товаров в корзине
	var summ = 0;// стоимость всех товаров в корзине
	if(cart_array == null || cart_array.length == 0)
	// если корзина пуста
	{
		if(document.getElementById('v_korzine'))
		{
			// $('#v_korzine').hide();
			$('#v_korzine').removeClass('interactive-link_counted');
		}
	}
	else
	// если в корзине что-то есть
	{
		for (var good of cart_array)
		{
			count += +good.count;
			summ += +good.count*good.price
			if(document.getElementById('count_goods_'+good.thlaspi_id))
			// если для этого товара есть поле "Есть в наличие", отнимаем купленное количество товаров
			{
				document.getElementById('count_goods_'+good.thlaspi_id).innerHTML = good.max_count - good.count;
			}
			if(document.getElementById('count_'+good.thlaspi_id))
			// также меняем максимальное количество у инпута
			{
				document.getElementById('count_'+good.thlaspi_id).setAttribute('max', (good.max_count - good.count));
			}
		}
		var v_korzine = document.getElementById('v_korzine');
		if(v_korzine)
		{
			if(document.getElementById('cart_summ'))
			{
				document.getElementById('cart_summ').innerHTML = summ;
			}
			if(document.getElementById('cart_count'))
			{
				document.getElementById('cart_count').innerHTML = count;
			}
			$('#v_korzine').addClass('interactive-link_counted');
		}
	}
	// также хотелки
	
	var wish_list = getLocalStorage('wish_list');
	var interactive_link_favorites = document.querySelector('.interactive-link_favorites');
	
	if(wish_list && interactive_link_favorites)
	{
		var wish_list_array = JSON.parse(wish_list);
		if(wish_list_array.length)
		// значит в списке хотелок что-то есть
		{
			var tagi = interactive_link_favorites.querySelector('.interactive-link__counter i');
			if(tagi)
			{
				tagi.innerHTML = wish_list_array.length;
			}
			else
			{
				// создаём тег i
				interactive_link_favorites.querySelector('.interactive-link__counter').innerHTML = '<i>'+wish_list_array.length+'</i>';
			}
			$(interactive_link_favorites).addClass('interactive-link_counted')
		}
		else
		// значит список хотелок пуст
		{
			$(interactive_link_favorites).removeClass('interactive-link_counted');
			var header_of_full_block_wish = document.getElementById('header-of-full-block-wish');// заголовок на странице корзины
			if(header_of_full_block_wish)
			{
				$(header_of_full_block_wish).hide();
			}
			var wishes_list_cart = document.querySelector('.wishes-list-cart');
			if(wishes_list_cart && !header_of_full_block_wish)
			{
				wishes_list_cart.innerHTML = 'Список отложенных товаров пуст.';
			}
		}
	}
}
//локальные хранилища
//initLocalStorage('gotop');
//putLocalStorage('key33', '152');
//removeLocalStorage('key33');
//alert(getLocalStorage('key33'));
function initLocalStorage(idStorageElement){
	if(!window.localStorage){//IE<8
		var storage = document.getElementById(idStorageElement);
		if (!storage.addBehavior) {
			//alert('return false');
			return false;
		} else {
			// поставить userData behavior
			storage.addBehavior("#default#userData");
			// загрузить пространство имен
			storage.load("namespace");
			return true;
		}
	}
}
function putLocalStorage(key, value) { // записать значение
	if(window.localStorage){
		var storage = window.localStorage;
		//storage.key=value;
		var retval = storage.setItem(key, value);
		if(retval==='S_OK'){
			return true;
		}else{
			return false;
		}
	}else{//ie
		storage.setAttribute(key, value);
		return storage.save("namespace");
	}
}
 
function getLocalStorage(key) { // получить значение
	if(window.localStorage){
		var storage = window.localStorage;
		//return storage.key;
		return storage.getItem(key);
	}else{//ie
		return storage.getAttribute(key);
	}
}
 
function removeLocalStorage(key) { // удалить значение
	if(window.localStorage){
		var storage = window.localStorage;
		var retval = storage.removeItem(key);
	}else{//ie
		storage.removeAttribute(key);
		storage.save("namespace");
	}
}
function confirmation(text,time){//выводит окно подтверждения действия
	/**
	Выводит на экран окно подтверждения действия. Окно держится time секунд 
	и потом исчезает. Для этой функции отдельный css файл со стилями
	@param text - текст подтверждения; строка
	@param time - количество секунд, которое подтверждение останется на экране; число
	*/
	if(document.getElementById('modalConfirmation') != null)
	{
		var textField = document.getElementById('modalConfirmation').querySelector('.h2');
		if(textField)
		{
			textField.innerHTML = text;
			$('#modalConfirmation').modal();
			if(time)
			{
				setTimeout(function(){
						document.getElementById('modalConfirmation').querySelector('.modal-close').click();
					},(+time*1000)
				);
			}
		}
	}
	else
	{
		console.log(text);
	}
}
function search_go(th)
/** Запускает поиск в шапке
* @param obj th - this объект input с поисковой строкой
*/
{
	del_ul_dropdown();
	if(search == undefined || th.value == '')
	{
		return;
	}
	// var container = document.querySelector('.search-input-wrapper');// контейнер, в котором лежит инпут поиска
	var container = th.parentElement;// контейнер, в котором лежит инпут поиска
	var ul = document.createElement('UL');
	ul.className = 'search-result-list navigation__list navigation__list_main';
	var i = 0;
	for (var plant of search)
	{
		if(frix_one(plant, th))
		{
			if(i == 0)
			// создаём контейнер для списка найденных товаров только когда хотя бы что-то нашлось
			{
				container.appendChild(ul);
			}
			add_search_list(plant, ul);
			// ограничение максимального количества в выдаче
			if(i > 15)
			{
				return;
			}
			i++;
		}
	}
}
function del_ul_dropdown()
/** Удаление ul с выпадающим списком
*/
{
	var container = document.querySelector('.search-input-wrapper ul');// контейнер, в котором лежит инпут поиска
	if(container)
	{
		container.parentNode.removeChild(container);
	}
}
// ставим уничтожитель выпадающего списка при любом клике
document.querySelector('BODY').addEventListener('click', function(){
	del_ul_dropdown();
})

function add_search_list(plant, ul)
/** Создаёт и добавляет li с найденным растением в ul 
* @param obj plant - объект с информацией о растении
* @param obj ul - объект контейнер списка найденных растений
*/
{
	var li = document.createElement('LI');
	li.className = 'navigation__item navigation__item_main';
	li.innerHTML = '<a href="'+plant.url+'"><img src="'+plant.image_href+'"><span class="text-dropdown-list">'+plant.name+'</span></a>';
	ul.appendChild(li);
}
function frix_one(plant, th)
/** Проверяет объект plant на соответствие поисковому запросу
* @param obj plant - объект с информацией о растении
* @param obj th - this объект input с поисковой строкой
* @return bool result - true - если удовлетворяет условию поиска и false - если нет
*/
{
	// plant.search - строка вида "Бузульник Пржевальского Ligularia przewalskii Многолетники" специально сформированная для поиска
	if(plant.search === undefined || plant.search == '') return false
	// переводим поисковую строку в нижний регистр
	var val = th.value.toLowerCase();
	
	if(plant.search.indexOf(val) >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function wish(th, id)
/** В вишлист
* @param obj th - this - элемент <span class="js_shop_wishlist shop_wishlist shop-like" с сердечком внутри
* @param int id - thlaspi_id товара
*/
{
	var wish_list = getLocalStorage('wish_list');
	if(wish_list && wish_list != '[]')// да, localStorage делает пустой массив вот таким
	{
		var wish_list_array = JSON.parse(wish_list);
		var index = wish_list_array.findIndex(function(item, index){if(item[0].thlaspi_id == id) return true;});
		if(index == -1)
		// значит добавляем в вишлист новый товар
		{
			var this_good = goods[id];
			if(this_good)
			{
				wish_list_array.push(this_good);
				// $(th.querySelector('i')).removeClass('fa-heart-o');
				$(th).addClass('favorited');
			}
		}
		else
		// значит такой товар уже есть в вишлисте, а index - его номер
		{
			// тут удаляем элемент массива с этим индексом
			wish_list_array.splice(index, 1);
			$(th).removeClass('favorited');

			// если родительский ul обладает классом if-unwish-delete, мы должны удалить этот товар из списка
			if($(th).closest('.if-unwish-delete').length)
			{
				// удаляем сам товар из dom
				$(th).closest('.shop-item-product').remove();
			}
		}
	}
	else
	{
		wish_list_array = [];
		var this_good = goods[id];
		if(this_good)
		{
			wish_list_array.push(this_good);
			$(th).addClass('favorited');
		}
		else
		{
			console.log(this_good);
			console.log('no this_good');
			return false;
		}
	}
	// после всех действий записываем массив хотелок в хранилище
	putLocalStorage('wish_list', JSON.stringify(wish_list_array));
	// обновляем виджеты в шапке	
	var cart_array = getLocalStorage('cart_array');
	// и если они есть -пишем об этом в шапке сайта
	var localStorageArray = JSON.parse(cart_array);
	reload_cart_widget(localStorageArray);
	
}
