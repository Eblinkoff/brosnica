// нажатие на превью под основной фотографией
$('.js_shop_preview_img').click(function(){
	$('.lineyka a').each(function(){
		$(this).removeClass("active");
	});
	$(this).addClass("active");
	$('#shop_id_img').attr('src', $(this).data('url'));
	return false;
});
$('.control-prev').click(function(){
	$('.lineyka').css('left',parseInt($('.lineyka').css('left')) + 85);
	return false;
});
$('.control-next').click(function(){
	$('.lineyka').css('left',parseInt($('.lineyka').css('left')) - 85);
	return false;
});
// нажатие на кнопку "Купить в один клик"
$('.one-click-id').click(function(){
	var id = $(this).attr('data-id');
	$('#cart_one_click_'+id).show();
});

// нажатие на кнопку "Добавить в корзину"
$('.notify-when-available').each(function(){
	$(this).click(function(){
		var id = $(this).attr('data-id');
		prepaireNotifyWhenAvailable(id);
	});
});

// нажатие на кнопку "Добавить в корзину"
$('.buy-button-id').each(function(){
	$(this).click(function(){
		var id = $(this).attr('data-id');
		buy(id);
	});
});

// нажатие на кнопку "Заказать" в "Купить в оин клик"
$('.one-click-correct').click(function(){
	var id = $(this).attr('data-id');
	buyOneclick(id);
});

// нажатие на кнопку "В вишлист"
$('.js_shop_wishlist').each(function(){
	$(this).click(function(){
		var id = $(this).attr('data-id');
		wish(this,id);
	});
});


// отмечаем товары, если они есть в вишлисте
if(typeof goods != 'undefined')
{
	var wish_list = getLocalStorage('wish_list');
	if(wish_list)
	{
		var wish_list_array = JSON.parse(wish_list);
		var wishes = document.querySelectorAll('.js_shop_wishlist');
		if(wishes)
		{
			// проходим все сердечки на странице
			for(var i = 0; i < wishes.length;i++)
			{
				for(var j = 0; j < wish_list_array.length; j++)
				{
					if(wish_list_array[j][0].thlaspi_id == wishes[i].getAttribute('data-id'))
					{
						$(wishes[i]).addClass('favorited');
					}
				}
			}
		}
	}
}

function get_this_offer(id)
/** Получаем выбранный оффер
* @param int id - thlaspi_id товара
* @return obj this_offer - объект с выбранным оффером или товаром
*/
{
	// берём массив с товарами и ищем в нём наш товар.
	if(!goods)
	{
		console.err('Нет массива с товарами');
		confirmation('Ошибка! На этой странице нет товаров.', 3);
		return;
	}
	var this_good = goods[id];
	// this_good - массив с объектами - офферами. Ищем один, который выбран пользователем или первый

	// ищем селект с офферами и получаем выделенный option, и, соответственно, выделенный оффер
	var selectElem = document.getElementById('offers_change_select_'+id);
	if(selectElem)
	{
		var option = selectElem.options[selectElem.selectedIndex];// выбранный option select-та с офферами
	}
	for (var offer of this_good)
	{
		if( offer.status == 'selected')
		// значит это товар без оффера
		{
			var this_offer = offer;
			break;
		}
		else
		{
			// значит это оффер. Проверяем, выделен ли он?
			// var input_offer = document.getElementById('m_'+offer.thlaspi_id+'_offer_'+offer.offer_id);
			// if(input_offer && input_offer.checked)
			// {
				// this_offer = offer;
				// break;
			// }
			if(selectElem && option && option.value == offer.offer_id)
			{
				this_offer = offer;
				break;
			}
		}
	}
	return this_offer;
}

/** Подготовить форму "Уведомить когда появится в наличии" */
function prepaireNotifyWhenAvailable(good_id)
{
	// подставляем кнопке "Отправить" модального окна id товара
	document.getElementById('notify_when_button').setAttribute('data-good-id', good_id);
	// также если есть емайл или телефон и имя - всиавляем их в поля
	var fio = document.getElementById('notify_when_fio');
	var tel = document.getElementById('notify_when_telefon');
	var email = document.getElementById('notify_when_email');

	var contacts_array = getLocalStorage('contacts_array');

	if(contacts_array)
	{
		var localStorageContactsArray = JSON.parse(contacts_array);
		if (typeof localStorageContactsArray['name'] != "undefined")
		{
			fio.value = localStorageContactsArray['name'];
		}
		if (typeof localStorageContactsArray['email'] != "undefined")
		{
			email.value = localStorageContactsArray['email'];
		}
		if (typeof localStorageContactsArray['phone'] != "undefined")
		{
			tel.value = localStorageContactsArray['phone'];
		}
	}
}

/** Отправить на сервер запрос об уведомлении о том, когда растение появится в наличии
 *
 * @param obj button - объект кнопки, на которую нажали, у кнопки есть атрибут data-good-id с id товара, о появлении которого надо сообщить
 */
function notifyWhenAvailable(button)
{
	var fio = document.getElementById('notify_when_fio');
	var tel = document.getElementById('notify_when_telefon');
	var email = document.getElementById('notify_when_email');
	var good_id = button.getAttribute('data-good-id')
	if(fio.value == '')
	{
		confirmation('Представьтесь, пожалуйста.', 5);
		fio.focus();
		return;
	}
	if(tel.value == '' && email.value == '')
	{
		confirmation('Напишите, пожалуйста, Ваши координаты: телефон или email', 5);
		if (tel.value == '')
		{
			tel.focus()
		}
		else
		{
			email.focus()
		}
		return;
	}

	// закрываем модальное окно
	document.getElementById('modalNotifyWhenAvailable').querySelector('.modal-close').click();


	// добавляем введённые кооринаты в массив контактов
	var contacts_array = getLocalStorage('contacts_array');

	if(contacts_array)
	{
		var localStorageContactsArray = JSON.parse(contacts_array);
	}
	else
	{
		localStorageContactsArray = {};
	}
	localStorageContactsArray['name'] = fio.value;
	if(email.value != '')
	{
		localStorageContactsArray['email'] = email.value;
	}
	if (tel.value != '')
	{
		localStorageContactsArray['phone'] = tel.value;
	}

	putLocalStorage('contacts_array', JSON.stringify(localStorageContactsArray));

	confirmation('Спасибо за проявленный интерес! Мы обязательно сообщим Вам когда данное растение появится! Также, видя интерес к данному растению, мы скорее пустим его в размножение.', 7);

	// отправляем уведомление на сервер
	jQuery.ajax({
		url:'/ajax/',
		type:'POST',
		data: {metka: 2, good_id: good_id, name: fio.value, email: email.value, phone: tel.value},
		success:function(data){
			console.log(data);
			var result = JSON.parse(data);
			console.log(result)
		},
		error: function() {
			console.log('error');
			confirmation('Ошибка: Не удалось отправить заказ. Перезагрузите, пожалуйста страницу и попробуйте ещё раз.', 5);
		}
	});
}

function buyOneclick(id)
/** Купить в один клик 
* @param int id - thlaspi_id товара
*/
{
	var inputNameReg = document.getElementById('one_click_name_'+id);
	var inputPhoneReg = document.getElementById('one_click_phone_'+id);
	if(inputNameReg.value == '')
	{
		confirmation('Представьтесь, пожалуйста.', 5);
		inputNameReg.focus();
		return;
	}
	if(inputPhoneReg.value == '')
	{
		confirmation('Напишите, пожалуйста, Ваши координаты: телефон или email', 5);
		inputPhoneReg.focus();
		return;
	}

	// закрываем модальное окно
	document.getElementById('modalOneClick_'+id).querySelector('.modal-close').click();
	
	// разбираемся: email или телефон?
	var is_email = false;// true - если это поле - email и false - если это поле - что угодно другое
	if(inputPhoneReg.value.indexOf('@') > 0 && inputPhoneReg.value.indexOf('.') > 0)
	{
		is_email = true;
	}
	
	var email = '';
	var phone = '';
	if(is_email)
	{
		email = inputPhoneReg.value;
	}
	else
	{
		phone = inputPhoneReg.value;
	}
	var this_offer = get_this_offer(id); // this_offer - объект с выбранным оффером или товаром
	if(!this_offer)
	{
		console.err('Нет выбранного оффера');
		confirmation('Ошибка! Товар не выбран.', 3);
		return;
	}

	var cart_array_one_click = [];
	
	// ищем выбранное количество товаров
	var count = countInput('count_'+this_offer.offer_id);
	
	// И записываем в массив купленных товаров наш товар.
	cart_array_one_click.push({
		thlaspi_id: this_offer.thlaspi_id,
		offer_id: this_offer.offer_id,
		offer_name: this_offer.offer_name,
		offer_value: this_offer.offer_value,
		article: this_offer.article,
		name: this_offer.name,
		price: this_offer.price,
		count: (count > this_offer.max_count ? this_offer.max_count : count),// выбранное количество товаров
		max_count: this_offer.max_count,
		discount: 0,
		this_page_url: this_offer.this_page_url,
		img_full_0: this_offer.img_full_0,
		img_thumbnail_0: this_offer.img_thumbnail_0,
		img_icon_0: this_offer.img_icon_0,
		cat_url: this_offer.cat_url,
		cat_name: this_offer.cat_name,
	});

	// добавляем введённые кооринаты в массив контактов
	var contacts_array = getLocalStorage('contacts_array');

	if(contacts_array)
	{
		var localStorageContactsArray = JSON.parse(contacts_array);
	}
	else
	{
		localStorageContactsArray = {};
	}
	localStorageContactsArray['name'] = inputNameReg.value;
	if(is_email)
	{
		localStorageContactsArray['email'] = email;
	}
	else
	{
		localStorageContactsArray['phone'] = phone;
	}

	putLocalStorage('contacts_array', JSON.stringify(localStorageContactsArray));	

	confirmation('Спасибо за Ваш заказ! Наш менеджер свяжется с Вами в самое ближайшее время!', 5);
	// отправляем заказ на сервер
	jQuery.ajax({
		url:'/ajax/',
		type:'POST',
		data: {metka: 1, goods: JSON.stringify(cart_array_one_click), name: inputNameReg.value, email: email, phone: phone, adress: '', comment: 'Заказ в один клик'},
		success:function(data){
			console.log(data);
			var result = JSON.parse(data);
			// result.order_id - номер заказа. Кладём его в хранилище
			putLocalStorage('order_id', result.order_id);
			
			setTimeout(function(){
				// записываем в массив последнего заказа содержимое корзины и перехоим на страницу последнего заказа
				putLocalStorage('last_order', JSON.stringify(cart_array_one_click));	
				document.location.href = "/shop/last_order/";
			},4000);
		},
		error: function() {
			console.log('error');
			confirmation('Ошибка: Не удалось отправить заказ. Перезагрузите, пожалуйста страницу и попробуйте ещё раз.', 5);
		}    
	});
}
function buy(id)
/** Купить. 
* @param int id - thlaspi_id товара
*/
{
	// removeLocalStorage('cart_array')
	// return
	var this_offer = get_this_offer(id); // this_offer - объект с выбранным оффером или товаром
	if(!this_offer)
	{
		console.log('Нет выбранного оффера');
		confirmation('Ошибка! Товар не выбран.', 3);
		return false;
	}

	// затем извлекаем из local Storage массив с покупками, если его нет - создаём.
	var cart_array = getLocalStorage('cart_array');
	if(cart_array)
	{
		var localStorageArray = JSON.parse(cart_array);
	}
	else
	{
		localStorageArray = [];
	}
	// localStorageArray - тут массив с объектами, отправленными в корзину
	
	// ищем выбранное количество товаров
	var count = countInput('count_'+this_offer.offer_id);
	
	// а вдруг такой товар уже покупали? Надо это выяснить и если да - просто прибавить количество
	var repeat_flag = true;
	for(var _good_in_cart of localStorageArray)
	{
		if(_good_in_cart.offer_id == this_offer.offer_id)
		{
			_good_in_cart.count = +_good_in_cart.count + +count;
			if(_good_in_cart.count > this_offer.max_count)
			// если мы перебрали общее максимальное количество
			{
				_good_in_cart.count = this_offer.max_count;
				confirmation('На складе больше нет товаров.', 3);
				// записываем массив купленных товаров в local Storage
				putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
				// Обновляем виджет "В корзине" в шапке
				reload_cart_widget(localStorageArray);
				return;
			}
			repeat_flag = false;// если товар уже есть в корзине, не создаём новый
			break;
		}
	}
	if(repeat_flag)
	{
		// И записываем в массив купленных товаров наш товар.
		localStorageArray.push({
			thlaspi_id: this_offer.thlaspi_id,
			offer_id: this_offer.offer_id,
			offer_name: this_offer.offer_name,
			offer_value: this_offer.offer_value,
			name: this_offer.name,
			price: this_offer.price,
			count: (count > this_offer.max_count ? this_offer.max_count : count),// выбранное количество товаров
			max_count: this_offer.max_count,
			discount: 0,
			this_page_url: this_offer.this_page_url,
			img_full_0: this_offer.img_full_0,
			img_thumbnail_0: this_offer.img_thumbnail_0,
			img_icon_0: this_offer.img_icon_0,
			cat_url: this_offer.cat_url,
			cat_name: this_offer.cat_name,
		});
	}

	// записываем массив купленных товаров в local Storage
	putLocalStorage('cart_array', JSON.stringify(localStorageArray));	
	// Обновляем виджет "В корзине" в шапке
	reload_cart_widget(localStorageArray);
	// removeLocalStorage('cart_array');
	// confirmation('Товар успешно добавлен в корзину!', 3);
}
function createInputsArray(form)
/** формируем из инпутов массив с выбранными галочками
* @param obj form - псевдоформа с фильтром (десктопным или мобильном)
*/
{
}
function goods_filter(th)
/** Применить к товарам фильтр
* @param obj th - this - кнопка "Подобрать"
*/
{
	var form = th.closest('.form');
	if(form.length)
	// если вызов откуда надо
	{
		if(document.querySelector('.shop-block-categories-page') || (window.location.pathname == '/shop/' && window.location.pathname.length <= 7))
		// если это страница категории то группируем товары прямо на странице
		{
			console.log('Это страница категории')
			
			var inputs = form.querySelectorAll('input'); // это инпуты из фильтра с значениями характеристик
			
			for(var thlaspi_id in goods) //проходим все товары
			{
				goods[thlaspi_id][0].checkedFilter = 'no';
				// console.log(goods[thlaspi_id]);continue;
				nextGood: for(var k = 0; k < goods[thlaspi_id][0].param.length;k++) // проходим все характеристики данного товара
				{
					for(var j = 0; j < inputs.length;j++) // проходим все выбранные галочки фильтра
					{
						if(inputs[j].className == 'dynamic-param-input' && inputs[j].checked == true)
						// значит это динамический инпут со значением характеристики, а не диапазон цен, не артикул, не оффер
						{
							console.log(inputs[j].value) // это значение одной из характеристик, выбранной в фильтре
							console.log(goods[thlaspi_id][0].param[k])// это объект со значением одной характеристики для данного товара вида { id: 6, name: "Год интродукции", value: "2000", unit: "г." }
							if(goods[thlaspi_id][0].param[k].unit != '')
							{
								var goodParamValue = goods[thlaspi_id][0].param[k].value+', '+goods[thlaspi_id][0].param[k].unit;
							}
							else
							{
								goodParamValue = goods[thlaspi_id][0].param[k].value;
							}
							if(goodParamValue == inputs[j].value)
							{
								goods[thlaspi_id][0].checkedFilter = 'yes';// ставим нашему товару отметку, что он проходит фильтрацию и должен быть показан.
								break nextGood;
							}
							// else
							// {
								// goods[thlaspi_id][0].checkedFilter = 'no';
							// }
						}
					}
				}
			}
			// и теперь скрываем все товары, не прошедшие фильтр
			for(var thlaspi_id in goods) //проходим все товары
			{
				if(typeof goods[thlaspi_id][0].checkedFilter != 'undefined')
				{
					var good_item = document.getElementById('shop_item_product_'+thlaspi_id);
					if(goods[thlaspi_id][0].checkedFilter == 'no' && good_item)
					// значит товар не прошёл фильтр и его надо спрятать
					{
						$(good_item).hide();
					}
					else
					// значит товар прошёл фильтр и его надо показать
					{
						$(good_item).show();
					}
				}
			}
			document.querySelector('.button-up').click();
		}
		else
		{
			console.log('Это НЕ страница категории')
			// то переходим на страницу поиска
		}
	}
	else
	{
		confirmation('Нет тела фильтров', 4);
	}
}