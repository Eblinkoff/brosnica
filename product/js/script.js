$(function () {

    var $page = $('.page'),
        $window = $(window),
        $pageTop = $('.page__top'),
        $buttonUp = $('.button-up'),
        $pageHeader = $('.page__header'),
        $headerContactsBlock = $('.header__contacts-block'),
        $navigationBlockShop = $('.navigation-block_shop'),
        $navigationBlockMobile = $('.navigation-block_mobile'),
        $shopProductGallery = $('.shop-product__gallery');

    $(".js_rating_votes").wrap('<div class="rating-block">');

    $buttonUp.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 1300);
        return false;
    });

    $window.on('scroll', function () {
        ($window.scrollTop() <= 200) ? $buttonUp.fadeOut('slow') : $buttonUp.fadeIn('slow');
    });

    // setTimeout(function () {
        // tunePage();
    // }, 300);

    // $window.on('scroll resize', function () {
        // tunePage();
    // });

    // function tunePage() {
        // var scrollY = $window.scrollTop(),
            // h = $pageHeader.outerHeight(),
            // ph = 0,
            // isBottomDiafanAdminPanel = false;

        // var $diafanAdminPanel = $('.diafan-admin-panel');
        // isDiafanAdminPanel = $diafanAdminPanel.length;

        // if (isDiafanAdminPanel) {
            // isBottomDiafanAdminPanel = $diafanAdminPanel.hasClass('diafan-admin-panel_bottom');
            // ph = $diafanAdminPanel.outerHeight();
        // }

        // if (scrollY > 0) {
            // if (!$page.hasClass('isPageHeaderFixed')) {
                // $page.addClass('isPageHeaderFixed');
            // }
            // if (isDiafanAdminPanel) {
                // $pageHeader.css('top', isBottomDiafanAdminPanel ? 0 : ph);
            // }
            // if (isBottomDiafanAdminPanel) {
                // $page.css({
                    // 'padding-top': 0,
                    // 'padding-bottom': ph
                // });
            // } else {
                // $page.css({
                    // 'padding-top': ph + h,
                    // 'padding-bottom': 0
                // });
            // }
        // } else {
            // $page.removeClass('isPageHeaderFixed');
            // if (isDiafanAdminPanel) {
                // if (isBottomDiafanAdminPanel) {
                    // $page.css({
                        // 'padding-top': 0,
                        // 'padding-bottom': ph
                    // });
                // } else {
                    // $page.css({
                        // 'padding-top': ph,
                        // 'padding-bottom': 0
                    // });
                // }
            // } else {
                // $page.removeAttr('style');
            // }
            // $pageHeader.removeAttr('style');
        // }

        // // fix
        // // if (isDiafanAdminPanel) {
            // tuneNavigationBlock(isBottomDiafanAdminPanel ? 0 : ph);
        // // }
        // // fix
    // }

    function tuneNavigationBlock(ph)
    {
        var th = $pageTop.outerHeight();
        th += ph;

        if ($page.hasClass('isNavigationBlockMobileOpened')) {
            $navigationBlockMobile.css({
                'height': 'calc(100% - ' + th +  'px)',
                'top': th + 'px'
            });
        } else {
            $navigationBlockMobile.removeAttr('style');
        }

        if ($page.hasClass('isNavigationBlockShopOpened')) {
            $navigationBlockShop.css({
                'maxHeight': 'calc(100vh - ' + th +  'px)'
            });
        } else {
            $navigationBlockShop.removeAttr('style');
        }
    }

    $(document).on('click', '.navigation-block__button', function (e) {
        e.preventDefault();

        var $button = $(e.currentTarget),
            isBottomDiafanAdminPanel = false,
            // fix
            ph = 0;
            // fix

        if ($button.hasClass('navigation-block__button_shop')) {
            $page
                .removeClass('isNavigationBlockMobileOpened')
                .toggleClass('isNavigationBlockShopOpened');
        } else if ($button.hasClass('navigation-block__button_nav')) {
            $page
                .removeClass('isNavigationBlockShopOpened')
                .toggleClass('isNavigationBlockMobileOpened');
        }

        var $diafanAdminPanel = $('.diafan-admin-panel');
        isDiafanAdminPanel = $diafanAdminPanel.length;

        // fix
        if (isDiafanAdminPanel) {
            isBottomDiafanAdminPanel = $diafanAdminPanel.hasClass('diafan-admin-panel_bottom');
            ph = $diafanAdminPanel.outerHeight();
            //tuneNavigationBlock(isBottomDiafanAdminPanel ? 0 : ph);
        }
        tuneNavigationBlock(isBottomDiafanAdminPanel ? 0 : ph);
        // fix

        return false;
    });

    $(document).on('click', function () {
        $page.removeClass('isNavigationBlockShopOpened');
    });

    $('.navigation i').on('click', function () {

        var $icon = $(this);
        var $item = $icon.closest('.navigation__item');

        if ($item.hasClass('active')) {
            $item
                .removeClass('active');
            $icon
                .removeClass('fa-minus-circle')
                .addClass('fa-plus-circle');
        } else {
            $item
                .addClass('active');
            $icon
                .removeClass('fa-plus-circle')
                .addClass('fa-minus-circle');
        }

        return false;
    });

    $('.tabs-block').on('click', '.tabs-block__rubric', function () {

        var $rubric = $(this);

        if (!$rubric.hasClass('active')) {
            var $tabs = $rubric.closest('.tabs-block');
            var index = $tabs.find('.tabs-block__rubric').index($rubric);

            $tabs
                .find('.tabs-block__rubric.active')
                .removeClass('active');

            $tabs
                .find('.tabs-block__tab.active')
                .removeClass('active');

            $rubric
                .addClass('active');

            $tabs
                .find('.tabs-block__tab')
                .eq(index)
                .addClass('active');
        }
    });

    $(document).on('click', '.js-shop-counter__adder', function () {

        var $counter = $(this).closest('.shop-counter').find('input');

        if ($counter.length)
		{
            var vCounter = $counter.val();
			var value = (+vCounter + 1);
			if(value > +$counter.attr('max'))
			{
				value = +$counter.attr('max');
			}
            $counter.val((vCounter && vCounter > 0) ? value : 1);
            $counter.trigger('input');
        }
    });

    $(document).on('click', '.js-shop-counter__reducer', function () {

        var $counter = $(this).closest('.shop-counter').find('input');

        if ($counter.length) {
            var vCounter = $counter.val();
            if (vCounter && vCounter > 1) {
                $counter.val(+vCounter - 1);
                $counter.trigger('input');
            }
        }
    });

    $(document).on('keyup', '.shop-counter input', function(e) {
        e.target.value = e.target.value.replace(/,/g,'.');
    });

    $(document).on('change', '.shop-settings select', function () {
        // window.location.href = $(this).find(':selected').first().val();
		sort_goods_in_category($(this).find(':selected').first().val(), '.shop-list-products')
		/*
			/sort1/ -- сначала большая цена
			/sort2/ -- сначала маленькая цена
			/sort3/ -- наименование товара с конца алфавита
			/sort4/ -- наименование товара по алфавиту
		*/
    });

    if ($shopProductGallery.length) {

        var $galleryBasic = $shopProductGallery.find('.gallery__basic .gallery__items');

        var $containerGallerySecond = $shopProductGallery.find('.gallery__second');
        var $gallerySecond = $containerGallerySecond.find('.gallery__items');

        $galleryBasic
            .slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: false,
                draggable: true,
                fade: true,
                asNavFor: $gallerySecond
            });

        $gallerySecond
            .slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                prevArrow: $containerGallerySecond.find('.gallery__button_prev'),
                nextArrow: $containerGallerySecond.find('.gallery__button_next'),
                asNavFor: $galleryBasic,
                mobileFirst: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 5
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 5
                        }
                    }
                ]
            });
    }

    $('.accordion').on('click', '.accordion__headline', function() {
        var $item = $(this).closest('.accordion__item');
        var $drop = $item.find('.accordion__drop');

        if (!$item.hasClass('active')) {
            $item
                .addClass('active');
            $drop
                .removeClass('fa-plus-circle')
                .addClass('fa-minus-circle');
        } else {
            $item
                .removeClass('active');
            $drop
                .removeClass('fa-minus-circle')
                .addClass('fa-plus-circle');
        }
    });

    function makeHeaderContactsBlock()
    {
        var touchstart = false;

        $headerContactsBlock.on('click touchstart', 'i', function (e) {
            e.stopPropagation();
            e.preventDefault();

            if ((window.matchMedia && window.matchMedia('(min-width: 768px)').matches) || ($window.width() >= 768)) {
                $headerContactsBlock
                    .find('.contacts-block__content')    
                    .removeClass('contacts-block__tooltip');
                return;
            }

            if (e.type == 'touchstart') {
                touchstart = true;
            }

            if (touchstart && e.type == 'click') {
                touchstart = false;
            } else {
                $headerContactsBlock
                    .find('.contacts-block__content')
                    .toggleClass('contacts-block__tooltip');
            }

            return false;
        });

        $(document).on('click', function (e) {
            $headerContactsBlock
                .find('.contacts-block__tooltip')
                .removeClass('contacts-block__tooltip');
        });
    }
    makeHeaderContactsBlock();

    (function shopInteractiveCompareApp() {

        $(document).on('click', '.js_comp:not(.disabled)', function () {

            var $btn = $(this),
                $inCompareCollection = $('.in-compare');
            var isCompared = $btn.hasClass('compared'),
                isRemoved = $btn.hasClass('removed'),
                $interactiveLink = $inCompareCollection.closest('.interactive-link');

            $.ajax({
                method: 'POST',
                dataType: 'json',
                data: {
                    module: 'shop',
                    action: 'compare_goods',
                    id: $btn.data('id'),
                    site_id: $btn.data('site_id'),
                    add: isCompared ? 0 : 1,
                    ajax: 1
                },
                beforeSend: function () {
                    $btn.addClass('disabled');
                }
            })
            .done(function (result, statusText, xhr) {

                var title = '',
                    count = '';

                if (result) {

                    if (isRemoved) {

                        var index = $btn.closest('[data-slick-index]').data('slickIndex');
                        if (index > -1) {
                            $shopComparedGallery = $('.shop-compared__gallery .gallery__items');
                            $shopComparedGallery.slick('slickRemove', index);
                        }
                    }

                    if (result.count) {
                        count = result.count;
                        $interactiveLink.addClass('interactive-link_counted');
                    } else {
                        $interactiveLink.removeClass('interactive-link_counted');
                    }

                    $inCompareCollection.html(prepare(count));

					if (result.redirect) {
						window.location = prepare(result.redirect);
					}

					if (isRemoved) return;

                    if (isCompared) {
                        title = $btn.data('addtitle');
                        $btn.removeClass('compared');
                    } else if (count) {
                        title = $btn.data('removetitle');
                        $btn.addClass('compared');
                    }

                    $btn.attr('title', title);
                }
            })
            .always(function () {
                $btn.removeClass('disabled');
            });
        });
    })();

    (function shopInteractiveFavoriteApp() {

        var $inFavoriteCollection = $('.in-favorite');
        var $interactiveLink      = $inFavoriteCollection.closest('.interactive-link');

        $(document).on('click', '.js_favorite:not(.disabled)', function () {
            var $form = $(this).closest('.js_shop').find('.js_shop_form').first();
            $form.find('input[name=action]').val('wish');
            // $form.submit();
        });

        // diafan_ajax.before['shop_wish'] = function (form) {
            // var $btn = $(form).closest('.js_shop').find('.js_favorite');
            // $btn.addClass('disabled');
        // }

        // diafan_ajax.success['shop_wish'] = function (form, result) {
            
            // var title = '',
                // count = '',    
                // $btn = $(form).closest('.js_shop').find('.js_favorite');
            // var isFavorited = $btn.hasClass('favorited');

            // if (result) {

                // if (result.count) {
                    // count = result.count;
                    // $interactiveLink.addClass('interactive-link_counted');
                // } else {
                    // $interactiveLink.removeClass('interactive-link_counted');
                // }

                // $inFavoriteCollection.html(prepare(count));

                // if (isFavorited) {
                    // title = $btn.data('addtitle');
                    // $btn.removeClass('favorited');
                // } else if (count) {
                    // title = $btn.data('removetitle');
                    // $btn.addClass('favorited');
                // }

                // $btn.attr('title', title);
            // }

            // $btn.removeClass('disabled');

            // return false;
        // }

    })();

    (function shopInteractiveCartApp() {

        var $inCartCollection = $('.in-cart');
        var $interactiveLink  = $inCartCollection.closest('.interactive-link');

        // diafan_ajax.success['shop_buy'] = function (form, result) {
            
            // var $container = $(form).find('.product-in-cart');

            // if (result) {

                // if (result.count) {
                    // $inCartCollection.html(prepare(result.count));
                    // $interactiveLink.addClass('interactive-link_counted');

                    // $('#modalInCart').modal();
                // }

                // if (result.product_count) {
                    // $container.html(prepare(result.product_count));
                    // $container.addClass('active');
                // }
            // }
        // }

    })();

    var selectApp = (function () {

        var _inited = false;

        function _up(selector) {
    
            if (!_make(selector)) {
                return false;
            }
    
            if (_isInit()) {
               return true;
            }

            $(document).on('change', 'select', function () {
                var $select = $(this);
                var $decorationSelect = $select.next('.select');

                _update($select, $decorationSelect);
            });

            $(document).on('click', '.select', function (e) {

                var $this = $(this);
                var $target = $(e.target);

                if ($target.hasClass('select__selected')) {
                    $this.addClass('clicked').toggleClass('opened');
                }

                $('.select.opened:not(.clicked)').removeClass('opened');
                $this.removeClass('clicked');
            });

            $(document).on('click', '.select__option', function () {
    
                var $decorationOption = $(this),
                    $decorationSelect = $decorationOption.closest('.select');
                var $select = $decorationSelect.prev('select' + '[name="' + $decorationSelect.data('name') + '"]');

                _setOption($select, $decorationOption.data('value'));
            });

            $(document).on('click', function (e) {
                if (!$(e.target).closest('.select').length) {
                    $('.select.opened').removeClass('opened clicked');
                }
            });

            _inited = true;
        };

        function _isInit() { 
            return _inited;
        }
    
        function _make(selector) {
            var $collection = $(selector);
            if($collection.length) {
                _decorate($collection);
                return true;
            }
        };
    
        function _decorate($collection)
		{
            $collection.each(function ()
			{
				var $select = $(this);
				if(this.className == 'js_shop_depend_param')
				// если не надо кастомизировать селект
				{
					// мы его просто скроем
					$select.hide();
				}
				else
				{
					var selectName = $select.attr('name');
		
					$select.addClass('decorated').hide();
		
					var placeholder = $select.data('placeholder');
					if (!placeholder) {
						placeholder = 'Не выбрано';
					}
		
					var $decoration = $("<span class='select' data-name='" + selectName + "'><span class='select__selected'>" + placeholder + "</span><span class='select__options'></span></span>");
		
					var $options = $select.find('option');
					if ($options.length) {
		
						var $decorationOptions = $decoration.find('.select__options');
						var decorationOptionsMarkup = '';
		
						$options.each(function () {
							
							var $option = $(this),
								optionText = $option.text(),
								optionValue = $option.val();
		
							if (!optionText) {
								optionText = 'Все';
							}
		
							decorationOptionsMarkup += "<span class='select__option' data-value='" + optionValue + "'>" + optionText + "</span>";
						});
		
						$decorationOptions.append(decorationOptionsMarkup);
					}
		
					_update($select, $decoration);
		
					$select.next('.select[data-name="' + selectName + '"]').remove();
					$select.after($decoration);
				}
            });
        }
    
        function _update($select, $decorationSelect) {
            
            if (!$select.length || !$decorationSelect.length) {
                return false;
            }

            var $selectedContainer = $decorationSelect.find('.select__selected');
            $decorationSelect.find('.select__option_selected').removeClass('select__option_selected');

            var $optionsSelected = $select.find(':selected');
            if (!$optionsSelected.length) {
                $selectedContainer.text('Не выбрано');
            } else {
                $optionsSelected.each(function () {
                    
                    var $option = $(this),
                        optionText = $option.text(),
                        optionValue = $option.val();

                    // if (!optionValue) {
                    //     var placeholder = $select.data('placeholder');
                    //     if (!placeholder) {
                    //         placeholder = 'Не выбрано';
                    //     }
                    //     optionText = placeholder;
                    // }

                    $decorationSelect.find('[data-value="' + optionValue + '"]').addClass('select__option_selected');
                    $selectedContainer.text(optionText);
                });
            }
        }
    
        function _setOption($select, value) {
            if($select.length) {
                $select.val(value).change();
            }
        }
    
        return {
            up: _up
        };
    
    })();

	selectApp.up('select:not(.decorated)');

	$(document).on('order_ajax_submit.after_last', function (e) {
		selectApp.up('select:not(.decorated)');
	});
	
    // $('select.select2').select2({
        // minimumResultsForSearch: Infinity
    // });

});
function check_radio_offer(plant_id, offer_id)
/** Меняем значение выбранного оффера путём клика на radio
* @param int plant_id - id растения
* @param int offer_id - id оффера
*/
{
	var select = document.getElementById('offers_change_select_'+plant_id);
	select.value = offer_id;
}
function offer_param_ch(th,good_id)
/** Меняем значение выбранного оффера
* @param obj th - this - select
* @param int good_id - id товара
*/
{
	// console.log(th.options[ th.selectedIndex ].value); // этот номер - значения атрибута param32 элемента списка цен офферов.
	
	// ищем элемент - список цен офферов .shop-item-product__numbers
	// var parentUl = $(th).closest('.shop-item-product__options');
	// var priceContainer = parentUl.next()[0];// вот этот элемент .shop-item-product__numbers
	
	// // теперь переключаем цену, соответствующую выбранному офферу
	// var pricesArray = priceContainer.querySelectorAll('.js_shop_param_price');
	// for(var i = 0; i < pricesArray.length; i++)
	// {
		// if(pricesArray[i].getAttribute('param32') == th.options[ th.selectedIndex ].value)
		// {
			// $(pricesArray[i]).show();
		// }
		// else
		// {
			// $(pricesArray[i]).hide();
		// }
	// }
	
	// // переключаем максимальное количество и элемент max в инпуте количества
	// // сначала ищем оффер в массиве товаров
	// if(typeof(goods) != "undefined")
	// {
		// for(var j = 0; j < goods[good_id].length; j++)
		// {
			// // goods[good_id][j] - это объект оффера.
			// if(goods[good_id][j].offer_id == th.options[ th.selectedIndex ].value)
			// {
				// var this_offer = goods[good_id][j];
				// break;
			// }
		// }
	// }
	// else
	// // бывает, что массива goods нет. Например, на странице хотелок или в корзине. Тогда пытаемся найти наше растение в массиве хотелок
	// {
		// var wish_list = getLocalStorage('wish_list');
		// if(wish_list)
		// {
			// var wishes_array = JSON.parse(wish_list);
			// if(typeof(wishes_array) != "undefined")
			// {
				// for(var i = 0; i < wishes_array.length; i++)
				// {
					// for(var j = 0; j < wishes_array[i].length; j++)
					// {
						// if(wishes_array[i][j].offer_id == th.options[ th.selectedIndex ].value)
						// {
							// var this_offer = wishes_array[i][j];
							// break;
						// }
					// }
				// }
			// }
		// }
	// }
	// if(this_offer)
	// // this_offer - type: "offer", thlaspi_id: 5541, offer_id: 2, name: "Размер", price: 800, max_count: 4, status: "normal", img_full_0: "/images/large/lizihiton-amerikanski…", img_thumbnail_0: "/images/list/lizihiton-amerikanskiy…", img_icon_0: "/images/icon/lizihiton-amerikanskiy…",
	// {
		// // ищем инпут с количеством
		// var count_input = document.getElementById('count_'+good_id);
		// if(count_input)
		// {
			// // console.log(this_offer.max_count)
			// count_input.setAttribute('max', this_offer.max_count);
		// }
		// // ищем характеристику "Осталось на складе"
		// var count_goods_ = document.getElementById('count_goods_'+good_id);
		// if(count_goods_)
		// {
			// count_goods_.innerHTML = this_offer.max_count;
		// }
	// }
}
function sort_goods_in_category(code, container_selector)
/* Сортирует товары в категории
* @param string code - тип сортировки
	/sort1/ -- сначала большая цена
	/sort2/ -- сначала маленькая цена
	/sort3/ -- наименование товара с конца алфавита
	/sort4/ -- наименование товара по алфавиту
* @param string container_selector - селектор контейнера вида .shop-list-products
*/
{
	// прохоим все товары и собираем нужные данные про каждый товар
	var container = document.querySelector(container_selector);
	goods.sort(function(prev, next){
		if(code == '/sort1/')
		{
			return -(prev[0].price - next[0].price);
		}
		else if(code == '/sort2/')
		{
			return prev[0].price - next[0].price;
		}
		else if(code == '/sort3/')
		{
			return (prev[0].name < next[0].name && 1) || (prev[0].name > next[0].name && -1) || 0; //по убыванию
		}
		else if(code == '/sort4/')
		{
			return (prev[0].name < next[0].name && -1) || (prev[0].name > next[0].name && 1) || 0; //по возрастанию
		}
	});
	// массив отсортирован. Теперь надо расставить товары в соответствии с массивом
	// console.log(goods)
	// console.log(container.children[0])
	for(var i = 0;typeof(goods[i]) != "undefined";i++)
	{
		console.log(goods[i][0].checkedFilter)
		// console.log(goods[i][0].thlaspi_id)
		container.insertBefore(document.getElementById('shop_item_product_'+goods[i][0].thlaspi_id), container.children[i])
	}
}
// /shop/podushki/?module=shop&action=search&cat_id=1&a=&pr1=0&pr2=0&brand%5B%5D=3&p2%5B%5D=38
