// Скрипты для страницы последнего заказа
// при каждой загрузке страницы обращаемся к localStorage и проверяем - нет ли там товаров в последнем заказе
// var last_order = getLocalStorage('last_order');
// if(last_order)
// {
	// // и если они есть - выкладываем информацию о товарах в контейнере .goods-list-cart
	// var localStorageArray = JSON.parse(last_order);
	// // console.dir(localStorageArray)
	// // получаем копию шаблона одной строки - товара в корзине
	// var empty_good = document.getElementById('set_one_good').firstElementChild;
	// // получаем контейнер - куда будем вставлять товары
	// var goods_cart_container = document.querySelector('.goods-list-cart');
	// for(var good of localStorageArray)
	// {
		// /** good - объект вида { thlaspi_id: 5732, offer_id: 5732, name: "Ирис бородатый  'название утеряно' …", price: 100, count: 11, max_count: 15, discount: 0, this_page_url: "/shop/irisy-vidovye-i-gibridy/iris-…", img_full_0: "/images/large/iris-borodatyy-nazvan…", img_thumbnail_0: "/images/list/iris-borodatyy-nazvani…", ещё 1… }
		// */
		// // клонируем шаблон строки
		// var clone = empty_good.cloneNode(true);
		// // заполняем клон актуальной информацией
		// clone.querySelector('.price-for-result').innerHTML = good.count*good.price;
		// clone.querySelector('.col-last-order').innerHTML = good.count;
		// clone.querySelector('.price-for-one').innerHTML = good.price;
		// clone.querySelector('.cart-good-link').href = good.this_page_url;
		// clone.querySelector('.name-good-cart').innerHTML = good.name;
		// clone.getElementsByTagName('IMG')[0].src = good.img_icon_0;
		// // и вставляем в таблицу товаров
		// // goods_cart_container.append(clone) // убираем, т.к. не все браузеры поддерживают append
		// goods_cart_container.appendChild(clone);
	// }
	// last_order_calculator();// перерасчитываем сумму
// }
// else
// {
	// console.log('Ошибка: нет массива с товарами из последнего заказа');
	// confirmation('Ошибка: нет массива с товарами из последнего заказа', 3);
// }

// теперь вставляем номер заказа
var order_id = getLocalStorage('order_id');
if(order_id && order_id != 'undefined')
{
	// записываем номер заказа
	var order_num_elem = document.querySelector('.order-id-last-order');
	if(order_num_elem)
	{
		order_num_elem.innerHTML = order_id;
	}
	// показываем заголовок с номером заказа
	var order_number_id_h2 = document.getElementById('order_number_id_h2');
	if(order_number_id_h2)
	{
		$(order_number_id_h2).show();
	}
}

// теперь вставляем контакты, если они сохранены
// var contacts_array = getLocalStorage('contacts_array');
// if(contacts_array)
// {
	// // и если они есть - выкладываем информацию
	// var localStorageContactsArray = JSON.parse(contacts_array);
	// // var localStorageContactsArray = {
		// // 'name': 'Anton',
		// // 'phone': '3223322',
		// // 'email': 'ebinkoff@mail.ru',
		// // 'adress': 'Твервская обл, Андреапольский р-н, д. Антоново, д.1',
		// // 'comment': 'Автосгенерированный массив',
	// // };
	// if(localStorageContactsArray['name'] !== undefined)
	// {
		// document.getElementById('inputNameReg').innerHTML = localStorageContactsArray['name'];
	// }
	// if(localStorageContactsArray['phone'] !== undefined)
	// {
		// document.getElementById('inputPhoneReg').innerHTML = localStorageContactsArray['phone'];
	// }
	// if(localStorageContactsArray['email'] !== undefined)
	// {
		// document.getElementById('inputEmailReg').innerHTML = localStorageContactsArray['email'];
	// }
	// if(localStorageContactsArray['adress'] !== undefined)
	// {
		// document.getElementById('inputAdressReg').innerHTML = localStorageContactsArray['adress'];
	// }
	// if(localStorageContactsArray['comment'] !== undefined)
	// {
		// document.getElementById('inputCommentReg').innerHTML = localStorageContactsArray['comment'];
	// }
// }

// function last_order_calculator()
// /** Подсчитывает и обновляет все суммарные показатели на странице последнего заказа
// */
// {
	// var last_order = getLocalStorage('last_order');
	// if(last_order)
	// {
		// var localStorageArray = JSON.parse(last_order);
		// var summ_count = 0;
		// var summ_price = 0;
		// for(var good of localStorageArray)
		// {
			// summ_count += +good.count;
			// summ_price += +good.count*good.price;
		// }
		// document.getElementById('count_result_cart').innerHTML = summ_count;
		// document.getElementById('summ_result_cart').innerHTML = summ_price;
	// }
	// else
	// {
		// console.log('Ошибка: нет массива с купленными товарами');
		// confirmation('Ошибка: нет массива с купленными товарами', 3);
	// }
// }
