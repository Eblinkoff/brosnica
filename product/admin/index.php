<?php

set_time_limit(0);
ini_set('MAX_EXECUTION_TIME', 86400);
ini_set('MAX_EXECUTION_TIME', -1);

error_reporting ( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR);

require_once('../../env.php');

if(ROOT == '')
// если продакт
{
    $gap = explode('/', $_SERVER['DOCUMENT_ROOT']);
    unset($gap[count($gap) - 1]);
    define('INSIDE_ABSPATH', implode('/',$gap));// для доступа к служебным файлам, недоступным для посетителя сайта
    define('ABSPATH', $_SERVER['DOCUMENT_ROOT']);// для доступа к статике, т.е. к файлам, доступным для посетителей сайта
}
else
// если локалка
{
    define('INSIDE_ABSPATH', $_SERVER['DOCUMENT_ROOT']);// для доступа к служебным файлам, недоступным для посетителя сайта
    define('ABSPATH', $_SERVER['DOCUMENT_ROOT'] . ROOT);// для доступа к статике, т.е. к файлам, доступным для посетителей сайта
}
/** Файлы в кастомной папке у нас называются точно так же, как и файлы ядра. А названия кастомных классов состоят из приставки Custom и названия основного класса, например, class CustomMain. Таким образом, мы можем создать новый кастомный класс, а также можем частично кастомизировать существующие классы при помощи наследования. */
function myAutoload($classname)
{
    if ($classname != 'Custom' && substr($classname,-6) == 'custom') // если вызывается кастомный класс
    {
        // выделяем название основного класса
        $gap = explode('_custom', $classname);
        $rootName = $gap[0];// например, "Main.php"
        // запускаем файл старого класса и кастомного, который ему наследует
        if(is_file(INSIDE_ABSPATH . '/classes/'.$rootName.'.php'))// его может не быть, т.к. мы можем создать новый, кастомизированный класс
        {
            include_once(INSIDE_ABSPATH . '/classes/' . $rootName.'.php');
        }
        if(is_file(INSIDE_ABSPATH . '/custom/classes/'.$rootName.'.php'))
        {
            include_once(INSIDE_ABSPATH . '/custom/classes/'.$rootName.'.php');
        }
    }
    else // если вызывается обыкновенный класс ядра
    {
        include_once(INSIDE_ABSPATH . '/classes/' . $classname.'.php');
    }
}
// регистрируем загрузчик
spl_autoload_register('myAutoload');

$main = Custom::new_class('Main');

$pull_plants = Custom::new_class('Pull_plants');

// сначала получаем выгрузку новостей, т.к. они используются на всех страницах сайта в сайдбаре
if(! ($news = $pull_plants->get_news(100)))
{
	echo $pull_plants->error_message.'<br>';
}

// потом получаем выгрузку статей, т.к. они тоже используются на всех страницах сайта в сайдбаре
if(! ($articles = $pull_plants->get_articles(100)))
{
	echo $pull_plants->error_message.'<br>';
}

if(! ($plants = Custom::static_class('Main')::get_goods($pull_plants)))
{
    die('Нет ни одного товара');
}

if(! ($categories = Custom::static_class('Category')::get_categories($pull_plants)))
{
    die('Нет ни одной категории');
}


// записываем объекты $categories и $plants, $news, $articles в специальной функции для того, чтобы не таскать их через параметры
Custom::static_class('Settings')::save_object('news', $news);
Custom::static_class('Settings')::save_object('articles', $articles);
Custom::static_class('Settings')::save_object('plants', $plants);
Custom::static_class('Settings')::save_object('categories', $categories);


if(! ($news_ok = Custom::static_class('News')::add_news_pages()))
{
    echo '<br><br>Новостей нет<br>';
}

if(! ($articles_ok = Custom::static_class('Articles')::add_articles_pages()))
{
    echo '<br><br>Статей нет<br>';
}

// затем - формируем страницы товаров и категорий
if(! ($result = $main->add_goods_pages()))
{
	die('Не получилось создать ни одной страницы');
}

// записываем sitemap
file_put_contents(ABSPATH.'/sitemap.xml', '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.Custom::static_class('Settings')::plus_sitemap_link(null, true).'</urlset>');
$main->add_version();// увеличиваем версию на единицу







// $image = new Images();
// $result_image = 'D:\OpenServer\domains\brosnica\images\large\roza-gibridnaya-jubilee-du-prince-de-monaco-rosa-hybridus-8708-43013.jpg';

	// // $_SERVER['DOCUMENT_ROOT'].'\css\fontawesome\webfonts\fa-solid-900.ttf'



// $image->add_watermark(
	// $result_image, 
	// 'D:\OpenServer\domains\brosnica\images\large\resulr.jpg', 
	// '©Brosnica.ru',
	// ABSPATH.'fonts/Times New Roman.ttf'
// );
echo '<br>';
die('всё');


function vd($v)
{
	echo '<pre>';
	ob_start();
	var_dump($v);
	$t = ob_get_contents();
	ob_end_clean();
	echo htmlspecialchars($t);
	echo '</pre>';
}

function dd($v)
{
	echo '<pre>';
	ob_start();
	var_dump($v);
	$t = ob_get_contents();
	ob_end_clean();
	echo htmlspecialchars($t);
	echo '</pre>';
	die;
}
function plus_str($string,$plus_str,$separator){//грамотно приращивает к строке $string строку $plus_str, используя разделитель $separator
	if($string==''||$string==null){
		return $plus_str;
	}else{
		return $string.$separator.$plus_str;
	}
}

/*

CREATE TABLE `brosnica`.`images` ( `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , `thlaspi_id` BIGINT UNSIGNED NOT NULL , `name` TEXT NOT NULL , `type` TINYINT(2) NOT NULL , `sync` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = MyISAM COMMENT = 'Таблица картинок сайта';
*/
/** Как работает кастомизация.
 * В корневой папке custom создаются кастомизирумые файлы с такими же названиями, как основные. Структура каталогов сохраняется такой же, т.е. если у нас есть файл /classes/Categories.php, и мы хотим его кастомизировать - мы создаём в папке custom каталог classes и в нём файл Categories.php
 * Темы (каталог views) заменяются полностью
 * Классы (каталог classes) наследуются следующим образом: к имени класса в конец добавляеся _custom, то есть если в файле /classes/Categories.php у нас был класс Categories (class Categories), то в кастомизируемом файле /custom/classes/Categories.php мы пишем class Categories_custom extends Categories
 */