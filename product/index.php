<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		<link href="/product/css/fancybox.min.css?ver=1424" rel="stylesheet" type="text/css">
<meta name="robots" content="all">
<meta charset="utf-8">
<title>Розы, клематисы, ирисы, видовые растения - купить</title>
<meta property="og:title" content="Розы, клематисы, ирисы, видовые растения - купить">
<meta content="Russian" name="language">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:description" content="Продажа растений почтой - 300 сортов роз, клематисы, сибирские ирисы, видовые растения, также бываем на выставках">
<meta name="description" content="Продажа растений почтой - 300 сортов роз, клематисы, сибирские ирисы, видовые растения, также бываем на выставках">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content="">
<link rel="stylesheet" href='/product/css/slick/slick.css?ver=1424'>
<link rel="stylesheet" href='/product/css/select2/css/select2.min.css?ver=1424'>
<link rel="stylesheet" href='/product/css/bootstrap/css/bootstrap.min.css?ver=1424'>
<link rel="stylesheet" href='/product/css/fontawesome/css/fontawesome-all.min.css?ver=1424'>
<link rel="stylesheet" href="/product/css/jquery-ui-1.8.18.custom.css?ver=1424" type="text/css">
<link rel="stylesheet" href="/product/css/default.css?ver=1424" type="text/css">
<link rel="stylesheet" href="/product/css/style.css?ver=1424" type="text/css">
<link rel="stylesheet" href="/product/css/colors.css?ver=1424" type="text/css">
<link rel="apple-touch-icon" sizes="180x180" href="/product/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/product/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/product/favicon-16x16.png">
<link rel="manifest" href="/product/site.webmanifest">
<link rel="mask-icon" href="/product/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#00a300">
<meta name="theme-color" content="#ffffff">


	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				<header class="page__header">
	<div class="header__container container-fluid">
		<div class="header__row row">
			<a class="header__logo col-auto">
				<span class="logo">
					<p>
						<img src="/product/img/logo.png" alt="Питомник Бросница" title="Питомник Бросница" width="110" height="110">
						<span>
							<span class="special-logo">
								Бросница
							</span>
							<span class="special-logo-slogan header__slogan">
								Розы для сада
							</span>
						</span>
					</p>
				</span>
			</a>
			<div class="header__contacts-block contacts-block col-auto">
				<i class="fas fa-phone"></i>
				<div class="contacts-block__content">
					<div class="contacts-block__phones">
						<p>
							<a href="tel:89206881070">8 (920) 688-10-70</a><a href="tel:89206881070"></a>
							<div class="svgs" style="display: none">
								<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/>
								</svg>
								<svg fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
									 viewBox="0 0 24 24" xml:space="preserve">
									<style type="text/css">
										.st0{fill:none;}
									</style>
									<path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2
										c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9
										C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/>
									<rect class="st0" width="24" height="24"/>
								</svg>
							</div>
						</p>
					</div>
					<div class="contacts-block__address">
						<p>Тверская обл., Андреапольский р-н, д. Антоново</p>
					</div>
					<div class="contacts-block__mailto">
						<p>
							<a href="mailto:eblinkoff@mail.ru">eblinkoff@mail.ru</a>
						</p>
					</div>
				</div>
			</div>
			<a class="interactive-link interactive-link_favorites col-auto" href="/product/shop/wishlist/">
				<i class="interactive-link__icon fas fa-heart"></i>
				<span class="interactive-link__title d-none d-lg-inline">Избранное</span>
				<span class="interactive-link__counter in-favorite"></span>
			</a>
			<a id="v_korzine" class="interactive-link interactive-link_cart col-auto" href="/product/shop/cart/">
				<i class="interactive-link__icon fas fa-shopping-bag"></i>
				<span class="interactive-link__title d-none d-lg-inline">Корзина</span>
				<span class="interactive-link__counter in-cart"><i id="cart_count"></i></span>
			</a>
		</div>
	</div>
</header>
<div class="navigation-block navigation-block_top">
	<div class="navigation-block__container container-fluid">
		<div class="navigation-block__row row">
			<div class="navigation-block__left col-auto">
				<span class="navigation-block__button navigation-block__button_shop d-none d-xl-flex">
					<i class="fas fa-bars"></i>
					<i class="fas fa-times"></i>
					Каталог
				</span>
				<span class="navigation-block__button navigation-block__button_nav d-xl-none">
					<i class="fas fa-bars"></i>
					<i class="fas fa-times"></i>
					Навигация
				</span>
				<nav class="navigation-block__menu navigation navigation_horizontal d-none d-xl-flex">
					<ul class="navigation__list navigation__list_main">
						
						<li class="navigation__item navigation__item_main">
							<a href="/product/o-nas/">О нас</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/kak-sazhat-nashi-rosi/">О розах</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/delivery/">Оплата и доставка</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/blog/">Блог</a>
						</li>
					
					</ul>
				</nav>
			</div>
			<div class="navigation-block__right col-auto col-sm col-xl-auto">
				<!--nav class="navigation-block__socials">
					<a href="https://www.instagram.com/eblinkoff"><i class="fab fa-instagram"></i></a>
					<a href="https://vk.com/eblinkoff"><i class="fab fa-vk"></i></a>
				</nav-->
				<div class="navigation-block__search d-none d-sm-block">
					<span class="form">
						<div class="control search-input-wrapper">
							<input class="input" type="text" name="searchword" placeholder="Поиск по сайту" oninput="search_go(this)" autocomplete="off">
						</div>
						<button class="button" type="button" title="Найти">
							<i class="fas fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</div>
		<div class="navigation-block__search d-block d-sm-none">
			<span class="form">
				<div class="control search-input-wrapper">
					<input class="input" type="text" name="searchword" placeholder="Поиск по сайту" oninput="search_go(this)" autocomplete="off">
				</div>
				<button class="button" type="button" title="Найти">
					<i class="fas fa-search"></i>
				</button>
			</span>
		</div>
	</div>
</div>
<div class="navigation-block navigation-block_shop d-none d-md-block">
	<div class="navigation-block__container container-fluid">
		<div class="navigation-block__row row">
			<div class="navigation-columns col-12">
				<div class="row">
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/floribunda">Флорибунда</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/chayno-gibridnye-rozy">Чайно-гибридные розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/angliyskie-rozy">Английские розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/shraby">Шрабы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/neukryvnye-rozy">Неукрывные розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/pletistye-rozy">Плетистые розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/kanadskie-rozy">Канадские розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/muskusnye-rozy">Мускусные розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/pochvopokrovnye-rozy">Почвопокровные розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/starinnye-rozy">Старинные розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/yaponskie-rozy">Японские розы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/klematisy-i-knyajiki">Клематисы и княжики</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/dekorativnye-travy">Декоративные травы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/drevesnye-rasteniya">Древесные растения</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/mnogoletniki">Многолетники</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/irisy">Ирисы</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/alpiyskaya-gorka">Альпийская горка</a>
									
								</li>
							</ul>
						</nav>
					
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="/product/shop/kollekciya">Коллекция</a>
									
								</li>
							</ul>
						</nav>
					
				</div>
			</div>
		</div>
	</div>
</div>
<nav class="navigation-block navigation-block_mobile navigation navigation_vertical d-xl-none">
	<div class="shop-block-navigation__box">
		<ul class="navigation__list navigation__list_main">
			<li class="navigation__item navigation__item_main navigation__item_parent">
				<a href="/product/shop/">Каталог<i class="fas fa-plus-circle"></i></a>
				
		<ul class="navigation__list navigation__list_child">
			
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/floribunda">Флорибунда</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/chayno-gibridnye-rozy">Чайно-гибридные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/angliyskie-rozy">Английские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/shraby">Шрабы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/neukryvnye-rozy">Неукрывные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pletistye-rozy">Плетистые розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kanadskie-rozy">Канадские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/muskusnye-rozy">Мускусные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pochvopokrovnye-rozy">Почвопокровные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/starinnye-rozy">Старинные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/yaponskie-rozy">Японские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/klematisy-i-knyajiki">Клематисы и княжики</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/dekorativnye-travy">Декоративные травы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/drevesnye-rasteniya">Древесные растения</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/mnogoletniki">Многолетники</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/irisy">Ирисы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/alpiyskaya-gorka">Альпийская горка</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kollekciya">Коллекция</a>
							
						</li>
					
		</ul>
		
			</li>
			
						<li class="navigation__item navigation__item_main">
							<a href="/product/o-nas/">О нас</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/kak-sazhat-nashi-rosi/">О розах</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/delivery/">Оплата и доставка</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/blog/">Блог</a>
						</li>
					
		</ul>
	</div>
</nav>
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
						
							<!--div class="carousel carousel_singleOnHomePage" id="carouselSingleOnHomePage" data-ride="carousel">
								<ol class="carousel-indicators">
									<li class="active" data-target="#carouselSingleOnHomePage" data-slide-to="0"></li>
									<li data-target="#carouselSingleOnHomePage" data-slide-to="1"></li>
								</ol>
								<div class="carousel-inner">
									<a href="http://bellasad.ru/news/interesno/komnatnye-rasteniya/" class="js_bs_counter carousel-item active" rel="2" style="background-image: url(https://bellasad.ru/userfls/bs/img_2554_2.jpg);" title="">
										<div class="carousel-caption">
											<object class="carousel-caption__content">
												<p style="font-size: 1.875em;"><b>Комнатные растения в ассортименте</b></p>
												<p style="font-size: 4.5em; font-weight: 300;">Глоксиния, Антуриум</p>
												<p style="font-size: 0.75em;">Эшинантус, Герань душистая, Нефролепис и др.</p>
											</object>
										</div>
									</a>
									<a href="//bellasad.ru/news/interesno/semena-tomatov-i-ogurtsov/" class="js_bs_counter carousel-item" rel="1" style="background-image: url(https://bellasad.ru/userfls/bs/img_4748_1.jpg);" title="">
										<div class="carousel-caption">
											<object class="carousel-caption__content">
												<p style="font-size: 1.875em;"><b>Семена томатов и огурцов</b></p>
												<p style="font-size: 4.5em; font-weight: 300;">"Партнер", "Русский огород", "Седек"</p>
												<p style="font-size: 0.75em;">и прочих в ассортименте</p>
											</object>
										</div>
									</a>
								</div>
								<a class="carousel-control-prev d-none d-sm-flex" href="#carouselSingleOnHomePage" role="button" data-slide="prev">
									<i class="fas fa-arrow-left" aria-hidden="true"></i>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next d-none d-sm-flex" href="#carouselSingleOnHomePage" role="button" data-slide="next">
									<i class="fas fa-arrow-right" aria-hidden="true"></i>
									<span class="sr-only">Next</span>
								</a>
							</div-->
							<div class="blog-item-post" style="text-align:center;color: #e73a63;padding-bottom: 1.25em;">
								<h6>Уважаемые покупатели!</h6>
								<span>Мы принимаем заказы на весну 2025 года.</span>
							</div>
							<div class="shop-block-categories page-block">
								<div class="h2">Каталог</div>
								<ul class="shop-list-categories row">
									<li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/floribunda/" style="background-image: url(/product/images/list/floribunda-49403.jpg);" title="Флорибунда"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/floribunda/" style="font-size:140%">Флорибунда</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/floribunda/" style="background-image: url(/product/images/list/floribunda-49403.jpg);" title="Флорибунда">
		<span class="shop-item-category__name">Флорибунда</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/chayno-gibridnye-rozy/" style="background-image: url(/product/images/list/chayno-gibridnye-49404.jpg);" title="Чайно-гибридные розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/chayno-gibridnye-rozy/" style="font-size:140%">Чайно-гибридные розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/chayno-gibridnye-rozy/" style="background-image: url(/product/images/list/chayno-gibridnye-49404.jpg);" title="Чайно-гибридные розы">
		<span class="shop-item-category__name">Чайно-гибридные розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/angliyskie-rozy/" style="background-image: url(/product/images/list/angliyskie-49396.jpg);" title="Английские розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/angliyskie-rozy/" style="font-size:140%">Английские розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/angliyskie-rozy/" style="background-image: url(/product/images/list/angliyskie-49396.jpg);" title="Английские розы">
		<span class="shop-item-category__name">Английские розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/shraby/" style="background-image: url(/product/images/list/shraby-49405.jpg);" title="Шрабы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/shraby/" style="font-size:140%">Шрабы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/shraby/" style="background-image: url(/product/images/list/shraby-49405.jpg);" title="Шрабы">
		<span class="shop-item-category__name">Шрабы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/neukryvnye-rozy/" style="background-image: url(/product/images/list/neukryvnye-49399.jpg);" title="Неукрывные розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/neukryvnye-rozy/" style="font-size:140%">Неукрывные розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/neukryvnye-rozy/" style="background-image: url(/product/images/list/neukryvnye-49399.jpg);" title="Неукрывные розы">
		<span class="shop-item-category__name">Неукрывные розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/pletistye-rozy/" style="background-image: url(/product/images/list/pletistye-49400.jpg);" title="Плетистые розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/pletistye-rozy/" style="font-size:140%">Плетистые розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/pletistye-rozy/" style="background-image: url(/product/images/list/pletistye-49400.jpg);" title="Плетистые розы">
		<span class="shop-item-category__name">Плетистые розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/kanadskie-rozy/" style="background-image: url(/product/images/list/kanadskie-49397.jpg);" title="Канадские розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/kanadskie-rozy/" style="font-size:140%">Канадские розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/kanadskie-rozy/" style="background-image: url(/product/images/list/kanadskie-49397.jpg);" title="Канадские розы">
		<span class="shop-item-category__name">Канадские розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/muskusnye-rozy/" style="background-image: url(/product/images/list/muskusnye-49398.jpg);" title="Мускусные розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/muskusnye-rozy/" style="font-size:140%">Мускусные розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/muskusnye-rozy/" style="background-image: url(/product/images/list/muskusnye-49398.jpg);" title="Мускусные розы">
		<span class="shop-item-category__name">Мускусные розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/pochvopokrovnye-rozy/" style="background-image: url(/product/images/list/pochvopokrovnye-49401.jpg);" title="Почвопокровные розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/pochvopokrovnye-rozy/" style="font-size:140%">Почвопокровные розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/pochvopokrovnye-rozy/" style="background-image: url(/product/images/list/pochvopokrovnye-49401.jpg);" title="Почвопокровные розы">
		<span class="shop-item-category__name">Почвопокровные розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/starinnye-rozy/" style="background-image: url(/product/images/list/starinnye-49402.jpg);" title="Старинные розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/starinnye-rozy/" style="font-size:140%">Старинные розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/starinnye-rozy/" style="background-image: url(/product/images/list/starinnye-49402.jpg);" title="Старинные розы">
		<span class="shop-item-category__name">Старинные розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/yaponskie-rozy/" style="background-image: url(/product/images/list/yaponskie-49516.jpg);" title="Японские розы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/yaponskie-rozy/" style="font-size:140%">Японские розы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/yaponskie-rozy/" style="background-image: url(/product/images/list/yaponskie-49516.jpg);" title="Японские розы">
		<span class="shop-item-category__name">Японские розы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/klematisy-i-knyajiki/" style="background-image: url(/product/images/list/klematisy-i-knyajiki-57938.jpg);" title="Клематисы и княжики"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/klematisy-i-knyajiki/" style="font-size:140%">Клематисы и княжики</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/klematisy-i-knyajiki/" style="background-image: url(/product/images/list/klematisy-i-knyajiki-57938.jpg);" title="Клематисы и княжики">
		<span class="shop-item-category__name">Клематисы и княжики</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/dekorativnye-travy/" style="background-image: url(/product/images/list/dekorativnye-travy-57983.jpg);" title="Декоративные травы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/dekorativnye-travy/" style="font-size:140%">Декоративные травы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/dekorativnye-travy/" style="background-image: url(/product/images/list/dekorativnye-travy-57983.jpg);" title="Декоративные травы">
		<span class="shop-item-category__name">Декоративные травы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/drevesnye-rasteniya/" style="background-image: url(/product/images/list/drevesnye-rasteniya-58335.jpg);" title="Древесные растения"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/drevesnye-rasteniya/" style="font-size:140%">Древесные растения</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/drevesnye-rasteniya/" style="background-image: url(/product/images/list/drevesnye-rasteniya-58335.jpg);" title="Древесные растения">
		<span class="shop-item-category__name">Древесные растения</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/mnogoletniki/" style="background-image: url(/product/images/list/drugie-rasteniya-49406.jpg);" title="Многолетники"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/mnogoletniki/" style="font-size:140%">Многолетники</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/mnogoletniki/" style="background-image: url(/product/images/list/drugie-rasteniya-49406.jpg);" title="Многолетники">
		<span class="shop-item-category__name">Многолетники</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/irisy/" style="background-image: url(/product/images/list/irisy-50108.jpg);" title="Ирисы"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/irisy/" style="font-size:140%">Ирисы</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/irisy/" style="background-image: url(/product/images/list/irisy-50108.jpg);" title="Ирисы">
		<span class="shop-item-category__name">Ирисы</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/alpiyskaya-gorka/" style="background-image: url(/product/images/list/alpiyskaya-gorka-50110.jpg);" title="Альпийская горка"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/alpiyskaya-gorka/" style="font-size:140%">Альпийская горка</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/alpiyskaya-gorka/" style="background-image: url(/product/images/list/alpiyskaya-gorka-50110.jpg);" title="Альпийская горка">
		<span class="shop-item-category__name">Альпийская горка</span>
	</a>
</li--><li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="/product/shop/kollekciya/" style="background-image: url(/product/images/list/kollekciya-57577.jpg);" title="Коллекция"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="/product/shop/kollekciya/" style="font-size:140%">Коллекция</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="/product/shop/kollekciya/" style="background-image: url(/product/images/list/kollekciya-57577.jpg);" title="Коллекция">
		<span class="shop-item-category__name">Коллекция</span>
	</a>
</li-->
								</ul>
							</div>
							
							<div class="text page-block">
								<h1>Многолетники и розы из Тверской области</h1>
								<p>
									Здравствуйте, уважаемые посетители сайта «Бросница»!
								</p>
								<p>
									Здесь Вы можете выбрать розы для своего сада, а также необычные многолетники и кустарники. Все растения выращены и размножены в Тверской области, т.е. растения, которые мы предлагаем, хорошо адаптированы к условиям Средней полосы и Северо-Запада.
								</p>
								<p>
									Мы выращиваем и размножаем розы из разных групп: английские, флорибунды, плетистые, мускусные, почвопокровные, старинные, шрабы. Все эти розы в районах с холодной зимой нуждаются в зимнем укрытии. Также мы собрали и продолжаем пополнять коллекцию зимостойких роз и шиповников, которые не требуют укрытия в средней полосе - неукрывные. Также у нас есть клематисы, ирисы и другие многолетники.
								</p>
								<p>
									Добро пожаловать на наш сайт "Бросница"! Выбирайте, задавайте вопросы, радуйтесь новым растениям в Вашем саду!
								</p>
							</div>
							
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							<div class="row d-block d-md-flex d-lg-block">
	<nav class="shop-block-navigation navigation navigation_vertical page-block page-block_aside col-12 d-none d-lg-block">
		<div class="shop-block-navigation__box">
			
		<ul class="navigation__list navigation__list_main">
			
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/floribunda">Флорибунда</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/chayno-gibridnye-rozy">Чайно-гибридные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/angliyskie-rozy">Английские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/shraby">Шрабы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/neukryvnye-rozy">Неукрывные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pletistye-rozy">Плетистые розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kanadskie-rozy">Канадские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/muskusnye-rozy">Мускусные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pochvopokrovnye-rozy">Почвопокровные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/starinnye-rozy">Старинные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/yaponskie-rozy">Японские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/klematisy-i-knyajiki">Клематисы и княжики</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/dekorativnye-travy">Декоративные травы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/drevesnye-rasteniya">Древесные растения</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/mnogoletniki">Многолетники</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/irisy">Ирисы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/alpiyskaya-gorka">Альпийская горка</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kollekciya">Коллекция</a>
							
						</li>
					
		</ul>
		
		</div>
	</nav>
	
	
			<div class="articles-block page-block page-block_aside col-12 col-md-6 order-md-1 col-lg-12">
				<a class="h2" href="/product/news/">Новости</a>
				<ul class="articles-block__list">
					
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/news/polzuytes-novoy-funkciey-soobshchit-kogda-poyavitsya/" style="background-image: url();" title="Пользуйтесь новой функцией "Сообщить когда появится"!"></a>
                            <div class="articles-block__content">
                                <div class="articles-block__date">25 декабря 2024г.</div>
                                <div class="articles-block__link"><a class="articles-block__link" href="/product/news/polzuytes-novoy-funkciey-soobshchit-kogda-poyavitsya/">На сайте питомника Бросница.ру появилась новая возможность - "Сообщить ...</a></div>
                            </div>
                        </li>
                    
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/news/prodaja-rasteniy-osen-2024-vesna-2025/" style="background-image: url(/product/images/icon/prodaja-rasteniy-osen-2024-vesna-2025-5-57522.jpg);" title="Продажа растений осень 2024 - весна 2025"></a>
                            <div class="articles-block__content">
                                <div class="articles-block__date">16 сентября 2024г.</div>
                                <div class="articles-block__link"><a class="articles-block__link" href="/product/news/prodaja-rasteniy-osen-2024-vesna-2025/">Уважаемые покупатели! Открыт заказ роз на весну 2025! А ...</a></div>
                            </div>
                        </li>
                    
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/news/rassylka-zakazov-2023/" style="background-image: url(/product/images/icon/rassylka-zakazov-2023-3-51555.jpg);" title="Рассылка заказов 2023"></a>
                            <div class="articles-block__content">
                                <div class="articles-block__date">12 марта 2023г.</div>
                                <div class="articles-block__link"><a class="articles-block__link" href="/product/news/rassylka-zakazov-2023/">Уважаемые покупатели!
Мы будем рассылать заказы в конце апреля - ...</a></div>
                            </div>
                        </li>
                    
				</ul>
			</div>
		
	
			<div class="articles-block page-block page-block_aside col-12 col-md-6 order-md-1 col-lg-12">
				<a class="h2" href="/product/blog/">Блог</a>
				<ul class="articles-block__list">
					
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/blog/fotografii-iz-sada-vypusk-2-naturgarden/" style="background-image: url(/product/images/icon/fotografii-iz-sada-vypusk-2-naturgarden-17-58620.jpg);" title="Фотографии из сада. Выпуск 2. Натургарден"></a>
                            <div class="articles-block__content">
                                <a class="articles-block__link" href="/product/blog/fotografii-iz-sada-vypusk-2-naturgarden/">Фотографии из сада. Выпуск 2. Натургарден</a>
                                <div class="articles-block__date">20 декабря 2024г.</div>
                            </div>
                        </li>
                    
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/blog/fotografii-iz-sada-vypusk-1/" style="background-image: url(/product/images/icon/fotografii-iz-sada-vypusk-1-16-58179.jpg);" title="Фотографии из сада. Выпуск 1."></a>
                            <div class="articles-block__content">
                                <a class="articles-block__link" href="/product/blog/fotografii-iz-sada-vypusk-1/">Фотографии из сада. Выпуск 1.</a>
                                <div class="articles-block__date">04 декабря 2024г.</div>
                            </div>
                        </li>
                    
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="/product/blog/chto-znachat-zony-usda-i-ih-svyaz-s-zimostoykostyu-roz/" style="background-image: url(/product/images/icon/chto-znachat-zony-usda-i-ih-svyaz-s-zimostoykostyu-roz-15-58133.jpg);" title="Что значат зоны USDA и их связь с зимостойкостью роз"></a>
                            <div class="articles-block__content">
                                <a class="articles-block__link" href="/product/blog/chto-znachat-zony-usda-i-ih-svyaz-s-zimostoykostyu-roz/">Что значат зоны USDA и их связь с зимостойкостью роз</a>
                                <div class="articles-block__date">02 декабря 2024г.</div>
                            </div>
                        </li>
                    
				</ul>
			</div>
		
	<!--div class="subscription-block page-block page-block_aside col-12 order-md-last text-sm-center text-lg-left">
		<div class="subscription-block__box" style="background-image: url(/subscription_7_7.jpg);">
			<div class="subscription-block__content">
				<div class="subscription-block__text">
					<p>Подпишитесь на рассылку и будьте вкурсе важных новостей:</p>
				</div>
				<span class="form ajax">
					<div class="d-sm-flex d-lg-block justify-content-sm-center">
						<div class="control">
							<input class="input" name="mail" type="email" placeholder="Электронная почта">
							<div class="errors error_mail" style="display:none"></div>
						</div>
						<button class="button" type="submit" title="Подписаться">Подписаться</button>
					</div>
					<div class="privacy">Отправляя форму, я даю согласие на <a href="/product/privacy/">обработку персональных данных</a>.</div>
					<div class="errors error" style="display:none"></div>
				</span>
			</div>
		</div>
	</div-->
</div>

						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				<nav class="shop-block-navigation navigation navigation_vertical page-block page-block_aside col-12 d-sm-none">
	<div class="shop-block-navigation__box">
		
		<ul class="navigation__list navigation__list_main">
			
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/floribunda">Флорибунда</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/chayno-gibridnye-rozy">Чайно-гибридные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/angliyskie-rozy">Английские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/shraby">Шрабы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/neukryvnye-rozy">Неукрывные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pletistye-rozy">Плетистые розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kanadskie-rozy">Канадские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/muskusnye-rozy">Мускусные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/pochvopokrovnye-rozy">Почвопокровные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/starinnye-rozy">Старинные розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/yaponskie-rozy">Японские розы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/klematisy-i-knyajiki">Клематисы и княжики</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/dekorativnye-travy">Декоративные травы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/drevesnye-rasteniya">Древесные растения</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/mnogoletniki">Многолетники</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/irisy">Ирисы</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/alpiyskaya-gorka">Альпийская горка</a>
							
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/shop/kollekciya">Коллекция</a>
							
						</li>
					
		</ul>
		
	</div>
</nav>
<div class="navigation-block navigation-block_bottom">
	<div class="navigation-block__container container-fluid">
		<div class="navigation-block__row row">
			<div class="navigation-block__left d-none d-lg-flex col-auto">
				<nav class="navigation-block__menu navigation">
					<ul class="navigation__list navigation__list_main">
						
			<li class="navigation__item navigation__item_main">
				<a href="/product/shop/">Каталог</a>
			</li>
		
						<li class="navigation__item navigation__item_main">
							<a href="/product/o-nas/">О нас</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/kontakty/">Контакты</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/kak-sazhat-nashi-rosi/">О розах</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/delivery/">Оплата и доставка</a>
						</li>
					
						<li class="navigation__item navigation__item_main">
							<a href="/product/blog/">Блог</a>
						</li>
					
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
<footer class="page__footer">
	<div class="footer__container container-fluid">
		<div class="footer__row row">
			<div class="footer__navigation navigation-columns d-none d-md-block col-12 col-xl-8">
			</div>
			<div class="footer__informer col">
				<div class="row">
					<div class="footer__contacts-block contacts-block col-12 col-md-6 order-md-last col-xl-12 order-xl-first">
						<div class="contacts-block__content">
							<div class="contacts-block__phones">
								<p>
									<a href="tel:89206881070"><i class="fas fa-phone" style="font-size: 16px"></i> 8 (920) 688-10-70</a> <a href="whatsapp:+79206881070?chat"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/></svg></a> <a href="tg://resolve?domain=+79206881070" style="position: relative;top: 1.5px;"><svg width="20" height="20" fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"viewBox="0 0 24 24" xml:space="preserve"><style type="text/css">.st0{fill:none;}</style><path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/><rect class="st0" width="24" height="24"/></svg></a>
									<br>
									<a href="tel:89312009928"><i class="fas fa-phone" style="font-size: 16px"></i> 8 (931) 200-99-28</a> <a href="whatsapp:+89312009928?chat"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/></svg></a> <a href="tg://resolve?domain=+89312009928" style="position: relative;top: 1.5px;"><svg width="20" height="20" fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"viewBox="0 0 24 24" xml:space="preserve"><style type="text/css">.st0{fill:none;}</style><path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/><rect class="st0" width="24" height="24"/></svg></a>
									<br>
									<a href="mailto:eblinkoff@mail.ru"><i class="fas fa-envelope" style="font-size: 16px"></i> eblinkoff@mail.ru</a>
								</p>
							</div>
							<div class="contacts-block__address">
								<p>Тверская обл., Андреапольский р-н, д. Антоново</p>
							</div>
						</div>
					</div>
					<div class="copyright col-12 col-md-6 order-md-first text-md-left col-xl-12 order-xl-last text-xl-right">
						<div class="copyright__content">
							<p>&copy; 2018 - 2025 Бросница<br>Розы для сада</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		<div class="modal modal_inCart fade" id="modalInCart" tabindex="-1" role="dialog" aria-labelledby="Товар в корзине" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
			<div class="h2">Товар в корзине</div>
			<div class="modal__footer">
				<a class="button" href="/product/shop/cart/">Оформить заказ</a>
				<button class="button" data-dismiss="modal" aria-label="Close">Продолжить покупки</button>
			</div>
		</div>
	</div>
</div>
<div class="modal modal_inCart fade" id="modalNotifyWhenAvailable" tabindex="-1" role="dialog" aria-labelledby="Оповестить когда товар появится в наличии" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
			<div class="h2">Сообщить когда появится</div>
			<div class="control">
				<label class="label">Ваше имя<span style="color:red;">*</span>:</label>
				<input id="notify_when_fio" class="input" type="text" name="fio" value="">
				<div class="errors error_fio" style="display:none"></div>
			</div>
			А также что-то одно из:<br>
			<div class="control">
				<label class="label">Телефон<span style="color:red;">*</span>:</label>
				<input id="notify_when_telefon" class="input" type="text" name="tel" value="">
				<div class="errors error_tel" style="display:none"></div>
			</div>
			или
			<div class="control">
				<label class="label">e-mail для связи<span style="color:red;">*</span>:</label>
				<input id="notify_when_email" class="input" type="text" name="email" value="">
				<div class="errors error_email" style="display:none"></div>
			</div>
			<br>
			<div class="modal__footer">
				<button id="notify_when_button" class="button" data-dismiss="modal" aria-label="Close" onclick="notifyWhenAvailable(this)">Отправить!</button>
			</div>
		</div>
	</div>
</div>

		<script type="text/javascript" src="/product/js/jquery.min.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript" src="/product/js/jquery.form.min.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript" src="/product/js/jquery-ui.min.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript" src="/product/js/timepicker.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript">
	jQuery(function(e){
	e.datepicker.setDefaults(e.datepicker.regional["ru"]);
	e.timepicker.setDefaults(e.timepicker.regional["ru"]);
	});
</script>
<script type="text/javascript" src="/product/js/jquery.scrollTo.min.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript" src="/product/js/jquery.touchSwipe.min.js?ver=1424" charset="UTF-8"></script>
<script type="text/javascript" src="/product/js/jquery.cookie.min.js?ver=1424" charset="UTF-8"></script>
<script src="/product/js/jquery.fancybox.min.js?ver=1424" async type="text/javascript" charset="UTF-8"></script>
<script src='/product/css/bootstrap/js/bootstrap.min.js?ver=1424'></script>
<script src='/product/js/slick.js?ver=1424'></script>
<script type="text/javascript" src="/product/js/site.js?ver=1424"></script>
<script type="text/javascript" async defer src="/product/js/search.js?ver=1424"></script>
<script type="text/javascript" async defer src="/product/js/script.js?ver=1424"></script>
<script src="/product/js/extsrc.js?ver=1424"></script>

		<script type="text/javascript" async src="/product/js/good.js?ver=1424"></script>
	</body>
</html>
