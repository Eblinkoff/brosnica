
const CACHE = 'cache-and-update-v1';

// При установке воркера мы должны закешировать часть данных (статику).
self.addEventListener('install', (event) => {
    console.log('Установлен');
    event.waitUntil( // event.waitUntil принимает промис для того, чтобы узнать, сколько времени займёт установка, и успешно или нет она завершилась.
        caches.open(CACHE)
            .then(cache => {
                console.log('[Service Worker] Precaching App Shell');
                cache.addAll([
                    '/',
                ]);
            }));
});

self.addEventListener('activate', (event) => {
    console.log('Активирован');
});


self.addEventListener('fetch', event => {
    event.respondWith( // Мы используем `respondWith()`, чтобы мгновенно ответить без ожидания ответа с сервера.
        // event.respondWith - Этот метод анализирует запрос и ищет кэшированные результаты для этого запроса в любом из созданных сервис-воркером кэшей.
        caches.match(event.request)
            .then(function(response) {
                if (response) {// если в кэше найдено то, что нужно, мы можем тут же вернуть ответ.
                    // console.log(1)
                    // console.log(response)
                    return response;
                }
                else
                {// в кеше ничего не нашлось
                    // Клонируем запрос. Так как объект запроса - это поток,
                    // обратиться к нему можно лишь один раз.
                    // При этом один раз мы обрабатываем его для нужд кэширования,
                    // ещё один раз он обрабатывается браузером, для запроса ресурсов,
                    // поэтому объект запроса нужно клонировать.
                    var fetchRequest = event.request.clone();

                    // В кэше ничего не нашлось, поэтому нужно выполнить загрузку материалов,
                    // что заключается в выполнении сетевого запроса и в возврате данных, если
                    // то, что нужно, может быть получено из сети.
                    return fetch(fetchRequest).then(
                        function(response) {
                            // Проверка того, получили ли мы правильный ответ
                            if(!response || response.status !== 200 || response.type !== 'basic') {
                                return response;
                            }

                            // Клонирование объекта ответа, так как он тоже является потоком.
                            // Так как нам надо, чтобы ответ был обработан браузером,
                            // а так же кэшем, его нужно клонировать,
                            // поэтому в итоге у нас будет два потока.
                            var responseToCache = response.clone();

                            caches.open(CACHE)
                                .then(function(cache) {
                                    // Добавляем ответ в кэш для последующего использования.
                                    //console.log(event.request)
                                    if(event.request.method !== "GET") {
                                        return Promise.reject('no-match')
                                    }
                                    else
                                    {
                                        cache.put(event.request, responseToCache);
                                    }
                                });

                            return response;
                        }
                    );
                }
            })
    );
});



// при событии fetch, мы используем кэш, и только потом обновляем его данным с сервера
// self.addEventListener('fetch', function(event) {
//     // Мы используем `respondWith()`, чтобы мгновенно ответить без ожидания ответа с сервера.
//     event.respondWith(fromCache(event.request));
//     // `waitUntil()` нужен, чтобы предотвратить прекращение работы worker'a до того как кэш обновиться.
//     event.waitUntil(update(event.request));
// });
//
// function fromCache(request) {
//     return caches.open(CACHE).then((cache) =>
//         cache.match(request).then((matching) =>
//             matching || Promise.reject('no-match')
//         ));
// }
//
// function update(request) {
//     return caches.open(CACHE).then((cache) =>
//         fetch(request).then((response) =>
//             cache.put(request, response)
//         )
//     );
// }