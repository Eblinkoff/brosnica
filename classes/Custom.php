<?php
/** Класс, отвечающий за кастомизацию. Помимо этого класса за кастомизацию отвечают функции myAutoload из admin и ajax.
 * Файлы в кастомной папке у нас называются точно так же, как и файлы ядра. А названия кастомных классов состоят из приставки Custom и названия основного класса, например, class CustomMain. Таким образом, мы можем создать новый кастомный класс, а также можем частично кастомизировать существующие классы при помощи наследования.
 */
class Custom
{
    /** Вызов классов с учётом кастомизации
     * @param string $className - имя класса без приставки Custom. При этом, если кастомизированный класс есть, то вызывается именно он.
     */
    public function new_class($className)
    {
        if(class_exists($className.'_custom'))
        {
            $className_ = $className.'_custom';
            return new $className_();
        }
        else
        {
            return new $className();
        }
    }

    /** Вызов статических классов с учётом кастомизации
     * @param string $className - имя класса без приставки Custom. При этом, если кастомизированный класс есть, то вызывается именно он.
     */
    public function static_class($className)
    {
        if(class_exists($className.'_custom'))
        {
            return $className.'_custom';
        }
        else
        {
            return $className;
        }
    }

}