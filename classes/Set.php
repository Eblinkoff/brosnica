<?php
class Set {
	/** Класс подключения бд
	*/	
	private static $_instance_settings = null;
	
	
	private function __construct(){
	}	
	
	
	static public function getSettings() {
		if(is_null(self::$_instance_settings)){
			$settings = new Settings();
			self::$_instance_settings = $settings;
		}
		return self::$_instance_settings;
	}
	
	
    private function __clone() {
    }

    private function __wakeup() {
    }	
}
