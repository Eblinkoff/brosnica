<?php
class Good
{
	
	public function big_page($good_tpl, $category, $plant, $full_tree, $full_tree_array)
	/** Формируем страницу отдельного товара из шаблона
	* @param string $good_tpl - шаблон товара
	* @param obj $category - объект данной категории из объекта $categories
	* @param obj $plant - объект данного товара из объекта $plants
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	* @param obj $full_tree_array - массив дерева категорий с объектами категорий (без shop и текущей) или array(), если категория $category - корневая
	* @return string $result_good - html страницы товара целиком
	*/
	{
		// vd($categories);die;
		$settings = Set::getSettings();
		$aside = Custom::static_class('Main')::aside();
		// основная картинка
		
		// берём имя картинки из бд по номеру thlaspi_id
		if($img_name = Custom::static_class('Images')::name_by_id($plant->images[0]->id))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
			$paths = Custom::static_class('Images')::kit('');
		}
		
		$head = Custom::static_class('Main')::head(
			self::true_name($plant).($plant->price > 0 ? ' - купить за '.$plant->price.' руб. плюс доставка в интернет-магазине '.$settings->get_param('shop_name_global') : ''), // title
			$plant->comment, // description
			$plant->rus_name, // keywords
			$paths->list // og_image
		);
		$header = Custom::static_class('Main')::header();
		$footer = Custom::static_class('Main')::footer();
		$show_js = Custom::static_class('Main')::show_js();
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		if($interesting_yet = self::list_goods_inner($plant->id, $full_tree,'rand',3))
		// заранее формируем список карточек для перелинковки
		{
			$interesting_yet = '
				<div class="shop-product-related shop-block-products page-block">
					<div class="h2">Похожие товары</div>
					<ul class="shop-list-products row">
						'.$interesting_yet.'
					</ul>
				</div>
			';
		}
		
		$this_page_url = ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit(self::true_name($plant)).$settings->get_param('chpu_postfix');
		
		// характеристики
		$params = '';
		if(isset($plant->param))
		{
			$param_tpl = Custom::static_class('Main')::get_tpl('param');
			$params = self::params($plant->param, $param_tpl);
		}
		// офферы
		$offers = self::offers($plant, $plant->offers, $this_page_url, $paths->list, $paths->list, $paths->icon, ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu, $category->name);

		$leftsidebar = Custom::static_class('Menu')::leftsidebar();
		
		$breadcrumbs = Custom::static_class('Main')::breadcrumbs(self::true_name($plant), $full_tree_array, $category);
		
		
		// создаём страницу растения
		$result_good = str_replace(
			array(
				'%id', 
				'%description', 
				'%keywords', 
				'%h1', 
				'%breadcrumbs',
				'%article', 
				'%catalog_path_name', 
				'%count', 
				'%this_page_url', 
				'%img_full_0', 
				'%img_thumbnail_0', 
				'%img_icon_0', 
				'%lineyka_photos', 
				'%big_lineyka_photos', 
				'%price', 
				'%comment', 
				'%leftsidebar', 
				'%interesting_yet', 
				'%params', 
				'%offers', 
				'%shead',
				'%header',
				'%aside', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js',
				'%hit',
				'%new',
				'%action',
				'%buy_buttons',
				'%version',
				'%modal_confirmation_tpl', 
				'%root', 
			),
			array(
				$plant->id,
				$plant->comment, // description
				$plant->rus_name, // keywords
				self::true_name($plant), // h1
				$breadcrumbs, // breadcrumbs
				$plant->article, 
				ROOT.'/'.$settings->get_param('catalog_path_name'), 
				$plant->count, 
				$this_page_url, // this_page_url
				$paths->large,// img_full_0
				$paths->list, // img_thumbnail_0
				$paths->icon, // img_icon_0
				self::lineyka_photos($plant->images),// lineyka_photos
				self::lineyka_photos_big($plant->images, self::true_name($plant)),// lineyka_photos_big
				$plant->price,
				$plant->comment,  // comment
				$leftsidebar, 
				$interesting_yet, 
				$params, 
				$offers, 
				$head, 
				$header, 
				$aside, 
				$footer,
                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'), // modal_in_cart
				$show_js, 
				($plant->hit == 1 ? '<li class="shop-sticker">Хит</li>' : ''),
				($plant->new == 1 ? '<li class="shop-sticker">Новинка</li>' : ''),
				($plant->action == 1 ? '<li class="shop-sticker">Акция!</li>' : ''),
				self::buy_buttons($plant),// кнопки "Купить"
                Custom::static_class('Main')::get_version(), // version
                Custom::static_class('Main')::get_tpl('modal_confirmation_tpl'), //
				ROOT,
			),
			$good_tpl
		);
		// также создаём ссылку на эту страницу в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').$this_page_url, 
			'date' => time(),
		]);

		// также создаём оффер этого товара для yml выгрузки для яндекса
        if(self::is_availability($plant)) {
            $yml_pictures = [];
            if ($plant->images == null) {
                $yml_pictures = null;// если картинок у товара нет
            }
            foreach ($plant->images as $img) {
                // получаем имя фотографии из бд (если есть)
                if ($img_name = Custom::static_class('Images')::name_by_id($img->id)) {
                    $yml_pictures[] = Custom::static_class('Images')::kit($img_name);
                }
            }
            $price = $plant->price;
            if ($plant->offers) {
                foreach ($plant->offers as $offer) {
                    if ($offer->price > $price) {
                        $price = $offer->price;
                    }
                }
            }
            Custom::static_class('Settings')::plus_offer_link([
                'id' => $plant->id,
                'url' => $settings->get_param('tcp') . '://' . $settings->get_param('shop_name_global') .
                    $this_page_url,
                'price' => $price,
                'cat_id' => $category->id,
                'name' => self::true_name($plant),
                'pictures' => $yml_pictures,
                'params' => $plant->param,
                'comment' => $plant->comment,
            ]);
        }
		return $result_good;
	}
	
	public function offers($plant, $offers, $this_page_url, $img_full_0, $img_thumbnail_0, $img_icon_0, $cat_url, $cat_name, $no_scripts_flag = false)
	/** Формирует список офферов, используя данные выгрузки и шаблон
	* @param obj $plant - объект выгрузки растения
	* @param obj $offers - объект выгрузки офферов вида:
	* @param string $this_page_url - урл страницы данного товара
    ["offers"]=>
    array(2) {
      [0]=>
      object(stdClass)#121 (4) {
        ["id"]=>
        string(1) "1"
        ["count"]=>
        string(1) "5"
        ["price"]=>
        string(3) "400"
        ["params"]=>
        array(1) {
          [0]=>
          object(stdClass)#122 (2) {
            ["name"]=>
            string(12) "Размер"
            ["value"]=>
            string(12) "Мелкий"
          }
        }
      }
	* @param string $img_full_0,// img_full_0
	* @param string $img_thumbnail_0, // img_thumbnail_0
	* @param string $img_icon_0, // img_icon_0
	* @param string $cat_url, // урл категории данного товара
	* @param string $cat_name, // имя категории данного товара
	* @param boolean $no_scripts_flag, // если false - скрипты пишутся, если true - нет (нужно для случая, если карточка товара используется как шаблон для клонирования и наполнения смыслом посредством js)
	* @return string $result_offers - html списка характеристик без оболочки
	*/
	{
		$settings = Set::getSettings();
		$offer_tpl = Custom::static_class('Main')::get_tpl('offer');
		$result = '';
		$plant_name = self::true_name($plant);// имя растения (маточника)
		$param = Custom::static_class('Main')::params_to_js_object($plant);
		if($param != '')
		{
			$param = '"param": '.$param.',';
		}
		$max_count = '';
		$numbers_inner = '';
		$cnt = 0;// либо количество в наличии, либо количество в наличии для текущего оффера
		if(! isset($offers))
		// офферов нет
		{
			if($plant->price > 0)
			{
				$max_count = '
					<div class="shop-product__anons text">
						<p class="like-h4">
							'.($plant->count > 0 ? '<b style="font-size:0.875em">Есть в наличии:</b> <span id="count_goods_'.$plant->id.'">&nbsp;'.$plant->count.'&nbsp;</span> шт.' : '').'
						</p>
					</div>
				';
				// а $numbers_inner - это цена либо растения целиком, либо цена для каждого оффера в отдельности
				$numbers_inner = '
					<span class="shop-prices js_shop_param_price" param32="209">
						<span class="shop-price">
							<span class="js_shop_price" summ="'.$plant->price.'" format_price_1="" format_price_2="" format_price_3="">'.$plant->price.'</span>&nbsp;р
						</span>
						<!--span class="shop-price shop-price_old">'.$plant->price.'&nbsp;р</span-->
					</span>
				';
			}
			// для случая когда офферов нет
			$control = ''; // select с возможностью выбора оффера
			// для случая когда офферов нет
			$js = '
				if(typeof(goods) == "undefined")
				{
					var goods = [];
				}
				// для товара - id товара, для оффера - id оффера
				// это значит, что этот оффер выбран по-умолчанию
				goods['.$plant->id.'] = [{
					type: "main",
					thlaspi_id: '.$plant->id.',
					offer_id: '.$plant->id.',
					article: '.$plant->article.',
					name: "'.$plant_name.'",
					price: '.$plant->price.',
					max_count: '.$plant->count.',
					status: "selected",
					img_full_0: "'.$img_full_0.'",
					img_thumbnail_0: "'.$img_thumbnail_0.'",
					img_icon_0: "'.$img_icon_0.'",
					this_page_url: "'.$this_page_url.'",
					cat_url: "'.$cat_url.'",
					cat_name: "'.$cat_name.'",
					'.$param.'
				}];
			';// для случая когда офферов нет
			$cnt = $plant->count;
		}
		else
		// офферы есть
		{
			$radio_inner = '';
			$ul_inner = '';
			$numbers_inner = '';
			$max_count = '';
            $summ_offer_count = 0;
			$js = '';
			$i = 0;
			foreach($offers as $offer)
			{
				
				// блок с названиями оффера. Он нужен для обработки случая, когда есть название оффера, но нет значения
				$name_offer = $offer->params[0]->name;
				$vaue_offer = $offer->params[0]->value;

				if($offer->params[0]->value == '')
				{
					$name_offer = '';
					$vaue_offer = $offer->params[0]->name;
				}
				$js .= '
					if(typeof(goods) == "undefined")
					{
						var goods = [];
					}
					if(goods['.$plant->id.'] == undefined)
					{
						// для товара - id товара, для оффера - id оффера
						// это значит, что этот оффер выбран по-умолчанию
						goods['.$plant->id.'] = [{
							type: "offer",
							thlaspi_id: '.$plant->id.',
							article: '.$plant->article.',
							offer_id: '.$offer->id.',
							offer_name: "'.$name_offer.'",
							offer_value: "'.$vaue_offer.'",
							name: "'.$plant_name.'",
							price: '.$offer->price.',
							max_count: '.$offer->count.',
							status: "normal",
							img_full_0: "'.$img_full_0.'",
							img_thumbnail_0: "'.$img_thumbnail_0.'",
							img_icon_0: "'.$img_icon_0.'",
							this_page_url: "'.$this_page_url.'",
							cat_url: "'.$cat_url.'",
							cat_name: "'.$cat_name.'",
							'.$param.'
						}];
					}
					else
					{
						goods['.$plant->id.'].push({
							type: "offer",
							thlaspi_id: '.$plant->id.',
							article: '.$plant->article.',
							offer_id: '.$offer->id.',
							offer_name: "'.$name_offer.'",
							offer_value: "'.$vaue_offer.'",
							name: "'.$plant_name.'",
							price: '.$offer->price.',
							max_count: '.$offer->count.',
							status: "normal",
							img_full_0: "'.$img_full_0.'",
							img_thumbnail_0: "'.$img_thumbnail_0.'",
							img_icon_0: "'.$img_icon_0.'",
							this_page_url: "'.$this_page_url.'",
							cat_url: "'.$cat_url.'",
							cat_name: "'.$cat_name.'",
							'.$param.'
						});
					}
				';
				if($offer->count == 0 || $offer->price == 0 || $offer->price == '0') continue;// если цена оффера = 0 - пропускаем этот оффер. Оставляем от него только js
                $summ_offer_count = $summ_offer_count + $offer->count;
				if($i == 0)
				{
					$checked = '';
					$checked_offer = ' checked="checked"';
					$selected = ' selected';
				}
				else
				{
					$checked_offer = '';
					$checked = ' style="display:none;"';
					$selected = '';
				}
				
				$radio_inner .= '
				<input class="offer-radio-checker" id="m_'.$plant->id.'_offer_'.$offer->id.'" name="show_offer_'.$plant->id.'" value="'.$offer->id.'"'.$checked_offer.' type="radio" onclick="check_radio_offer('.$plant->id.','.$offer->id.')">
				<label class="js_shop_param_price shop_param_price shop-item-price" for="m_'.$plant->id.'_offer_'.$offer->id.'">
					<div class="offer-param-name">
						<strong>'.$vaue_offer.'</strong>
					</div>
					<div class="is_avalible white">
						'.($offer->count > 0 ? 'Есть в наличии <span id="count_goods_%id">'.$offer->count.'</span> шт.' : '').'
					</div>
					<div>
						<span class="shop-counter js_shop_buy">
							<span class="shop-counter__widget">
								<a class="shop-counter__reducer js-shop-counter__reducer shop-counter__button far fa-minus-square" href="javascript:void(0)" title="Уменьшить" role="button"></a>
								<input type="text" value="1" name="count" class="number" step="any" min="1" max="'.$offer->count.'" id="count_'.$offer->id.'" autocomplete="off">
								<a class="shop-counter__adder js-shop-counter__adder shop-counter__button far fa-plus-square" href="javascript:void(0)" title="Добавить" role="button"></a>
							</span>
							<span class="product-in-cart"></span>
						</span>
						<span class="shop-prices js_shop_param_price" param32="'.$offer->id.'">
							<span class="shop-price">
								<span class="js_shop_price" summ="'.$offer->price.'" format_price_1="" format_price_2="" format_price_3="">'.$offer->price.'</span>&nbsp;р
							</span>
							<!--span class="shop-price shop-price_old">151990&nbsp;р</span-->
						</span>
					</div>
				</label>				
				';
				$ul_inner .= '<option data-count="'.$offer->count.'" value="'.$offer->id.'"'.$selected.'>'.$vaue_offer.'</option>';
				// а $numbers_inner - это цена либо растения целиком, либо цена для каждого оффера в отдельности
				// $numbers_inner .= '
					// <span class="shop-prices js_shop_param_price" param32="'.$offer->id.'"'.$checked.'>
						// <span class="shop-price">
							// <span class="js_shop_price" summ="'.$offer->price.'" format_price_1="" format_price_2="" format_price_3="">'.$offer->price.'</span>&nbsp;р
						// </span>
						// <!--span class="shop-price shop-price_old">151990&nbsp;р</span-->
					// </span>
				// ';
				$cnt = $offer->count;
				$i++;
			}

            // строка "Есть в наличии".
            if ($summ_offer_count > 0)
            {
                $max_count = '
                    <div class="shop-product__anons text">
                        <p class="like-h4">
                            <b style="font-size:0.875em">Есть в наличии:</b> <span id="count_goods_'.$plant->id.'">&nbsp;'.$summ_offer_count.'&nbsp;</span> шт.
                        </p>
                    </div>
                ';
            }
			// select с возможностью выбора оффера
			if($ul_inner != '')
			// может быть так, что все офферы пустые (с нулевой ценой). Тогда и выбирать не из чего и control - select с офферами - пустая строка
			{
				// <label class="label">'.$name_offer.'</label>
				$control = '
					<select id="offers_change_select_'.$plant->id.'" onchange="offer_param_ch(this,'.$plant->id.')" name="param32" class="js_shop_depend_param">
						'.$ul_inner.'
					</select>';
			}
		}
		$js = '<script>'.$js.'</script>';
		if($no_scripts_flag)
		// если скрипты не пишем
		{
			$js = '';
		}
		// '<strong>'.$offer->params[0]->name.'</strong>'.($offer->params[0]->value ? ': <span style="font-weight: 300">'.$offer->params[0]->value.'</span>' : ''), // характеристика оффера
		$ub_um = '';
		if($numbers_inner != '')
		{
			$ub_um = '
				<span class="shop-counter js_shop_buy">
					<span class="shop-counter__widget">
						<a class="shop-counter__reducer js-shop-counter__reducer shop-counter__button far fa-minus-square" href="javascript:void(0)" title="Уменьшить" role="button"></a>
						<input type="text" value="1" name="count" class="number" step="any" min="1" max="'.$cnt.'" id="count_'.$offer->id.'" autocomplete="off">
						<a class="shop-counter__adder js-shop-counter__adder shop-counter__button far fa-plus-square" href="javascript:void(0)" title="Добавить" role="button"></a>
					</span>
					<span class="product-in-cart"></span>
				</span>
			';
		}
		$result = str_replace(
			array(
				'%id', 
				'%count', 
				'%max_count', 
				'%control', 
				'%numbers', 
				'%js', 
				'%ub_um', 
			),
			array(
				$plant->id,
				$plant->count,
				$max_count, 
				$control.$radio_inner, // radio с возможностью выбора оффера
				$numbers_inner, // numbers $numbers_inner - это цена либо растения целиком, либо цена для каждого оффера в отдельности
				$js, // js
				$ub_um, // js
			),
			$offer_tpl
		);
		return $result;
	}
	
	public function params($param, $param_tpl)
	/** Формирует список характеристик, используя данные выгрузки и шаблон
	* @param obj $param - объект выгрузки характеристик вида:
		array(1) {
		  [0]=>
		  object(stdClass)#69 (6) {
			["id"]=>
			string(1) "2"
			["name"]=>
			string(8) "Цвет"
			["value"]=>
			string(12) "Жёлтый"
			["type"]=>
			string(1) "1"
			["use_in_filter"]=>
			string(1) "2"
			["unit"]=>
			string(0) ""
		  }
		}
	* @param string $param_tpl - шаблон параметров
	* @return string $result_params - html списка характеристик без оболочки
	*/
	{
		$settings = Set::getSettings();
		$result_params = '';
		foreach($param as $one_par)
		{
			$result_params .= str_replace(
				array(
					'%name', 
					'%value', 
					'%unit', 
				),
				array(
					$one_par->name, // name
					$one_par->value, // value
					$one_par->unit,
				),
				$param_tpl
			);
		}
		return $result_params;
	}
	
	public function list_goods_inner($krome_id, $full_tree, $param='rand', $count=3)
	/** Список растений на странице товара для перелинковки. Содержимое блока (только товары без оболочки)
	* @param int $krome_id - id товара, который нужно исключить из результатов. Если исключать не нужно - $krome_id = 0
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	* @param string $param - параметр. Если = 'rand' - случайные товары, если равно '2,6,8,...' - список id товаров, которые должны быть в результате. При этом параметр $count игнорируется, также может быть hit, new, action, соответственно, выдаются только хиты, новинки или товары по акции
	* @param int $count - количество товаров в результате
	* @return string $result - html содержимого блока списка товаров
	*/
	{
        $settings = Set::getSettings();
        $plants = $settings->save_object('plants');

		$good_in_list_tpl = Custom::static_class('Main')::get_tpl('good_in_list_tpl');
		$result = '';
		if($param == 'rand')
		{
			// $_plants = array_rand($plants,$count);
			shuffle($plants);
			$_plants = array();
			$i = 1;
			$j = 0;
			foreach($plants as $plant)
			{
				if($i > $count)
				{
					break;
				}
				if(! isset($plant->categories))
				// игнорируем растения, не привязанные ни к какой категории
				{
					continue;
				}
				if($plant->id == $krome_id)
				{
					$j++;
					if($j >= 2) break;
					continue;
				}
				$_plants[] = $plant;
				$i++;
			}
		}
		elseif($param == 'hit' || $param == 'new' || $param == 'action')
		{
			$_plants = array();
			$i = 1;
			foreach($plants as $plant)
			{
				if($i > $count)
				{
					break;
				}
				if(! isset($plant->categories))
				// игнорируем растения, не привязанные ни к какой категории
				{
					continue;
				}
				if(! isset($plant->$param) || $plant->$param == 0)
				{
					continue;
				}
				if($plant->$param == 1)
				{
					$_plants[] = $plant;
					$i++;
				}
			}
		}
		else
		{
			$indexes = explode(',',$param);
			$_plants = array();
			foreach($plants as $plant)
			{
				foreach($indexes as $index)
				{
					if($plant->porjadk_n == trim($index))
					{
						$_plants[] = $plant;
					}
				}
			}
		}
		foreach($_plants as $plant)
		{
			if(! isset($plant->categories))
			// если растение не присоединено ни к одной категории, игнорируем его
			{
				continue;
			}
			$result .= self::good_cart_in_list($good_in_list_tpl, $plant, $full_tree);
		}
		return $result;
	}
	
	public function good_cart_in_list($good_in_list_tpl, $plant, $full_tree, $no_scripts_flag = false)
	/** Одна карточка товара в списке
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param string $good_in_list_tpl - шаблон маленькой карточки товара
	* @param obj $category - объект данной категории из объекта $categories
	* @param obj $plant - объект данного товара из объекта $plants
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	* @param boolean $no_scripts_flag, - если false - скрипты пишутся, если true - нет (нужно для случая, если карточка товара используется как шаблон для клонирования и наполнения смыслом посредством js)
	* @return string $result_good - html страницы товара целиком
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		foreach($categories as $category)
		{
			if($plant->categories[0] == $category->id)
			{
				$category_url = $category->chpu;
				$this_page_url = '/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit(self::true_name($plant)).$settings->get_param('chpu_postfix');
				$cat_url = '/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu;
				$cat_name = $category->name;
				break;
            }
		}
		
		// берём имя картинки из бд по номеру thlaspi_id
        // echo __METHOD__.' или '.__FUNCTION__.'; Строка '.__LINE__.' в файле'.__FILE__.'; thlaspi_id = '.$plant->images[0]->id;
		if($img_name = Custom::static_class('Images')::name_by_id($plant->images[0]->id))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
			$paths = Custom::static_class('Images')::kit('');
		}
		
		// офферы
		$offers = self::offers($plant, $plant->offers, $this_page_url, $paths->large, $paths->list, $paths->icon, $cat_url, $cat_name, $no_scripts_flag);
		
		// главного оффера не будет, любая цена - это оффер
		// $main_offer = '';
		// if($offers == '')
		// {
			// $main_offer = self::main_offer($plant, $this_page_url, $img_full_0, $img_thumbnail_0, $img_icon_0);
		// }
		
		// характеристики
		$params = '';
		if(isset($plant->param))
		{
			$param_tpl = Custom::static_class('Main')::get_tpl('param');
			$params = self::params($plant->param, $param_tpl);
		}
		
		// buy buttons
		$buy_buttons = self::buy_buttons($plant);
				
		$result = str_replace(
			array(
				'%id', 
				'%name', 
				'%article', 
				'%this_page_url', 
				'%price', 
				'%img_thumbnail_0', 
				'%img_full_0', 
				'%img_icon_0', 
				'%image_id', 
				'%count', 
				'%offers', 
				'%hit',
				'%new',
				'%action',
				'%params',
				'%buy_buttons',
			),
			array(
				$plant->id, // id
				self::true_name($plant), // name
				$plant->article,
				ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category_url.'/'.Custom::static_class('Settings')::my_translit(self::true_name($plant)).$settings->get_param('chpu_postfix'), // this_page_url
				$plant->price, // price
				$paths->list,
				$paths->large,
				$paths->icon,
				$plant->images[0]->id, // image_id
				$plant->count,
				$offers,
				($plant->hit == 1 ? '<li class="shop-sticker">Хит</li>' : ''),
				($plant->new == 1 ? '<li class="shop-sticker">Новинка</li>' : ''),
				($plant->action == 1 ? '<li class="shop-sticker">Акция!</li>' : ''),
				$params,
				$buy_buttons,
			),
			$good_in_list_tpl
		);
		return $result;
	}
	function true_name($plant)
	/** Правильное имя растения (товара)
	* @param obj $plant - объект данного товара из объекта $plants
	*/
	{
        if (isset($plant->meta_h1) && $plant->meta_h1 != '')
        {
            return $plant->meta_h1;
        }
		$name = $plant->rus_name.($plant->sort != '' ? " '".$plant->sort."'" : '').(! empty($plant->lat_name) ? ' ('.$plant->lat_name.')' : '');
		if($plant->sort != '')
		// если есть сорт, берём первое слово от основного названия и сорт
		{
			$_name = explode(' ', $plant->rus_name);
			if($_name[0] == 'Шиповник')
			{
				$_name[0] = 'Роза';
			}
			$name = $_name[0].' '.$plant->sort;
		}
		return $name;
	}
	
	public function buy_buttons($plant)
	/** Кнопки "Купить" и "Купить в один клик"
	* @param obj $plant - объект выгрузки растения
	*/
	{
        $settings = Set::getSettings();
		if(self::is_availability($plant))
		{
            if (! $settings->get_param('cancel_sales'))
            {
                return '
                    <div class="shop-item-product__actions shop-actions">
                        <input class="button buy-button-id" action="buy" value="В корзину" type="button" data-target="#modalInCart" data-toggle="modal" data-id="'.$plant->id.'">
                        <input class="button js_shop_one_click" data-toggle="modal" data-target="#modalOneClick_'.$plant->id.'" type="button" value="Купить в 1 клик" action="one_click" data-id="'.$plant->id.'">
                    </div>
                    <div class="error" style="display:none;"></div>
                ';
            }
            else
            {
                return '';
            }
		}
		else
		{
			return '
				<!--span class="not-available-string">
					Нет в наличии
				</span-->
                <div class="shop-item-product__actions shop-actions">
                    <input class="button notify-when-available" action="notify-when-available" value="Сообщить когда появится" type="button" data-target="#modalNotifyWhenAvailable" data-toggle="modal" data-id="'.$plant->id.'">
                </div>
			';
		}
	}
	
	public function is_availability($plant)
	/** Есть ли в наличии? - true или нет - false
	* @param obj $plant - объект выгрузки растения
	*/
	{
		if(! isset($plant->offers))
		// офферов нет
		{
			if($plant->price == 0 || $plant->count == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		// офферы есть
		{
			foreach($plant->offers as $offer)
			{
				if($offer->price != 0 && $offer->count != 0)
				{
					return true;
				}
			}
			return false;
		}
	}
	static public function first_availability()
	/** для шаблона карточки товара нужен товар в наличии. Ищем его
	*/
	{
		$settings = Set::getSettings();
		$plants = $settings->save_object('plants');
		foreach($plants as $plant)
		{
			if(self::is_availability($plant))
			{
				return $plant;
			}
		}
		return $plants[0];// если не дай боже ни одного товара в наличии не нашлось - посылаем просто первый товар
	}
	
	public function lineyka_photos($imgs)
	/** Линейка дополнительных фотографий под основной
	* @param array $imgs - массив фоток вида array(5) {
      [0]=>
      object(stdClass)#76 (3) {
        ["id"]=>
        string(5) "35780"
        ["thumbnail"]=>
        string(76) "https://156909.selcdn.ru/thlaspi/6e/1d/6e1dfc1de8c68215e512d7a776b82b13.jpeg"
        ["full"]=>
        string(76) "https://156909.selcdn.ru/thlaspi/79/be/79bebbe0dca2d3237214149ddadf18f8.jpeg"
      }...
	* @return string $result - html содержимого линейки фотографий
	*/
	{
		$settings = Set::getSettings();
		$result = '';
		$i = 0;
		if($imgs == null)
		{
			return '';// если картинок у товара нет
		}
		foreach($imgs as $img)
		{
			$active = '';
			if($i == 0)
			{
				$active = ' active';
			}
			
			// получаем имя фотографии из бд (если есть)
			if($img_name = Custom::static_class('Images')::name_by_id($img->id))
			{
				$paths = Custom::static_class('Images')::kit($img_name);
			}
			else
			{
				$paths = Custom::static_class('Images')::kit('');
			}
				
			$result .= '
				<div class="gallery__item">
					<figure class="gallery__image" style="background-image:url('.$paths->icon.');"></figure>
				</div>
			';
			$i++;
		}
		return $result;
	}
	
	public function lineyka_photos_big($imgs, $h1)
	/** Линейка Основных фотографий (для этого шаблона нужна тоже линейка)
	* @param string $h1 - название страницы растения
	* @param array $imgs - массив фоток вида array(5) {
      [0]=>
      object(stdClass)#76 (3) {
        ["id"]=>
        string(5) "35780"
        ["thumbnail"]=>
        string(76) "https://156909.selcdn.ru/thlaspi/6e/1d/6e1dfc1de8c68215e512d7a776b82b13.jpeg"
        ["full"]=>
        string(76) "https://156909.selcdn.ru/thlaspi/79/be/79bebbe0dca2d3237214149ddadf18f8.jpeg"
      }...
	* @return string $result - html содержимого линейки фотографий
	*/
	{
		$settings = Set::getSettings();
		$result = '';
		$i = 0;
		if($imgs == null)
		{
			return '';// если картинок у товара нет
		}
		foreach($imgs as $img)
		{
			$active = '';
			if($i == 0)
			{
				$active = ' active';
			}
			
			// получаем имя фотографии из бд (если есть)
			if($img_name = Custom::static_class('Images')::name_by_id($img->id))
			{
				$paths = Custom::static_class('Images')::kit($img_name);
			}
			else
			{
				$paths = Custom::static_class('Images')::kit('');
			}
				
			$result .= '
				<div class="gallery__item" image_id="'.$img->id.'" data-item-index="'.$i.'">
					<figure data-fancybox="gallery15" href="'.$paths->large.'" class="gallery__image" style="background-image:url('.$paths->large.');" title="'.$h1.'"></figure>
				</div>
			';
			$i++;
		}
		return $result;
	}
}