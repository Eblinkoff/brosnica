<?php
class DB {
	/** Класс подключения бд
	*/	
	private static $_instance_mySQLi = null;
	
	
	private function __construct(){
	}	
	
	
	static public function getMySQLi() {
		if(is_null(self::$_instance_mySQLi)){
			$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
			if ($mysqli->connect_error) {
				die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
			}
			self::$_instance_mySQLi = $mysqli;
		}
		return self::$_instance_mySQLi;
	}
	
	
    private function __clone() {
    }

    private function __wakeup() {
    }	
}
