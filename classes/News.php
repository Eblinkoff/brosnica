<?php
class News
/** В отличие от товаров и категорий мы только один раз вносим в бд данные о каждой новости, при этом когда мы формируем страницы новостей и списка новостей, мы эти страницы формируем каждый раз заново (как и страницы товаров), но данные берём не из выгрузки, а из бд. То есть если нам нужно поменять что-то в тексте новости, то делать это надо в бд brosnica. НО если надо добавить новое фото, то его надо закачать на сайт при помощи api, а потом уже поменять в бд news номер фото. При этом в сайдбаре данные берутся из api...
*/
{
	public function add_news_pages()
	/** Сформировать страницы новостей на основе выгрузки новостей по api
	* @param obj
	*/
	{
		$db = DB::getMySQLi();
        $settings = Set::getSettings();
		$news = self::get_news();// заносим новости в бд brosnica (если есть новые)
		
		// создаём ссылку на страницу /blog/ в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/news/', 
			'date' => time(),
		]);
		
		if($settings->get_param('add_new_articles_only'))
		{
			$where = '`edit_label` = 3';
		}
		else
		{
			$where = 1;
		}
		// теперь проверяем, есть ли новые новости в бд. Делаем что-то только если есть
		if ($result = $db->query('
			SELECT * 
			FROM `news` 
			WHERE '.$where.' AND trash = "0"'
		))
		{
			if($result->num_rows > 0)
			// значит количество строк в результате запроса не равно 0
			{
				// проверяем, есть ли каталог с новостями
				// $_SERVER['DOCUMENT_ROOT'] - путь к корню сайта без слеша, например, "D:/OpenServer/domains/hortulus"
				// проходим массив с новостями и создаём для них каталоги в корне сайта или в папке с каталогом
				$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/news/';
				if(!is_dir($dir))
				// если её нет - создаём её
				{
					if(! mkdir($dir, 0777, true))
					{
						die('Невозможно создать каталог '.$dir);
					}
					else
					// надо создать ссылку sitemap
					{
						// также создаём ссылку на эту страницу в файле sitemap.xml
                        Custom::static_class('Settings')::plus_sitemap_link([
							'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/'.$dir.'/', 
							'date' => time(),
						]);
					}
                    Custom::static_class('Main')::create_htaccess($dir.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
				}
				else
				// если она есть - ничего не делаем
				{
					// $settings::removeDirectory($dir);
					// if(! mkdir($dir, 0777, true))
					// {
						// die('Невозможно создать каталог '.$dir.' после удаления');
					// }
				}
				
				// и получаем из файла шаблон страницы одной новости
				$new_tpl = Custom::static_class('Main')::get_tpl('new_tpl');
				
				while($row = $result->fetch_object())
				{
                    if ($row->trash == '1')// delete article
                    { // из бд не удаляем, просто скрываем
                        // удаляем её из каталога.
                        if(file_exists($dir.$row->chpu.'/index.php'))
                        {
                            unlink($dir.$row->chpu.'/index.php');
                        }

                        // удаляем каталог статьи
                        if(is_dir($dir.$row->chpu.'/'))
                        {
                            if(! rmdir($dir.$row->chpu.'/'))
                            {
                                die('Невозможно удалить каталог '.$dir.$row->chpu.'/');
                            }
                        }
                    }
                    else// формируем страницы новых статей
                    {
                        // берём имя картинки из бд по номеру thlaspi_id
                        if($row->image_id != 0 && $img_name = Custom::static_class('Images')::name_by_id($row->image_id, $alt, 3))
                        {
                            $paths = Custom::static_class('Images')::kit($img_name);
                        }
                        else
                        {
                            $paths = Custom::static_class('Images')::kit('');
                        }
                        $date = Custom::static_class('Main')::beauty_date($row->date);
                        $aside = Custom::static_class('Main')::aside();


                        $head = Custom::static_class('Main')::head(
                            $row->title_meta, // title
                            $row->descr, // description
                            '', // keywords
                            $paths->large // og_image
                        );
                        $header = Custom::static_class('Main')::header();
                        $footer = Custom::static_class('Main')::footer();
                        $show_js = Custom::static_class('Main')::show_js();

                        $result_new_page = str_replace(
                            array(
                                '%shead',
                                '%header',
                                '%footer',
                                '%modal_in_cart',
                                '%show_js',
                                '%category_text',
                                '%h1',
                                '%aside',
                                '%catalog_path_name',
                                '%date',
                                '%img_full_0',
                                '%img_thumbnail_0',
                                '%version',
                                '%root',
                            ),
                            array(
                                $head,
                                $header,
                                $footer,
                                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
                                $show_js,
                                $row->text, // category_text
                                $row->header, // h1
                                $aside,
                                ROOT.'/'.$settings->get_param('catalog_path_name'),
                                $date,
                                $paths->large,
                                $paths->list,
                                Custom::static_class('Main')::get_version(), // version
                                ROOT,
                            ),
                            $new_tpl
                        );
                        // создаём каталог новости
                        if(!is_dir($dir.$row->chpu.'/'))
                        // если её нет - создаём её
                        {
                            if(! mkdir($dir.$row->chpu.'/', 0777, true))
                            {
                                die('Невозможно создать каталог '.$dir.$row->chpu.'/');
                            }
                            Custom::static_class('Main')::create_htaccess($dir.$row->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
                        }
                        // и записываем её в каталог. Если такая новость уже есть, перезаписываем её
                        if(file_put_contents($dir.$row->chpu.'/index.php',$result_new_page) === false)
                        {
                            die('Не удалось создать шаблон '.$dir.$row->chpu.'/index.php в каталоге news');
                        }
                        // также создаём ссылку на эту страницу в файле sitemap.xml
                        Custom::static_class('Settings')::plus_sitemap_link([
                            'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/'.$dir.$row->chpu.'/',
                            'date' => strtotime($row->date),
                        ]);
                    }
                    // надо ещё переключить флаг edit_label на 2-ку, что будет сигнализировать нам о том, что страница этой новости создана
                    if($settings->get_param('add_new_articles_only'))
                    {
                        $db->query('
                                UPDATE `news` 
                                SET `edit_label` = 2
                                WHERE id = '.$row->id
                        );
                    }
				}

				// формируем страницу со списком всех новостей
				self::create_page_with_all_news_list();
			}
			else
			{
				echo 'Новых новостей нет';
			}
		}
		else
		{
			echo 'Какая-то ошибка при запросе новых новостей';
		}
		// также надо учесть, что надо создать ссылки в sitemap для новостей с edit_label = 2
		if($settings->get_param('add_new_articles_only'))
		{
			if ($result = $db->query('
				SELECT * 
				FROM `news` 
				WHERE edit_label = 2 AND trash = "0"'
			))
			{
				if($result->num_rows > 0)
				// значит количество строк в результате запроса не равно 0
				{
					while($row = $result->fetch_object())
					{
						// также создаём ссылку на эту страницу в файле sitemap.xml
                        Custom::static_class('Settings')::plus_sitemap_link([
							'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/news/'.$row->chpu.'/', 
							'date' => strtotime($row->date),
						]);
					}
				}
			}
		}
		return true;
	}
	
	public function one_item_in_list($row)
	/** Формируем страницу со списком всех новостей. Формирует список заново. Вызывается кога есть новые новости, но работает всё равно. 
	*/
	{
		// берём имя картинки из бд по номеру thlaspi_id
		if($row->image_id != 0 && $img_name = Custom::static_class('Images')::name_by_id($row->image_id, $alt, 3))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
			$paths = Custom::static_class('Images')::kit('');
		}
		$date = Custom::static_class('Main')::beauty_date($row->date);
		return '
			<li class="blog-item-post">
				<a class="blog-item-post__image" href="'.$dir.$row->chpu.'/" style="background-image: url('.$paths->large.');" title="'.$row->title_meta.'"></a>
				<div class="blog-item-post__content">
					<div class="blog-item-post__date">'.$date.'</div>
					<a class="blog-item-post__name h4" href="'.$dir.$row->chpu.'/">'.$row->title_meta.'</a>
					<div class="blog-item-post__anons"><p>'.$row->text.'</p></div>
				</div>
			</li>
		';
	}
	
	public function create_page_with_all_news_list()
	/** Формируем страницу со списком всех новостей. Формирует список заново. Вызывается когда есть новые новости, но работает всё равно.
	*/
	{
		$settings = Set::getSettings();
		$db = DB::getMySQLi();

		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/news/';
		// получаем из файла шаблон страницы списка новостей
		$news_tpl = Custom::static_class('Main')::get_tpl('news_tpl');
		$items_list = '';
		
		if ($result = $db->query('
			SELECT * 
			FROM `news` 
			WHERE 1 AND trash = "0"
			ORDER BY `date` DESC'
		))
		{
			if($result->num_rows > 0)
			// значит количество строк в результате запроса не равно 0
			{
				while($row = $result->fetch_object())
				{
					$items_list .= self::one_item_in_list($row);
				}
			}
		}
		$aside = Custom::static_class('Main')::aside();
		
		
		$head = Custom::static_class('Main')::head(
			'Новости', // title
			'Что у нас нового', // description
			'', // keywords
			'' // og_image
		);
		$header = Custom::static_class('Main')::header();
		$footer = Custom::static_class('Main')::footer();
		$show_js = Custom::static_class('Main')::show_js();

		// у нас есть список растений в данной категории. Теперь вставим его в шаблон
		$result_news_page = str_replace(
			array(
				'%shead', 
				'%header', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js', 
				'%category_text', 
				'%h1', 
				'%aside', 
				'%catalog_path_name', 
				'%items_list', 
				'%version', 
				'%root', 
			),
			array(
				$head, 
				$header, 
				$footer,
                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
				$show_js, 
				'<p>Новости нашего сайта</p>', // category_text
				'Новости', // h1
				$aside, 
				ROOT.'/'.$settings->get_param('catalog_path_name'), 
				$items_list,
                Custom::static_class('Main')::get_version(), // version
				ROOT,
			),
			$news_tpl
		);
		// и записываем её в каталог shop
		if(file_put_contents($dir.'/index.php',$result_news_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге news');
		}
		return true;
	}
	
	public function last_news($count = 3)
	/** Запрашивает новости по api, дополняет их в бд и отдаёт новости, страницы которых ещё не сформированы
	* Для сайдбара!
	* @param int $count - количество последних новостей
	* @return string $resut - html блока послених новостей для сайдбара с оболочкой
	* Одна новость:
	object(stdClass)#13 (7) {
		["id"]=>
		string(2) "26"
		["fix"]=>
		string(19) "2021-12-16 18:11:35"
		["header"]=>
		string(24) "Роза Gentle Hermione"
		["text"]=>
		string(99) "27 июля уже не такая нежная, вполне себе зрелый румянец."
		["thumbnail160x120path"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
		["full"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		["images"]=>
		array(1) {
		  [0]=>
		  object(stdClass)#14 (3) {
			["id"]=>
			string(5) "47332"
			["thumbnail"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
			["full"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		  }
		}
	  }
	*/
	{
        $db = DB::getMySQLi();
        if ($result = $db->query('
			SELECT * 
			FROM `news` 
			WHERE trash = "0"
			ORDER BY `date` DESC
			LIMIT '.$count
        )) {
            if ($result->num_rows > 0) // значит количество строк в результате запроса не равно 0
            {
                $items = '';
                $i = 0;
                while ($one_new = $result->fetch_object())
		        {
                    $dir = ROOT.'/news/'.$one_new->chpu.'/';
                    // берём имя картинки из бд по номеру thlaspi_id
                    if($one_new->image_id != 0)
                    {
                        $img_name = Custom::static_class('Images')::name_by_id($one_new->image_id, $alt, 3);
                        $paths = Custom::static_class('Images')::kit($img_name);
                    }
                    else
                    {
                        $paths = Custom::static_class('Images')::kit('');
                    }
                    $date = Custom::static_class('Main')::beauty_date($one_new->date);
                    $items .= '
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="'.$dir.'" style="background-image: url('.$paths->icon.');" title="'.$one_new->header.'"></a>
                            <div class="articles-block__content">
                                <div class="articles-block__date">'.$date.'</div>
                                <div class="articles-block__link"><a class="articles-block__link" href="'.$dir.'">'.Custom::static_class('Main')::first_words($one_new->text,9).' ...</a></div>
                            </div>
                        </li>
                    ';
                    $i++;
                }
            }
        }
		return '
			<div class="articles-block page-block page-block_aside col-12 col-md-6 order-md-1 col-lg-12">
				<a class="h2" href="'.ROOT.'/news/">Новости</a>
				<ul class="articles-block__list">
					'.$items.'
				</ul>
			</div>
		';
	}
	
	public function get_news()
	/** Запрашивает новости по api, дополняет их в бд и отдаёт новости, страницы которых ещё не сформированы
	* @return obj $news - объект с новыми новостями
	* Одна новость:
	object(stdClass)#13 (7) {
		["id"]=>
		string(2) "26"
		["fix"]=>
		string(19) "2021-12-16 18:11:35"
		["header"]=>
		string(24) "Роза Gentle Hermione"
		["text"]=>
		string(99) "27 июля уже не такая нежная, вполне себе зрелый румянец."
		["thumbnail160x120path"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
		["full"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		["images"]=>
		array(1) {
		  [0]=>
		  object(stdClass)#14 (3) {
			["id"]=>
			string(5) "47332"
			["thumbnail"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
			["full"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		  }
		}
	  }
	*/
	{
        $settings = Set::getSettings();
        $news = $settings->save_object('news');

		if($news == null) return null;
		$db = DB::getMySQLi();

		$stmt = $db->stmt_init();
		foreach($news as &$one_new)
		{
			// делаем для каждой новости новое поле name, т.к. оно используется при создании фотографий
			$one_new->name = $one_new->header.' '.$one_new->text;
			// для каждой новости обращаемся к бд и проверяем, может такая новость уже есть в бд?
			if(
				($stmt->prepare('SELECT thlaspi_id 
					FROM `news` 
					WHERE thlaspi_id = ?') ===FALSE)
				// привязываем переменные к плейсхолдорам
				or ($stmt->bind_param('i', $one_new->id) === FALSE)
				// отправляем даные, которые на данный момент находятся в привязанных переменных
				or ($stmt->execute() === FALSE)
				// привязывем переменую для получения в нее результата
				or ($stmt->bind_result($_thlaspi_id) === FALSE)
				// делаем запрос буферизированным, 
				// если бы этой строки не было, запрос был бы небуферезированым
				or ($stmt->store_result() === FALSE)
			)
			{
				die('error (' . $stmt->errno . ') ' . $stmt->error);
			}
			// $check_news = $db->query('
				// SELECT * 
				// FROM `news` 
				// WHERE thlaspi_id = '.$one_new->id.'
			// ');
			if(! $stmt->fetch())
			// то есть если такой новости нет
			{
				// ВСТАВЛЯЕМ ЕЁ В БАЗУ
				//insert into name_table (site, description) values ('sitear.ru', 'SiteAR – создание сайтов')
				if(
				($stmt->prepare('
					INSERT INTO `news` (`date`, `image_id`, `chpu`, `title_meta`, `descr`, `header`, `text`, `thlaspi_id`, `edit_label`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, 3)') ===FALSE)
				// привязываем переменные к плейсхолдорам
				or ($stmt->bind_param(
					'sisssssi',
					$one_new->fix,
					$one_new->images[0]->id, 
					$settings->my_translit($one_new->header), 
					$one_new->header, 
					$one_new->text, 
					$one_new->header, 
					$one_new->text,
					$one_new->id) === FALSE)
				// отправляем даные, которые на данный момент находятся в привязанных переменных
				or ($stmt->execute() === FALSE)
				)
				{
					die('error (' . $stmt->errno . ') ' . $stmt->error);
				}
				vd('Новости нет, вставляем');
			}
			else
			{
				// vd('Новость есть');
			}
		}
		return $news;
	}
}