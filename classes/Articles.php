<?php
class Articles
/** В отличие от товаров и категорий мы не формируем страницы статей каждый раз. Вместо этого мы формируем только страницы новых статей и общий список, а существующие страницы никуда не деваются
*/
{
	public function add_articles_pages()
	/** Сформировать страницы статей на основе выгрузки статей по api
	*/
	{
		$settings = Set::getSettings();
		$db = DB::getMySQLi();
		// нет ли новых статей, есть ли они - всё равно проверяем, т.к. статьи могли появиться в бд не только с thlaspi.com
        $articles = self::get_articles();// заносим новости в бд brosnica (если есть новые)
		// создаём ссылку на страницу /blog/ в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/blog/', 
			'date' => time(),
		]);
		
		if($settings->get_param('add_new_articles_only'))
		{
			$where = '`edit_label` = 3';
		}
		else
		{
			$where = 1;
		}
		// теперь проверяем, есть ли новые статьи в бд. Делаем что-то только если есть
		if ($result = $db->query('
			SELECT * 
			FROM `articles` 
			WHERE '.$where.' AND trash = "0"'
		))
		{
			if($result->num_rows > 0)
			// значит количество строк в результате запроса не равно 0
			{
				// проверяем, есть ли каталог со статьями
				// $_SERVER['DOCUMENT_ROOT'] - путь к корню сайта без слеша, например, "D:/OpenServer/domains/hortulus"
				// проходим массив со статьями и создаём для них каталоги в папке blog
				$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/blog/';
				if(!is_dir($dir))
				// если её нет - создаём её
				{
					if(! mkdir($dir, 0777, true))
					{
						die('Невозможно создать каталог '.$dir);
					}
                    Custom::static_class('Main')::create_htaccess($dir.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
				}
				
				// и получаем из файла шаблон страницы одной статьи
				$article_tpl = Custom::static_class('Main')::get_tpl('article_tpl');
				
				while($row = $result->fetch_object())
				{
                    if ($row->trash == '1')// delete article
                    { // из бд не удаляем, просто скрываем
                        // удаляем её из каталога.
                        if(file_exists($dir.$row->chpu.'/index.php'))
                        {
                            unlink($dir.$row->chpu.'/index.php');
                        }

                        // удаляем каталог статьи
                        if(is_dir($dir.$row->chpu.'/'))
                        {
                            if(! rmdir($dir.$row->chpu.'/'))
                            {
                                die('Невозможно удалить каталог '.$dir.$row->chpu.'/');
                            }
                        }
                    }
                    else// формируем страницы новых статей
                    {
                        // блок с фотографиями
                        $images = '';
                        if ($images_art = $db->query('
						SELECT * 
						FROM `articles_images_rel` 
						WHERE articles_id = '.$row->id.'
						ORDER BY `id` DESC'
                        ))
                        {
                            if($images_art->num_rows > 0)
                            // значит количество строк в результате запроса не равно 0
                            {
                                while($image = $images_art->fetch_object())
                                {
                                    $img_name = Custom::static_class('Images')::name_by_id($image->thlaspi_id, $alt, 4);
                                    $caption = '';
                                    if($alt == '')
                                    {
                                        $alt = $row->header;
                                    }
                                    else
                                    {
                                        $caption = '<span class="article-image-caption">'.$alt.'</span>';
                                    }
                                    $paths = Custom::static_class('Images')::kit($img_name);
                                    $images .= '
                                        <div class="col-12 col-sm-12">
                                            <a href="'.$paths->very_big.'" data-fancybox="gallery19post" style="position:relative;" title="'.$alt.'" caption="'.$alt.'">
                                                <img src="'.$paths->very_big.'" alt="'.$alt.'" title="'.$alt.'">
                                                '.$caption.'
                                            </a>
                                        </div>
                                    ';
                                }
                            }
                        }

                        // блок с привязанными к статье товарами
                        $goods_block = '';
                        if ($goods_art = $db->query('
						SELECT * 
						FROM `articles_plants_rel` 
						WHERE articles_id = '.$row->id.'
						ORDER BY `id` DESC'
                        ))
                        {
                            if($goods_art->num_rows > 0)
                            // значит количество строк в результате запроса не равно 0
                            {
                                $numbers = '';
                                $plants = $settings->save_object('plants');
                                $show_article_plants_rel_block = false;// флаг, который говорит показывать ли блок привязанных к статье растений - true или нет -  false
                                while($one_good = $goods_art->fetch_object())
                                {
                                    // тут надо ещё проверить выгружено ли такое растение?
                                    foreach ($plants as $plant)
                                    {
                                        if ($plant->id == $one_good->plant_id)
                                        {
                                            $numbers = plus_str($numbers, $one_good->plant_id, ',');
                                            $show_article_plants_rel_block = true;
                                            break;
                                        }
                                    }
                                }
                                if ($show_article_plants_rel_block)
                                {
                                    $goods_block = '
                                        <br>
                                        <br>
                                        <br>
                                        <div class="shop-product-related shop-block-products page-block">
                                            <div class="h2">Растения, о которых говорилось в статье:</div>
                                            <ul class="shop-list-products row">
                                                '.Custom::static_class('Good')::list_goods_inner(0, '', $numbers).'
                                            </ul>
                                        </div>
                                    ';
                                }
                            }
                        }

                        $date = Custom::static_class('Main')::beauty_date($row->date);
                        $aside = Custom::static_class('Main')::aside();


                        $head = Custom::static_class('Main')::head(
                            $row->title_meta, // title
                            $row->descr, // description
                            '', // keywords
                            $paths->large // og_image
                        );
                        $header = Custom::static_class('Main')::header();
                        $footer = Custom::static_class('Main')::footer();
                        $show_js = Custom::static_class('Main')::show_js();

                        $result_new_page = str_replace(
                            array(
                                '%shead',
                                '%header',
                                '%footer',
                                '%modal_in_cart',
                                '%show_js',
                                '%category_text',
                                '%h1',
                                '%aside',
                                '%catalog_path_name',
                                '%date',
                                '%images',
                                '%goods_block',
                                '%version',
                                '%modal_confirmation_tpl',
                                '%root',
                            ),
                            array(
                                $head,
                                $header,
                                $footer,
                                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
                                $show_js,
                                $row->text, // category_text
                                $row->header, // h1
                                $aside,
                                ROOT.'/'.$settings->get_param('catalog_path_name'),
                                $date,
                                $images,
                                $goods_block,
                                Custom::static_class('Main')::get_version(), // version
                                Custom::static_class('Main')::get_tpl('modal_confirmation_tpl'), // шаблон модального окна
                                ROOT,
                            ),
                            $article_tpl
                        );
                        // создаём каталог статьи
                        if(!is_dir($dir.$row->chpu.'/'))
                            // если её нет - создаём её
                        {
                            if(! mkdir($dir.$row->chpu.'/', 0777, true))
                            {
                                die('Невозможно создать каталог '.$dir.$row->chpu.'/');
                            }
                            Custom::static_class('Main')::create_htaccess($dir.'/'.$row->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
                        }
                        // и записываем её в каталог. Если такая статья уже есть, перезаписываем её
                        if(file_put_contents($dir.$row->chpu.'/index.php',$result_new_page) === false)
                        {
                            die('Не удалось создать шаблон '.$dir.$row->chpu.'/index.php в каталоге blog');
                        }
                        // также создаём ссылку на эту страницу в файле sitemap.xml
                        Custom::static_class('Settings')::plus_sitemap_link([
                            'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/'.$dir.$row->chpu.'/',
                            'date' => strtotime($row->date),
                        ]);
                    }
                    // надо ещё переключить флаг edit_label на 2-ку, что будет сигнализировать нам о том, что страница этой статьи создана
                    if($settings->get_param('add_new_articles_only'))
                    {
                        $db->query('
							UPDATE `articles` 
							SET `edit_label` = 2
							WHERE id = '.$row->id
                        );
                    }
				}
				// формируем страницу со списком всех статей
				self::create_page_with_all_articles_list();
			}
			else
			{
				echo '<br><br>Новых статей нет';
			}
		}
		else
		{
			echo 'Какая-то ошибка при запросе новых статей';
		}
		// также надо учесть, что надо создать ссылки в sitemap для статей с edit_label = 2
		if($settings->get_param('add_new_articles_only'))
		{
			if ($result = $db->query('
				SELECT * 
				FROM `articles` 
				WHERE edit_label = 2 AND trash = "0"'
			))
			{
				if($result->num_rows > 0)
				// значит количество строк в результате запроса не равно 0
				{
					while($row = $result->fetch_object())
					{
						// также создаём ссылку на эту страницу в файле sitemap.xml
                        Custom::static_class('Settings')::plus_sitemap_link([
							'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/blog/'.$row->chpu.'/', 
							'date' => strtotime($row->date),
						]);
					}
				}
			}
		}
		return true;
	}
	
	public function one_item_in_list($row)
	/** Формируем страницу со списком всех статей. Формирует список заново. Вызывается когда есть новые статьи, но работает всё равно. 
	*/
	{
		//$settings = Set::getSettings();
		// берём имя картинки из бд по номеру thlaspi_id
		if($row->image_id != 0 && $img_name = Custom::static_class('Images')::name_by_id($row->image_id, $alt, 4))
		{
            $paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
            $paths = Custom::static_class('Images')::kit('');
		}
		$date = Custom::static_class('Main')::beauty_date($row->date);
		return '
			<li class="blog-item-post">
				<a class="blog-item-post__image" href="'.$dir.$row->chpu.'/" style="background-image: url('.$paths->list.');" title="'.$row->header.'"></a>
				<div class="blog-item-post__content">
					<div class="blog-item-post__date">'.$date.'</div>
					<a class="blog-item-post__name h4" href="'.$dir.$row->chpu.'/">'.$row->header.'</a>
					<div class="blog-item-post__anons">
						<a href="'.$dir.$row->chpu.'/">
							<p>'.Custom::static_class('Main')::first_words($row->text,19).' ... читать дальше</p>
						</a>
					</div>
				</div>
			</li>
		';
	}
	
	public function create_page_with_all_articles_list()
	/** Формируем страницу со списком всех статей. Формирует список заново. Вызывается когда есть новые статьи, но работает всё равно. 
	*/
	{
		$settings = Set::getSettings();
		$db = DB::getMySQLi();

		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/blog/';
		// получаем из файла шаблон страницы списка статей
		$articles_tpl = Custom::static_class('Main')::get_tpl('articles_tpl');
		$items_list = '';
		
		if ($result = $db->query('
			SELECT * 
			FROM `articles` 
			WHERE 1 AND trash = "0"
			ORDER BY `date` DESC'
		))
		{
			if($result->num_rows > 0)
			// значит количество строк в результате запроса не равно 0
			{
				while($row = $result->fetch_object())
				{
					$items_list .= self::one_item_in_list($row);
				}
			}
		}
		else
		{
			echo 'Ошибка выгрузки статей из бд articles';
		}
		$aside = Custom::static_class('Main')::aside();
		
		
		$head = Custom::static_class('Main')::head(
			'Статьи', // title
			'Список статей и заметок о розах и многолетних растениях', // description
			'', // keywords
			'' // og_image
		);
		$header = Custom::static_class('Main')::header();
		$footer = Custom::static_class('Main')::footer();
		$show_js = Custom::static_class('Main')::show_js();
				
		$result_articles_page = str_replace(
			array(
				'%shead', 
				'%header', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js', 
				'%category_text', 
				'%h1', 
				'%aside', 
				'%catalog_path_name', 
				'%items_list', 
				'%version', 
				'%root', 
			),
			array(
				$head, 
				$header, 
				$footer,
                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
				$show_js, 
				'<p>Блог нашего сайта.</p>', // category_text
				'Статьи', // h1
				$aside, 
				ROOT.'/'.$settings->get_param('catalog_path_name'), 
				$items_list,
                Custom::static_class('Main')::get_version(), // version
				ROOT,
			),
			$articles_tpl
		);
		// и записываем её в каталог shop
		if(file_put_contents($dir.'/index.php',$result_articles_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге news');
		}
		return true;
	}
	
	public function last_articles($count = 3)
	/** Запрашивает статьи по api, дополняет их в бд и отдаёт статьи, страницы которых ещё не сформированы
	* @param int $count - количество последних статей
	* @return string $resut - html блока послених статей для сайдбара с оболочкой
	* Одна новость:
	object(stdClass)#13 (7) {
		["id"]=>
		string(2) "26"
		["fix"]=>
		string(19) "2021-12-16 18:11:35"
		["header"]=>
		string(24) "Роза Gentle Hermione"
		["text"]=>
		string(99) "27 июля уже не такая нежная, вполне себе зрелый румянец."
		["thumbnail160x120path"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
		["full"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		["images"]=>
		array(1) {
		  [0]=>
		  object(stdClass)#14 (3) {
			["id"]=>
			string(5) "47332"
			["thumbnail"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/05/d8/05d8c7b5395e3f58f103f40b14c2c56d.jpeg"
			["full"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/0f/fb/0ffbdfc09f8126ef14430606ba8197a8.jpeg"
		  }
		}
	  }
	*/
	{
        $db = DB::getMySQLi();
        if ($result = $db->query('
			SELECT * 
			FROM `articles` 
			WHERE trash = "0"
			ORDER BY `date` DESC
			LIMIT '.$count
        )) {
            if ($result->num_rows > 0) // значит количество строк в результате запроса не равно 0
            {
                $items = '';
                $i = 0;
                while ($one_article = $result->fetch_object()) {
                    $dir = ROOT.'/blog/'.$one_article->chpu.'/';
                    // берём имя картинки из бд по номеру thlaspi_id
                    if($one_article->image_id != 0)
                    {
                        $img_name = Custom::static_class('Images')::name_by_id($one_article->image_id, $alt, 4);
                        $paths = Custom::static_class('Images')::kit($img_name);
                    }
                    else
                    {
                        $paths = Custom::static_class('Images')::kit('');
                    }
                    $date = Custom::static_class('Main')::beauty_date($one_article->date);
                    $items .= '
                        <li class="articles-block__item">
                            <a class="articles-block__image" href="'.$dir.'" style="background-image: url('.$paths->icon.');" title="'.$one_article->header.'"></a>
                            <div class="articles-block__content">
                                <a class="articles-block__link" href="'.$dir.'">'.$one_article->header.'</a>
                                <div class="articles-block__date">'.$date.'</div>
                            </div>
                        </li>
                    ';
                    $i++;
                }
            }
            else
            {
                return '';
            }
        }
        else
        {
            return '';
        }

		return '
			<div class="articles-block page-block page-block_aside col-12 col-md-6 order-md-1 col-lg-12">
				<a class="h2" href="'.ROOT.'/blog/">Блог</a>
				<ul class="articles-block__list">
					'.$items.'
				</ul>
			</div>
		';
	}
	
	public function get_articles()
	/** Запрашивает статьи по api, дополняет их в бд и отдаёт статьи, страницы которых ещё не сформированы
	* @return obj $articles - объект с новыми статьями
	* Одна статья:
	[0]=>
	  object(stdClass)#63 (8) {
		["id"]=>
		string(1) "3"
		["fix"]=>
		string(19) "2022-01-31 09:19:13"
		["header"]=>
		string(15) "Роза Briosa"
		["text"]=>
		string(33) "может быть и такой"
		["thumbnail160x120path"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/de/89/de89be5f4d0fc51f9db8edbaffa0de43.jpeg"
		["full"]=>
		string(76) "https://156909.selcdn.ru/thlaspi/f4/20/f420b69d55c6b6e079c170a84a4cd68c.jpeg"
		["images"]=>
		array(2) {
		  [0]=>
		  object(stdClass)#64 (4) {
			["id"]=>
			string(1) "7"
			["thumbnail"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/85/a2/85a2948da9ca53f230e1512d83a27d3c.jpeg"
			["full"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/e8/aa/e8aa7a8ac35908b70bc4ab122e66b5dd.jpeg"
			["title"]=>
			string(0) ""
		  }
		  [1]=>
		  object(stdClass)#65 (4) {
			["id"]=>
			string(1) "6"
			["thumbnail"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/de/89/de89be5f4d0fc51f9db8edbaffa0de43.jpeg"
			["full"]=>
			string(76) "https://156909.selcdn.ru/thlaspi/f4/20/f420b69d55c6b6e079c170a84a4cd68c.jpeg"
			["title"]=>
			string(0) ""
		  }
		}
		["tie_goods"]=>
		array(2) {
		  [0]=>
		  object(stdClass)#66 (1) {
			["thlaspi_id"]=>
			string(4) "8713"
		  }
		  [1]=>
		  object(stdClass)#67 (1) {
			["thlaspi_id"]=>
			string(4) "8708"
		  }
		}
	  }
	  [1]=> ...
  */
	{
        $settings = Set::getSettings();
        $articles = $settings->save_object('articles');

		if($articles == null) return null;
		$db = DB::getMySQLi();

		$stmt = $db->stmt_init();
		foreach($articles as &$one_article)
		{
			// делаем для каждой статьи новое поле name, т.к. оно используется при создании фотографий
			$one_article->name = $one_article->header.' '.$one_article->text;
			// для каждой статьи обращаемся к бд и проверяем, может такая статья уже есть в бд?
			if(
				($stmt->prepare('SELECT thlaspi_id 
					FROM `articles` 
					WHERE thlaspi_id = ?') ===FALSE)
				// привязываем переменные к плейсхолдорам
				or ($stmt->bind_param('i', $one_article->id) === FALSE)
				// отправляем даные, которые на данный момент находятся в привязанных переменных
				or ($stmt->execute() === FALSE)
				// привязывем переменую для получения в нее результата
				or ($stmt->bind_result($_thlaspi_id) === FALSE)
				// делаем запрос буферизированным, 
				// если бы этой строки не было, запрос был бы небуферезированым
				or ($stmt->store_result() === FALSE)
			)
			{
				die('error (' . $stmt->errno . ') ' . $stmt->error);
			}
			if(! $stmt->fetch())
			// то есть если такой статьи нет
			{
				// ВСТАВЛЯЕМ ЕЁ В БАЗУ
				if(
				($stmt->prepare('
					INSERT INTO `articles` (`date`, `chpu`, `title_meta`, `descr`, `header`, `text`, `thlaspi_id`, `edit_label`) VALUES (?, ?, ?, ?, ?, ?, ?, 3)') ===FALSE)
				// привязываем переменные к плейсхолдорам
				or ($stmt->bind_param(
					'ssssssi',
					$one_article->fix,
					$settings->my_translit($one_article->header), 
					$one_article->title, // title_meta
					$one_article->description, // descr
					$one_article->header, 
					$one_article->text,
					$one_article->id
				) === FALSE)
				// отправляем даные, которые на данный момент находятся в привязанных переменных
				or ($stmt->execute() === FALSE)
				)
				{
					die('Ошибка вставки данных в методе get_articles (' . $stmt->errno . ') ' . $stmt->error);
				}
				$insert_id = $stmt->insert_id;
				// вставляем также фотографии для статей
				if(isset($one_article->images))
				{
					$stmt = $db->stmt_init();
					foreach($one_article->images as $image)
					{
						if(
						($stmt->prepare('
							INSERT INTO `articles_images_rel` 
							(`articles_id`, `thlaspi_id`) 
							VALUES (?, ?)') ===FALSE)
						// привязываем переменные к плейсхолдорам
						or ($stmt->bind_param(
							'ii',
							$insert_id,
							$image->id
						) === FALSE)
						// отправляем даные, которые на данный момент находятся в привязанных переменных
						or ($stmt->execute() === FALSE)
						)
						{
							die('Ошибка вставки данных в таблицу articles_plants_rel в методе get_articles (' . $stmt->errno . ') ' . $stmt->error);
						}
						// если thumbnail160x120path в статье совпадает с thumbnail160x120path фото - эта фотка главная. Надо поставить её номер thlaspi_id в поле image_id в таблице articles
						if($one_article->thumbnail160x120path == $image->thumbnail)
						{
							if(
							($stmt->prepare('
								UPDATE `articles` 
								SET image_id = ?
								WHERE id = '.$insert_id.'
							') ===FALSE)
							// привязываем переменные к плейсхолдорам
							or ($stmt->bind_param(
								'i',
								$image->id
							) === FALSE)
							// отправляем даные, которые на данный момент находятся в привязанных переменных
							or ($stmt->execute() === FALSE)
							)
							{
								die('Ошибка вставки номера главной картинки в таблице articles в методе get_articles (' . $stmt->errno . ') ' . $stmt->error);
							}
						}
					}
				}
				// вставляем также присоединённые товары (растения) для статей
				if(isset($one_article->tie_goods))
				{
					$stmt = $db->stmt_init();
					foreach($one_article->tie_goods as $tie_good)
					{
						if(
						($stmt->prepare('
							INSERT INTO `articles_plants_rel` 
							(`articles_id`, `plant_id`) 
							VALUES (?, ?)') ===FALSE)
						// привязываем переменные к плейсхолдорам
						or ($stmt->bind_param(
							'ii',
							$insert_id,
							$tie_good->thlaspi_id
						) === FALSE)
						// отправляем даные, которые на данный момент находятся в привязанных переменных
						or ($stmt->execute() === FALSE)
						)
						{
							die('Ошибка вставки данных в таблицу articles_plants_rel в методе get_articles (' . $stmt->errno . ') ' . $stmt->error);
						}
					}
				}
				vd('<br><br>Статьи нет, вставляем');
			}
			else
			{
				// vd('Статья есть');
			}
		}
		return $articles;
	}
}