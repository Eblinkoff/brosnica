<?php
class Settings
/**
Настройки 
*/
{
	var $params = array(
		'tcp' => 'http',// протокол
		'shop_name_global' => 'brosnica.ru',// название сайта
		'chpu_postfix' => '.html',// постфикс путей товаров
		'catalog_path_name' => 'shop',// каталог, в котором лежат директории с товарами
		'admin_dir' => 'admin',// каталог, в котором лежат файлы CMS, по-умолчанию - admin, в проде д.б. что-то типа khj4jhfjk - секретный адрес
		'good_tpl' => 'good.tpl',// название файла - шаблона страницы товара в папке views
		'catalog_tpl' => 'catalog.tpl',// название файла - шаблона всего каталога в папке views
		'cart_js_tpl' => 'cart.js.tpl',// название файла - шаблона js-файла корзины
		'offer' => 'offer.tpl',// название файла - шаблона одного оффера
		'param' => 'param.tpl',// название файла - шаблона одной характеристики
		'cart_tpl' => 'cart.tpl',// название файла - шаблона корзины в папке views
		'wish_tpl' => 'wish.tpl',// название файла - шаблона хотелок в папке views
		'last_order_tpl' => 'last_order.tpl',// название файла - шаблона последнего заказа в папке views
		'main_tpl' => 'main.tpl',// название файла - шаблона главной страницы в папке views
		'category_tpl' => 'category.tpl',// название файла - шаблона категории в папке views
		'good_in_list_tpl' => 'good_in_list.tpl',// название файла - шаблона товара в списке в папке views
		'wish_good_in_list_tpl' => 'wish_good_in_list.tpl',// название файла - шаблона товара в списке без массива goods
		'modal_in_cart_tpl' => 'modal_in_cart.tpl',// модальное окно после нажатия кнпки "Купить"
		'modal_confirmation_tpl' => 'modal_confirmation.tpl',// модальное окно для вывода произвольного сообщения
		'head_tpl' => 'head.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'header_tpl' => 'header.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'footer_tpl' => 'footer.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'aside_tpl' => 'aside.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'static_page_tpl' => 'static_page.tpl',// название файла - шаблона статической страницы
		'show_js_tpl' => 'show_js.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'cat_in_list_tpl' => 'cat_in_list.tpl',// название файла - шаблона категории в списке (для общего каталога)
		'news_tpl' => 'news.tpl',// название файла - шаблона списка новостей
		'articles_tpl' => 'articles.tpl',// название файла - шаблона списка статей
		'article_tpl' => 'article.tpl',// название файла - шаблона одной статьи
		'new_tpl' => 'new.tpl',// название файла - шаблона одной новости
		'htaccess_tpl' => 'htaccess.tpl',// название файла - шаблона .htaccess
		'images_catalog_path_name' => 'images',// каталог, в котором лежат директории с картинками
		'images_templates' => array(
			'very_big' => array(
				'crop_quadrat' => 0, // обрезать в виде квадрата = 1 или нет - = 0
				'size' => 900, // размер фото по меньшей стороне в пикселях или native - тогда фото равно размеру меньшей стороны
				'catalog' => 'very_big', // название каталога для картинок этого размера в папке с картинками
			),
			'large' => array(
				'crop_quadrat' => 1, // обрезать в виде квадрата = 1 или нет - = 0
				'size' => 640, // размер фото по меньшей стороне в пикселях или native - тогда фото равно размеру меньшей стороны
				'catalog' => 'large', // название каталога для картинок этого размера в папке с картинками
			),
			'list' => array(
				'crop_quadrat' => 1, // обрезать в виде квадрата = 1 или нет - = 0
				'size' => 480, // размер фото по меньшей стороне в пикселях или native - тогда фото равно размеру меньшей стороны
				'catalog' => 'list', // название каталога для картинок этого размера в папке с картинками
			),
			'icon' => array(
				'crop_quadrat' => 1, // обрезать в виде квадрата = 1 или нет - = 0
				'size' => 118, // размер фото по меньшей стороне в пикселях или native - тогда фото равно размеру меньшей стороны
				'catalog' => 'icon', // название каталога для картинок этого размера в папке с картинками
			),
		),// массив с настройками картинок, адаптированных под данный сайт. list, large итд - просто любые названия, как названия переменных, они нужны чтобы использовать их в шаблонах
		'smtp' => true,// использовать SMTP-авторизацию при отправке почтовых сообщений true/false (да/нет)
		'watermark' => true,// использовать водяные знаки для изображений true/false (да/нет)
		'watermark_value' => '©brosnica.ru',// значение водяного знака для изображений (текст для вставки)
		'add_new_articles_only' => false,// создавать только новые страницы статей и новостей [true] или каждый раз все [false]
		'cancel_sales' => false,// отменить продажи [true] или разрешить продажи [false]
	);
	
	public function get_param($index)
	/** Получаем настройку по её названию
	* @param string $index - название настройки (и индекс массива с настройками)
	*/
	{
		if(isset($this->params[$index]))
		{
			return $this->params[$index];
		}
		else
		{
			return false;
		}
	}
	
	function removeDirectory($dir)
	{
		if ($objs = glob($dir."/*"))
		{
			foreach($objs as $obj)
			{
				if(is_dir($obj))
				{
					if(file_exists($obj.'/.htaccess'))
					{
						unlink($obj.'/.htaccess');
					}
					self::removeDirectory($obj);
				}
				else
				{
					unlink($obj);
				}
			}
		}
		rmdir($dir);
	}
	
	function my_translit($s)
	{
		//функция сырая. Буду переделывать сообразно моим потребностям. Функция предназначена для
		//ЧПУ и не для чего больше.
		//НА АНГЛИЙСКОМ - ТОЛЬКО ЧПУ, на русском - ещё и транслит.
		//!!!!!!!!!!!!!!!!!!!Не работает. Проблемы с кодировкой Разрешилось http://www.softtime.ru/forum/read.php?id_forum=1&id_theme=91840#post546653!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		$text_new='';//результирующая переменная
		$array_translit=array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'yo','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'',
		'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'yo','Ж'=>'j','З'=>'z','И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'','Ь'=>'',
		//'-'=>'-',
		//'0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9',
		'a'=>'a','b'=>'b','c'=>'c','d'=>'d','e'=>'e','f'=>'f','g'=>'g','h'=>'h','i'=>'i','j'=>'j','k'=>'k','l'=>'l','m'=>'m','n'=>'n','o'=>'o','p'=>'p','q'=>'q','r'=>'r','s'=>'s','t'=>'t','u'=>'u','v'=>'v','w'=>'w','x'=>'x','y'=>'y','z'=>'z',
		'A'=>'a','B'=>'b','C'=>'c','D'=>'d','E'=>'e','F'=>'f','G'=>'g','H'=>'h','I'=>'i','J'=>'j','K'=>'k','L'=>'l','M'=>'m','N'=>'n','O'=>'o','P'=>'p','Q'=>'q','R'=>'r','S'=>'s','T'=>'t','U'=>'u','V'=>'v','W'=>'w','X'=>'x','Y'=>'y','Z'=>'z');
		$s = (string) $s; // преобразуем в строковое значение
		$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		// удаляем повторяющие пробелы
		$s = trim($s,'-'); // убираем пробелы в начале и конце строки
		//return mb_strlen($s);
		for ( $i = 0; $i < mb_strlen($s,'utf-8'); $i++ ){//проход всей строки $s
			$text_vspom=mb_substr($s, $i, 1,'utf-8');//вспомогательная переменная для ускорения работы подпрограммы.
			//$text_vspom= (string) $text_vspom;
			//в $text_vspom находится i-я буква переменной $s.
			//echo '$S[i]= '.$text_vspom.'<br/>';
			foreach($array_translit as $key=>$value){//проход массива допустимой транслитерации
				//echo $value.'<br/>';
				if($text_vspom===$key){
					$text_new.=$value;
					//echo $red_string.'$key= '.$key.', value= '.$value.', $text_vspom= '.$text_vspom.', $text_new= '.$text_new;
					break 1;
				}elseif($text_vspom==='0'){
					$text_new.='0';
					break 1;
				}elseif($text_vspom==='1'){
					$text_new.='1';
					break 1;
				}elseif($text_vspom==='2'){
					$text_new.='2';
					break 1;
				}elseif($text_vspom==='3'){
					$text_new.='3';
					break 1;
				}elseif($text_vspom==='4'){
					$text_new.='4';
					break 1;
				}elseif($text_vspom==='5'){
					$text_new.='5';
					break 1;
				}elseif($text_vspom==='6'){
					$text_new.='6';
					break 1;
				}elseif($text_vspom==='7'){
					$text_new.='7';
					break 1;
				}elseif($text_vspom==='8'){
					$text_new.='8';
					break 1;
				}elseif($text_vspom==='9'){
					$text_new.='9';
					break 1;
				}
			}
			//if no coincidences - separate check on spaces
			if($text_vspom==' '&&mb_substr($s, ($i-1), 1,'utf-8')!=' '){
				//echo '*'.mb_substr($s, ($i-1), 1,'utf-8').'*';
				if($text_new[mb_strlen($text_new,'utf-8')-1]!='-'){ //the last character
					$text_new.='-';
				}
			}
			if($text_vspom=='-'&&mb_substr($s, ($i-1), 1,'utf-8')!=' '&&mb_substr($s, ($i+1), 1,'utf-8')!=' '){
				//echo '*'.mb_substr($s, ($i-1), 1,'utf-8').'*';
				if($text_new[mb_strlen($text_new,'utf-8')-1]!='-'){ //the last character
					$text_new.='-';
				}
			}
		}
			// переводим строку в нижний регистр (иногда надо задать локаль)
		//$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		// очищаем строку от недопустимых символов
		// заменяем пробелы знаком минус
		return $text_new; // возвращаем результат
	}
	function save_object($name, $obj = null)
	/** Сохраняем объект
	* @param obj $obj - объект сохранения
	* @param string $name - имя объекта сохранения
	*/
	{
		static $save = [];
		if(isset($save[$name]))
		{
			return $save[$name];
		}
		else
		{
			$save[$name] = $obj;
		}
	}
	
	function plus_sitemap_link($obj, $return = false)
	/** Добавляем ссылку на страницу для файла sitemap.xml
	* @param obj $obj - объект с данными для одной ссылки на страницу
	* @param bool $return - отдавать результат (true) или нет - (false)
	* @return string $body - тело файла sitemap без родительского тега <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	*/
	{
		static $body = '';// тело файла sitemap
		if(!$return)
		{
			$body .= '
	<url>
		<loc>'.$obj['url'].'</loc>
		<lastmod>' . date('Y-m-d', $obj['date']) . '</lastmod>
		<priority>' . ((($obj['date'] + 604800) > time()) ? '1' : '0.5') . '</priority>
	</url>
			';
		}
		else
		{
			return $body;
		}
	}

    /** Добавляем offer для yml фида для яндекса
     * @param obj $obj - объект с данными для одной ссылки на страницу
     * @param bool $return - отдавать результат (true) или нет - (false)
     * @return string $body - тело файла sitemap без родительского тега <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
     */
	function plus_offer_link($obj, $return = false)
	{
        $settings = Set::getSettings();
        static $body = '';// тело файла sitemap
		if(!$return)
		{
			$body .= '
                <offer id="'.$obj['id'].'" available="true">
                    <url>'.$obj['url'].'</url>
                    <price>'.$obj['price'].'</price>
                    <currencyId>RUB</currencyId>
                    <categoryId>'.$obj['cat_id'].'</categoryId>
                    <name>'.$obj['name'].'</name>';
                    if (isset($obj['pictures']))
                    {
                        foreach ($obj['pictures'] as $img)
                        {
                            $body .= '<picture>'.$settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').$img->large.'</picture>';
                        }
                    }
                    if (isset($obj['params']))
                    {
                         foreach ($obj['params'] as $param)
                        {
                            $body .= '<param name="'.$param->name.'"'.(isset($param->unit) && $param->unit != '' ? ' unit="'.$param->unit.'"' : '').'>'.$param->value.'</param>';
                        }
                    }
            $body .= '
                    <description>
                         <![CDATA[<p>'.$obj['comment'].'</p>]]>
                    </description>
                </offer>
			';
		}
		else
		{
			return $body;
		}
	}

}