<?php
class Main
/**
* Класс действий
*/
{
	private $search;
	
	public function first_words($text, $count_words)
	/** Отдаёт первые $count_words слов из текста $text
	*/
	{
        $text = strip_tags($text);// добавил это сюда т.к. если теги (например, картинки) были размещены слишком близко к началу текста статьи, то незаэкранированные кавычки ломали вёрстку
		$gap = explode(' ',$text);
		// $arr = array_slice($gap, 0, $count_words);
		return implode(' ', array_slice($gap, 0, $count_words));
	}
	
	public function get_goods($pull_plants)
	/** Отдаёт список товаров данного пользователя для всех садов
	*/
	{
		//получаем список id всех садов данного пользователя

		if(! $ids = $pull_plants->get_gardens_ids())
		{
			die($pull_plants->error_message);
		}

		//подтягиваем все растения из всех садов с thlaspi.com  по их id - $ids

		if(! ($plants = $pull_plants->get_gardens_plants($ids)))
		{
			die($pull_plants->error_message);
		}
		return $plants;
	}
	
	public function get_tpl($key)
	/** Получить шаблон по ключу.
	* @param string $key - ключ шаблона (название его настройки в классе Settings)
	* @return string $result - html шаблона
	*/
	{
		$settings = Set::getSettings();
        if(is_file(INSIDE_ABSPATH . '/custom/views/'.$settings->get_param($key)))
        {
            $result = file_get_contents(INSIDE_ABSPATH . '/custom/views/'.$settings->get_param($key));
            if($result === false)
            {
                die('Не удалось прочитать шаблон: '.INSIDE_ABSPATH . '/custom/views/'.$settings->get_param($key));
            }
        }
        else
        {
            $result = file_get_contents(INSIDE_ABSPATH . '/views/'.$settings->get_param($key));
            if($result === false)
            {
                die('Не удалось прочитать шаблон: '.$_SERVER['DOCUMENT_ROOT'].'../'.$settings->get_param('admin_dir').'/views/'.$settings->get_param($key));
            }
        }
		return $result;
	}
	
	public function aside()
	/** Формируем содержимое блока слева - всё вместе, в том числе меню, новости итд - всё, что нужно
	* @param obj $categories - объект выгрузки категорий
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param obj $news - объект выгрузки новостей с thlaspi.com
	* @param obj $articles - объект выгрузки статей с thlaspi.com
	* @return string $result - html содержимого левго сайдбара
	*/
	{
		static $result = '';
		if($result == '')
		{
			$aside_tpl = self::get_tpl('aside_tpl');
			$leftsidebar = Custom::static_class('Menu')::leftsidebar();
			// $filter_shop = self::filter_shop('desktop', $plants);
			$filter_shop = '';
			$result = str_replace(
				array(
					'%leftsidebar', 
					'%filter_shop', 
					'%news_block', 
					'%articles_block', 
				),
				array(
					$leftsidebar, 
					$filter_shop,
                    Custom::static_class('News')::last_news(),
                    Custom::static_class('Articles')::last_articles(),
				),
				$aside_tpl
			);
		}
		return $result;
	}
	
	public function beauty_date($date)
	/** Отдаёт красивую дату по datetime
	* @param string $date - дата в формате 2021-09-26 23:07:44
	* @return string $result - дата в формате 13 октября 2018 г
	*/
	{
		switch(date('m', strtotime($date)))
		{
			case '01':
				$month = 'января';
				break;
			case '02':
				$month = 'февраля';
				break;
			case '03':
				$month = 'марта';
				break;
			case '04':
				$month = 'апреля';
				break;
			case '05':
				$month = 'мая';
				break;
			case '06':
				$month = 'июня';
				break;
			case '07':
				$month = 'июля';
				break;
			case '08':
				$month = 'августа';
				break;
			case '09':
				$month = 'сентября';
				break;
			case '10':
				$month = 'октября';
				break;
			case '11':
				$month = 'ноября';
				break;
			case '12':
				$month = 'декабря';
				break;
		}
		return date('d', strtotime($date)).' '.$month.' '.date('Y', strtotime($date)).'г.';
	}
	
	public function params_to_js_object($plant)
	/** Формирует массив характеристик для использования в js
	* @param obj $plant - объект выгрузки растения
	*/
	{
		$param = '';
		if(isset($plant->param) && $plant->param != '')
		{
			foreach($plant->param as $one_param)
			{
				$param .= '{id: '.$one_param->id.', name: "'.$one_param->name.'", value: "'.$one_param->value.'", unit: "'.$one_param->unit.'"},';
			}
			
			if($param != '')
			{
				$param = '['.$param.']';
			}
		}
		return $param;
	}
	
	public function breadcrumbs($name, $full_tree_array, $category, $is_good = true)
	/** Отдаёт html хлебных крошек на основани массива full_tree_array
	* @param string $name - имя текущей страницы
	* @param obj $full_tree_array - массив дерева категорий с объектами категорий (без shop и текущей) или array(), если категория $category - корневая
	* @param obj $category - объект данной категории из объекта $categories
	*/
	{
		$settings = Set::getSettings();
		$result = '
			<li typeof="v:Breadcrumb">
				<a href="'.ROOT.'/" rel="v:url" property="v:title">Главная</a>
			</li>
			<li>—</li>
			<li typeof="v:Breadcrumb">
				<a href="'.ROOT.'/shop/" rel="v:url" property="v:title">Каталог</a>
			</li>
		';
		$full_tree_c = '';
		if(count($full_tree_array))
		{
			foreach($full_tree_array as $link)
			{
				$result .= '
					<li>—</li>
					<li typeof="v:Breadcrumb">
						<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree_c.$link->chpu.'/" rel="v:url" property="v:title">'.$link->name.'</a>
					</li>
				';
				$full_tree_c .= $link->chpu.'/';
			}
		}
		if($is_good)
		{
			$result .= '
				<li>—</li>
				<li typeof="v:Breadcrumb">
					<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree_c.$category->chpu.'/" rel="v:url" property="v:title">'.$category->name.'</a>
				</li>
			';
		}
		$result .= '
			<li>—</li>
			<li typeof="v:Breadcrumb">
				'.$name.'
			</li>
		';
		$result = '
			<ul class="breadcrumbs text" xmlns:v="http://rdf.data-vocabulary.org/#">
				'.$result.'
			</ul>
		';
		return $result;
	}
	
	public function cart_page($cart_tpl, $full_tree)
	/** создаём страницу корзины
	* @param obj $categories - объект выгрузки по api всех категорий с сайта донора
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param string $cart_tpl - шаблон каталога
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	*/
	{
		$settings = Set::getSettings();
		$one_wish_good = Custom::static_class('Good')::good_cart_in_list(self::get_tpl('good_in_list_tpl'), Custom::static_class('Good')::first_availability(), $full_tree, true);
		
		$head = self::head(
			'Скоро они будут моими!', // title
			'Список моих покупок', // description
			'', // keywords
			'' // og_image
		);
		$header = self::header();
		$footer = self::footer();
		$show_js = self::show_js();
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');

		// у нас есть список растений в данной категории. Теперь вставим его в шаблон
		$result_cart_page = str_replace(
			array(
				'%shead',
				'%header',
				'%catalog_path_name',
				'%h1', 
				'%this_page_url', 
				'%one_wish_good', 
				'%footer', 
				'%modal_confirmation_tpl', 
				'%show_js', 
				'%version', 
				'%root', 
			),
			array(
				$head, // head
				$header, // header
				ROOT.'/'.$settings->get_param('catalog_path_name'), // catalog_path_name shop
				'Корзина', // h1
				$dir.'/', // this_page_url
				$one_wish_good, // шаблон товара для создания карточки товара при помощи js, например, для wish-листа
				$footer, // footer
				self::get_tpl('modal_confirmation_tpl'), // 
				$show_js, // show_js
				self::get_version(), // version
				ROOT,
			),
			$cart_tpl
		);
		// создаём каталог
		if(! mkdir($dir.'/cart/', 0777, true))
		{
			die('Невозможно создать категорию '.$dir.'/cart/');
		}
		self::create_htaccess($dir.'/cart/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		// и записываем её в каталог shop/cart
		if(file_put_contents($dir.'/cart/index.php',$result_cart_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге shop');
		}
		return $result_cart_page;
	}
	
	public function last_page($last_page_tpl)
	/** создаём страницу последнего заказа
	* @param obj $categories - объект выгрузки по api всех категорий с сайта донора
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param string $cart_tpl - шаблон каталога
	*/
	{
		$settings = Set::getSettings();

		
		$head = self::head(
			'Заказ оформлен!', // title
			'Заказ оформлен', // description
			'', // keywords
			'' // og_image
		);
		$header = self::header();
		$footer = self::footer();
		$show_js = self::show_js();
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		$aside = self::aside();
		
		// у нас есть список растений в данной категории. Теперь вставим его в шаблон
		$result_last_order_page = str_replace(
			array(
				'%shead',
				'%header',
				'%h1', 
				'%footer', 
				'%show_js', 
				'%aside', 
				'%catalog_path_name',
				'%version',
				'%root',
			),
			array(
				$head, // head
				$header, // header
				'Заказ оформлен успешно.', // h1
				$footer, // footer
				$show_js, // show_js
				$aside, // aside
				ROOT.'/'.$settings->get_param('catalog_path_name'), // catalog_path_name shop
				self::get_version(), // version
				ROOT,
			),
			$last_page_tpl
		);
		
		// создаём каталог
		if(! mkdir($dir.'/last_order/', 0777, true))
		{
			die('Невозможно создать категорию '.$dir.'/last_order/');
		}
		self::create_htaccess($dir.'/last_order/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		// и записываем её в каталог shop/last_order
		if(file_put_contents($dir.'/last_order/index.php',$result_last_order_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге shop');
		}
		return $result_last_order_page;
	}
	
	public function wish_page($wish_tpl, $full_tree)
	/** создаём страницу Хотелок
	* @param obj $categories - объект выгрузки по api всех категорий с сайта донора
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param string $wish_tpl - шаблон
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	*/
	{
		$settings = Set::getSettings();
		// $good_in_list_tpl = self::get_tpl('good_in_list_tpl');
		
		$aside = self::aside();
		// для шаблона карточки товара нужен товар в наличии. Ищем его:
		$availability_plant = Custom::static_class('Good')::first_availability();
		$one_wish_good = Custom::static_class('Good')::good_cart_in_list(self::get_tpl('good_in_list_tpl'), $availability_plant, $full_tree, true);
		$leftsidebar = Custom::static_class('Menu')::leftsidebar();
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		
		$head = self::head(
			'Список моих хотелок', // title
			'Список растений, которые я хочу приобрести', // description
			'', // keywords
			'' // og_image
		);
		$header = self::header();
		$footer = self::footer();
		$show_js = self::show_js();
		// $mobile_filter_shop = self::filter_shop('mobile', $plants);
		$mobile_filter_shop = '';
		
		$result_wish_page = str_replace(
			array(
				'%shead',
				'%header',
				'%catalog_path_name',
				'%h1', 
				'%this_page_url', 
				'%one_wish_good', 
				'%footer', 
				'%modal_confirmation_tpl', 
				'%show_js', 
				'%leftsidebar', 
				'%aside', 
				'%mobile_filter_shop', 
				'%version', 
				'%root', 
			),
			array(
				$head, // head
				$header, // header
				ROOT.'/'.$settings->get_param('catalog_path_name'), // catalog_path_name shop
				'Мои хотелки', // h1
				$dir.'/', // this_page_url
				$one_wish_good, // шаблон товара для создания карточки товара при помощи js, например, для wish-листа
				$footer, // footer
				self::get_tpl('modal_confirmation_tpl'), // 
				$show_js, // show_js
				$leftsidebar, 
				$aside, 
				$mobile_filter_shop, 
				self::get_version(), // version
				ROOT,
			),
			$wish_tpl
		);
		// создаём каталог
		if(! mkdir($dir.'/wishlist/', 0777, true))
		{
			die('Невозможно создать каталог '.$dir.'/wishlist/');
		}
		self::create_htaccess($dir.'/wishlist/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		// и записываем её в каталог shop/wishlist
		if(file_put_contents($dir.'/wishlist/index.php',$result_wish_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге shop');
		}
		return $result_wish_page;
	}
	
	public function filter_shop_char($type, $params_fiter_array)
	/** Одна характеристика - мобильная или десктопная
	* @param string $type - mobile или desktop
	* @param array $params_fiter_array - массив с характеристиками, по которым будет фильтровать фильтр
	*/
	{
		$settings = Set::getSettings();
		$dop_classes = '';
		$prefix = '';
		if($type == 'mobile')
		{
			$dop_classes = ' col-12 col-sm-6';
			$prefix = 'm_';
		}
		$i = 1;
		foreach($params_fiter_array as $name => $param)
		{
			$values_list = '';
			foreach($param as $values)
			{
				$values_list .= '
					<div class="control control_checkbox">
						<input class="dynamic-param-input" value="'.$values.'" id="'.$prefix.'shop_filter_input_'.$settings->my_translit($values).'" type="checkbox">
						<label class="label" for="'.$prefix.'shop_filter_input_'.$settings->my_translit($values).'">'.$values.'</label>
					</div>
				';
				$i++;
			}
			$result .= '
				<div class="shop-filter__control-group control-group'.$dop_classes.'">
					<label class="control-group__label">'.$name.'</label>
					<div class="roll">
						'.$values_list.'
					</div>
				</div>
			';
		}
		return $result;
	}
	
	public function filter_shop($type)
	/** Создаём фильтр - мобильный или десктопный
	* @param string $type - mobile или desktop
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	*/
	{
        $settings = Set::getSettings();
        $plants = $settings->save_object('plants');
		$params_fiter_array = []; // массив с характеристиками, по которым будет фильтровать фильтр
		$max_price = 10000;
		// проходим все растения и извлекаем из них характеристики и их значения.
		foreach($plants as $plant)
		{
			if(isset($plant->param))
			{
				foreach($plant->param as $param)
				{
					if(!isset($params_fiter_array[$param->name]))
					// если такой характеристики в массиве ещё нет - создаём её
					{
						$params_fiter_array[$param->name] = []; // создаём массив значений характеристики
					}
					if(!in_array($param->value, $params_fiter_array[$param->name]))
					// если такого значения данной характеристики в массиве ещё нет - создаём его
					{
						$params_fiter_array[$param->name][] = $param->value.($param->unit != '' ? ', '.$param->unit : '');
					}
				}
			}
		}
		// а теперь формируем список характеристик

		if($type == 'mobile')
		{
			return '
				<div class="shop-block-filter shop-block-filter_mobile d-lg-none">
					<div class="shop-block-filter__box">
						<span class="shop-filter form">
							<div class="shop-filter__accordion accordion">
								<div class="accordion__item">
									<div class="accordion__headline">
										<i class="accordion__icon fas fa-sliders-h"></i>Подбор<i class="accordion__drop fas fa-plus-circle"></i>
									</div>
									<div class="accordion__body">
										<div class="row">
											<div class="control col-12 col-sm-6">
												<label class="label">Артикул</label>
												<input class="input" name="a" value="" type="text">
											</div>
											<div class="control col-12 col-sm-6">
												<label class="label">Цена</label>
												<div class="shop-filter__range">
													<input class="input" name="pr1" value="0" type="text"><span>—</span><input class="input" name="pr2" value="'.$max_price.'" type="text">
												</div>
											</div>
											<div class="shop-filter__control-group control-group col-12 col-sm-6">
												<div class="control control_checkbox">
													<input name="ac" value="1" id="m_shop_filter_input_action" type="checkbox">
													<label class="label" for="m_shop_filter_input_action">Товар по акции</label>
												</div>
												<div class="control control_checkbox">
													<input name="ne" value="1" id="m_shop_filter_input_new" type="checkbox">
													<label class="label" for="m_shop_filter_input_new">Новинка</label>
												</div>
												<div class="control control_checkbox">
													<input name="hi" value="1" id="m_shop_filter_input_hit" type="checkbox">
													<label class="label" for="m_shop_filter_input_hit">Хит</label>
												</div>
											</div>
											'.self::filter_shop_char($type, $params_fiter_array).'
										</div>
										<div class="col-12" onclick="goods_filter(this)">
											<button class="button" type="button">Подобрать</button>
										</div>
									</div>
								</div>
							</div>
						</span>
					</div>
				</div>
			';
		}
		else
		// десктопная версия
		{
			return '
				<div class="shop-block-filter page-block page-block_aside col-12 d-none d-lg-block">
					<div class="shop-block-filter__box">
						<span class="shop-filter form">
							<div class="control">
								<label class="label">Артикул</label>
								<input class="input" name="a" value="" type="text">
							</div>
							<div class="control">
								<label class="label">Цена</label>
								<div class="shop-filter__range">
									<input class="input" name="pr1" value="0" type="text"><span>&mdash;</span><input class="input" name="pr2" value="'.$max_price.'" type="text">
								</div>
							</div>
							<div class="shop-filter__control-group control-group">
								<div class="control control_checkbox">
									<input name="ac" value="1" id="shop_filter_input_action" type="checkbox">
									<label class="label" for="shop_filter_input_action">Товар по акции</label>
								</div>
								<div class="control control_checkbox">
									<input name="ne" value="1" id="shop_filter_input_new" type="checkbox">
									<label class="label" for="shop_filter_input_new">Новинка</label>
								</div>
								<div class="control control_checkbox">
									<input name="hi" value="1" id="shop_filter_input_hit" type="checkbox">
									<label class="label" for="shop_filter_input_hit">Хит</label>
								</div>
							</div>							
							'.self::filter_shop_char($type, $params_fiter_array).'
							<button class="button" type="button" onclick="goods_filter(this)">Подобрать</button>
						</span>
					</div>
				</div>
			';
		}
	}
	
	public function head($title='',$description='',$keywords='',$og_image='')
	/** Создаём шаблон - содержимое тега <head>...</head>
	* @param string $title - title данной страницы
	* @param string $description - description данной страницы
	* @param string $keywords - keywords данной страницы
	* @param string $og_image - путь до какой-то главной картинки для данной страницы
	*/
	{
		$settings = Set::getSettings();
		$head_tpl = self::get_tpl('head_tpl');
		$result = str_replace(
			array(
				'%title',
				'%description', 
				'%keywords', 
				'%og_image', 
				'%version', 
				'%root',
			),
			array(
				$title, // title
				$description, // description
				$keywords, // keywords
				$og_image, // keywords
				self::get_version(), // version
				ROOT,
			),
			$head_tpl
		);
		return $result;
	}
	
	public function header($is_main = false)
	/** Создаём шаблон - шапка всех страниц
	* @param string $categories - объект всех категорий
	* @param boolean $is_main - = true, если это главная страница и false - если это все остальные страницы
	*/
	{
		$settings = Set::getSettings();
		static $result = '';
		// на главной странице шапка отличается
		if($is_main)
		{
			$header_tpl = self::get_tpl('header_tpl');
			$menu_mobile = Custom::static_class('Menu')::mobile();
			$menu_full_screen = Custom::static_class('Menu')::full_screen();
            //define("ROOT", '/product');

            $result_main = str_replace(
				array(
					'%menu_mobile',
					'%menu_full_screen',
					'%static_pages',
					'%catalog_path_name',
					'%avatar_href',
				),
				array(
					$menu_mobile, // каталог для мобильного меню, очень похож на leftsidebar
					$menu_full_screen, // каталог для полноэкранного меню
                    Custom::static_class('Menu')::static_pages_menu(), // меню статических страниц
					ROOT.(ROOT != '' ? '/' : '').$settings->get_param('catalog_path_name'), // catalog_path_name shop
					'', // avatar_href На главной не надо делать ссылку саму на себя
				),
				$header_tpl
			);
			return $result_main;
		}
		if($result == '')
		{
			$header_tpl = self::get_tpl('header_tpl');
			$menu_mobile = Custom::static_class('Menu')::mobile();
			$menu_full_screen = Custom::static_class('Menu')::full_screen();
			$result = str_replace(
				array(
					'%menu_mobile',
					'%menu_full_screen',
					'%static_pages',
					'%catalog_path_name',
					'%avatar_href',
				),
				array(
					$menu_mobile, // каталог для мобильного меню, очень похож на leftsidebar
					$menu_full_screen, // каталог для полноэкранного меню
                    Custom::static_class('Menu')::static_pages_menu(), // меню статических страниц
					ROOT.'/'.$settings->get_param('catalog_path_name'), // catalog_path_name shop
					' href="'.ROOT.'/"', // avatar_href
				),
				$header_tpl
			);
		}
		return $result;
	}
	
	public function footer()
	/** Создаём шаблон - подвал всех страниц
	* @param string $title - title данной страницы
	* @param string $description - description данной страницы
	* @param string $keywords - keywords данной страницы
	* @param string $og_image - путь до какой-то главной картинки для данной страницы
	*/
	{
		$settings = Set::getSettings();
		$footer_tpl = self::get_tpl('footer_tpl');
		$catalog_point = '
			<li class="navigation__item navigation__item_main">
				<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/">Каталог</a>
			</li>
		';
		$categories = $settings->save_object('categories');
		$result = str_replace(
			array(
				'%static_pages',
				'%leftsidebar',
				'%year',
			),
			array(
				$catalog_point.Custom::static_class('Menu')::static_pages_menu(5, 'low'), // нижнее меню статических страниц,
				Custom::static_class('Menu')::leftsidebar(), // левое меню в футере
                date('Y'),
			),
			$footer_tpl
		);
		return $result;
	}
	
	public function show_js($addition = '')
	/** Создаём шаблон - js-файлы, которые загружаются на каждой странице
	* @param string $addition - дополнительный js код в виде <script>...</script>
	*/
	{
		$settings = Set::getSettings();
		$show_js_tpl = self::get_tpl('show_js_tpl');
        $metrica = '';
        if(ROOT == '')
        {
            $metrica = '
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
               (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
               m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
               (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            
               ym(88799533, "init", {
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
               });
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/88799533" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->';
        }
		$result = str_replace(
			array(
				'%version',
				'%root',
				'%metrica',
			),
			array(
				self::get_version(), // version
				ROOT, // изменение путей на продакте и на локалке
                $metrica,
			),
			$show_js_tpl
		);
		$result = $result.$addition;
		return $result;
	}
	
	public function full_tree_array($category)
	/** Отдаёт массив с объектами категорий по порядку от shop (не включая shop) до категории $category (не включая $category).
	* @param obj $category - объект выгрузки текущей категории, для которой нужно получить массив с названиями высших категорий 
	* @param obj $categories - объект выгрузки по api всех категорий с сайта донора
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');
		$result = array();
		$current = $category;
		$i = 0;
		while($current->level != 0 && $i < 10)
		{
			foreach($categories as $cat)
			{
				// получаем название предыдущей категории
				if($current->parent_id == $cat->id)
				{
					array_unshift($result, $cat);
					$current = $cat;
					break;
				}
			}
			$i++;
		}
		return $result;
	}
	
	public function main_page($main_page_tpl, $full_tree)
	/** создаём главную страницу
	* @param obj $categories - объект выгрузки по api всех категорий с сайта донора
	* @param obj $plants - объект выгрузки по api всех растений с сайта донора
	* @param string $main_page_tpl - шаблон страницы
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	*/
	{
		$settings = Set::getSettings();
        $categories = $settings->save_object('categories');
		
		$aside = self::aside();
		$head = self::head(
			'Розы, клематисы, ирисы, видовые растения - купить', // title
			'Продажа растений почтой - 300 сортов роз, клематисы, сибирские ирисы, видовые растения, также бываем на выставках', // description
			'', // keywords
			'' // og_image
		);
		$header = self::header( true);
		$footer = self::footer();
		$show_js = self::show_js();
		
		// список категорий
		$cat_in_list_tpl = self::get_tpl('cat_in_list_tpl');

		// создаём список категорий
		$plantsListInner = '';// html списка всех категорий
		foreach($categories as $category)
		{
			if($category->level == 0)
			// тут отсеиваем категории низших уровней - их место в подкатегориях
			{
				$plantsListInner .= Custom::static_class('Category')::cats_in_list($cat_in_list_tpl, $category);
			}
		}
		
		// хиты, новинки, акции
		$hit = Custom::static_class('Good')::list_goods_inner(0, $full_tree, 'hit', 30);
		$new = Custom::static_class('Good')::list_goods_inner(0, $full_tree, 'new', 30);
		$action = Custom::static_class('Good')::list_goods_inner(0, $full_tree, 'action', 30);

		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT;
		
		$result_main_page = str_replace(
			array(
				'%shead',
				'%header',
				'%h1', 
				'%this_page_url', 
				'%aside', 
				'%catalog', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js', 
				'%hit', 
				'%new', 
				'%action', 
				'%version', 
				'%root', 
			),
			array(
				$head,
				$header,
				'Розы, клематисы, ирисы, видовые растения - питомник Бросница', // h1
				$dir.'/', // this_page_url
				$aside, 
				$plantsListInner, // catalog
				$footer, 
				self::get_tpl('modal_in_cart_tpl'), 
				$show_js, 
				$hit, 
				$new, 
				$action, 
				self::get_version(), // version
				ROOT,
			),
			$main_page_tpl
		);
		// записываем шаблон в корневой каталог
		if(file_put_contents($dir.'/index.php',$result_main_page) === false)
		{
			die('Не удалось создать шаблон главной страницы');
		}
		self::create_htaccess($dir.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		// также создаём ссылку на эту страницу в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/', 
			'date' => time(),
		]);
		return $result_main_page;
	}
	
	
	public function add_goods_pages()
	/** Сформировать статические страницы сайта на основе выгрузки товаров и категорий по api
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');
        $plants = $settings->save_object('plants');
        $articles = $settings->save_object('articles');
        $news = $settings->save_object('news');
		/* $categories = array(
			[0]=>
			  object(stdClass)#3 (7) {
				["id"]=>
				string(1) "1"
				["name"]=>
				string(24) "Злаки и осоки"
				["title"]=>
				string(0) ""
				["description"]=>
				string(0) ""
				["keywords"]=>
				string(0) ""
				["level"]=>
				string(1) "0"
				["parent_id"]=>
				string(1) "0"
			  }
			  [1]=>
			  object(stdClass)#4 (7) {
				["id"]=>
				string(1) "2"
				["name"]=>
				string(30) "Многолетники А-З"
				["title"]=>
				string(0) ""
				["description"]=>
				string(0) ""
				["keywords"]=>
				string(0) ""
				["level"]=>
				string(1) "0"
				["parent_id"]=>
				string(1) "0"
			  }
			)
			{"id":"17","name":"\u041f\u043b\u043e\u0434\u043e\u0432\u044b\u0435","title":"","description":"","keywords":"","level":"0","parent_id":"0","chpu":"plodovye","images":{"id":"43045","thumbnail":"https:\/\/156909.selcdn.ru\/thlaspi\/ee\/63\/ee63a3c0487c671f3577153da67c2252.jpeg","full":"https:\/\/156909.selcdn.ru\/thlaspi\/0f\/2d\/0f2d5e50c21f1e19fb17fe811b926187.jpeg"}}
		*/

		// проверяем, есть ли каталог с категориями
		// $settings->get_param('catalog_path_name') - название каталога, например, 'shop'
		// $_SERVER['DOCUMENT_ROOT'] - путь к корню сайта без слеша, например, "D:/OpenServer/domains/hortulus"
		// проходим массив с категориями и создаём для них каталоги в корне сайта или в папке с каталогом
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		if(!is_dir($dir))
		// если её нет - создаём её
		{
			if(! mkdir($dir, 0777, true))
			{
				die('Невозможно создать категорию '.$dir);
			}
			self::create_htaccess($dir.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		}
		else
		// если она есть - стираем её и её содержимое и снова создаём
		{
			if(file_exists($dir.'/.htaccess'))
			{
				unlink($dir.'/.htaccess');
			}
			$settings::removeDirectory($dir);
			if(! mkdir($dir, 0777, true))
			{
				die('Невозможно создать категорию '.$dir);
			}
			self::create_htaccess($dir.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		}

		// проходим все картинки всех растений и адаптируем их под нужды нашего сайта. Данные записываем в бд,  картинки - на диск
		$images = Custom::new_class('Images');
		$images->prepare($plants,1,false);// обрабатываем и синхронизируем картинки растений
		$images->prepare($categories,2,false);// обрабатываем и синхронизируем картинки категорий
		$images->prepare($news,3,false);// обрабатываем и синхронизируем картинки новостей
		$images->prepare($articles,4,false);// обрабатываем и синхронизируем картинки статей
		
		
		// получаем из файла шаблон страниц растения
		$good_tpl = self::get_tpl('good_tpl');
		
		// получаем из файла шаблон страниц категории
		$category_tpl = self::get_tpl('category_tpl');

		// теперь внутри категории с каталогами создаём каталоги категорий, а в них - принадлежащие им страницы товаров

		$search = '';
		foreach($categories as $category)
		{
			// создаём категорию
			$full_tree = ''; // дерево категорий вида rozy/foribundy/kornesobstvennye/
			$full_tree_array = array();// 
			if($category->level != 0)
			// значит это категория НЕ высшего порядка и мы должны найти для неё всё дерево, ведущее своими корнями к корню дерева категорий - каталогу shop
			{
				$full_tree_array = self::full_tree_array($category, $categories);
				// $full_tree_array - массив с объектами категорий по порядку от shop (не включая shop) до категории $category (не включая $category). 
				foreach($full_tree_array as $one_parent_cat)
				// теперь будем поочерёдно создавать всё дерево категорий, проверяя, не созданы ли какие-либо из них ранее
				{
					if(! is_dir($dir.'/'.$full_tree.$one_parent_cat->chpu))
					// если такой директории нет - создаём её
					{
						if(! mkdir($dir.'/'.$full_tree.$one_parent_cat->chpu, 0777, true))
						{
							die('Невозможно создать категорию '.$dir.'/'.$full_tree.$one_parent_cat->chpu);
						}
						self::create_htaccess($dir.'/'.$full_tree.$one_parent_cat->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
					}
					// и дополняем $full_tree для дальнейшего использования
					$full_tree = $full_tree.$one_parent_cat->chpu.'/';
				}
			}
			// ага, после этого родительские категории уже созданы, значит мы должны проверять нет ли родительских категорий и если есть - не создавть их ещё раз
			if(! is_dir($dir.'/'.$full_tree.$category->chpu))
			// если такой директории нет - создаём её
			{
				if(! mkdir($dir.'/'.$full_tree.$category->chpu, 0777, true))
				{
					die('Невозможно создать категорию '.$dir.'/'.$full_tree.$category->chpu);
				}
				self::create_htaccess($dir.'/'.$full_tree.$category->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
			}
			
			// создаём страницу со списком товаров для данной категории с пагинацией
            Custom::static_class('Category')::category_page($category_tpl, $category, $full_tree, $full_tree_array);
			// создаём товары для данной категории
			foreach($plants as $plant)
			// проходим все растения из выгрузки и если они есть в данной категории - создаём для них страницу
			{
				if(isset($plant->categories) && $plant->categories[0] == $category->id)
				{
					// создаём одну страницу товара
					$result_good = Custom::static_class('Good')::big_page($good_tpl, $category, $plant, $full_tree, $full_tree_array);
					// и записываем её в каталог категории
					if(file_put_contents($dir.'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit(Custom::static_class('Good')::true_name($plant)).$settings->get_param('chpu_postfix'),$result_good) === false)
					{
						die('Не удалось создать шаблон '.$dir.'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit(Custom::static_class('Good')::true_name($plant)).$settings->get_param('chpu_postfix'));
					}
		
					// создаём массив поиска на основе данных о растениях
					$search .= self::search_array($plant, $category, $full_tree);
					
				}
			}
		}
		
		// создаём страницу каталога
		// получаем из файла шаблон страницы каталога
		$catalog_tpl = self::get_tpl('catalog_tpl');
        Custom::static_class('Category')::catalog_page($catalog_tpl);
		
		// создаём страницу корзины
		// получаем из файла шаблон страницы корзины
		$cart_tpl = self::get_tpl('cart_tpl');
		self::cart_page($cart_tpl, $full_tree);
		
		// создаём страницу последний заказ
		// получаем из файла шаблон страницы последнего заказа
		$last_page_tpl = self::get_tpl('last_order_tpl');
		self::last_page($last_page_tpl);
		
		// создаём страницу Хотелки
		// получаем из файла шаблон страницы Хотелок
		$wish_tpl = self::get_tpl('wish_tpl');
		self::wish_page($wish_tpl, $full_tree);
		
		// создаём главную страницу
		// получаем из файла шаблон главной страницы
		$main_page_tpl = self::get_tpl('main_tpl');
		self::main_page($main_page_tpl, $full_tree);
		
		// создаём статические страницы
		self::static_pages();
		
		// записываем массив поиска в файл js, который будем присоединять к каждому шаблону
		if(file_put_contents($_SERVER['DOCUMENT_ROOT'].ROOT.'/js/search.js', 'var search = ['.$search.']') === false)
		{
			die('Не удалось создать шаблон '.$_SERVER['DOCUMENT_ROOT'].'/js/search.js');
		}
        // меняем путь к файлу ajax в файле js/cart.js
        $cart_js_tpl = self::get_tpl('cart_js_tpl');
        $result_cart_js_page = str_replace(
            array(
                '%root_path',
            ),
            array(
                ROOT,
            ),
            $cart_js_tpl
        );
        // и записываем её в каталог js
        if(file_put_contents($_SERVER['DOCUMENT_ROOT'].ROOT.'/js/cart.js', $result_cart_js_page) === false)
        {
            die('Не удалось создать шаблон '.$_SERVER['DOCUMENT_ROOT'].ROOT.'/js/cart.js');
        }

        // создаём eml-feed товаров для яндекса
        self::yml_feed();


        return true;
	}

    /** создаём eml-feed товаров для яндекса
     * Категории создаются тут, а офферы (товары) в методе Good::big_page (в ней формируются данные), из которого вызывается функция plus_offer_link из класса Settings, в которой формируются сами офферы в формате yml и хранятся в static.
     */
    public static function yml_feed()
    {
        $settings = Set::getSettings();
        $yml_cats = '';
        $categories = $settings->save_object('categories');

        foreach($categories as $category)
        {
            /**
             $category = object(stdClass)#3 (7) {
                ["id"]=>
                string(1) "1"
                ["name"]=>
                string(24) "Злаки и осоки"
                ["title"]=>
                string(0) ""
                ["description"]=>
                string(0) ""
                ["keywords"]=>
                string(0) ""
                ["level"]=>
                string(1) "0"
                ["parent_id"]=>
                string(1) "0"
            }
             */
            $yml_cats .= '<category id="'.$category->id.'"'.($category->parent_id > 0 ? ' parentId="'.$category->parent_id.'"' : '').'>'.$category->name.'</category>';
        }
        $yml_goods = Custom::static_class('Settings')::plus_offer_link(null, true);

        file_put_contents(
            ABSPATH.'/feed.yml',
            '<?xml version="1.0" encoding="UTF-8"?>
                <yml_catalog date="'.date("Y-m-d H:i").'">
                    <shop>
                        <name>Бросница.ру</name>
                        <url>https://brosnica.ru</url>
                        <version>1.0</version>
                        <email>eblinkoff@yandex.ru</email>
                        <currencies>
                            <currency id="RUB" rate="1"></currency>
                        </currencies>
                        <categories>
                            '.$yml_cats.'
                        </categories>
                        <offers>
                            '.$yml_goods.'
                        </offers>
                    </shop>
                </yml_catalog>'
        );
    }

	public function update_goods_pages()
	/** Обновить страницы растений (товары) сайта на основе выгрузки товаров и категорий по api
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');
        $plants = $settings->save_object('plants');

		// проверяем, есть ли каталог с категориями
		// $settings->get_param('catalog_path_name') - название каталога, например, 'shop'
		// $_SERVER['DOCUMENT_ROOT'] - путь к корню сайта без слеша, например, "D:/OpenServer/domains/hortulus"
		// проходим массив с категориями и создаём для них каталоги в корне сайта или в папке с каталогом
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		if(!is_dir($dir))
		// если её нет - выходим
		{
			die('Нет каталога категорий');
		}
		
		// получаем из файла шаблон страниц растения
		$good_tpl = self::get_tpl('good_tpl');
		
		// получаем из файла шаблон страниц категории
		$category_tpl = self::get_tpl('category_tpl');
		
		$search = '';
		foreach($categories as $category)
		{
			// создаём категорию
			$full_tree = ''; // дерево категорий вида rozy/foribundy/kornesobstvennye/
			$full_tree_array = array();// 
			if($category->level != 0)
			// значит это категория НЕ высшего порядка и мы должны найти для неё всё дерево, ведущее своими корнями к корню дерева категорий - каталогу shop
			{
				$full_tree_array = self::full_tree_array($category);
				// $full_tree_array - массив с объектами категорий по порядку от shop (не включая shop) до категории $category (не включая $category). 
				foreach($full_tree_array as $one_parent_cat)
				// теперь будем поочерёдно создавать всё дерево категорий, проверяя, не созданы ли какие-либо из них ранее
				{
					// if(! is_dir($dir.'/'.$full_tree.$one_parent_cat->chpu))
					// // если такой директории нет - создаём её
					// {
						// if(! mkdir($dir.'/'.$full_tree.$one_parent_cat->chpu, 0777, true))
						// {
							// die('Невозможно создать категорию '.$dir.'/'.$full_tree.$one_parent_cat->chpu);
						// }
						// self::create_htaccess($dir.'/'.$full_tree.$one_parent_cat->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
					// }
					// и дополняем $full_tree для дальнейшего использования
					$full_tree = $full_tree.$one_parent_cat->chpu.'/';
				}
			}
			
			// создаём страницу со списком товаров для данной категории с пагинацией
            Custom::static_class('Category')::category_page($category_tpl, $category, $full_tree, $full_tree_array);
			// создаём товары для данной категории
			foreach($plants as $plant)
			// проходим все растения из выгрузки и если они есть в данной категории - создаём для них страницу
			{
				if(isset($plant->categories) && $plant->categories[0] == $category->id)
				{
					// создаём одну страницу товара
					$result_good = Custom::static_class('Good')::big_page($good_tpl, $category, $plant, $full_tree, $full_tree_array);
					// и записываем её в каталог категории
					file_put_contents($dir.'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit(Custom::static_class('Good')::true_name($plant)).$settings->get_param('chpu_postfix'),$result_good);
		
					// создаём массив поиска на основе данных о растениях
					$search .= self::search_array($plant, $category, $full_tree);
				}
			}
		}
		
		// записываем массив поиска в файл js, который будем присоединять к каждому шаблону
		if(file_put_contents($_SERVER['DOCUMENT_ROOT'].ROOT.'/js/search.js', 'var search = ['.$search.']') === false)
		{
			die('Не удалось создать шаблон '.$_SERVER['DOCUMENT_ROOT'].'/js/search.js');
		}
		$this->add_version();// увеличиваем версию на единицу

		return true;
	}
	
	
	public function static_pages()
	/** создание статических страниц
	*/
	{
		$aside = self::aside();
		$header = self::header();
		$footer = self::footer();
		$settings = Set::getSettings();
		$db = DB::getMySQLi();
		if ($static_page = $db->query('
			SELECT * 
			FROM `static_pages` 
			WHERE act = "1" AND `trash` = "0"
			ORDER BY sort
		'))
		{
			while($row = $static_page->fetch_object())
			{
				if(! $row->theme)
				// если идентификатор шаблона не указан в бд, берём шаблон по умолчанию
				{
					$static_page_tpl = self::get_tpl('static_page_tpl');
				}
				else
				{
					$static_page_tpl = self::get_tpl($row->theme);
				}
				$head = self::head(
					$row->title_meta, // title
					$row->descr, // description
					$row->keywords, // keywords
					'' // og_image
				);
				$show_js = self::show_js(($row->js ? '<script>'.$row->js.'</script>' : ''));

				$dir = $_SERVER['DOCUMENT_ROOT'].ROOT;
				
				$result_static_page = str_replace(
					array(
						'%shead',
						'%header',
						'%h1', 
						'%text', 
						'%aside', 
						'%footer', 
						'%show_js', 
						'%version', 
						'%root', 
					),
					array(
						$head,
						$header,
						$row->name, // h1
						$row->text, // контент страницы
						$aside, 
						$footer, 
						$show_js, 
						self::get_version(), // version
						ROOT,
					),
					$static_page_tpl
				);
				if(!is_dir($dir.'/'.$row->chpu.'/'))
				// если её нет - создаём её
				{
					// создаём каталог
					if(! mkdir($dir.'/'.$row->chpu.'/', 0777, true))
					{
						die('Невозможно создать каталог '.$dir.'/'.$row->chpu.'/');
					}
				}
				// и записываем статическую страницу в созданный ранее каталог
				if(file_put_contents($dir.'/'.$row->chpu.'/index.php',$result_static_page) === false)
				{
					die('Не удалось создать статическую страницу '.$dir.'/'.$row->chpu.'/');
				}
				self::create_htaccess($dir.'/'.$row->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
			}
		}
	}
	
	function create_htaccess($dir)
	/** создаём файл .htaccess в категории $dir
	* @param string $dir - путь к директории, в которой надо создать файл .htaccess вида .../shop/angliyskie/
	*/
	{
		$settings = Set::getSettings();
        if(ROOT == '')
        // если продакт
        {
            if(MAKE_HTACCESS && file_put_contents($dir.'.htaccess',self::get_tpl('htaccess_tpl')) === false)
            {
                var_dump('Не удалось создать страницу '.$dir.'/.htaccess');
            }
		}
        elseif(is_file($dir.'.htaccess'))
        {
            unlink($dir.'.htaccess');
        }
	}
	
	function search_array($plant, $category, $full_tree)
	/** создаём массив поиска на основе данных о растениях. Даже два массива - один с данными для поиска, другой - с данными товаров по номеру. Всё это создаётся в js синтаксисе для вставки в шаблоны
	*/
	{
		$settings = Set::getSettings();
		$param = self::params_to_js_object($plant);
		if($param != '')
		{
			$param = '"param": '.$param.',';
		}
		$result_g = '';// элемент массива с данными всех товара (в виде текста js)
		// берём имя картинки из бд по номеру thlaspi_id
        // echo __METHOD__.' или '.__FUNCTION__.'; Строка '.__LINE__.' в файле'.__FILE__.'; thlaspi_id = '.$plant->images[0]->id;
		if($img_name = Custom::static_class('Images')::name_by_id($plant->images[0]->id))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		$search = Custom::static_class('Good')::true_name($plant).' '.$plant->comment.' '.$category->name;
		// $chars = ["\r","\n",'<br>', chr(10)]; // символы для удаления
		// $search = str_replace($chars, '', $search);// удаляем символы перевода каретки
		$search = preg_replace("/[^а-яёa-z _-]/iu", '', $search);// удаляем все лишние символы
		$comment = preg_replace("/[^а-яёa-z _-]/iu", '', $plant->comment);// удаляем все лишние символы из описания
		// ещё убираем одно-, дву-, и трёх-буквенные слова
		$_search = explode(' ',$search);
		foreach($_search as $key => $gap)
		{
			if(mb_strlen($gap, 'utf-8') <= 3)
			{
				// удаляем эту строку
				unset($_search[$key]);
			}
		}
		$search = implode(' ',$_search);
		// переводим в нижний регистр
		$search = mb_strtolower($search,'utf-8');
		
		return '{
		"thlaspi_id": "'.$plant->id.'",
		"name": "'.	Custom::static_class('Good')::true_name($plant).'",
		"comment": "'.str_replace(PHP_EOL, '\ ', $comment)./* заодно убираем символы перевода стоки под js */'",
		"url": "'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu.'/'.Custom::static_class('Settings')::my_translit((Custom::static_class('Good')::true_name($plant))).$settings->get_param('chpu_postfix').'",
		"image_href": "'.$paths->icon.'",
		"search": "'.$search.' '.$plant->article.'",
		'.$param.'
		},
		';
	}
	/*
	$plant->id,
	$plant->rus_name.($plant->sort != '' ? " '".$plant->sort."'" : '').' ('.$plant->lat_name.') - купить за '.$plant->price.' руб. с доставкой почтой в интернет-магазине '.$settings->get_param('shop_name_global'), 
	$plant->comment, // description
	$plant->rus_name, // keywords
	$plant->rus_name.($plant->sort != '' ? " '".$plant->sort."'" : '').(! empty($plant->lat_name) ? ' ('.$plant->lat_name.')' : ''), // h1
	'/'.$settings->get_param('catalog_path_name').'/'.$category->chpu, // category_url
	$category->name, // category_name
	$plant->article, 
	$settings->get_param('css_file_path'), 
	$settings->get_param('catalog_path_name'), 
	$plant->count, 
	'/'.$settings->get_param('catalog_path_name').'/'.$category->chpu.'/'.$plant->chpu.$settings->get_param('chpu_postfix'), // this_page_url
	$img_full_0,// img_full_0
	$img_thumbnail_0, // img_thumbnail_0
	$img_icon_0, // img_icon_0
	self::lineyka_photos($plant->images),// lineyka_photos
	$plant->price, 
	$plant->comment, 
	
	*/
	/* $categories = array(
		[0]=>
		  object(stdClass)#3 (7) {
			["id"]=>
			string(1) "1"
			["name"]=>
			string(24) "Злаки и осоки"
	*/
	
	
	function get_version()
	/** Получаем из бд текущую версию нашей сборки
	*/
	{
		static $version = 0;
		if($version === 0)
		{
			$db = DB::getMySQLi();
			if ($query_result = $db->query('
				SELECT * 
				FROM `version_counter` 
				WHERE `id` = 1
			'))
			{
				$_version = $query_result->fetch_object();
				$version = $_version->value;
			}
			else
			{
				$version = 0;
			}
		}
		return $version;
	}
	
	
	function add_version()
	/** Увеличиваем на 1 текущую версию нашей сборки
	*/
	{
		$db = DB::getMySQLi();
		$old_version = $this->get_version();
		$old_version++;
		
		if ($query_result = $db->query('
			UPDATE `version_counter` 
			SET `value` = '.$old_version.' 
			WHERE id = 1
		'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}