<?php
class Menu
{
	
	public function childrens_ul($category)
	/** Формируем содержимое дочерних пунктов верхнего меню - список ссылок с категориями
	* @param obj $category - одна категория, та, которая является родительской
	* @return string $result - html содержимого дочернего левго сайдбара вида <ul><li>...</li><li>...
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		$result = '';
		foreach($categories as $child_category)
		{
			if($category->id == $child_category->parent_id)
			// значит child_category - это дочерняя категория 
			{
				$childrens = self::leftsidebar_childrens_ul($child_category);// <ul>...</ul>
				$result .= '
					<li class="navigation__item navigation__item_main'.($childrens ? ' navigation__item_parent' : '').'">
						<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$child_category->chpu.'">'.$child_category->name.($childrens ? '<i class="fas fa-plus-circle"></i>' : '').'</a>
						'.$childrens.'
					</li>
				';
			}
		}
		if($result == '')
		// если дочерних категорий нет
		{
			return '';
		}
		else
		{
			return '
				<ul class="navigation__list navigation__list_child">'.
					$result.
				'</ul>'
			;
		}
	}
	
	public function leftsidebar()
	/** Формируем содержимое блока левого меню - список ссылок с категориями
	* @return string $result - html содержимого левго сайдбара вида <ul><li>...</li><li>...
	*/
	{
		return '
		<ul class="navigation__list navigation__list_main">
			'.self::items().'
		</ul>
		';
	}
	
	public function mobile()
	/** Формируем содержимое блока левого меню - список ссылок с категориями
	* @return string $result - html содержимого левго сайдбара вида <ul><li>...</li><li>...
	*/
	{
		return '
		<ul class="navigation__list navigation__list_child">
			'.self::items().'
		</ul>
		';
	}
	
	
	public function full_screen()
	/** Формируем содержимое блока центрального полноэкранного меню - список ссылок с категориями
	* @return string $result - html содержимого левго сайдбара вида <li>...</li>...
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		$result = '';
		if($result == '')
		{
			foreach($categories as $category)
			{
				if($category->level == 0)
				{
					$childrens = self::leftsidebar_childrens_ul($category);// <ul>...</ul>
					$result .= '
						<nav class="navigation-columns__col navigation col">
							<ul class="navigation__list">
								<li class="navigation__item navigation__item_main navigation__item_parent">
									<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$category->chpu.'">'.$category->name.'</a>
									'.$childrens.'
								</li>
							</ul>
						</nav>
					';
				}
			}
		}
		return $result;
	}
	
	
	
	public function leftsidebar_childrens_ul($category)
	/** Формируем содержимое блока левого меню - список ссылок с категориями
	* @param obj $category - одна категория, та, которая является родительской
	* @return string $result - html содержимого дочернего левго сайдбара вида <ul><li>...</li><li>...
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		$result = '';
		foreach($categories as $child_category)
		{
			if($category->id == $child_category->parent_id)
			// значит child_category - это дочерняя категория 
			{
				$childrens = self::leftsidebar_childrens_ul($child_category);// <ul>...</ul>
				$current_tree_array = Custom::static_class('Main')::full_tree_array($child_category);
				$current_full_tree = '';
				if(count($current_tree_array))
				{
					foreach($current_tree_array as $branch)
					{
						$current_full_tree .= $branch->chpu.'/';
					}
				}
				$result .= '
					<li class="navigation__item'.($childrens ? ' navigation__item_parent' : ' navigation__item_child').'">
						<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$current_full_tree.$child_category->chpu.'">'.$child_category->name.($childrens ? '<i class="fas fa-plus-circle"></i>' : '').'</a>
						'.$childrens.'
					</li>
				';
			}
		}
		if($result == '')
		// если дочерних категорий нет
		{
			return '';
		}
		else
		{
			return '
				<ul class="navigation__list navigation__list_child">'.
					$result.
				'</ul>'
			;
		}
	}
	
	public function items()
	/** Формируем содержимое верхнего меню - список ссылок с категориями
	* @return string $result - html содержимого левго сайдбара вида <li>...</li>...
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		$result = '';
		if($result == '')
		{
			foreach($categories as $category)
			{
				if($category->level == 0)
				{
					$childrens = self::leftsidebar_childrens_ul($category);// <ul>...</ul>
					$result .= '
						<li class="navigation__item navigation__item_main'.($childrens ? ' navigation__item_parent' : '').'">
							<a href="'.ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$category->chpu.'">'.$category->name.($childrens ? '<i class="fas fa-plus-circle"></i>' : '').'</a>
							'.$childrens.'
						</li>
					';
				}
			}
		}
		return $result;
	}
	
	public function static_pages_menu($yet = 5, $type = 'top')
	/** меню статических страниц
	* @param int $yet - число, означающее, через сколько пунктов прятать все последующие пункты меню в "ещё"
	* @param string $type - тип меню. Если top - верхнее меню, если low - нижнее меню
	* Результат должен выглядеть как-то вот так:
	<li class="navigation__item navigation__item_main"><a href="/aktsiya/">Акции</a></li>
	<li class="navigation__item navigation__item_main"><a href="/motoservis/">Мотосервис</a></li>
	<li class="navigation__item navigation__item_main"><a href="/dostavka/">Доставка</a></li>
	<li class="navigation__item navigation__item_main"><a href="/kredit/">Кредит</a></li>
	<li class="navigation__item navigation__item_main"><a href="/kontakty/">Контакты</a></li>
	<li class="navigation__item navigation__item_main navigation__item_parent">
		<a href="/eschyo/">Ещё</a>
		<ul class="navigation__list navigation__list_child">
			<li class="navigation__item navigation__item_child"><a href="/clauses/">Статьи</a></li>
			<li class="navigation__item navigation__item_child"><a href="/about/">Гарантия</a></li>
			<li class="navigation__item navigation__item_child"><a href="/faq/">Вопрос-Ответ</a></li>
		</ul>
	</li>
	!!!!!!!!!!!!!!!!!!!!!!
		ЧТОБЫ СОЗДАТЬ ПУНКТ В МЕНЮ ДЛЯ СТРАНИЦЫ, КОТОРАЯ УЖЕ ЕСТЬ, НО НЕ СТАТИЧЕСКАЯ, НАПРИМЕР, ДЛЯ СПИСКА СТАТЕЙ, НАДО СОЗДАТЬ В БД static_page запись, в которой act = 0, А menu_no_show = 1
	!!!!!!!!!!!!!!!!!!!!!!
	*/
	{
		$result = '';
		$result_yet = '';
		$settings = Set::getSettings();
		$db = DB::getMySQLi();
		if($type == 'top')
		{
			$field = 'menu_no_show';
		}
		elseif($type == 'low')
		{
			$field = 'low_menu_no_show';
		}
		if ($static_page = $db->query('
			SELECT * 
			FROM `static_pages` 
			WHERE '.$field.' = "1" AND `trash` = "0"
			ORDER BY sort
		'))
		{
			$i = 1;
			while($row = $static_page->fetch_object())
			{
				if($i <= $yet)
				// начало меню
				{
					$result .= '
						<li class="navigation__item navigation__item_main">
							<a href="'.ROOT.'/'.$row->chpu.'/">'.$row->name.'</a>
						</li>
					';
				}
				else
				// меню ещё
				{
					$result_yet .= '
						<li class="navigation__item navigation__item_child">
							<a href="'.ROOT.'/'.$row->chpu.'/">'.$row->name.'</a>
						</li>
					';
				}
				$i++;
			}
		}
		else
		{
			$result = '';
		}
		if($result_yet != '')
		{
			$result_yet = '
				<li class="navigation__item navigation__item_main navigation__item_parent">
					<a href="'.ROOT.'/eschyo/">Ещё</a>
					<ul class="navigation__list navigation__list_child">
						'.$result_yet.'
					</ul>
				</li>
			';
		}
		return $result.$result_yet;
	}
}