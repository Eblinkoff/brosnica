<?php
class Images
/* действия с картинками */
{
	public function get_image_name($good_name)
	/** Получаем имя фотографии (без расширения, посто имя и всё) по имени товара и ключу, говорящему, какая это фотография
	* @param string $good_name - имя товара, который относится к данной фотографии
	*/
	{
		$settings = Set::getSettings();
		return $settings->my_translit($good_name);
	}


	public function create_image(&$image, $good_name, $type, $photo_id = 0)
	/** Получаем шаблон фотографий из настроек и создаём "свой" набор фотографий нужного размера для карточки товара
	* @param obj $image - объект выгрузка одной фотографии с сайта-донора (thlaspi.com) вида:
		$image = {
		  ["id"]=>
		  string(5) "38189"
		  ["thumbnail"]=>
		  string(76) "https://156909.selcdn.ru/thlaspi/5d/2c/5d2c0d141e4df00e88f93631243606f1.jpeg"
		  ["full"]=>
		  string(76) "https://156909.selcdn.ru/thlaspi/f9/97/f997cb507d7f563dbb828ab71d828611.jpeg"
		}
	* @param string $good_name - имя товара, который относится к данной фотографии
	* @param int $type [1,2,3,...] - Число - тип картинок. Нужен чтобы синхронизировать разные типы картинок отдельно, например, картинки товаров, категорий итд. Сами цифры 1, 2, 3 не несут никакой смысловой нагрузки
	* @param int $photo_id - id этой фотографии в бд САЙТА. Если != 0 - это значит, что запись с этой фотографией уже есть в бд и, соответственно, эта фотография перезаписывается, а не создаётся заново.
	*/
	{
		$db = DB::getMySQLi();
		$settings = Set::getSettings();
		$images_templates = $settings->get_param('images_templates');// это разные размеры фотографий

		// $image->vs = array();
		// создаём корневую категорию с картинками
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name')))
		// если её нет - создаём её
		{
			if(! mkdir($_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name'), 0777, true))
			{
				die('Невозможно создать категорию '.$_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name'));
			}
		}

		// имя файла картинки без расширения
		$file_name = $this->get_image_name($good_name);
		foreach($images_templates as $key => $image_template)
		{
			// создаём категорию с картинками определённого размера
			if(!is_dir($_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$image_template['catalog']))
			// если её нет - создаём её
			{
				if(! mkdir($_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$image_template['catalog'], 0777, true))
				{
					die('Невозможно создать категорию '.$_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$image_template['catalog']);
				}
			}

			// обрезаем фотографию согласно данным $image_template, даём ей имя и копируем в нужный каталог
			$result_image = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$image_template['catalog'].'/'.$file_name.'.jpg';


            // создаём картинку
            if($image_template['crop_quadrat'])
            {
                $this->createsquarethumbnail(
                    2,
                    $image->full,
                    $result_image,
                    (is_numeric($image_template['size']) ? $image_template['size'] : null)
                );
            }
            else
            {
                $size = GetImageSize($image->full);
                // по умолчанию - горизонтальная
                $width = $image_template['size'];// размер картинки
                $height = 0;
                if($size[0] < $size[1])
                {//если картинка вертикальная (Индексы 0 и 1 содержат ширину и высоту изображения. )
                    $width = 0;
                    $height = $image_template['size'];
                }
                $this->img_resize($image->full, $result_image, $height, $width, $size);
            }
			if($settings->get_param('watermark'))
			{
				$this->add_watermark(
					$result_image,
					$result_image,
					$settings->get_param('watermark_value'),
					ABSPATH.'/fonts/Times New Roman.ttf',
					255,
					255,
					255,
					70
				);
			}
			// добавляем в объект информацию об этой картинке
			// $image->vs[$key] = $result_image;
		}
		// записываем эту картинку в бд
		if($photo_id == 0)
		{
			//значит такой картинки нет
			$stmt = $db->stmt_init();
			// если нам не присылают описание картинки, считаем, что оно (описание) - пустое
			if($image->title == null)
			{
				$image->title = '';
			}
			if(
			($stmt->prepare('INSERT INTO `images` (`thlaspi_id`,`name`,`type`,`alt`) VALUES (?,?,'.$type.',?)') ===FALSE)
			// привязываем переменные к плейсхолдорам
			or ($stmt->bind_param('iss', $image->id, $file_name, $image->title) === FALSE)
			// отправляем даные, которые на данный момент находятся в привязанных переменных
			or ($stmt->execute() === FALSE)
			) {
				die('error (' . $stmt->errno . ') ' . $stmt->error);
			}
		}
		else
		// если картинка есть, делаем отметку о том, что она соответствует выгрузке и её не надо удалять
		{
			$db->query('
				UPDATE `images` 
				SET `sync` = 1, `type` = '.$type.' 
				WHERE id = '.$photo_id
			);
		}
	}

	public function prepare(&$plants, $type, $upgrade_all = false)
	/** Подготавливает картинки для всех растений в сответствии с нуждами сайта
	* @param obj $plants - объект выгрузка всех растений с сайта-донора
	* @param int $type [1,2,3,...] - Число - тип картинок. Нужен чтобы синхронизировать разные типы картинок отдельно, например, картинки товаров, категорий итд. Сами цифры 1, 2, 3 не несут никакой смысловой нагрузки
	* @param bool $upgrade_all - пересоздавать все картинки без исключения (это может быть нужно, например, в случае, если параметры картинок на сайте изменились и их надо пересоздать) - true или создать только те картинки, которых ещё нет на диске - false
	Дополнение. Если картинка есть в бд, то она есть и на диске.
	*/
	{
		$db = DB::getMySQLi();
		$stmt = $db->stmt_init();
		$settings = Set::getSettings();
		if(!is_array($settings->get_param('images_templates')) || count($settings->get_param('images_templates')) == 0)
		// если параметров картинок нет - выходим
		{
			vd(count($settings->get_param('images_templates')));
			vd('если параметров картинок нет - выходим<br>Images.php, str.113');
			return false;
		}
		// каждая картинка проверяется на соответствие выгрузке с тласпи
		$db->query('
			UPDATE `images` 
			SET `sync` = 0 
			WHERE `type` = '.$type,
		MYSQLI_USE_RESULT);
		if($plants != null)
		{
			// проходим все растения в объекте
			foreach($plants as &$plant)
			{
				// название данного растения
				$plant_name = $plant->name;
				if(isset($plant->rus_name))
				// если это именно растение, а не, например, категория
				{
					$plant_name = Custom::static_class('Good')::true_name($plant).' '.$plant->id;
				}
				elseif(isset($plant->header))
				// если это новость или статья
				{
					$plant_name = $plant->header.' '.$plant->id;
				}
				// теперь проходим все картинки данного растения
				if($plant->images != null)
				{
					foreach($plant->images as &$image)
					{
						/*
							$image = {
							  ["id"]=>
							  string(5) "38189"
							  ["thumbnail"]=>
							  string(76) "https://156909.selcdn.ru/thlaspi/5d/2c/5d2c0d141e4df00e88f93631243606f1.jpeg"
							  ["full"]=>
							  string(76) "https://156909.selcdn.ru/thlaspi/f9/97/f997cb507d7f563dbb828ab71d828611.jpeg"
							}
						*/
						if(($stmt->prepare('
							SELECT id 
							FROM `images` 
							WHERE thlaspi_id = ? AND `type` = '.$type.'
						') ===FALSE)
						// привязываем переменные к плейсхолдорам
						or ($stmt->bind_param('i', $image->id) === FALSE)
						// отправляем даные, которые на данный момент находятся в привязанных переменных
						or ($stmt->execute() === FALSE)
						// привязывем переменую для получения в нее результата
						or ($stmt->bind_result($_id) === FALSE)
						// делаем запрос буферизированным, 
						// если бы этой строки не было, запрос был бы небуферезированым
						or ($stmt->store_result() === FALSE)
						) {
							die('error (' . $stmt->errno . ') ' . $stmt->error);
						}
						if(!$stmt->fetch())
						{
							// такой фотки нет, надо создать
							$this->create_image($image, $plant_name.' '.$image->id, $type);
						}
						else
						{
							// такая фотка есть в бд, а есть ли на диске?
                            //и мы перезаписываем её только если у нас $upgrade_all == true или если фотка пропала с диска физически
							$paths = self::kit($image->name);
							// if($upgrade_all || ! file_exists($paths->list) || ! file_exists($paths->large) || ! file_exists($paths->icon))
							if($upgrade_all)
							{
								$this->create_image($image, $plant_name.' '.$image->id, $type, $_id);
							}
							else
							{
								// не забываем включить отметку о синхронизации
								$db->query('
									UPDATE `images` 
									SET `sync` = 1 
									WHERE id = '.$_id.' AND `type` = '.$type
								);
							}
						}
					}
				}
				// break;
			}
		}
		// удаляем все картинки, которых нет в выгрузке с тласпи, но есть на нашем сайте
		if ($result = $db->query('
			SELECT * 
			FROM `images` 
			WHERE sync = 0 AND `type` = '.$type
		))
		{
			while($row = $result->fetch_object())
			{
				$this->delete_image($row);
			}
			$db->query('
				DELETE FROM `images` 
				WHERE sync = 0 AND `type` = '.$type
			);
		}
		else
		{
			echo 'mimo';
		}
	}
	
	function delete_image($row)
	/** Удаляем картику с диска физически (бд не трогаем)
	* @param obj $row - объект выгрузки картинки из бд images
	*/
	{
		// vd($row);
		$settings = Set::getSettings();
		$images_templates = $settings->get_param('images_templates');
		foreach($images_templates as $key => $image_template)
		{
			$image_path = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$image_template['catalog'].'/'.$row->name.'.jpg';
			if(file_exists($image_path))
			{
				if(unlink($image_path))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				vd('No file for delete: '.$image_path);
				return false;
			}
		}
	}
	
	function createsquarethumbnail($filetype, $origfile, $thumbfile, $new_size=null)
	/** Превращает картинку в квадрат, равномерно обрезая с двух сторон. Пример вызова: createsquarethumbnail(2, getcwd()."/IMG_6870.JPG", getcwd()."/IMG_6870_box.JPG", null);
	*/
	{
		$compression = "gd2";
		if ($filetype == 1) { $origimage = imagecreatefromgif($origfile); }
		elseif ($filetype == 2) { $origimage = imagecreatefromjpeg($origfile); }
		elseif ($filetype == 3) { $origimage = imagecreatefrompng($origfile); }

		$old_x = imagesx($origimage);
		$old_y = imagesy($origimage);

		$x = 0; $y = 0;

		if ($old_x > $old_y)
		{
			$x = ceil(($old_x - $old_y) / 2);
			$old_x = $old_y;
			if($new_size == null)
			{
				$new_size = $old_y;
			}
		}
		elseif ($old_y > $old_x)
		{
			$y = ceil(($old_y - $old_x) / 2);
			$old_y = $old_x;
			if($new_size == null)
			{
				$new_size = $old_x;
			}
		}
		$new_image = imagecreatetruecolor($new_size,$new_size);
		if ($filetype == 3 && $compression != "gd1")
		{
			imagealphablending($new_image, false);
			imagesavealpha($new_image, true);
		}
		imagecopyresampled($new_image,$origimage,0,0,$x,$y,$new_size,$new_size,$old_x,$old_y);
		if ($filetype == 1) { imagegif($new_image,$thumbfile,100); }
		elseif ($filetype == 2) { imagejpeg($new_image,$thumbfile,100); }
		elseif ($filetype == 3) { imagepng($new_image,$thumbfile,5); }

		
		imagedestroy($origimage);
		imagedestroy($new_image);
	}
	
	function add_watermark($img, $new_image, $text, $font, $r = 128, $g = 128, $b = 128, $alpha = 108)
	/** Добавить watermark, просто написав нужный текст на изображении
	* @param $img – путь к старому изображению, на которое добавляется водяной знак
	* @param $new_image – путь к новому изображению, на которое добавляется водяной знак
	* @param $text – текст надписи
	* @param $font – имя файла шрифта .ttf
	* @param $r,$g,$b – цвет надписи в формате RGB
	* @param $alpha – уровень прозрачности , от 0 до 128, при этом 0 - непрозрачный, 128 - полностью прозрачный
	
	* Пример вызова функции:
		$image = add_watermark($img2, $new_image, 'Grigorieff.ru','erasbd.ttf');
	*/
	{
		// преобразуем изображение из пути в объект
		$img = imagecreatefromjpeg($img);
		//получаем ширину и высоту исходного изображения
		$width = imagesx($img);
		$height = imagesy($img);
		//угол поворота текста
		// $angle =  -rad2deg(atan2((-$height),($width))); 
		$angle = 0;
		//добавляем пробелы к строке
		$text = " ".$text." ";

		$c = imagecolorallocatealpha($img, $r, $g, $b, $alpha);

		$size = (($width+$height)/2)*2/strlen($text);
		$size = $size - ($size * 40)/100;// уменьшаем размер шрифта на 40 %
        if ($width > 890)
        {
            $size = $size - ($size * 40)/100;// уменьшаем размер шрифта ЕЩЁ на 40 % если у нас большая картинка
        }
        if ($width < $height) // vertical
        {
            $size = $size - ($size * 40)/100;
        }
		$box  = imagettfbbox ( $size, $angle, $font, $text );
		$x = $width/2 - abs($box[4] - $box[0])/2;
		$y = $height/2 + abs($box[5] - $box[1])/2;

		$x = $x + ($x * 50)/100;// двигаем текст вправо на 50%
		$y = $y + ($y * 70)/100;// опускаем текст вниз на 70% (относительно середины)
		//записываем строку на изображение
		imagettftext($img,$size ,$angle, $x, $y, $c, $font, $text);
		 
		//выводим изображение
		imageJPEG($img, $new_image);
		//освобождаем память
		imagedestroy($img);
	}

    /** Функция img_resize(): генерация thumbnails http://www.wellsait.ru/articles/?url=image_resize
     * Если нужен только один параметр, например ширина 800px, то высоту задаем равную 0 (ноль). При этом получим пропорциональное фото с шириной 800px.
     * @param string $src - имя исходного файла
     * @param string $dest - имя генерируемого файла
     * @param int $width, $height  - ширина и высота генерируемого изображения, в пикселях
     * @param string $size - результат вызова ;. Вынесен наружу для того, чтобы не вызывать getimagesize дважды
    Необязательные параметры:
     * @param string $rgb - цвет фона, по умолчанию - белый
     * @param string $quality - качество генерируемого JPEG, по умолчанию - максимальное (100)
     */
    function img_resize($src, $dest, $width, $height, $size, $rgb = 0xFFFFFF, $quality = 100){//по умолчанию считаем
        //что все необходимые проверки сделаны
        //if (!file_exists($src))
        //return false;

        //$size = getimagesize($src);//передаём с параметром

        //if ($size === false) 493-46-90
        //return false;

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));//ZB jpeg. Формат файла
        $icfunc = 'imagecreatefrom'.$format;

        if (!function_exists($icfunc)) return false;

        $x_ratio = $width  / $size[0];//ratio - соотношение, коэфициент
        $y_ratio = $height / $size[1];
        //echo '$x_ratio= '.$x_ratio.' $y_ratio= '.$y_ratio;exit;
        if ($height == 0){
            $y_ratio = $x_ratio;
            $height  = $y_ratio * $size[1];
        }
        elseif ($width == 0){
            $x_ratio = $y_ratio;
            $width   = $x_ratio * $size[0];
        }

        $ratio       = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);

        $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);//floor -- Округляет дробь в меньшую сторону
        $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
        $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width)   / 2);
        $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

        // если не нужно увеличивать маленькую картинку до указанного размера
        if ($size[0]<$new_width && $size[1]<$new_height){
            $width = $new_width = $size[0];
            $height = $new_height = $size[1];
        }

        $isrc  = $icfunc($src);
        $idest = imagecreatetruecolor($width, $height);

        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

        /*
        Получается что? Задавая расширение файла в $dest мы можем перевести файл из формата в формат.
        Но мне тут это не нужно. Поэтому мы программно записываем файл в том же формате, в котором он
        и был изначально. Поэтому вместо switch ($ext) пишем switch ($format)
        $i = strrpos($dest,'.');
        if (!$i) return '';//а почему '', а не false? И почему бы файлу, пусть и графическому, не
        //быть без расширения?
        $l = strlen($dest) - $i;
        $ext = substr($dest,$i+1,$l);*/
        switch ($format)
        {
            case 'jpeg':
            case 'jpg':
                imagejpeg($idest,$dest,$quality);//вот он - вывод файла. Наплохо бы его выводить сразу куда надо без промежуточных звеньев
                //что для этого нужно. Новый путь, а значит - новое название, а значит - пропустить его через
                // md5, а значит, похоже, записать всё-таки. Значит придётся перезаписывать куда-то.
                break;
            case 'gif':
                imagegif($idest,$dest);
                break;
            case 'png':
                imagepng($idest,$dest);
                break;
        }

        imagedestroy($isrc);
        imagedestroy($idest);

        return true;
    }

	public function name_by_id($thlaspi_id, &$title = '', $type = 0)
	/** Имя файла картинки из бд по id из выгрузки
	* @param int $thlaspi_id - id картинки из выгрузки
	* @param string $title - описание картинки, если есть
	* @param int $type - тип картинки, дополнительное условие поиска. 1 - растения, 2 - категории, 3 - новости, 4 - статьи
	* @return string $_name - имя файла картинки без расширения или false, если такого файла нет в бд
	*/
	{
		$db = DB::getMySQLi();
		$title = '';
		$stmt = $db->stmt_init();
		if(($stmt->prepare('
			SELECT name, alt 
			FROM `images` 
			WHERE thlaspi_id = ?'.($type != 0 ? ' AND type='.$type : '').'
		') ===FALSE)
		// привязываем переменные к плейсхолдорам
		or ($stmt->bind_param('i', $thlaspi_id) === FALSE)
		// отправляем даные, которые на данный момент находятся в привязанных переменных
		or ($stmt->execute() === FALSE)
		// привязывем переменую для получения в нее результата
		or ($stmt->bind_result($_name, $_alt) === FALSE)
		// делаем запрос буферизированным, 
		// если бы этой строки не было, запрос был бы небуферезированым
		or ($stmt->store_result() === FALSE)
		) {
			die('error (' . $stmt->errno . ') ' . $stmt->error);
		}
		if($stmt->fetch())
		{
			$title = $_alt;
			return $_name;
		}
		else
		{
            //echo 'Нет имени картинки (function name_by_id) - thl_id='.$thlaspi_id;
			return false;
		}
	}
	
	public function kit($name)
	/** Набор путей к картинкам
	* @param string $name - имя файла картинки без расширения
	* @return stdClass $result - объект с путями до картинок различных размеров. Если имя пустое, то создаётся объект с пустыми значениями
	*/
	{
		$settings = Set::getSettings();
		
		$result = new stdClass();
		$images_templates = $settings->get_param('images_templates');
		
		foreach($images_templates as $key => $image_template)
		{
			if($name != '')
			{
				// $img_thumbnail_0 = ROOT.'/'.$settings->get_param('images_catalog_path_name').'/list/'.$img_name.'.jpg';
				// $img_full_0 = ROOT.'/'.$settings->get_param('images_catalog_path_name').'/large/'.$img_name.'.jpg';
				// $img_icon_0 = ROOT.'/'.$settings->get_param('images_catalog_path_name').'/icon/'.$img_name.'.jpg';
				$result->{$key} = ROOT.'/'.$settings->get_param('images_catalog_path_name').'/'.$key.'/'.$name.'.jpg';
			}
			else
			{
				$result->{$key} = '';
			}
		}
        return $result;
	}
}
