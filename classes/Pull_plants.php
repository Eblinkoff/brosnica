<?php
class Pull_plants
/**
* Класс, работающий с thlaspi.com по api
*/
{
	var $synchr_code;
	
	var $error_message;
	
	var $api_version;
	
	function __construct()
	{
		$this->synchr_code = SYNCHR_CODE;
		
		$this->error_message = '';
		
		$this->api_version = API_VERSION;
	}
	
	public function set_new_order($url, $label, $data)
	/** Отправить json на thlaspi.com
	* @param string $url - урл, по которому отправляем запрос
	* @param string $label - метка POST, например, "checkout"
	* @param stdclass $data - объект с данными, который отправляем
	* @return void  - ответ api или false, если произошла какая-то ошибка
	*/
	{
		$data = json_encode($data);
		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
		);
		// ed(http_build_query(['checkout'=>$data]));
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([$label => $data]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
		// отключаем проверку сертификата
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$output = curl_exec($ch);

		//Получаем информацию о запросе
		$info = curl_getinfo($ch);

		curl_close($ch);

		// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];
		// echo $output;
		// die;
		
		if($info['http_code'] == 200)
		{
			return json_decode($output);
		}
		elseif($info['http_code'] == 204)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Массив с данными не передался';
			return false;
		}
		elseif($info['http_code'] == 417)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Ошибка в методе Sales::add_new';
			return false;
		}
		elseif($info['http_code'] == 418)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Ошибка получения объекта заказа-пустышки';
			return false;
		}
		elseif($info['http_code'] == 419)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Ошибка получения объекта растения в заказе';
			return false;
		}
		elseif($info['http_code'] == 415)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Данные не прошли валидацию';
			// $this->error_message = $output;
			return false;
		}
		elseif($info['http_code'] == 420)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Ошибка в методе Sales::buy_plant_by_o';
			return false;
		}
		elseif($info['http_code'] == 421)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' Невозможно записать контактные данные';
			return false;
		}
		else
		{
			echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'].'<br><br>';
			echo $output;
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет садов1.';
			return false;
		}
	}
	
	public function get_news($count = 0)
	/** Получить новости пользователя по коду синхронизации
	* @param int $count - количество последних новостей. Если = 0 - получаем все новости, что есть
	*/
	{
		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
			// "Content-Type: application/json",
		);
	
		$url = 'https://api.thlaspi.com/users/code/'.$this->synchr_code.'/news/'.$count;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
		// отключаем проверку сертификата
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
		$output = curl_exec($ch);
		// print curl_error($ch);

		//Получаем информацию о запросе
		$info = curl_getinfo($ch);

		curl_close($ch);
		// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];
		// echo $output;
		// die;
		
		if($info['http_code'] == 200)
		{
			return json_decode($output);
		}
		elseif($info['http_code'] == 205)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' новостей нет на thlaspi.com.';
			return null;
		}
		else
		{
			echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'].'<br><br>';
			echo '*;'.$output.'*';
			$this->error_message = 'Error code: '.$info['http_code'].' такой страницы нет';
			return null;
		}
	}
	
	public function get_articles($count = 0)
	/** Получить статьи пользователя по коду синхронизации
	* @param int $count - количество последних статей. Если = 0 - получаем все статьи, что есть
	*/
	{
		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
			// "Content-Type: application/json",
		);
	
		$url = 'https://api.thlaspi.com/users/code/'.$this->synchr_code.'/articles/'.$count;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
		// отключаем проверку сертификата
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
		$output = curl_exec($ch);
		// print curl_error($ch);

		//Получаем информацию о запросе
		$info = curl_getinfo($ch);

		curl_close($ch);
		// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];
		// echo $output;
		// die;
		
		if($info['http_code'] == 200)
		{
			return json_decode($output);
		}
		elseif($info['http_code'] == 205)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' статей нет на thlaspi.com.';
			return null;
		}
		else
		{
			echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'].'<br><br>';
			echo '*;'.$output.'*';
			$this->error_message = 'Error code: '.$info['http_code'].' такой страницы нет';
			return null;
		}
	}
	
	public function get_categories()
	/** Получить все категории пользователя по коду синхронизации
	*/
	{
		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
			// "Content-Type: application/json",
		);
	
		$url = 'https://api.thlaspi.com/users/code/'.$this->synchr_code.'/categories';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
		// отключаем проверку сертификата
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
		$output = curl_exec($ch);
		// print curl_error($ch);

		//Получаем информацию о запросе
		$info = curl_getinfo($ch);

		curl_close($ch);
		// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];
		// echo $output;
		// die;
		
		if($info['http_code'] == 200)
		{
			return json_decode($output);
		}
		elseif($info['http_code'] == 204)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет такого пользователя на thlaspi.com.';
			return false;
		}
		elseif($info['http_code'] == 205)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет садов у данного пользователя на thlaspi.com.';
			return false;
		}
		else
		{
			echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'].'<br><br>';
			echo '*;'.$output.'*';
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет садов3.';
			return false;
		}
	}
	
	public function get_gardens_ids()
	/** Получить массив id всех садов пользователя по коду синхронизации
	*/
	{

		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
			// "Content-Type: application/json",
		);
	
		$url = 'https://api.thlaspi.com/users/code/'.$this->synchr_code.'/gardens/ids';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// отключаем проверку сертификата
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		$output = curl_exec($ch);

		//Получаем информацию о запросе
		$info = curl_getinfo($ch);

		curl_close($ch);
		// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];
		if($info['http_code'] == 200)
		{
			return json_decode($output);
		}
		elseif($info['http_code'] == 204)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет такого пользователя на thlaspi.com.';
			return false;
		}
		elseif($info['http_code'] == 205)
		{
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет садов у данного пользователя на thlaspi.com.';
			return false;
		}
		else
		{
			$this->error_message = 'Error code: '.$info['http_code'].' или просто нет садов5.';
			return false;
		}
	}
	
	public function get_gardens_plants($ids)
	/** подтягиваем все растения из всех садов с thlaspi.com  по их id - $ids и записываются в буферные таблицы, определённые константами GOODS_TABLE итд
	*/
	{

		$headers = array(
			'Accept: application/version'.$this->api_version.'.+json',
			// "Content-Type: application/json",
		);
	
		//очищаем столбец action перед записью взятых с thlaspi товаров
		//а вообще значение action может быть DELETE, UPDATE, INSERT
		// $this->mysqli->query('
			// UPDATE `'.GOODS_TABLE.'`
			// SET `action` = "DELETE" 
			// WHERE 1 
		// ');
		
		//а также картинки
		// $this->mysqli->query('
			// UPDATE `'.PICTURES_TABLE.'`
			// SET `action` = "DELETE" 
			// WHERE 1 
		// ');
		
		$plants_rez = array();
		foreach($ids as $value)
		{
			$url = 'https://api.thlaspi.com/gardens/'.$value->id.'/plants';

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			// отключаем проверку сертификата
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
			$output = curl_exec($ch);

			//Получаем информацию о запросе
			$info = curl_getinfo($ch);

			curl_close($ch);
			//Выводим какую-то инфомрацию
			// echo 'Запрос выполнился за  ' . $info['total_time'] . ' сек. к URL: ' . $info['url'].', http_code - '.$info['http_code'];

			if($info['http_code'] == 200)
			{
				$plants = json_decode($output);
				 //vd($plants);die;
				// $plants - массив с товарами из одного сада
				$plants_rez = array_merge($plants_rez, $plants);
				
			}
		}
		// $sql_shop_goods = substr($sql_shop_goods, 0, -1);
		//vd($plants_rez);die;
		return $plants_rez;
	}
	


}