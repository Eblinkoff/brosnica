<?php
class Category
{
	
	public function get_categories($pull_plants)
	{
		if(! ($categories = $pull_plants->get_categories()))
		{
			die($pull_plants->error_message);
		}
		return $categories;
	}
	
	public function catalog_page($catalog_tpl)
	/** создаём страницу общего каталога сайта
	* @param string $catalog_tpl - шаблон каталога
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');

		$cat_in_list_tpl = Custom::static_class('Main')::get_tpl('cat_in_list_tpl');

		// создаём список категорий
		$plantsListInner = '';// html списка всех категорий для вставки на страницу общего каталога
		foreach($categories as $category)
		{
			if($category->level == 0)
			// тут отсеиваем категории низших уровней - их место в подкатегориях
			{
				$plantsListInner .= self::cats_in_list($cat_in_list_tpl, $category);
			}
		}
		
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		$aside = Custom::static_class('Main')::aside();
		
		
		$head = Custom::static_class('Main')::head(
			'Каталог растений на продажу', // title
			'Полный каталог наших растений на продажу: Ирисы, Розы, Многолетники, Злаки, Осоки, Пряно-ароматические растения, Деревья и кустарники, Растения для альпинария, Рододендроны и Плодовые', // description
			'', // keywords
			'' // og_image
		);
		$header = Custom::static_class('Main')::header();
		$footer = Custom::static_class('Main')::footer();
		$show_js = Custom::static_class('Main')::show_js();
		
		// у нас есть список растений в данной категории. Теперь вставим его в шаблон
		$result_category_page = str_replace(
			array(
				'%shead', 
				'%header', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js', 
				'%category_text', 
				'%h1', 
				'%catalog_path_name', 
				'%this_page_url', 
				'%aside', 
				'%plantsListInner', 
				'%version', 
				'%modal_confirmation_tpl', 
				'%root',
			),
			array(
				$head, 
				$header, 
				$footer,
                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
				$show_js, 
				'<p>Полный каталог наших растений на продажу:</p>', // category_text
				'Каталог растений', // h1
				ROOT.'/'.$settings->get_param('catalog_path_name'), 
				$dir.'/', // this_page_url
				$aside, 
				$plantsListInner,
                Custom::static_class('Main')::get_version(), // version
                Custom::static_class('Main')::get_tpl('modal_confirmation_tpl'), // шаблон модального окна
				ROOT,
			),
			$catalog_tpl
		);
		// и записываем её в каталог shop
		if(file_put_contents($dir.'/index.php',$result_category_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/index.php в каталоге shop');
		}
		// также создаём ссылку на эту страницу в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/'.$settings->get_param('catalog_path_name').'/', 
			'date' => time(),
		]);
		return $result_category_page;
	}
	
	public function category_page($category_tpl, $category, $full_tree, $full_tree_array)
	/** создаём страницу со списком товаров для данной категории с пагинацией
	* @param string $category_tpl - шаблон категории
	* @param obj $category - объект данной категории из объекта $categories
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	* @param obj $full_tree_array - массив дерева категорий с объектами категорий (без shop и текущей) или array(), если категория $category - корневая
	*/
	{
        $settings = Set::getSettings();
        $categories = $settings->save_object('categories');
        $plants = $settings->save_object('plants');


		$good_in_list_tpl = Custom::static_class('Main')::get_tpl('good_in_list_tpl');

		// создаём список категорий
		$catschildrens = '';// html списка дочерних категорий для вставки на страницу общего каталога
		$cat_in_list_tpl = Custom::static_class('Main')::get_tpl('cat_in_list_tpl');
		foreach($categories as $cat)
		{
			if($cat->parent_id == $category->id)
			// тут отсеиваем лишние - недочерние - категории
			{
				$catschildrens .= self::cats_in_list($cat_in_list_tpl, $cat, $category->chpu.'/'.$full_tree);
			}
		}
		if($catschildrens != '')
		{
			$catschildrens = '
				<ul class="shop-list-categories row">
					'.$catschildrens.'
				</ul>
			';
		}
		
		// создаём список растений, которые принадлежат к данной категории
		$pl_araay = [];
		// записываем объекты растений в массив $pl_araay, который мы будем сортировать перед тем, как сгенерировать из него ленту растений в данной категории
		foreach($plants as $plant)
		{
			if(isset($plant->categories))
			{
				foreach($plant->categories as $plantCategory)
				{
					// $plantCategory - это номер каждой категории, к которой прикреплено данное растение
					if($plantCategory == $category->id)
					// значит данное растение следует разместить на странице данной категории
					{
						$pl_araay[] = $plant;
						// $plantsListInner .= Good::good_cart_in_list($good_in_list_tpl, $plant, $full_tree);
						break;
					}
				}
			}
		}
		// сортировка массива
		usort($pl_araay, array("Category", "plants_sort"));// plants_sort - название метода, в котором осуществляется сортировка. Category - название класса
		
		$plantsListInner = '';// html списка растений в категории для вставки на её страницу
		foreach($pl_araay as $_plant)
		{
			$plantsListInner .= Custom::static_class('Good')::good_cart_in_list($good_in_list_tpl, $_plant, $full_tree);
		}
		
		$dir = $_SERVER['DOCUMENT_ROOT'].ROOT.'/'.$settings->get_param('catalog_path_name');
		$aside = Custom::static_class('Main')::aside();
		
		// основная картинка
		
		// берём имя картинки из бд по номеру thlaspi_id
		if($img_name = Custom::static_class('Images')::name_by_id($category->images[0]->id))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
			$paths = Custom::static_class('Images')::kit('');
		}
		
		$head = Custom::static_class('Main')::head(
			$category->name, // title
			$category->description, // description
			$category->keywords, // keywords
			$paths->large // og_image
		);
		$header = Custom::static_class('Main')::header();
		$footer = Custom::static_class('Main')::footer();
		$show_js = Custom::static_class('Main')::show_js();
		// $mobile_filter_shop = Custom::static_class('Main')::filter_shop('mobile', $plants);
		$mobile_filter_shop = '';
		
		$breadcrumbs = Custom::static_class('Main')::breadcrumbs($category->name, $full_tree_array, $category, false);
		
		// у нас есть список растений в данной категории. Теперь вставим его в шаблон
		$result_category_page = str_replace(
			array(
				'%shead', 
				'%header', 
				'%footer', 
				'%modal_in_cart', 
				'%show_js', 
				'%category_text', 
				'%h1', 
				'%breadcrumbs', 
				'%aside', 
				'%plantsListInner', 
				'%catschildrens', 
				'%img_full_0',
				'%mobile_filter_shop',
				'%version',
				'%modal_confirmation_tpl',
				'%root',
			),
			array(
				$head, // head
				$header, // header
				$footer, // footer
                Custom::static_class('Main')::get_tpl('modal_in_cart_tpl'),
				$show_js, // footer
				'<p>'.$category->description.'</p>', // category_text
				$category->name, // h1
				$breadcrumbs, // breadcrumbs
				$aside, 
				$plantsListInner, // html списка растений в категории
				$catschildrens, // html списка дочерних категорий
				$paths->large, //Картинка категории
				$mobile_filter_shop, // мобильный фильтр
                Custom::static_class('Main')::get_version(), // version
                Custom::static_class('Main')::get_tpl('modal_confirmation_tpl'), // шаблон модального окна
				ROOT,
			),
			$category_tpl
		);
		// и записываем её в каталог категории
		if(file_put_contents($dir.'/'.$full_tree.$category->chpu.'/index.php',$result_category_page) === false)
		{
			die('Не удалось создать шаблон '.$dir.'/'.$full_tree.$category->chpu.'/index.php для категории '.$category->name);
		}
        Custom::static_class('Main')::create_htaccess($dir.'/'.$full_tree.$category->chpu.'/');// также туда записываем файл .htaccess - для статического сайта он должен быть в каждом каталоге кроме admin
		// также создаём ссылку на эту страницу в файле sitemap.xml
        Custom::static_class('Settings')::plus_sitemap_link([
			'url' => $settings->get_param('tcp').'://'.$settings->get_param('shop_name_global').'/'.$full_tree.$category->chpu.'/', 
			'date' => time(),
		]);
		return $result_category_page;
	}
	
	public function cats_in_list($cat_in_list_tpl, $category, $full_tree = '')
	/** Создаём маленькую карточку категории для списка категорий
	* @param string $cat_in_list_tpl - шаблон маленькой карточки категории
	* @param obj $category - объект данной категории из объекта $categories
	* @param string $full_tree - дерево категорий вида rozy/foribundy/kornesobstvennye/ или '', если категория $category - корневая
	* @return string $result_good - html страницы товара целиком
	*/
	{
        $settings = Set::getSettings();
        //$categories = $settings->save_object('categories');
		// foreach($categories as $category)
		// {
			// if($plant->categories[0] == $category->id)
			// {
				// $category_url = $category->chpu;
				// $this_page_url = '/'.$settings->get_param('catalog_path_name').'/'.$category->chpu.'/'.$plant->chpu.$settings->get_param('chpu_postfix');
				// break;
			// }
		// }
		
		// берём имя картинки из бд по номеру thlaspi_id
		if($img_name = Custom::static_class('Images')::name_by_id($category->images[0]->id))
		{
			$paths = Custom::static_class('Images')::kit($img_name);
		}
		else
		{
			$paths = Custom::static_class('Images')::kit('');
		}

		$result = str_replace(
			array(
				'%name', 
				'%this_page_url', 
				'%img_thumbnail_0', 
				'%img_full_0', 
				'%img_icon_0', 
			),
			array(
				$category->name, // name
				ROOT.'/'.$settings->get_param('catalog_path_name').'/'.$full_tree.$category->chpu.'/', // this_page_url
				$paths->list,
				$paths->large,
				$paths->icon,
			),
			$cat_in_list_tpl
		);
		return $result;
	}
	
	public static function plants_sort($a, $b)
	/** Сортировка массива с объектами растений для вставки на странице категорий
	*/
	{
		$al = Custom::static_class('Good')::is_availability($a);
		$bl = Custom::static_class('Good')::is_availability($b);
		if ($al == true && $bl == true)
		{
			return 0;
		}
		if ($al == false && $bl == false)
		{
			return 0;
		}
		if ($al == true && $bl == false)
		{
			return -1;
		}
		if ($al == false && $bl == true)
		{
			return +1;
		}
		// return ($al > $bl) ? +1 : -1;
	}
	

}