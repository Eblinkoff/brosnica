-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 08 2022 г., 05:26
-- Версия сервера: 5.5.50-MariaDB
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `brosnica`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL COMMENT 'Дата создания новости',
  `image_id` bigint(20) unsigned NOT NULL COMMENT 'thlaspi_id картинки',
  `chpu` text NOT NULL COMMENT 'ЧПУ',
  `title_meta` text NOT NULL COMMENT 'Title',
  `descr` text NOT NULL COMMENT 'Description',
  `header` text NOT NULL COMMENT 'Заголовок новости',
  `text` text NOT NULL COMMENT 'Текст новости',
  `thlaspi_id` bigint(20) NOT NULL COMMENT 'id новости на сайте thaspi.com Этого id может и не быть, ведь новости могут создаваться не только на сайте thlaspi',
  `edit_label` tinyint(1) NOT NULL DEFAULT '2' COMMENT 'Если =2 - пересоздавать новость не требуется. А если =3 - страницу с новостью нао пересоздать'
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Новости на сайте';

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `date`, `image_id`, `chpu`, `title_meta`, `descr`, `header`, `text`, `thlaspi_id`, `edit_label`) VALUES
(31, '2022-01-31 09:19:13', 47677, 'roza-briosa', 'Роза Briosa', 'может быть и такой', 'Роза Briosa', 'может быть и такой', 3, 2),
(32, '0000-00-00 00:00:00', 47542, 'iris-mechevidnyy-beringiya', 'Ирис мечевидный Берингия', 'Находка этого года. Потрясающе красивый!', 'Ирис мечевидный Берингия', 'Находка этого года. Потрясающе красивый!', 2, 2),
(33, '2022-01-21 21:38:26', 47530, 'iris-pseudata-kurokawa-noh', 'Iris pseudata Kurokawa Noh', 'Очень красивый! В этой серии особенно изящные очертания цветка.', 'Iris pseudata Kurokawa Noh', 'Очень красивый! В этой серии особенно изящные очертания цветка.', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `articles_images_rel`
--

CREATE TABLE IF NOT EXISTS `articles_images_rel` (
  `id` bigint(20) unsigned NOT NULL,
  `articles_id` bigint(20) unsigned NOT NULL COMMENT 'Номер (id) статьи из таблицы articles',
  `thlaspi_id` bigint(20) unsigned NOT NULL COMMENT 'Порядковый номер картинки (thlaspi_id)'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Связь растений из сада со статьёй';

--
-- Дамп данных таблицы `articles_images_rel`
--

INSERT INTO `articles_images_rel` (`id`, `articles_id`, `thlaspi_id`) VALUES
(1, 31, 47678),
(2, 31, 47677),
(3, 32, 47543),
(4, 32, 47542),
(5, 33, 47530);

-- --------------------------------------------------------

--
-- Структура таблицы `articles_plants_rel`
--

CREATE TABLE IF NOT EXISTS `articles_plants_rel` (
  `id` bigint(20) unsigned NOT NULL,
  `articles_id` bigint(20) unsigned NOT NULL COMMENT 'Номер (id) статьи из таблицы articles',
  `plant_id` bigint(20) unsigned NOT NULL COMMENT 'Порядковый номер растения (thlaspi_id)'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Связь растений из сада со статьёй';

--
-- Дамп данных таблицы `articles_plants_rel`
--

INSERT INTO `articles_plants_rel` (`id`, `articles_id`, `plant_id`) VALUES
(1, 31, 8713),
(2, 31, 8708);

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) unsigned NOT NULL,
  `thlaspi_id` bigint(20) unsigned NOT NULL,
  `name` text NOT NULL,
  `type` tinyint(2) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=850 DEFAULT CHARSET=utf8 COMMENT='Таблица картинок сайта';

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `thlaspi_id`, `name`, `type`, `sync`) VALUES
(1, 43012, 'roza-gibridnaya-jubilee-du-prince-de-monaco-rosa-hybridus-8708-43012', 1, 1),
(2, 43013, 'roza-gibridnaya-jubilee-du-prince-de-monaco-rosa-hybridus-8708-43013', 1, 1),
(3, 42366, 'roza-gibridnaya-jubilee-du-prince-de-monaco-rosa-hybridus-8708-42366', 1, 1),
(777, 49201, 'roza-princess-anne-9151-49201', 1, 1),
(776, 49221, 'roza-reine-des-violettes-8998-49221', 1, 1),
(6, 41843, 'roza-gibridnaya-gebruder-grimm-rosa-hybridus-8712-41843', 1, 1),
(315, 46612, '-46612', 3, 1),
(316, 46494, '-46494', 3, 1),
(313, 46769, '-46769', 3, 1),
(314, 46694, '-46694', 3, 1),
(603, 46089, 'roza-magenta-10417-46089', 1, 1),
(602, 46088, 'roza-magenta-10417-46088', 1, 1),
(11, 43009, 'roza-gibridnaya-salita-rosa-hybridus-8713-43009', 1, 1),
(12, 43010, 'roza-gibridnaya-salita-rosa-hybridus-8713-43010', 1, 1),
(13, 42600, 'roza-gibridnaya-salita-rosa-hybridus-8713-42600', 1, 1),
(14, 41153, 'roza-gibridnaya-salita-rosa-hybridus-8713-41153', 1, 1),
(15, 42411, 'roza-gibridnaya-the-poets-wife-rosa-hybridus-8745-42411', 1, 1),
(356, 41841, 'roza-burgund-81-8859-41841', 1, 1),
(293, 46495, 'roza-gibridnaya-the-poets-wife-rosa-hybridus-8745-46495', 1, 1),
(20, 43004, 'roza-gibridnaya-gartnerfreude-rosa-hybridus-8746-43004', 1, 1),
(21, 41824, 'roza-gibridnaya-gartnerfreude-rosa-hybridus-8746-41824', 1, 1),
(22, 43005, 'roza-gibridnaya-gartnerfreude-rosa-hybridus-8746-43005', 1, 1),
(23, 41828, 'roza-gibridnaya-gartnerfreude-rosa-hybridus-8746-41828', 1, 1),
(24, 41827, 'roza-gibridnaya-gartnerfreude-rosa-hybridus-8746-41827', 1, 1),
(601, 46118, 'roza-lavaglut-10416-46118', 1, 1),
(600, 46115, 'roza-lavaglut-10416-46115', 1, 1),
(599, 46117, 'roza-lavaglut-10416-46117', 1, 1),
(598, 46116, 'roza-lavaglut-10416-46116', 1, 1),
(597, 46114, 'roza-lavaglut-10416-46114', 1, 1),
(31, 41820, 'roza-gibridnaya-robusta-rosa-rugosa-8854-41820', 1, 1),
(32, 41912, 'roza-gibridnaya-robusta-rosa-rugosa-8854-41912', 1, 1),
(33, 41649, 'roza-gibridnaya-robusta-rosa-rugosa-8854-41649', 1, 1),
(34, 41822, 'roza-gibridnaya-robusta-rosa-rugosa-8854-41822', 1, 1),
(35, 41648, 'roza-gibridnaya-robusta-rosa-rugosa-8854-41648', 1, 1),
(596, 46498, 'roza-schone-koblenzerin-10304-46498', 1, 1),
(595, 47135, 'roza-schone-koblenzerin-10304-47135', 1, 1),
(594, 46110, 'roza-schone-koblenzerin-10304-46110', 1, 1),
(593, 46109, 'roza-schone-koblenzerin-10304-46109', 1, 1),
(592, 46500, 'roza-schone-koblenzerin-10304-46500', 1, 1),
(591, 46120, 'roza-moin-moin-10303-46120', 1, 1),
(590, 46119, 'roza-moin-moin-10303-46119', 1, 1),
(589, 46188, 'roza-topaz-10112-46188', 1, 1),
(588, 46046, 'roza-florence-delattre-10109-46046', 1, 1),
(587, 46047, 'roza-florence-delattre-10109-46047', 1, 1),
(586, 46787, 'roza-florence-delattre-10109-46787', 1, 1),
(585, 46786, 'roza-florence-delattre-10109-46786', 1, 1),
(584, 46788, 'roza-florence-delattre-10109-46788', 1, 1),
(583, 46222, 'roza-sahara-10108-46222', 1, 1),
(582, 47336, 'roza-sahara-10108-47336', 1, 1),
(581, 47335, 'roza-sahara-10108-47335', 1, 1),
(580, 46224, 'roza-sahara-10108-46224', 1, 1),
(579, 46223, 'roza-sahara-10108-46223', 1, 1),
(577, 48631, 'roza-arabia-10107-48631', 1, 1),
(578, 48632, 'roza-arabia-10107-48632', 1, 1),
(576, 48630, 'roza-arabia-10107-48630', 1, 1),
(574, 46063, 'roza-arabia-10107-46063', 1, 1),
(575, 46064, 'roza-arabia-10107-46064', 1, 1),
(573, 46113, 'roza-midnight-blue-10106-46113', 1, 1),
(572, 46112, 'roza-midnight-blue-10106-46112', 1, 1),
(571, 46111, 'roza-midnight-blue-10106-46111', 1, 1),
(570, 48823, 'roza-florentina-10105-48823', 1, 1),
(569, 48822, 'roza-florentina-10105-48822', 1, 1),
(568, 48821, 'roza-florentina-10105-48821', 1, 1),
(567, 46019, 'roza-florentina-10105-46019', 1, 1),
(566, 46018, 'roza-florentina-10105-46018', 1, 1),
(565, 46304, 'roza-mona-lisa-10104-46304', 1, 1),
(564, 46303, 'roza-mona-lisa-10104-46303', 1, 1),
(563, 46122, 'roza-mona-lisa-10104-46122', 1, 1),
(562, 46121, 'roza-mona-lisa-10104-46121', 1, 1),
(561, 46295, 'roza-mona-lisa-10104-46295', 1, 1),
(560, 48709, 'roza-burgundy-ice-10103-48709', 1, 1),
(559, 48708, 'roza-burgundy-ice-10103-48708', 1, 1),
(558, 48707, 'roza-burgundy-ice-10103-48707', 1, 1),
(557, 46123, 'roza-burgundy-ice-10103-46123', 1, 1),
(556, 46124, 'roza-burgundy-ice-10103-46124', 1, 1),
(555, 48646, 'roza-ascot-10101-48646', 1, 1),
(554, 48645, 'roza-ascot-10101-48645', 1, 1),
(553, 48644, 'roza-ascot-10101-48644', 1, 1),
(552, 46130, 'roza-ascot-10101-46130', 1, 1),
(551, 46129, 'roza-ascot-10101-46129', 1, 1),
(550, 48869, 'roza-gloria-dei-peace-10100-48869', 1, 1),
(549, 46106, 'roza-gloria-dei-peace-10100-46106', 1, 1),
(547, 46105, 'roza-gloria-dei-peace-10100-46105', 1, 1),
(548, 46103, 'roza-gloria-dei-peace-10100-46103', 1, 1),
(546, 46104, 'roza-gloria-dei-peace-10100-46104', 1, 1),
(544, 46074, 'roza-micol-fontana-10074-46074', 1, 1),
(545, 46075, 'roza-micol-fontana-10074-46075', 1, 1),
(543, 46073, 'roza-micol-fontana-10074-46073', 1, 1),
(541, 46370, 'roza-micol-fontana-10074-46370', 1, 1),
(542, 46371, 'roza-micol-fontana-10074-46371', 1, 1),
(540, 46016, 'roza-hedi-grimm-10073-46016', 1, 1),
(539, 47313, 'roza-hedi-grimm-10073-47313', 1, 1),
(538, 47312, 'roza-hedi-grimm-10073-47312', 1, 1),
(537, 46017, 'roza-hedi-grimm-10073-46017', 1, 1),
(536, 46015, 'roza-hedi-grimm-10073-46015', 1, 1),
(535, 46011, 'roza-rambling-rosie-10072-46011', 1, 1),
(534, 46014, 'roza-rambling-rosie-10072-46014', 1, 1),
(533, 46013, 'roza-rambling-rosie-10072-46013', 1, 1),
(532, 46012, 'roza-rambling-rosie-10072-46012', 1, 1),
(531, 48782, 'roza-eclats-d39ambre-10071-48782', 1, 1),
(530, 46072, 'roza-eclats-d39ambre-10071-46072', 1, 1),
(529, 46071, 'roza-eclats-d39ambre-10071-46071', 1, 1),
(528, 46070, 'roza-eclats-d39ambre-10071-46070', 1, 1),
(527, 46069, 'roza-eclats-d39ambre-10071-46069', 1, 1),
(525, 46391, 'roza-old-port-10070-46391', 1, 1),
(524, 46100, 'roza-old-port-10070-46100', 1, 1),
(523, 46101, 'roza-old-port-10070-46101', 1, 1),
(829, 45959, 'roza-isabella-rossellini-9150-45959', 1, 1),
(775, 49085, 'roza-mimi-eden-8995-49085', 1, 1),
(521, 46134, 'roza-jazzz-10069-46134', 1, 1),
(311, 46939, '-46939', 3, 1),
(312, 46844, '-46844', 3, 1),
(309, 47332, '-47332', 3, 1),
(310, 47328, '-47328', 3, 1),
(520, 46133, 'roza-jazzz-10069-46133', 1, 1),
(519, 46132, 'roza-jazzz-10069-46132', 1, 1),
(518, 46131, 'roza-jazzz-10069-46131', 1, 1),
(517, 46010, 'roza-guirlande-d39amour-10067-46010', 1, 1),
(516, 46009, 'roza-guirlande-d39amour-10067-46009', 1, 1),
(515, 48616, 'roza-alberich-cl-10065-48616', 1, 1),
(514, 46023, 'roza-alberich-cl-10065-46023', 1, 1),
(513, 46026, 'roza-alberich-cl-10065-46026', 1, 1),
(512, 46025, 'roza-alberich-cl-10065-46025', 1, 1),
(511, 46024, 'roza-alberich-cl-10065-46024', 1, 1),
(510, 46221, 'roza-summer-lodge-10041-46221', 1, 1),
(509, 46068, 'roza-summer-lodge-10041-46068', 1, 1),
(508, 46067, 'roza-summer-lodge-10041-46067', 1, 1),
(507, 47133, 'roza-summer-lodge-10041-47133', 1, 1),
(506, 48692, 'roza-blue-for-you-10040-48692', 1, 1),
(505, 47406, 'roza-blue-for-you-10040-47406', 1, 1),
(504, 47408, 'roza-blue-for-you-10040-47408', 1, 1),
(503, 46102, 'roza-blue-for-you-10040-46102', 1, 1),
(502, 47407, 'roza-blue-for-you-10040-47407', 1, 1),
(501, 46505, 'roza-eye-paint-10038-46505', 1, 1),
(500, 46504, 'roza-eye-paint-10038-46504', 1, 1),
(499, 46128, 'roza-eye-paint-10038-46128', 1, 1),
(498, 46127, 'roza-eye-paint-10038-46127', 1, 1),
(497, 46126, 'roza-eye-paint-10038-46126', 1, 1),
(496, 46066, 'roza-robin-hood-10037-46066', 1, 1),
(495, 46065, 'roza-robin-hood-10037-46065', 1, 1),
(494, 46297, 'roza-robin-hood-10037-46297', 1, 1),
(493, 46296, 'roza-robin-hood-10037-46296', 1, 1),
(492, 46077, 'roza-sibelius-10036-46077', 1, 1),
(491, 46076, 'roza-sibelius-10036-46076', 1, 1),
(751, 49017, 'roza-jubilee-du-prince-de-monaco-8708-49017', 1, 1),
(490, 46849, 'roza-sibelius-10036-46849', 1, 1),
(489, 48742, 'roza-claire-austin-9152-48742', 1, 1),
(488, 46210, 'roza-claire-austin-9152-46210', 1, 1),
(802, 49325, 'roza-souvenir-du-docteur-jamain-8967-49325', 1, 1),
(485, 46331, 'roza-princess-anne-9151-46331', 1, 1),
(484, 45929, 'roza-princess-anne-9151-45929', 1, 1),
(483, 48861, 'roza-gertrude-jekyll-9149-48861', 1, 1),
(150, 42866, 'iris-borodatyy-nazvanie-uteryano-iris-germanica-l-5732-42866', 1, 1),
(151, 10193, 'iris-borodatyy-nazvanie-uteryano-iris-germanica-l-5732-10193', 1, 1),
(152, 37295, 'iris-borodatyy-nazvanie-uteryano-iris-germanica-l-5732-37295', 1, 1),
(153, 10192, 'iris-borodatyy-nazvanie-uteryano-iris-germanica-l-5732-10192', 1, 1),
(154, 42865, 'iris-borodatyy-nazvanie-uteryano-iris-germanica-l-5732-42865', 1, 1),
(482, 48860, 'roza-gertrude-jekyll-9149-48860', 1, 1),
(481, 45934, 'roza-gertrude-jekyll-9149-45934', 1, 1),
(480, 46465, 'roza-gertrude-jekyll-9149-46465', 1, 1),
(479, 45933, 'roza-gertrude-jekyll-9149-45933', 1, 1),
(478, 45928, 'roza-l-d-braithwaite-9144-45928', 1, 1),
(477, 45927, 'roza-l-d-braithwaite-9144-45927', 1, 1),
(476, 45926, 'roza-l-d-braithwaite-9144-45926', 1, 1),
(475, 46337, 'roza-l-d-braithwaite-9144-46337', 1, 1),
(474, 45936, 'roza-winchester-cathedral-9143-45936', 1, 1),
(473, 45935, 'roza-winchester-cathedral-9143-45935', 1, 1),
(472, 48908, 'roza-graham-thomas-9142-48908', 1, 1),
(471, 48907, 'roza-graham-thomas-9142-48907', 1, 1),
(470, 48906, 'roza-graham-thomas-9142-48906', 1, 1),
(469, 48905, 'roza-graham-thomas-9142-48905', 1, 1),
(468, 45932, 'roza-graham-thomas-9142-45932', 1, 1),
(467, 46209, 'roza-reine-des-violettes-8998-46209', 1, 1),
(466, 46930, 'roza-reine-des-violettes-8998-46930', 1, 1),
(465, 46929, 'roza-reine-des-violettes-8998-46929', 1, 1),
(464, 46928, 'roza-reine-des-violettes-8998-46928', 1, 1),
(463, 47636, 'roza-petite-de-hollande-8997-47636', 1, 1),
(462, 46208, 'roza-mimi-eden-8995-46208', 1, 1),
(461, 46207, 'roza-mimi-eden-8995-46207', 1, 1),
(460, 47637, 'roza-duchesse-de-cambridge-8990-47637', 1, 1),
(459, 46268, 'roza-duchesse-de-cambridge-8990-46268', 1, 1),
(179, 10421, 'roza-gibridnaya-chinatown-rosa-hybridus-5796-10421', 1, 1),
(180, 42955, 'roza-gibridnaya-chinatown-rosa-hybridus-5796-42955', 1, 1),
(181, 42998, 'roza-gibridnaya-chinatown-rosa-hybridus-5796-42998', 1, 1),
(182, 42999, 'roza-gibridnaya-chinatown-rosa-hybridus-5796-42999', 1, 1),
(183, 42710, 'roza-gibridnaya-chinatown-rosa-hybridus-5796-42710', 1, 1),
(458, 46267, 'roza-duchesse-de-cambridge-8990-46267', 1, 1),
(457, 46184, 'roza-paul-neyron-8989-46184', 1, 1),
(456, 46183, 'roza-paul-neyron-8989-46183', 1, 1),
(455, 46852, 'roza-paul-neyron-8989-46852', 1, 1),
(454, 45985, 'roza-mrs-john-laing-8977-45985', 1, 1),
(453, 45984, 'roza-mrs-john-laing-8977-45984', 1, 1),
(452, 47368, 'roza-mrs-john-laing-8977-47368', 1, 1),
(451, 47367, 'roza-mrs-john-laing-8977-47367', 1, 1),
(450, 45894, 'roza-tranquillity-8970-45894', 1, 1),
(449, 45893, 'roza-tranquillity-8970-45893', 1, 1),
(448, 45892, 'roza-tranquillity-8970-45892', 1, 1),
(447, 46461, 'roza-tuscany-old-velvet-8969-46461', 1, 1),
(446, 46460, 'roza-tuscany-old-velvet-8969-46460', 1, 1),
(445, 46423, 'roza-tuscany-old-velvet-8969-46423', 1, 1),
(444, 45983, 'roza-souvenir-du-docteur-jamain-8967-45983', 1, 1),
(443, 46422, 'roza-cardinal-de-richelieu-8966-46422', 1, 1),
(442, 41846, 'roza-les-quatre-saisons-8858-41846', 1, 1),
(441, 41869, 'roza-les-quatre-saisons-8858-41869', 1, 1),
(440, 42419, 'roza-les-quatre-saisons-8858-42419', 1, 1),
(439, 41866, 'roza-les-quatre-saisons-8858-41866', 1, 1),
(438, 46924, 'roza-les-quatre-saisons-8858-46924', 1, 1),
(437, 43003, 'roza-james-galway-8856-43003', 1, 1),
(436, 47278, 'roza-james-galway-8856-47278', 1, 1),
(435, 47277, 'roza-james-galway-8856-47277', 1, 1),
(434, 42519, 'roza-james-galway-8856-42519', 1, 1),
(433, 48917, 'roza-grande-amore-8747-48917', 1, 1),
(432, 48916, 'roza-grande-amore-8747-48916', 1, 1),
(431, 48915, 'roza-grande-amore-8747-48915', 1, 1),
(430, 48914, 'roza-grande-amore-8747-48914', 1, 1),
(429, 48899, 'roza-grace-8744-48899', 1, 1),
(428, 48898, 'roza-grace-8744-48898', 1, 1),
(427, 48897, 'roza-grace-8744-48897', 1, 1),
(426, 48896, 'roza-grace-8744-48896', 1, 1),
(425, 42480, 'roza-grace-8744-42480', 1, 1),
(424, 42410, 'roza-lady-of-shalott-8743-42410', 1, 1),
(423, 42415, 'roza-lady-of-shalott-8743-42415', 1, 1),
(422, 42408, 'roza-lady-of-shalott-8743-42408', 1, 1),
(421, 46339, 'roza-lady-of-shalott-8743-46339', 1, 1),
(420, 46338, 'roza-lady-of-shalott-8743-46338', 1, 1),
(804, 49327, 'roza-souvenir-du-docteur-jamain-8967-49327', 1, 1),
(418, 47645, 'roza-olivia-rose-austin-8742-47645', 1, 1),
(417, 42399, 'roza-olivia-rose-austin-8742-42399', 1, 1),
(416, 42413, 'roza-olivia-rose-austin-8742-42413', 1, 1),
(415, 42395, 'roza-olivia-rose-austin-8742-42395', 1, 1),
(414, 43002, 'roza-rosarium-uetersen-8714-43002', 1, 1),
(413, 47279, 'roza-rosarium-uetersen-8714-47279', 1, 1),
(412, 42596, 'roza-rosarium-uetersen-8714-42596', 1, 1),
(411, 47280, 'roza-rosarium-uetersen-8714-47280', 1, 1),
(410, 43008, 'roza-pierre-arditi-8710-43008', 1, 1),
(409, 46942, 'roza-pierre-arditi-8710-46942', 1, 1),
(408, 46926, 'roza-chippendale-8707-46926', 1, 1),
(407, 46925, 'roza-chippendale-8707-46925', 1, 1),
(406, 42518, 'roza-chippendale-8707-42518', 1, 1),
(405, 42517, 'roza-chippendale-8707-42517', 1, 1),
(404, 43011, 'roza-chippendale-8707-43011', 1, 1),
(403, 41834, 'roza-leonardo-da-vinci-8706-41834', 1, 1),
(402, 42470, 'roza-leonardo-da-vinci-8706-42470', 1, 1),
(401, 42422, 'roza-leonardo-da-vinci-8706-42422', 1, 1),
(400, 41870, 'roza-leonardo-da-vinci-8706-41870', 1, 1),
(398, 46051, 'roza-frontenac-10474-46051', 1, 1),
(397, 46513, 'roza-violette-parfumee-10439-46513', 1, 1),
(396, 46512, 'roza-violette-parfumee-10439-46512', 1, 1),
(395, 46511, 'roza-violette-parfumee-10439-46511', 1, 1),
(394, 46245, 'roza-violette-parfumee-10439-46245', 1, 1),
(393, 46187, 'roza-wild-blue-yonder-10419-46187', 1, 1),
(392, 46279, 'roza-wild-blue-yonder-10419-46279', 1, 1),
(391, 46278, 'roza-wild-blue-yonder-10419-46278', 1, 1),
(390, 46186, 'roza-wild-blue-yonder-10419-46186', 1, 1),
(389, 46185, 'roza-wild-blue-yonder-10419-46185', 1, 1),
(388, 46125, 'roza-kerio-10102-46125', 1, 1),
(387, 46029, 'roza-henry-kelsey-9911-46029', 1, 1),
(386, 46028, 'roza-henry-kelsey-9911-46028', 1, 1),
(385, 48608, 'roza-adelaide-hoodless-9910-48608', 1, 1),
(384, 46044, 'roza-adelaide-hoodless-9910-46044', 1, 1),
(383, 47639, 'roza-adelaide-hoodless-9910-47639', 1, 1),
(399, 41832, 'roza-leonardo-da-vinci-8706-41832', 1, 1),
(382, 46042, 'roza-adelaide-hoodless-9910-46042', 1, 1),
(381, 46043, 'roza-adelaide-hoodless-9910-46043', 1, 1),
(379, 46033, 'roza-george-vancouver-9909-46033', 1, 1),
(380, 47641, 'roza-george-vancouver-9909-47641', 1, 1),
(377, 47416, 'roza-george-vancouver-9909-47416', 1, 1),
(378, 47417, 'roza-george-vancouver-9909-47417', 1, 1),
(375, 46847, 'roza-prairie-joy-9908-46847', 1, 1),
(376, 46034, 'roza-george-vancouver-9909-46034', 1, 1),
(374, 46225, 'roza-prairie-joy-9908-46225', 1, 1),
(373, 46203, 'roza-prairie-joy-9908-46203', 1, 1),
(372, 46202, 'roza-prairie-joy-9908-46202', 1, 1),
(371, 46035, 'roza-morden-centennial-9907-46035', 1, 1),
(370, 46045, 'roza-john-cabot-9906-46045', 1, 1),
(369, 47409, 'roza-john-cabot-9906-47409', 1, 1),
(368, 48831, 'roza-frontenac-9905-48831', 1, 1),
(367, 48830, 'roza-frontenac-9905-48830', 1, 1),
(366, 47640, 'roza-frontenac-9905-47640', 1, 1),
(365, 46814, 'roza-frontenac-9905-46814', 1, 1),
(364, 46036, 'roza-frontenac-9905-46036', 1, 1),
(363, 46032, 'roza-morden-amorette-9904-46032', 1, 1),
(362, 46030, 'roza-morden-amorette-9904-46030', 1, 1),
(361, 47582, 'roza-morden-amorette-9904-47582', 1, 1),
(360, 46031, 'roza-morden-amorette-9904-46031', 1, 1),
(359, 46332, 'roza-burgund-81-8859-46332', 1, 1),
(358, 46802, 'roza-burgund-81-8859-46802', 1, 1),
(357, 46801, 'roza-burgund-81-8859-46801', 1, 1),
(292, 46346, 'roza-gibridnaya-the-poets-wife-rosa-hybridus-8745-46346', 1, 1),
(294, 46496, 'roza-gibridnaya-the-poets-wife-rosa-hybridus-8745-46496', 1, 1),
(295, 46497, 'roza-gibridnaya-the-poets-wife-rosa-hybridus-8745-46497', 1, 1),
(355, 46375, 'roza-burgund-81-8859-46375', 1, 1),
(303, 46915, 'roza-gibridnaya-gebruder-grimm-rosa-hybridus-8712-46915', 1, 1),
(304, 46916, 'roza-gibridnaya-gebruder-grimm-rosa-hybridus-8712-46916', 1, 1),
(305, 46917, 'roza-gibridnaya-gebruder-grimm-rosa-hybridus-8712-46917', 1, 1),
(306, 46918, 'roza-gibridnaya-gebruder-grimm-rosa-hybridus-8712-46918', 1, 1),
(317, 46492, '-46492', 3, 1),
(318, 46487, '-46487', 3, 1),
(319, 46479, '-46479', 3, 1),
(320, 46472, '-46472', 3, 1),
(321, 46467, '-46467', 3, 1),
(322, 46463, '-46463', 3, 1),
(323, 46457, '-46457', 3, 1),
(324, 46455, '-46455', 3, 1),
(325, 46452, '-46452', 3, 1),
(326, 46445, '-46445', 3, 1),
(327, 46443, '-46443', 3, 1),
(328, 46430, '-46430', 3, 1),
(329, 46428, '-46428', 3, 1),
(330, 46424, '-46424', 3, 1),
(331, 46423, '-46423', 3, 1),
(332, 46422, '-46422', 3, 1),
(333, 46418, '-46418', 3, 1),
(334, 46415, '-46415', 3, 1),
(335, 47336, '-47336', 3, 1),
(336, 47401, '-47401', 3, 1),
(337, 47409, '-47409', 3, 1),
(339, 47514, '-47514', 3, 1),
(345, 47678, 'roza-briosa-3-47678', 4, 1),
(346, 47677, 'roza-briosa-3-47677', 4, 1),
(347, 47543, 'iris-mechevidnyy-beringiya-2-47543', 4, 1),
(348, 47542, 'iris-mechevidnyy-beringiya-2-47542', 4, 1),
(349, 47530, 'iris-pseudata-kurokawa-noh-1-47530', 4, 1),
(350, 46165, 'roza-grande-amore-8747-46165', 1, 1),
(351, 38778, 'iris-bolotnyy-bastardii-iris-pseudacorus-var-bastardii-7063-38778', 1, 1),
(352, 39127, 'iris-bolotnyy-bastardii-iris-pseudacorus-var-bastardii-7063-39127', 1, 1),
(353, 39128, 'iris-bolotnyy-bastardii-iris-pseudacorus-var-bastardii-7063-39128', 1, 1),
(354, 39129, 'iris-bolotnyy-bastardii-iris-pseudacorus-var-bastardii-7063-39129', 1, 1),
(604, 46090, 'roza-magenta-10417-46090', 1, 1),
(605, 46091, 'roza-magenta-10417-46091', 1, 1),
(606, 47413, 'roza-hommage-a-barbara-10418-47413', 1, 1),
(607, 46092, 'roza-hommage-a-barbara-10418-46092', 1, 1),
(608, 46093, 'roza-hommage-a-barbara-10418-46093', 1, 1),
(609, 47414, 'roza-hommage-a-barbara-10418-47414', 1, 1),
(610, 47415, 'roza-hommage-a-barbara-10418-47415', 1, 1),
(611, 46272, 'roza-pomponella-10420-46272', 1, 1),
(612, 46086, 'roza-pomponella-10420-46086', 1, 1),
(613, 46087, 'roza-pomponella-10420-46087', 1, 1),
(614, 46138, 'roza-nina-weibull-10421-46138', 1, 1),
(615, 46139, 'roza-nina-weibull-10421-46139', 1, 1),
(616, 46399, 'roza-nina-weibull-10421-46399', 1, 1),
(617, 46400, 'roza-nina-weibull-10421-46400', 1, 1),
(618, 46006, 'roza-momo-10424-46006', 1, 1),
(619, 46007, 'roza-momo-10424-46007', 1, 1),
(620, 46008, 'roza-momo-10424-46008', 1, 1),
(621, 47303, 'roza-momo-10424-47303', 1, 1),
(622, 46766, 'roza-ventilo-10426-46766', 1, 1),
(623, 47583, 'roza-ann-henderson-10427-47583', 1, 1),
(624, 47584, 'roza-ann-henderson-10427-47584', 1, 1),
(625, 46099, 'roza-ann-henderson-10427-46099', 1, 1),
(626, 47585, 'roza-ann-henderson-10427-47585', 1, 1),
(627, 48624, 'roza-ann-henderson-10427-48624', 1, 1),
(628, 46140, 'roza-tickled-pink-10428-46140', 1, 1),
(629, 46790, 'roza-tickled-pink-10428-46790', 1, 1),
(630, 46510, 'roza-tickled-pink-10428-46510', 1, 1),
(631, 46401, 'roza-tickled-pink-10428-46401', 1, 1),
(632, 46402, 'roza-tickled-pink-10428-46402', 1, 1),
(633, 46214, 'roza-chandos-beauty-10429-46214', 1, 1),
(634, 48715, 'roza-chandos-beauty-10429-48715', 1, 1),
(635, 48716, 'roza-chandos-beauty-10429-48716', 1, 1),
(636, 48717, 'roza-chandos-beauty-10429-48717', 1, 1),
(637, 48718, 'roza-chandos-beauty-10429-48718', 1, 1),
(638, 46353, 'roza-mainzer-fastnacht-10430-46353', 1, 1),
(639, 46351, 'roza-mainzer-fastnacht-10430-46351', 1, 1),
(640, 46352, 'roza-mainzer-fastnacht-10430-46352', 1, 1),
(641, 46147, 'roza-mainzer-fastnacht-10430-46147', 1, 1),
(642, 46284, 'roza-purple-star-10432-46284', 1, 1),
(643, 46285, 'roza-purple-star-10432-46285', 1, 1),
(644, 46143, 'roza-purple-star-10432-46143', 1, 1),
(645, 46144, 'roza-purple-star-10432-46144', 1, 1),
(803, 49326, 'roza-souvenir-du-docteur-jamain-8967-49326', 1, 1),
(647, 46094, 'roza-chantal-merieux-10434-46094', 1, 1),
(648, 46095, 'roza-chantal-merieux-10434-46095', 1, 1),
(649, 46382, 'roza-chantal-merieux-10434-46382', 1, 1),
(650, 46519, 'roza-chantal-merieux-10434-46519', 1, 1),
(651, 46403, 'roza-chantal-merieux-10434-46403', 1, 1),
(652, 47397, 'roza-prieure-de-st-cosme-10435-47397', 1, 1),
(653, 46082, 'roza-prieure-de-st-cosme-10435-46082', 1, 1),
(654, 46083, 'roza-prieure-de-st-cosme-10435-46083', 1, 1),
(655, 47398, 'roza-prieure-de-st-cosme-10435-47398', 1, 1),
(656, 47399, 'roza-prieure-de-st-cosme-10435-47399', 1, 1),
(657, 46204, 'roza-institut-lumiere-10437-46204', 1, 1),
(658, 46765, 'roza-institut-lumiere-10437-46765', 1, 1),
(659, 46205, 'roza-institut-lumiere-10437-46205', 1, 1),
(660, 46206, 'roza-institut-lumiere-10437-46206', 1, 1),
(661, 46764, 'roza-institut-lumiere-10437-46764', 1, 1),
(662, 46851, 'roza-lavande-parfumee-10438-46851', 1, 1),
(663, 47401, 'roza-lavande-parfumee-10438-47401', 1, 1),
(664, 46141, 'roza-lavande-parfumee-10438-46141', 1, 1),
(665, 46142, 'roza-lavande-parfumee-10438-46142', 1, 1),
(666, 46097, 'roza-dioressence-10442-46097', 1, 1),
(667, 46098, 'roza-dioressence-10442-46098', 1, 1),
(668, 46950, 'roza-dioressence-10442-46950', 1, 1),
(669, 46359, 'roza-christian-tetedoie-10443-46359', 1, 1),
(670, 47403, 'roza-christian-tetedoie-10443-47403', 1, 1),
(671, 47404, 'roza-christian-tetedoie-10443-47404', 1, 1),
(672, 46137, 'roza-christian-tetedoie-10443-46137', 1, 1),
(673, 46396, 'roza-christian-tetedoie-10443-46396', 1, 1),
(674, 46215, 'roza-scent-of-woman-10445-46215', 1, 1),
(675, 46096, 'roza-out-of-rosenheim-10446-46096', 1, 1),
(676, 46789, 'roza-out-of-rosenheim-10446-46789', 1, 1),
(677, 46145, 'roza-leah-tutu-10449-46145', 1, 1),
(678, 46146, 'roza-leah-tutu-10449-46146', 1, 1),
(679, 46287, 'roza-leah-tutu-10449-46287', 1, 1),
(680, 46311, 'roza-leah-tutu-10449-46311', 1, 1),
(774, 49124, 'roza-mrs-john-laing-8977-49124', 1, 1),
(683, 46148, 'roza-gebruder-grimm-10480-46148', 1, 1),
(684, 46217, 'roza-gartnerfreude-10481-46217', 1, 1),
(685, 46216, 'roza-gartnerfreude-10481-46216', 1, 1),
(686, 46243, 'roza-olivia-rose-austin-10486-46243', 1, 1),
(687, 46244, 'roza-olivia-rose-austin-10486-46244', 1, 1),
(688, 46298, 'roza-olivia-rose-austin-10486-46298', 1, 1),
(689, 46360, 'roza-grace-10491-46360', 1, 1),
(690, 46361, 'roza-grace-10491-46361', 1, 1),
(691, 46000, 'roza-grace-10491-46000', 1, 1),
(773, 49154, 'roza-olivia-rose-austin-8742-49154', 1, 1),
(772, 49249, 'roza-rosarium-uetersen-8714-49249', 1, 1),
(695, 46356, 'roza-artemis-10496-46356', 1, 1),
(696, 46357, 'roza-artemis-10496-46357', 1, 1),
(697, 46355, 'roza-artemis-10496-46355', 1, 1),
(698, 46761, 'roza-artemis-10496-46761', 1, 1),
(699, 45994, 'roza-artemis-10496-45994', 1, 1),
(700, 46213, 'roza-aspirin-rose-10497-46213', 1, 1),
(701, 46212, 'roza-aspirin-rose-10497-46212', 1, 1),
(702, 48648, 'roza-aspirin-rose-10497-48648', 1, 1),
(703, 48649, 'roza-aspirin-rose-10497-48649', 1, 1),
(704, 48650, 'roza-aspirin-rose-10497-48650', 1, 1),
(705, 46812, 'roza-ebb-tide-10498-46812', 1, 1),
(706, 47328, 'roza-ebb-tide-10498-47328', 1, 1),
(707, 47329, 'roza-ebb-tide-10498-47329', 1, 1),
(708, 46813, 'roza-ebb-tide-10498-46813', 1, 1),
(709, 48778, 'roza-ebb-tide-10498-48778', 1, 1),
(710, 46170, 'roza-huddersfield-choral-society-10499-46170', 1, 1),
(711, 46171, 'roza-huddersfield-choral-society-10499-46171', 1, 1),
(712, 46172, 'roza-huddersfield-choral-society-10499-46172', 1, 1),
(713, 46059, 'roza-huddersfield-choral-society-10499-46059', 1, 1),
(714, 46060, 'roza-huddersfield-choral-society-10499-46060', 1, 1),
(715, 46354, 'roza-julia-child-10500-46354', 1, 1),
(716, 46151, 'roza-julia-child-10500-46151', 1, 1),
(717, 47400, 'roza-julia-child-10500-47400', 1, 1),
(718, 46192, 'roza-mokarosa-10501-46192', 1, 1),
(719, 46193, 'roza-mokarosa-10501-46193', 1, 1),
(720, 46328, 'roza-mokarosa-10501-46328', 1, 1),
(721, 46149, 'roza-nimbus-10503-46149', 1, 1),
(722, 46235, 'roza-nimbus-10503-46235', 1, 1),
(723, 46150, 'roza-nimbus-10503-46150', 1, 1),
(724, 46282, 'roza-nimbus-10503-46282', 1, 1),
(725, 47330, 'roza-nimbus-10503-47330', 1, 1),
(726, 46055, 'roza-distant-drums-10504-46055', 1, 1),
(727, 46056, 'roza-distant-drums-10504-46056', 1, 1),
(728, 46324, 'roza-distant-drums-10504-46324', 1, 1),
(729, 46325, 'roza-distant-drums-10504-46325', 1, 1),
(730, 46326, 'roza-distant-drums-10504-46326', 1, 1),
(731, 47560, 'roza-lea-mege-10505-47560', 1, 1),
(732, 46194, 'roza-lea-mege-10505-46194', 1, 1),
(733, 46316, 'roza-lea-mege-10505-46316', 1, 1),
(734, 46317, 'roza-lea-mege-10505-46317', 1, 1),
(735, 46318, 'roza-lea-mege-10505-46318', 1, 1),
(736, 46198, 'roza-rhapsody-in-blue-10506-46198', 1, 1),
(737, 46199, 'roza-rhapsody-in-blue-10506-46199', 1, 1),
(738, 46200, 'roza-rhapsody-in-blue-10506-46200', 1, 1),
(739, 47581, 'roza-rhapsody-in-blue-10506-47581', 1, 1),
(740, 46368, 'roza-felicia-10516-46368', 1, 1),
(741, 46369, 'roza-felicia-10516-46369', 1, 1),
(742, 46246, 'roza-felicia-10516-46246', 1, 1),
(743, 47315, 'roza-felicia-10516-47315', 1, 1),
(744, 48805, 'roza-felicia-10516-48805', 1, 1),
(745, 46274, 'roza-mozart-10517-46274', 1, 1),
(746, 46273, 'roza-mozart-10517-46273', 1, 1),
(747, 47304, 'roza-mozart-10517-47304', 1, 1),
(748, 47305, 'roza-mozart-10517-47305', 1, 1),
(749, 47501, 'roza-green-ice-10551-47501', 1, 1),
(750, 47502, 'roza-green-ice-10551-47502', 1, 1),
(752, 49018, 'roza-jubilee-du-prince-de-monaco-8708-49018', 1, 1),
(753, 48993, 'roza-james-galway-8856-48993', 1, 1),
(754, 49037, 'roza-l-d-braithwaite-9144-49037', 1, 1),
(755, 49011, 'roza-john-cabot-9906-49011', 1, 1),
(756, 49012, 'roza-john-cabot-9906-49012', 1, 1),
(771, 49167, 'roza-pierre-arditi-8710-49167', 1, 1),
(757, 48950, 'roza-henry-kelsey-9911-48950', 1, 1),
(758, 48951, 'roza-henry-kelsey-9911-48951', 1, 1),
(759, 48919, 'roza-guirlande-d39amour-10067-48919', 1, 1),
(760, 48920, 'roza-guirlande-d39amour-10067-48920', 1, 1),
(761, 48921, 'roza-guirlande-d39amour-10067-48921', 1, 1),
(762, 46394, 'roza-jazzz-10069-46394', 1, 1),
(763, 49078, 'roza-midnight-blue-10106-49078', 1, 1),
(764, 49079, 'roza-midnight-blue-10106-49079', 1, 1),
(765, 49065, 'roza-magenta-10417-49065', 1, 1),
(766, 49070, 'roza-mainzer-fastnacht-10430-49070', 1, 1),
(767, 49055, 'roza-lavande-parfumee-10438-49055', 1, 1),
(768, 49060, 'roza-leah-tutu-10449-49060', 1, 1),
(769, 49024, 'roza-julia-child-10500-49024', 1, 1),
(770, 49025, 'roza-julia-child-10500-49025', 1, 1),
(778, 49202, 'roza-princess-anne-9151-49202', 1, 1),
(779, 49203, 'roza-princess-anne-9151-49203', 1, 1),
(780, 49107, 'roza-morden-amorette-9904-49107', 1, 1),
(781, 49114, 'roza-morden-centennial-9907-49114', 1, 1),
(782, 49115, 'roza-morden-centennial-9907-49115', 1, 1),
(783, 49116, 'roza-morden-centennial-9907-49116', 1, 1),
(784, 49117, 'roza-morden-centennial-9907-49117', 1, 1),
(785, 49181, 'roza-prairie-joy-9908-49181', 1, 1),
(801, 49272, 'roza-salita-8713-49272', 1, 1),
(786, 49242, 'roza-robin-hood-10037-49242', 1, 1),
(787, 49152, 'roza-old-port-10070-49152', 1, 1),
(788, 49153, 'roza-old-port-10070-49153', 1, 1),
(789, 49216, 'roza-rambling-rosie-10072-49216', 1, 1),
(790, 49174, 'roza-pomponella-10420-49174', 1, 1),
(791, 49175, 'roza-pomponella-10420-49175', 1, 1),
(792, 49097, 'roza-momo-10424-49097', 1, 1),
(793, 49212, 'roza-purple-star-10432-49212', 1, 1),
(794, 49160, 'roza-out-of-rosenheim-10446-49160', 1, 1),
(795, 49161, 'roza-out-of-rosenheim-10446-49161', 1, 1),
(796, 49162, 'roza-out-of-rosenheim-10446-49162', 1, 1),
(797, 49091, 'roza-mokarosa-10501-49091', 1, 1),
(798, 49092, 'roza-mokarosa-10501-49092', 1, 1),
(799, 49226, 'roza-rhapsody-in-blue-10506-49226', 1, 1),
(800, 49121, 'roza-mozart-10517-49121', 1, 1),
(805, 49328, 'roza-souvenir-du-docteur-jamain-8967-49328', 1, 1),
(816, 49359, 'roza-tuscany-old-velvet-8969-49359', 1, 1),
(806, 49293, 'roza-sibelius-10036-49293', 1, 1),
(807, 49294, 'roza-sibelius-10036-49294', 1, 1),
(808, 49331, 'roza-summer-lodge-10041-49331', 1, 1),
(809, 49283, 'roza-moin-moin-10303-49283', 1, 1),
(810, 49284, 'roza-moin-moin-10303-49284', 1, 1),
(811, 49285, 'roza-moin-moin-10303-49285', 1, 1),
(812, 49279, 'roza-scent-of-woman-10445-49279', 1, 1),
(813, 49280, 'roza-scent-of-woman-10445-49280', 1, 1),
(814, 49281, 'roza-scent-of-woman-10445-49281', 1, 1),
(815, 49282, 'roza-scent-of-woman-10445-49282', 1, 1),
(817, 49360, 'roza-tuscany-old-velvet-8969-49360', 1, 1),
(818, 49354, 'roza-tranquillity-8970-49354', 1, 1),
(819, 49355, 'roza-tranquillity-8970-49355', 1, 1),
(820, 49378, 'roza-winchester-cathedral-9143-49378', 1, 1),
(821, 49379, 'roza-winchester-cathedral-9143-49379', 1, 1),
(822, 49380, 'roza-winchester-cathedral-9143-49380', 1, 1),
(824, 45978, 'roza-zaide-8987-45978', 1, 1),
(823, 49364, 'roza-violette-parfumee-10439-49364', 1, 1),
(825, 45979, 'roza-zaide-8987-45979', 1, 1),
(826, 45980, 'roza-zaide-8987-45980', 1, 1),
(827, 49390, 'roza-zaide-8987-49390', 1, 1),
(828, 49391, 'roza-zaide-8987-49391', 1, 1),
(830, 48984, 'roza-isabella-rossellini-9150-48984', 1, 1),
(831, 48985, 'roza-isabella-rossellini-9150-48985', 1, 1),
(832, 48986, 'roza-isabella-rossellini-9150-48986', 1, 1),
(833, 48987, 'roza-isabella-rossellini-9150-48987', 1, 1),
(834, 47678, 'roza-briosa-8972-47678', 1, 1),
(835, 45923, 'roza-briosa-8972-45923', 1, 1),
(836, 47677, 'roza-briosa-8972-47677', 1, 1),
(837, 45924, 'roza-briosa-8972-45924', 1, 1),
(838, 48701, 'roza-briosa-8972-48701', 1, 1),
(839, 49403, 'floribunda-49403', 2, 1),
(840, 49404, 'chayno-gibridnye-49404', 2, 1),
(841, 49396, 'angliyskie-49396', 2, 1),
(842, 49405, 'shraby-49405', 2, 1),
(843, 49399, 'neukryvnye-49399', 2, 1),
(844, 49400, 'pletistye-49400', 2, 1),
(845, 49397, 'kanadskie-49397', 2, 1),
(846, 49398, 'muskusnye-49398', 2, 1),
(847, 49401, 'pochvopokrovnye-49401', 2, 1),
(848, 49402, 'starinnye-49402', 2, 1),
(849, 49406, 'drugie-rasteniya-49406', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL COMMENT 'Дата создания новости',
  `image_id` bigint(20) unsigned NOT NULL COMMENT 'thlaspi_id картинки',
  `chpu` text NOT NULL COMMENT 'ЧПУ',
  `title_meta` text NOT NULL COMMENT 'Title',
  `descr` text NOT NULL COMMENT 'Description',
  `header` text NOT NULL COMMENT 'Заголовок новости',
  `text` text NOT NULL COMMENT 'Текст новости',
  `thlaspi_id` bigint(20) NOT NULL COMMENT 'id новости на сайте thaspi.com Этого id может и не быть, ведь новости могут создаваться не только на сайте thlaspi',
  `edit_label` tinyint(1) NOT NULL DEFAULT '2' COMMENT 'Если =2 - пересоздавать новость не требуется. А если =3 - страницу с новостью нао пересоздать'
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Новости на сайте';

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `date`, `image_id`, `chpu`, `title_meta`, `descr`, `header`, `text`, `thlaspi_id`, `edit_label`) VALUES
(1, '2021-12-30 19:49:38', 47401, 'lavande-parfumee', 'Lavande Parfumee', 'Выглядит двухцветной и такие плавные акварельные переливы цвета!', 'Lavande Parfumee', 'Выглядит двухцветной и такие плавные акварельные переливы цвета!', 28, 2),
(2, '2021-12-18 13:13:49', 47336, 'roza-sahara', 'Роза Sahara', 'Издалека выглядит оранжевой', 'Роза Sahara', 'Издалека выглядит оранжевой', 27, 2),
(3, '2021-12-16 18:11:35', 47332, 'roza-gentle-hermione', 'Роза Gentle Hermione', '27 июля уже не такая нежная, вполне себе зрелый румянец.', 'Роза Gentle Hermione', '27 июля уже не такая нежная, вполне себе зрелый румянец.', 26, 2),
(4, '2021-12-14 22:55:07', 47328, 'roza-gibridnaya-ebb-tide', 'Роза гибридная Ebb Tide', 'роскошный', 'Роза гибридная Ebb Tide', 'роскошный', 25, 2),
(5, '2021-11-13 11:01:11', 46939, 'belamkanda-kitayskaya-belamcanda-chinensis', 'Беламканда китайская - Belamcanda chinensis', 'Расцвела у нас впервые. Любит песчаную почву.', 'Беламканда китайская - Belamcanda chinensis', 'Расцвела у нас впервые. Любит песчаную почву.', 24, 2),
(6, '2021-11-08 11:18:54', 46844, 'roza-royal-jubilee', 'Роза Royal Jubilee', 'Ничего особенного - просто изящная симпатичная роза', 'Роза Royal Jubilee', 'Ничего особенного - просто изящная симпатичная роза', 23, 2),
(7, '2021-11-06 12:40:05', 46769, 'knyajik-frances-rivis', 'Княжик Frances Rivis', 'удивительный, плохо передаваемый на фото цвет', 'Княжик Frances Rivis', 'удивительный, плохо передаваемый на фото цвет', 22, 2),
(8, '2021-11-05 13:34:25', 46694, 'vpervye-rascvyol-u-nas-beetroot', 'Впервые расцвёл у нас Beetroot', 'Очень красивый', 'Впервые расцвёл у нас Beetroot', 'Очень красивый', 21, 2),
(9, '2021-11-01 21:36:40', 46612, 'clara-ellen-iris-spuria', 'Clara Ellen - Iris spuria', 'Очень красивый', 'Clara Ellen - Iris spuria', 'Очень красивый', 20, 2),
(10, '2021-10-25 16:00:37', 46494, 'klematis-melodiya', 'Клематис Мелодия', 'Таинственные оттенки', 'Клематис Мелодия', 'Таинственные оттенки', 19, 2),
(11, '2021-10-24 11:22:49', 46492, 'lupinaster-pacificus-bobrov-latsch', 'Lupinaster pacificus (Bobrov) Latsch.', 'Люпинастер немного похож на клевер', 'Lupinaster pacificus (Bobrov) Latsch.', 'Люпинастер немного похож на клевер', 18, 2),
(12, '2021-10-23 12:28:38', 46487, 'rotes-phaenomen', 'Rotes Phaenomen', 'Растёт и цветёт у нас уже год. За это время красивого цветка не видали. Но цвет совершенно потрясающий. Ну и название тоже доставляет. Неукрывная ругоза. Ждём когда она проявит себя.', 'Rotes Phaenomen', 'Растёт и цветёт у нас уже год. За это время красивого цветка не видали. Но цвет совершенно потрясающий. Ну и название тоже доставляет. Неукрывная ругоза. Ждём когда она проявит себя.', 17, 2),
(13, '2021-10-22 19:56:46', 46479, 'knyajik-yutta', 'Княжик Ютта', 'Цветёт всё лето волнами', 'Княжик Ютта', 'Цветёт всё лето волнами', 16, 2),
(14, '2021-10-20 09:49:35', 46472, 'klematis-34alyonushka34', 'Клематис &#34;Алёнушка&#34;', 'Создаёт пятна, сплошь покрытые цветами', 'Клематис &#34;Алёнушка&#34;', 'Создаёт пятна, сплошь покрытые цветами', 15, 2),
(15, '2021-10-20 09:44:44', 46467, 'hrizantema-zolotaya-osen', 'Хризантема Золотая осень', 'В этом году почти не поцвела - морозы', 'Хризантема Золотая осень', 'В этом году почти не поцвела - морозы', 14, 2),
(16, '2021-10-19 09:34:36', 46463, 'wartburg', 'Wartburg', 'Первое цветение довольно нелепое, но всё равно цветы изящные', 'Wartburg', 'Первое цветение довольно нелепое, но всё равно цветы изящные', 13, 2),
(17, '2021-10-18 10:12:37', 46457, 'ritausma', 'Ritausma', 'Белая ругоза. Неукрывная.', 'Ritausma', 'Белая ругоза. Неукрывная.', 12, 2),
(18, '2021-10-17 15:31:14', 46455, 'golden-age', 'Golden age', 'В этом году ему повредила влага, но всё равно этот цвет ни с чем не сравним', 'Golden age', 'В этом году ему повредила влага, но всё равно этот цвет ни с чем не сравним', 11, 2),
(19, '2021-10-17 15:22:05', 46452, 'iris-raznocvetnyy-versilaev-princess-iris-versicolor', 'Ирис разноцветный Versilaev Princess - Iris versicolor', 'Расцвёл впервые в этом году', 'Ирис разноцветный Versilaev Princess - Iris versicolor', 'Расцвёл впервые в этом году', 10, 2),
(20, '2021-10-16 18:42:10', 46445, 'i-c-stars', 'I C Stars', 'Расцвёл впервые в этом году. Очень красивый!', 'I C Stars', 'Расцвёл впервые в этом году. Очень красивый!', 9, 2),
(21, '2021-10-16 18:29:08', 46443, 'cape-cod-boys-nepohoj-na-sebya', 'Cape cod boys непохож на себя.', 'Неужели, пересорт?', 'Cape cod boys непохож на себя.', 'Неужели, пересорт?', 8, 2),
(22, '2021-10-15 10:07:20', 46430, 'iris-versicolor-39cascade-mist39', 'Iris versicolor &#39;Cascade Mist&#39;', 'Шмель делает новые сорта', 'Iris versicolor &#39;Cascade Mist&#39;', 'Шмель делает новые сорта', 7, 2),
(23, '2021-10-14 09:50:50', 46428, 'geschwinds-orden', 'Geschwinds orden', 'Должна быть неукрывной. Интересно, будет ли выдерживать морозы?', 'Geschwinds orden', 'Должна быть неукрывной. Интересно, будет ли выдерживать морозы?', 6, 2),
(24, '2021-10-13 09:33:36', 46424, 'crocus-rose', 'Crocus Rose', 'Первое цветение было вот такое. Я бы сказал шокирующее. Вообще не похоже на фотографии этого сорта. Но зато потом...', 'Crocus Rose', 'Первое цветение было вот такое. Я бы сказал шокирующее. Вообще не похоже на фотографии этого сорта. Но зато потом...', 5, 2),
(25, '2021-10-11 09:21:30', 46423, 'tuscany-old-velvet', 'Tuscany (Old Velvet)', 'Очень красивая', 'Tuscany (Old Velvet)', 'Очень красивая', 4, 2),
(26, '2021-10-08 10:46:49', 46422, 'cardinal-de-richelieu', 'Cardinal de Richelieu', 'Старинная роза', 'Cardinal de Richelieu', 'Старинная роза', 3, 2),
(27, '2021-10-05 17:15:55', 46418, 'pink-grootendorst-neukryvnaya-roza', 'Pink Grootendorst - неукрывная роза', 'Гвоздика, как она есть. В отличие от многих ругоз прекрасно смотрится с большого расстояния.', 'Pink Grootendorst - неукрывная роза', 'Гвоздика, как она есть. В отличие от многих ругоз прекрасно смотрится с большого расстояния.', 2, 2),
(28, '2021-10-04 19:45:07', 46415, 'souvenir-de-ucien-massad', 'Souvenir de ucien Massad', 'Скромная и нежная', 'Souvenir de ucien Massad', 'Скромная и нежная', 1, 2),
(29, '2021-12-31 00:02:46', 47409, 'kanadka-john-cabot', 'Канадка John Cabot', 'Цветок распускается. Надеюсь, в следующем году он порадует нас обильным цветением!', 'Канадка John Cabot', 'Цветок распускается. Надеюсь, в следующем году он порадует нас обильным цветением!', 29, 2),
(30, '2022-01-17 17:44:12', 47514, 'iris-raznocvetnyy-for-jay', 'Ирис разноцветный For Jay', 'расцвёл в этом году впервые', 'Ирис разноцветный For Jay', 'расцвёл в этом году впервые', 30, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `static_pages`
--

CREATE TABLE IF NOT EXISTS `static_pages` (
  `id` int(11) unsigned NOT NULL COMMENT 'идентификатор',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'идентификатор родителя из таблицы `static_pages`',
  `count_children` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'количество вложенных страниц',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'название',
  `chpu` text NOT NULL COMMENT 'Чпу страницы',
  `title_meta` varchar(250) NOT NULL DEFAULT '' COMMENT 'заголовок окна в браузере, тег Title',
  `keywords` varchar(250) NOT NULL DEFAULT '' COMMENT 'ключевые слова, тег Keywords',
  `descr` text COMMENT 'описание, тэг Description',
  `canonical` varchar(100) NOT NULL DEFAULT '' COMMENT 'канонический тег',
  `text` longtext COMMENT 'контент',
  `act` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'показывать на сайте: 0 - нет, 1 - да',
  `date_start` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'дата начала показа',
  `date_finish` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'дата окончания показа',
  `map_no_show` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'не показывать на карте сайта: 0 - нет, 1 - да',
  `menu_no_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Показывать в меню = 1 или нет - =0',
  `low_menu_no_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Показывать в нижнем меню =1 или нет = 0',
  `changefreq` enum('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL DEFAULT 'always' COMMENT 'Changefreq для sitemap.xml',
  `priority` varchar(3) NOT NULL DEFAULT '' COMMENT 'Priority для sitemap.xml',
  `noindex` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'не индексировать: 0 - нет, 1 - да',
  `search_no_show` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'не участвует в поисковой выдаче: 0 - нет, 1 - да',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'подрядковый номер для сортировки',
  `timeedit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'время последнего изменения в формате UNIXTIME',
  `theme` varchar(50) NOT NULL DEFAULT '' COMMENT 'шаблон страницы сайта',
  `js` text COMMENT 'JS-код',
  `trash` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'запись удалена в корзину: 0 - нет, 1 - да'
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COMMENT='Страницы сайта';

--
-- Дамп данных таблицы `static_pages`
--

INSERT INTO `static_pages` (`id`, `parent_id`, `count_children`, `name`, `chpu`, `title_meta`, `keywords`, `descr`, `canonical`, `text`, `act`, `date_start`, `date_finish`, `map_no_show`, `menu_no_show`, `low_menu_no_show`, `changefreq`, `priority`, `noindex`, `search_no_show`, `sort`, `timeedit`, `theme`, `js`, `trash`) VALUES
(1, 0, 0, 'О нас', 'o-nas', 'О нас', '', 'Про наш питомник', '', 'Мы Оля и Женя, мы коллекционеры растений. На этом сайте мы продаём излишки своих растений', '1', 0, 0, '0', 1, 1, 'always', '', '1', '0', 50, 0, '', NULL, '0'),
(160, 0, 0, 'Оплата и доставка', 'delivery', 'Доставка растений', '', 'Тут описываются способы доставки купленных растений', '', 'Мы отправляем растения почтой России', '1', 0, 0, '0', 1, 1, 'always', '', '0', '0', 10, 0, '', NULL, '0'),
(161, 0, 0, 'Согласие на обработку персональных данных', 'privacy', 'Согласие на обработку персональных данных', '', 'Согласие на обработку персональных данных', '', '<div class="row mb-4">\n	<div class="col">\n		<h4><strong>Политика в отношении обработки персональных данных</strong></h4>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>1. Общие положения</h5>\n		<div class="descr">Настоящая политика обработки персональных данных составлена в соответствии с требованиями Федерального закона от 27.07.2006. №152-ФЗ «О персональных данных»  (далее - Закон о персональных данных) и определяет порядок обработки персональных данных и меры по обеспечению безопасности персональных данных, предпринимаемые <span class="link mark">Блинковым Евгением Вадимовичем</span> (далее – Оператор).</div>\n		<div class="ol">\n			<div class="li">1.1. Оператор ставит своей важнейшей целью и условием осуществления своей деятельности соблюдение прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну.</div>\n			<div class="li">1.2. Настоящая политика Оператора в отношении обработки персональных данных (далее – Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <span class="link mark">https://brosnica.ru</span>.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>2. Основные понятия, используемые в Политике</h5>\n		<div class="ol">\n			<div class="li">2.1. Автоматизированная обработка персональных данных – обработка персональных данных с помощью средств вычислительной техники.</div>\n			<div class="li">2.2. Блокирование персональных данных – временное прекращение обработки персональных данных (за исключением случаев, если обработка необходима для уточнения персональных данных).</div>\n			<div class="li">2.3. Веб-сайт – совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <span class="link mark">https://brosnica.ru</span>.</div>\n			<div class="li">2.4. Информационная система персональных данных — совокупность содержащихся в базах данных персональных данных, и обеспечивающих их обработку информационных технологий и технических средств.</div>\n			<div class="li">2.5. Обезличивание персональных данных — действия, в результате которых невозможно определить без использования дополнительной информации принадлежность персональных данных конкретному Пользователю или иному субъекту персональных данных.</div>\n			<div class="li">2.6. Обработка персональных данных – любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</div>\n			<div class="li">2.7. Оператор – государственный орган, муниципальный орган, юридическое или физическое лицо, самостоятельно или совместно с другими лицами организующие и (или) осуществляющие обработку персональных данных, а также определяющие цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными.</div>\n			<div class="li">2.8. Персональные данные – любая информация, относящаяся прямо или косвенно к определенному или определяемому Пользователю веб-сайта <span class="link mark">https://brosnica.ru</span>.</div>\n			<div class="li">2.9. Персональные данные, разрешенные субъектом персональных данных для распространения, - персональные данные, доступ неограниченного круга лиц к которым предоставлен субъектом персональных данных путем дачи согласия на обработку персональных данных, разрешенных субъектом персональных данных для распространения в порядке, предусмотренном Законом о персональных данных (далее - персональные данные, разрешенные для распространения).</div>\n			<div class="li">2.10. Пользователь – любой посетитель веб-сайта <span class="link mark">https://brosnica.ru</span>.</div>\n			<div class="li">2.11. Предоставление персональных данных – действия, направленные на раскрытие персональных данных определенному лицу или определенному кругу лиц.</div>\n			<div class="li">2.12. Распространение персональных данных – любые действия, направленные на раскрытие персональных данных неопределенному кругу лиц (передача персональных данных) или на ознакомление с персональными данными неограниченного круга лиц, в том числе обнародование персональных данных в средствах массовой информации, размещение в информационно-телекоммуникационных сетях или предоставление доступа к персональным данным каким-либо иным способом.</div>\n			<div class="li">2.13. Трансграничная передача персональных данных – передача персональных данных на территорию иностранного государства органу власти иностранного государства, иностранному физическому или иностранному юридическому лицу.</div>\n			<div class="li">2.14. Уничтожение персональных данных – любые действия, в результате которых персональные данные уничтожаются безвозвратно с невозможностью дальнейшего восстановления содержания персональных данных в информационной системе персональных данных и (или) уничтожаются материальные носители персональных данных.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>3. Основные права и обязанности Оператора</h5>\n		<div class="ol">\n			<div class="li">3.1. Оператор имеет право:</div>\n			<div class="li">– получать от субъекта персональных данных достоверные информацию и/или документы, содержащие персональные данные;</div>\n			<div class="li">– в случае отзыва субъектом персональных данных согласия на обработку персональных данных Оператор вправе продолжить обработку персональных данных без согласия субъекта персональных данных при наличии оснований, указанных в Законе о персональных данных;</div>\n			<div class="li">– самостоятельно определять состав и перечень мер, необходимых и достаточных для обеспечения выполнения обязанностей, предусмотренных Законом о персональных данных и принятыми в соответствии с ним нормативными правовыми актами, если иное не предусмотрено Законом о персональных данных или другими федеральными законами.</div>\n			<div class="li">3.2. Оператор обязан:</div>\n			<div class="li">– предоставлять субъекту персональных данных по его просьбе информацию, касающуюся обработки его персональных данных;</div>\n			<div class="li">– организовывать обработку персональных данных в порядке, установленном действующим законодательством РФ;</div>\n			<div class="li">– отвечать на обращения и запросы субъектов персональных данных и их законных представителей в соответствии с требованиями Закона о персональных данных;</div>\n			<div class="li">– сообщать в уполномоченный орган по защите прав субъектов персональных данных по запросу этого органа необходимую информацию в течение 30 дней с даты получения такого запроса;</div>\n			<div class="li">– публиковать или иным образом обеспечивать неограниченный доступ к настоящей Политике в отношении обработки персональных данных;</div>\n			<div class="li">– принимать правовые, организационные и технические меры для защиты персональных данных от неправомерного или случайного доступа к ним, уничтожения, изменения, блокирования, копирования, предоставления, распространения персональных данных, а также от иных неправомерных действий в отношении персональных данных;</div>\n			<div class="li">– прекратить передачу (распространение, предоставление, доступ) персональных данных, прекратить обработку и уничтожить персональные данные в порядке и случаях, предусмотренных Законом о персональных данных;</div>\n			<div class="li">– исполнять иные обязанности, предусмотренные Законом о персональных данных.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>4. Основные права и обязанности субъектов персональных данных</h5>\n		<div class="ol">\n			<div class="li">4.1. Субъекты персональных данных имеют право:</div>\n			<div class="li">– получать информацию, касающуюся обработки его персональных данных, за исключением случаев, предусмотренных федеральными законами. Сведения предоставляются субъекту персональных данных Оператором в доступной форме, и в них не должны содержаться персональные данные, относящиеся к другим субъектам персональных данных, за исключением случаев, когда имеются законные основания для раскрытия таких персональных данных. Перечень информации и порядок ее получения установлен Законом о персональных данных;</div>\n			<div class="li">– требовать от оператора уточнения его персональных данных, их блокирования или уничтожения в случае, если персональные данные являются неполными, устаревшими, неточными, незаконно полученными или не являются необходимыми для заявленной цели обработки, а также принимать предусмотренные законом меры по защите своих прав;</div>\n			<div class="li">– выдвигать условие предварительного согласия при обработке персональных данных в целях продвижения на рынке товаров, работ и услуг;</div>\n			<div class="li">– на отзыв согласия на обработку персональных данных;</div>\n			<div class="li">– обжаловать в уполномоченный орган по защите прав субъектов персональных данных или в судебном порядке неправомерные действия или бездействие Оператора при обработке его персональных данных;</div>\n			<div class="li">– на осуществление иных прав, предусмотренных законодательством РФ.</div>\n			<div class="li">4.2. Субъекты персональных данных обязаны:</div>\n			<div class="li">– предоставлять Оператору достоверные данные о себе;</div>\n			<div class="li">– сообщать Оператору об уточнении (обновлении, изменении) своих персональных данных.</div>\n			<div class="li">4.3. Лица, передавшие Оператору недостоверные сведения о себе, либо сведения о другом субъекте персональных данных без согласия последнего, несут ответственность в соответствии с законодательством РФ.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>5. Оператор может обрабатывать следующие персональные данные Пользователя</h5>\n		<div class="ol"><div class="li">5.1. <mark>Фамилия, имя, отчество.</mark></div><div class="li">5.2. <mark>Электронный адрес.</mark></div>\n<div class="li">5.3. <mark>Номера телефонов.</mark></div>\n<div class="li">5.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</div>\n<div class="li">5.5. Вышеперечисленные данные далее по тексту Политики объединены общим понятием Персональные данные.</div>\n<div class="li">5.6. Обработка специальных категорий персональных данных, касающихся расовой, национальной принадлежности, политических взглядов, религиозных или философских убеждений, интимной жизни, Оператором не осуществляется.</div>\n<div class="li">5.7. Обработка персональных данных, разрешенных для распространения, из числа специальных категорий персональных данных, указанных в ч. 1 ст. 10 Закона о персональных данных, допускается, если соблюдаются запреты и условия, предусмотренные ст. 10.1 Закона о персональных данных.</div>\n<div class="li">5.8. Согласие Пользователя на обработку персональных данных, разрешенных для распространения, оформляется отдельно от других согласий на обработку его персональных данных. При этом соблюдаются условия, предусмотренные, в частности, ст. 10.1 Закона о персональных данных. Требования к содержанию такого согласия устанавливаются уполномоченным органом по защите прав субъектов персональных данных.</div>\n<div class="li">5.8.1 Согласие на обработку персональных данных, разрешенных для распространения, Пользователь предоставляет Оператору непосредственно.</div>\n<div class="li">5.8.2 Оператор обязан в срок не позднее трех рабочих дней с момента получения указанного согласия Пользователя опубликовать информацию об условиях обработки, о наличии запретов и условий на обработку неограниченным кругом лиц персональных данных, разрешенных для распространения.</div>\n<div class="li">5.8.3 Передача (распространение, предоставление, доступ) персональных данных, разрешенных субъектом персональных данных для распространения, должна быть прекращена в любое время по требованию субъекта персональных данных. Данное требование должно включать в себя фамилию, имя, отчество (при наличии), контактную информацию (номер телефона, адрес электронной почты или почтовый адрес) субъекта персональных данных, а также перечень персональных данных, обработка которых подлежит прекращению. Указанные в данном требовании персональные данные могут обрабатываться только Оператором, которому оно направлено.</div>\n<div class="li">5.8.4 Согласие на обработку персональных данных, разрешенных для распространения, прекращает свое действие с момента поступления Оператору требования, указанного в п. 5.8.3 настоящей Политики в отношении обработки персональных данных.</div></div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>6. Принципы обработки персональных данных</h5>\n		<div class="ol">\n			<div class="li">6.1. Обработка персональных данных осуществляется на законной и справедливой основе.</div>\n			<div class="li">6.2. Обработка персональных данных ограничивается достижением конкретных, заранее определенных и законных целей. Не допускается обработка персональных данных, несовместимая с целями сбора персональных данных.</div>\n			<div class="li">6.3. Не допускается объединение баз данных, содержащих персональные данные, обработка которых осуществляется в целях, несовместимых между собой.</div>\n			<div class="li">6.4. Обработке подлежат только персональные данные, которые отвечают целям их обработки.</div>\n			<div class="li">6.5. Содержание и объем обрабатываемых персональных данных соответствуют заявленным целям обработки. Не допускается избыточность обрабатываемых персональных данных по отношению к заявленным целям их обработки.</div>\n			<div class="li">6.6. При обработке персональных данных обеспечивается точность персональных данных, их достаточность, а в необходимых случаях и актуальность по отношению к целям обработки персональных данных. Оператор принимает необходимые меры и/или обеспечивает их принятие по удалению или уточнению неполных или неточных данных.</div>\n			<div class="li">6.7. Хранение персональных данных осуществляется в форме, позволяющей определить субъекта персональных данных, не дольше, чем этого требуют цели обработки персональных данных, если срок хранения персональных данных не установлен федеральным законом, договором, стороной которого, выгодоприобретателем или поручителем по которому является субъект персональных данных. Обрабатываемые персональные данные уничтожаются либо обезличиваются по достижении целей обработки или в случае утраты необходимости в достижении этих целей, если иное не предусмотрено федеральным законом.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>7. Цели обработки персональных данных</h5>\n		<div class="ol">\n			<div class="li">7.1. Цель обработки персональных данных Пользователя:</div>\n				<div><div class="li">– <mark>информирование Пользователя посредством отправки электронных писем;</mark></div><div class="li">– <mark>заключение, исполнение и  прекращение гражданско-правовых договоров;</mark></div>\n<div class="li">– <mark>предоставление доступа Пользователю к сервисам, информации и/или материалам, содержащимся на веб-сайте <span class="link mark">https://brosnica.ru</span>;</mark></div>\n<div class="li">– <mark>идентификация покупателя при заказе.</mark></div></div>\n			<div class="li">7.2. Также Оператор имеет право направлять Пользователю уведомления о новых продуктах и услугах, специальных предложениях и различных событиях. Пользователь всегда может отказаться от получения информационных сообщений, направив Оператору письмо на адрес электронной почты <span class="link mark">eblinkoff@mail.ru</span> с пометкой «Отказ от уведомлений о новых продуктах и услугах и специальных предложениях».</div>\n			<div class="li">7.3. Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания. </div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>8. Правовые основания обработки персональных данных</h5>\n		<div class="ol">\n			<div class="li">8.1. Правовыми основаниями обработки персональных данных Оператором являются:</div>\n			<div><div class="li">– <mark>Федеральный закон "Об информации, информационных технологиях и о защите информации" от 27.07.2006 N 149-ФЗ;</mark></div></div>\n			<div class="li">– федеральные законы, иные нормативно-правовые акты в сфере защиты персональных данных;</div>\n			<div class="li">– согласия Пользователей на обработку их персональных данных, на обработку персональных данных, разрешенных для распространения.</div>\n			<div class="li">8.2. Оператор обрабатывает персональные данные Пользователя только в случае их заполнения и/или отправки Пользователем самостоятельно через специальные формы, расположенные на сайте <span class="link mark">https://brosnica.ru</span> или направленные Оператору посредством электронной почты. Заполняя соответствующие формы и/или отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</div>\n			<div class="li">8.3. Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</div>\n			<div class="li">8.4. Субъект персональных данных самостоятельно принимает решение о предоставлении его персональных данных и дает согласие свободно, своей волей и в своем интересе.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>9. Условия обработки персональных данных</h5>\n		<div class="ol">\n			<div class="li">9.1. Обработка персональных данных осуществляется с согласия субъекта персональных данных на обработку его персональных данных.</div>\n			<div class="li">9.2. Обработка персональных данных необходима для достижения целей, предусмотренных международным договором Российской Федерации или законом, для осуществления возложенных законодательством Российской Федерации на оператора функций, полномочий и обязанностей.</div>\n			<div class="li">9.3. Обработка персональных данных необходима для осуществления правосудия, исполнения судебного акта, акта другого органа или должностного лица, подлежащих исполнению в соответствии с законодательством Российской Федерации об исполнительном производстве.</div>\n			<div class="li">9.4. Обработка персональных данных необходима для исполнения договора, стороной которого либо выгодоприобретателем или поручителем по которому является субъект персональных данных, а также для заключения договора по инициативе субъекта персональных данных или договора, по которому субъект персональных данных будет являться выгодоприобретателем или поручителем.</div>\n			<div class="li">9.5. Обработка персональных данных необходима для осуществления прав и законных интересов оператора или третьих лиц либо для достижения общественно значимых целей при условии, что при этом не нарушаются права и свободы субъекта персональных данных.</div>\n			<div class="li">9.6. Осуществляется обработка персональных данных, доступ неограниченного круга лиц к которым предоставлен субъектом персональных данных либо по его просьбе (далее – общедоступные персональные данные).</div>\n			<div class="li">9.7. Осуществляется обработка персональных данных, подлежащих опубликованию или обязательному раскрытию в соответствии с федеральным законом.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>10. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h5>\n		<div class="descr">Безопасность персональных данных, которые обрабатываются Оператором, обеспечивается путем реализации правовых, организационных и технических мер, необходимых для выполнения в полном объеме требований действующего законодательства в области защиты персональных данных.</div>\n		<div class="ol">\n			<div class="li">10.1. Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</div>\n			<div class="li">10.2. Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства либо в случае, если субъектом персональных данных дано согласие Оператору на передачу данных третьему лицу для исполнения обязательств по гражданско-правовому договору.</div>\n			<div class="li">10.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их самостоятельно, путем направления Оператору уведомление на адрес электронной почты Оператора <span class="link mark">eblinkoff@mail.ru</span> с пометкой «Актуализация персональных данных».</div>\n			<div class="li">10.4. Срок обработки персональных данных определяется достижением целей, для которых были собраны персональные данные, если иной срок не предусмотрен договором или действующим законодательством.<br>Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление посредством электронной почты на электронный адрес Оператора <span class="link mark">eblinkoff@mail.ru</span> с пометкой «Отзыв согласия на обработку персональных данных».</div>\n			<div class="li">10.5. Вся информация, которая собирается сторонними сервисами, в том числе платежными системами, средствами связи и другими поставщиками услуг, хранится и обрабатывается указанными лицами (Операторами) в соответствии с их Пользовательским соглашением и Политикой конфиденциальности. Субъект персональных данных и/или Пользователь обязан самостоятельно своевременно ознакомиться с указанными документами. Оператор не несет ответственность за действия третьих лиц, в том числе указанных в настоящем пункте поставщиков услуг.</div>\n			<div class="li">10.6. Установленные субъектом персональных данных запреты на передачу (кроме предоставления доступа), а также на обработку или условия обработки (кроме получения доступа) персональных данных, разрешенных для распространения, не действуют в случаях обработки персональных данных в государственных, общественных и иных публичных интересах, определенных законодательством РФ.</div>\n			<div class="li">10.7. Оператор при обработке персональных данных обеспечивает конфиденциальность персональных данных.</div>\n			<div class="li">10.8. Оператор осуществляет хранение персональных данных в форме, позволяющей определить субъекта персональных данных, не дольше, чем этого требуют цели обработки персональных данных, если срок хранения персональных данных не установлен федеральным законом, договором, стороной которого, выгодоприобретателем или поручителем по которому является субъект персональных данных.</div>\n			<div class="li">10.9. Условием прекращения обработки персональных данных может являться достижение целей обработки персональных данных, истечение срока действия согласия субъекта персональных данных или отзыв согласия субъектом персональных данных, а также выявление неправомерной обработки персональных данных.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>11. Перечень действий, производимых Оператором с полученными персональными данными</h5>\n		<div class="ol">\n			<div class="li">11.1. Оператор осуществляет сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление и уничтожение персональных данных.</div>\n			<div class="li">11.2. Оператор осуществляет автоматизированную обработку персональных данных с получением и/или передачей полученной информации по информационно-телекоммуникационным сетям или без таковой.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>12. Трансграничная передача персональных данных</h5>\n		<div class="ol">\n			<div class="li">12.1. Оператор до начала осуществления трансграничной передачи персональных данных обязан убедиться в том, что иностранным государством, на территорию которого предполагается осуществлять передачу персональных данных, обеспечивается надежная защита прав субъектов персональных данных.</div>\n			<div class="li">12.2. Трансграничная передача персональных данных на территории иностранных государств, не отвечающих вышеуказанным требованиям, может осуществляться только в случае наличия согласия в письменной форме субъекта персональных данных на трансграничную передачу его персональных данных и/или исполнения договора, стороной которого является субъект персональных данных.</div>\n		</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>13. Конфиденциальность персональных данных</h5>\n		<div class="descr">Оператор и иные лица, получившие доступ к персональным данным, обязаны не раскрывать третьим лицам и не распространять персональные данные без согласия субъекта персональных данных, если иное не предусмотрено федеральным законом.</div>\n	</div>\n</div>\n<div class="row mb-4">\n	<div class="col">\n		<h5>14. Заключительные положения</h5>\n		<div class="ol">\n			<div class="li">14.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <span class="link mark">eblinkoff@mail.ru</span>.</div>\n			<div class="li">14.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. Политика действует бессрочно до замены ее новой версией.</div>\n			<div class="li">14.3. Актуальная версия Политики в свободном доступе расположена в сети Интернет по адресу <span class="link mark">https://brosnica.ru/privacy.html</span>.</div>\n		</div>\n	</div>\n</div>\n', '1', 0, 0, '0', 0, 0, 'always', '', '0', '0', 0, 0, '', NULL, '0'),
(162, 0, 0, 'Блог', 'blog', '', '', NULL, '', NULL, '0', 0, 0, '1', 1, 1, 'always', '', '0', '1', 30, 0, '', NULL, '0'),
(163, 0, 0, 'Контакты', 'kontakty', 'Контакты', '', 'Как нас найти', '', '9312009928', '1', 0, 0, '0', 0, 1, 'always', '', '0', '0', 60, 0, '', NULL, '0'),
(164, 0, 0, 'Новости', 'news', 'Новости', '', 'Что нового на нашем сайте', '', NULL, '0', 0, 0, '0', 0, 1, 'always', '', '0', '0', 80, 0, '', NULL, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `version_counter`
--

CREATE TABLE IF NOT EXISTS `version_counter` (
  `id` int(11) NOT NULL,
  `value` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Тут хранится счётчик версий';

--
-- Дамп данных таблицы `version_counter`
--

INSERT INTO `version_counter` (`id`, `value`) VALUES
(1, 26);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thlaspi_id` (`thlaspi_id`);

--
-- Индексы таблицы `articles_images_rel`
--
ALTER TABLE `articles_images_rel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_id` (`articles_id`),
  ADD KEY `thlaspi_id` (`thlaspi_id`);

--
-- Индексы таблицы `articles_plants_rel`
--
ALTER TABLE `articles_plants_rel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_id` (`articles_id`),
  ADD KEY `pant_id` (`plant_id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thlaspi_id` (`thlaspi_id`);

--
-- Индексы таблицы `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `version_counter`
--
ALTER TABLE `version_counter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `articles_images_rel`
--
ALTER TABLE `articles_images_rel`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `articles_plants_rel`
--
ALTER TABLE `articles_plants_rel`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=850;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',AUTO_INCREMENT=165;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
