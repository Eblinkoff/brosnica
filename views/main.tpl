<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
						
							<!--div class="carousel carousel_singleOnHomePage" id="carouselSingleOnHomePage" data-ride="carousel">
								<ol class="carousel-indicators">
									<li class="active" data-target="#carouselSingleOnHomePage" data-slide-to="0"></li>
									<li data-target="#carouselSingleOnHomePage" data-slide-to="1"></li>
								</ol>
								<div class="carousel-inner">
									<a href="http://bellasad.ru/news/interesno/komnatnye-rasteniya/" class="js_bs_counter carousel-item active" rel="2" style="background-image: url(https://bellasad.ru/userfls/bs/img_2554_2.jpg);" title="">
										<div class="carousel-caption">
											<object class="carousel-caption__content">
												<p style="font-size: 1.875em;"><b>Комнатные растения в ассортименте</b></p>
												<p style="font-size: 4.5em; font-weight: 300;">Глоксиния, Антуриум</p>
												<p style="font-size: 0.75em;">Эшинантус, Герань душистая, Нефролепис и др.</p>
											</object>
										</div>
									</a>
									<a href="//bellasad.ru/news/interesno/semena-tomatov-i-ogurtsov/" class="js_bs_counter carousel-item" rel="1" style="background-image: url(https://bellasad.ru/userfls/bs/img_4748_1.jpg);" title="">
										<div class="carousel-caption">
											<object class="carousel-caption__content">
												<p style="font-size: 1.875em;"><b>Семена томатов и огурцов</b></p>
												<p style="font-size: 4.5em; font-weight: 300;">"Партнер", "Русский огород", "Седек"</p>
												<p style="font-size: 0.75em;">и прочих в ассортименте</p>
											</object>
										</div>
									</a>
								</div>
								<a class="carousel-control-prev d-none d-sm-flex" href="#carouselSingleOnHomePage" role="button" data-slide="prev">
									<i class="fas fa-arrow-left" aria-hidden="true"></i>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next d-none d-sm-flex" href="#carouselSingleOnHomePage" role="button" data-slide="next">
									<i class="fas fa-arrow-right" aria-hidden="true"></i>
									<span class="sr-only">Next</span>
								</a>
							</div-->
							<div class="blog-item-post" style="text-align:center;color: #e73a63;padding-bottom: 1.25em;">
								<h6>Уважаемые покупатели!</h6>
								<span>Мы принимаем заказы на весну 2025 года.</span>
							</div>
							<div class="shop-block-categories page-block">
								<div class="h2">Каталог</div>
								<ul class="shop-list-categories row">
									%catalog
								</ul>
							</div>
							
							<div class="text page-block">
								<h1>Многолетники и розы из Тверской области</h1>
								<p>
									Здравствуйте, уважаемые посетители сайта «Бросница»!
								</p>
								<p>
									Здесь Вы можете выбрать розы для своего сада, а также необычные многолетники и кустарники. Все растения выращены и размножены в Тверской области, т.е. растения, которые мы предлагаем, хорошо адаптированы к условиям Средней полосы и Северо-Запада.
								</p>
								<p>
									Мы выращиваем и размножаем розы из разных групп: английские, флорибунды, плетистые, мускусные, почвопокровные, старинные, шрабы. Все эти розы в районах с холодной зимой нуждаются в зимнем укрытии. Также мы собрали и продолжаем пополнять коллекцию зимостойких роз и шиповников, которые не требуют укрытия в средней полосе - неукрывные. Также у нас есть клематисы, ирисы и другие многолетники.
								</p>
								<p>
									Добро пожаловать на наш сайт "Бросница"! Выбирайте, задавайте вопросы, радуйтесь новым растениям в Вашем саду!
								</p>
							</div>
							
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_in_cart
		%show_js
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
	</body>
</html>
