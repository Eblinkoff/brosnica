<script type="text/javascript" src="%root/js/jquery.min.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript" src="%root/js/jquery.form.min.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript" src="%root/js/jquery-ui.min.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript" src="%root/js/timepicker.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript">
	jQuery(function(e){
	e.datepicker.setDefaults(e.datepicker.regional["ru"]);
	e.timepicker.setDefaults(e.timepicker.regional["ru"]);
	});
</script>
<script type="text/javascript" src="%root/js/jquery.scrollTo.min.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript" src="%root/js/jquery.touchSwipe.min.js?ver=%version" charset="UTF-8"></script>
<script type="text/javascript" src="%root/js/jquery.cookie.min.js?ver=%version" charset="UTF-8"></script>
<script src="%root/js/jquery.fancybox.min.js?ver=%version" async type="text/javascript" charset="UTF-8"></script>
<script src='%root/css/bootstrap/js/bootstrap.min.js?ver=%version'></script>
<script src='%root/js/slick.js?ver=%version'></script>
<script type="text/javascript" src="%root/js/site.js?ver=%version"></script>
<script type="text/javascript" async defer src="%root/js/search.js?ver=%version"></script>
<script type="text/javascript" async defer src="%root/js/script.js?ver=%version"></script>
<script src="%root/js/extsrc.js?ver=%version"></script>
%metrica