<link href="%root/css/fancybox.min.css?ver=%version" rel="stylesheet" type="text/css">
<meta name="robots" content="all">
<meta charset="utf-8">
<title>%title</title>
<meta property="og:title" content="%title">
<meta content="Russian" name="language">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:description" content="%description">
<meta name="description" content="%description">
<meta name="keywords" content="%keywords">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content="%og_image">
<link rel="stylesheet" href='%root/css/slick/slick.css?ver=%version'>
<link rel="stylesheet" href='%root/css/select2/css/select2.min.css?ver=%version'>
<link rel="stylesheet" href='%root/css/bootstrap/css/bootstrap.min.css?ver=%version'>
<link rel="stylesheet" href='%root/css/fontawesome/css/fontawesome-all.min.css?ver=%version'>
<link rel="stylesheet" href="%root/css/jquery-ui-1.8.18.custom.css?ver=%version" type="text/css">
<link rel="stylesheet" href="%root/css/default.css?ver=%version" type="text/css">
<link rel="stylesheet" href="%root/css/style.css?ver=%version" type="text/css">
<link rel="stylesheet" href="%root/css/colors.css?ver=%version" type="text/css">
<link rel="apple-touch-icon" sizes="180x180" href="%root/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="%root/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="%root/favicon-16x16.png">
<link rel="manifest" href="%root/site.webmanifest">
<link rel="mask-icon" href="%root/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#00a300">
<meta name="theme-color" content="#ffffff">

