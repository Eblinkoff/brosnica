<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
							<ul class="breadcrumbs text" xmlns:v="http://rdf.data-vocabulary.org/#">
								<li typeof="v:Breadcrumb">
									<a href="%root/" rel="v:url" property="v:title">Главная</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									<a href="%catalog_path_name/" rel="v:url" property="v:title">Интернет-магазин</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									Новости
								</li>
							</ul>
							<h1>%h1</h1>
							<div class="blog-block-categories">
								<ul class="blog-list-categories">
									<li class="blog-item-category">
										<div class="category-intro">
											<div class="category-intro__content text">
												<div class="category-intro__anons">%category_text</div>
											</div>
										</div>
										<ul class="blog-list-posts">
											%items_list
										</ul>
									</li>
								</ul>
							</div>
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_in_cart
		%show_js
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
	</body>
</html>
