<header class="page__header">
	<div class="header__container container-fluid">
		<div class="header__row row">
			<a%avatar_href class="header__logo col-auto">
				<span class="logo">
					<p>
						<img src="%root/img/logo.png" alt="Питомник Бросница" title="Питомник Бросница" width="110" height="110">
						<span>
							<span class="special-logo">
								Бросница
							</span>
							<span class="special-logo-slogan header__slogan">
								Розы для сада
							</span>
						</span>
					</p>
				</span>
			</a>
			<div class="header__contacts-block contacts-block col-auto">
				<i class="fas fa-phone"></i>
				<div class="contacts-block__content">
					<div class="contacts-block__phones">
						<p>
							<a href="tel:89206881070">8 (920) 688-10-70</a><a href="tel:89206881070"></a>
							<div class="svgs" style="display: none">
								<svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/>
								</svg>
								<svg fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
									 viewBox="0 0 24 24" xml:space="preserve">
									<style type="text/css">
										.st0{fill:none;}
									</style>
									<path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2
										c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9
										C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/>
									<rect class="st0" width="24" height="24"/>
								</svg>
							</div>
						</p>
					</div>
					<div class="contacts-block__address">
						<p>Тверская обл., Андреапольский р-н, д. Антоново</p>
					</div>
					<div class="contacts-block__mailto">
						<p>
							<a href="mailto:eblinkoff@mail.ru">eblinkoff@mail.ru</a>
						</p>
					</div>
				</div>
			</div>
			<a class="interactive-link interactive-link_favorites col-auto" href="%catalog_path_name/wishlist/">
				<i class="interactive-link__icon fas fa-heart"></i>
				<span class="interactive-link__title d-none d-lg-inline">Избранное</span>
				<span class="interactive-link__counter in-favorite"></span>
			</a>
			<a id="v_korzine" class="interactive-link interactive-link_cart col-auto" href="%catalog_path_name/cart/">
				<i class="interactive-link__icon fas fa-shopping-bag"></i>
				<span class="interactive-link__title d-none d-lg-inline">Корзина</span>
				<span class="interactive-link__counter in-cart"><i id="cart_count"></i></span>
			</a>
		</div>
	</div>
</header>
<div class="navigation-block navigation-block_top">
	<div class="navigation-block__container container-fluid">
		<div class="navigation-block__row row">
			<div class="navigation-block__left col-auto">
				<span class="navigation-block__button navigation-block__button_shop d-none d-xl-flex">
					<i class="fas fa-bars"></i>
					<i class="fas fa-times"></i>
					Каталог
				</span>
				<span class="navigation-block__button navigation-block__button_nav d-xl-none">
					<i class="fas fa-bars"></i>
					<i class="fas fa-times"></i>
					Навигация
				</span>
				<nav class="navigation-block__menu navigation navigation_horizontal d-none d-xl-flex">
					<ul class="navigation__list navigation__list_main">
						%static_pages
					</ul>
				</nav>
			</div>
			<div class="navigation-block__right col-auto col-sm col-xl-auto">
				<!--nav class="navigation-block__socials">
					<a href="https://www.instagram.com/eblinkoff"><i class="fab fa-instagram"></i></a>
					<a href="https://vk.com/eblinkoff"><i class="fab fa-vk"></i></a>
				</nav-->
				<div class="navigation-block__search d-none d-sm-block">
					<span class="form">
						<div class="control search-input-wrapper">
							<input class="input" type="text" name="searchword" placeholder="Поиск по сайту" oninput="search_go(this)" autocomplete="off">
						</div>
						<button class="button" type="button" title="Найти">
							<i class="fas fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</div>
		<div class="navigation-block__search d-block d-sm-none">
			<span class="form">
				<div class="control search-input-wrapper">
					<input class="input" type="text" name="searchword" placeholder="Поиск по сайту" oninput="search_go(this)" autocomplete="off">
				</div>
				<button class="button" type="button" title="Найти">
					<i class="fas fa-search"></i>
				</button>
			</span>
		</div>
	</div>
</div>
<div class="navigation-block navigation-block_shop d-none d-md-block">
	<div class="navigation-block__container container-fluid">
		<div class="navigation-block__row row">
			<div class="navigation-columns col-12">
				<div class="row">
					%menu_full_screen
				</div>
			</div>
		</div>
	</div>
</div>
<nav class="navigation-block navigation-block_mobile navigation navigation_vertical d-xl-none">
	<div class="shop-block-navigation__box">
		<ul class="navigation__list navigation__list_main">
			<li class="navigation__item navigation__item_main navigation__item_parent">
				<a href="%catalog_path_name/">Каталог<i class="fas fa-plus-circle"></i></a>
				%menu_mobile
			</li>
			%static_pages
		</ul>
	</div>
</nav>