RewriteEngine on
RewriteCond %{HTTP:X-Forwarded-Proto} !https
RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301,NE]

##Ставим 301 редирект с www.имя.сайта на имя.сайта(убираем "www"). Делается для поисковых машин, чтобы не бить статистику.
#RewriteCond %{HTTP_HOST} ^www\.brosnica\.ru$
#RewriteRule (.*) https://brosnica.ru/$1 [R=301,L]
#RewriteRule ^([^.]+)$ index.php

#Склеиваем страницы со слешем и без слеша
RewriteCond %{REQUEST_URI} (.*/[^/.]+)($|\?)
RewriteRule .* %1/ [R=301,L]

#При запросе файла .htaccess говорим, что он не существует (410, GONE).
#RewriteRule ^/.htaccess$ /error/htaccess/404/

#php_value error_log /home/a0009190/domains/brosnica.ru/log/php_errors.log
#php_flag display_errors On
#php_value error_reporting "E_ALL"

#php_value display_errors 1

<IfModule mod_expires.c>
Header append Cache-Control "public"
FileETag MTime Size
ExpiresActive On
ExpiresDefault "access plus 0 minutes"
#ExpiresByType image/ico "access plus 1 years"
ExpiresByType text/css "access plus 1 years"
ExpiresByType text/javascript "access plus 1 years"
ExpiresByType image/gif "access plus 1 years"
ExpiresByType image/jpg "access plus 1 years"
ExpiresByType image/jpeg "access plus 1 years"
ExpiresByType image/bmp "access plus 1 years"
ExpiresByType image/png "access plus 1 years"
ExpiresByType audio/ogg "access plus 1 years"
ExpiresByType application/manifest+json "access plus 1 years"
ExpiresByType video/mp4 "access plus 1 years"
ExpiresByType video/ogg "access plus 1 years"
ExpiresByType video/webm "access plus 1 years"
# Web feeds
ExpiresByType application/atom+xml "access plus 1 years"
ExpiresByType application/rss+xml "access plus 1 years"
# Web fonts
ExpiresByType application/font-woff "access plus 1 years"
ExpiresByType application/vnd.ms-fontobject "access plus 1 years"
ExpiresByType application/x-font-ttf "access plus 1 years"
ExpiresByType font/opentype "access plus 1 years"
ExpiresByType image/svg+xml "access plus 1 years"
</IfModule>
