<input class="offer-radio-checker" id="m_%id_offer_%id" name="show_offer_%id" value="%id" type="radio" checked="checked">
<label class="js_shop_param_price shop_param_price shop-item-price" for="offer_%id">
	<div class="is_avalible white">
		<i class="fa fa-check"></i> Есть в наличии: <span id="count_goods_%id">%count</span> шт.
	</div>
	<div>
		<span class="js_shop_buy shop_buy to-cart">
			<div>111</div>
			<input type="number" value="1" name="count" class="number js_inp_gray" step="any" min="1" max="%count" id="count_%id" autocomplete="off">
		</span>
		<span class="price">
			<span class="js_shop_price" summ="%price" format_price_1="2" format_price_2="," format_price_3="">
				%price
			</span>
			<span class="currency">руб.</span>
		</span>
	</div>
</label>
<script>
	if(!goods)
	{
		var goods = [];
	}
	// для товара - id товара, для оффера - id оффера
	// это значит, что этот оффер выбран по-умолчанию
	goods[%id] = [{
		type: "main",
		thlaspi_id: %id,
		article: %article,
		offer_id: %id,
		name: "%h1",
		price: %price,
		max_count: %count,
		status: 'selected',
		img_full_0: "%img_full_0",
		img_thumbnail_0: "%img_thumbnail_0",
		img_icon_0: "%img_icon_0",
		this_page_url: "%this_page_url",
	}];
</script>
