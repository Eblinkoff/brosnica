<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
							%breadcrumbs
							<h1>%h1</h1>
							<div class="shop-block-categories shop-block-categories-page">
								<div class="category-intro">
									<figure class="category-intro__image" style="background-image: url(%img_full_0);" title="%h1"></figure>
									<div class="category-intro__content text">
										%category_text
									</div>
								</div>
								%catschildrens
							</div>
							<div class="shop-settings">
								<div class="shop-sorting">
									<div class="shop-sorting__unit shop-sorting__title">Сортировать:</div>
									<div class="shop-sorting__unit shop-sorting__order-select">
										<select name="shop_sorting_order_select">
											<option value="/sort2/">Цена &darr;</option>
											<option value="/sort1/">Цена &uarr;</option>
											<option value="/sort4/">Наименование товара &darr;</option>
											<option value="/sort3/">Наименование товара &uarr;</option>
										</select>
									</div>
								</div>
							</div>
							%mobile_filter_shop
							<ul class="shop-list-products row">
								%plantsListInner
							</ul>
							<div class="blog-item-post" style="text-align:center;padding-bottom: 1.25em;">
								<h6 style="color: #e73a63;">Испытываете трудности с выбором?</h6>
								<span>Звоните, не стесняйтесь!</span><br>
								Мы с радостью поможем Вам, проконсультируем по любым вопросам. Куда сажать, как ухаживать, как укрывать, какая роза лучше, какая подходит, какая - нет. <br>Мы любим поговорить о растениях!<br>
								<a href="tel:89206881070"><i class="fas fa-phone" style="font-size: 16px"></i>Оля 8 (920) 688-10-70</a> <a href="whatsapp:+79206881070?chat"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/></svg></a> <a href="tg://resolve?domain=+79206881070" style="position: relative;top: 1.5px;"><svg width="20" height="20" fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"viewBox="0 0 24 24" xml:space="preserve"><style type="text/css">.st0{fill:none;}</style><path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/><rect class="st0" width="24" height="24"/></svg></a>
								<br>
								<a href="tel:89312009928"><i class="fas fa-phone" style="font-size: 16px"></i>Женя 8 (931) 200-99-28</a> <a href="whatsapp:+89312009928?chat"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.014 8.00613C6.12827 7.1024 7.30277 5.87414 8.23488 6.01043L8.23339 6.00894C9.14051 6.18132 9.85859 7.74261 10.2635 8.44465C10.5504 8.95402 10.3641 9.4701 10.0965 9.68787C9.7355 9.97883 9.17099 10.3803 9.28943 10.7834C9.5 11.5 12 14 13.2296 14.7107C13.695 14.9797 14.0325 14.2702 14.3207 13.9067C14.5301 13.6271 15.0466 13.46 15.5548 13.736C16.3138 14.178 17.0288 14.6917 17.69 15.27C18.0202 15.546 18.0977 15.9539 17.8689 16.385C17.4659 17.1443 16.3003 18.1456 15.4542 17.9421C13.9764 17.5868 8 15.27 6.08033 8.55801C5.97237 8.24048 5.99955 8.12044 6.014 8.00613Z" fill="#E73A63"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12 23C10.7764 23 10.0994 22.8687 9 22.5L6.89443 23.5528C5.56462 24.2177 4 23.2507 4 21.7639V19.5C1.84655 17.492 1 15.1767 1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23ZM6 18.6303L5.36395 18.0372C3.69087 16.4772 3 14.7331 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C11.0143 21 10.552 20.911 9.63595 20.6038L8.84847 20.3397L6 21.7639V18.6303Z" fill="#E73A63"/></svg></a> <a href="tg://resolve?domain=+89312009928" style="position: relative;top: 1.5px;"><svg width="20" height="20" fill="#E73A63" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"viewBox="0 0 24 24" xml:space="preserve"><style type="text/css">.st0{fill:none;}</style><path d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.9,8.1l-1.7,8.2c-0.1,0.6-0.5,0.7-0.9,0.4l-2.6-2c-0.6,0.6-1.2,1.1-1.3,1.3c-0.2,0.1-0.3,0.3-0.5,0.3c-0.3,0-0.3-0.2-0.4-0.4l-0.9-3L5.9,12c-0.6-0.2-0.6-0.6,0.1-0.9l10.2-3.9C16.6,7.1,17.1,7.3,16.9,8.1z M14.5,9l-5.7,3.6l0.9,3l0.2-2l4.9-4.4C15.1,8.9,14.9,8.9,14.5,9z"/><rect class="st0" width="24" height="24"/></svg></a>
							</div>
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_confirmation_tpl
		%modal_in_cart
		%show_js
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
	</body>
</html>
