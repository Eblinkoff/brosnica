<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
							%breadcrumbs
							<h1>%h1</h1>
							<div class="shop-product js_shop">
								<div class="row">
									<div class="shop-product__gallery gallery col-12 col-md-6 col-lg-4 col-xl-6">
										<div class="gallery__basic">
											<ul class="shop-stickers">
												%hit
												%new
												%action
											</ul>
											<div class="shop-interactive">
												<i class="shop-interactive__item shop-favorite js_favorite far fa-heart js_shop_wishlist" title="Добавить в избранное" data-addtitle="Добавить в избранное" data-removetitle="Убрать из избранного"  data-id="%id"></i>
											</div>
											<div class="gallery__items">
												%big_lineyka_photos
											</div>
										</div>
										<div class="gallery__second">
											<div class="gallery__items">
												%lineyka_photos
											</div>
											<a class="gallery__button gallery__button_prev" href="javascript:void(0)" title="Предыдущий">
												<i class="fas fa-arrow-left" aria-hidden="true"></i>
												<span class="sr-only">Previous</span>
											</a>
											<a class="gallery__button gallery__button_next" href="javascript:void(0)" title="Следующий">
												<i class="fas fa-arrow-right" aria-hidden="true"></i>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
									<div class="shop-product__details col-12 col-md-6 col-lg-8 col-xl-6">
										<h3>О товаре</h3>
										<div class="shop-product__anons text">
											<p class="like-h4">
												%params
											</p>
										</div>
										<div class="shop-item-product__article">Артикул: %article</div>
										<span class="shop-product__offer js_shop_form form ajax">
											%offers
											%buy_buttons
										</span>
										<div class="modal modal_oneClick fade" id="modalOneClick_%id" tabindex="-1" role="dialog" aria-labelledby="Купить в 1 клик" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
													<div class="js_cart_one_click cart_one_click">
														<span class="js_cart_one_click_form cart_one_click_form form ajax">
															<div class="form__header">
																<div class="h2">Купить в 1 клик</div>
															</div>
															<div class="control">
																<label class="label">Ваши ФИО<span style="color:red;">*</span>:</label>
																<input id="one_click_name_%id" class="input" type="text" name="p11" value="">
																<div class="errors error_p11" style="display:none"></div>
															</div>
															<div class="control">
																<label class="label">Email или телефон<span style="color:red;">*</span>:</label>
																<input id="one_click_phone_%id" class="input" type="tel" name="p12" value="">
																<div class="errors error_p12" style="display:none"></div>
															</div>
															<input class="button" type="button" value="Заказать" data-id="%id" onclick="buyOneclick(%id)">
															<div class="privacy text">Отправляя форму, я даю согласие на <a href="%root/privacy/">обработку персональных данных</a>.</div>
															<div class="required"><span style="color:red;">*</span> — Поля, обязательные для заполнения</div>
															<div class="errors error" style="display:none"></div>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="shop-product-tabs tabs-block page-block">
								<ul class="tabs-block__rubrics h2">
									<li class="tabs-block__rubric active">Описание</li>
								</ul>
								<ul class="tabs-block__collection">
									<li class="tabs-block__tab active">
										<div class="text">
											<p>
												%comment
											</p>
										</div>
									</li>
								</ul>
							</div>
							%interesting_yet
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_confirmation_tpl
		%modal_in_cart
		%show_js
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
	</body>
</html>
