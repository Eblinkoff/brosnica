<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
							<ul class="breadcrumbs text" xmlns:v="http://rdf.data-vocabulary.org/#">
								<li typeof="v:Breadcrumb">
									<a href="%root/" rel="v:url" property="v:title">Главная</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									%h1
								</li>
							</ul>
							<h1>%h1</h1>
							<div class="text">
								<p>
									<span>Спасибо за Ваш заказ! В ближайшее время мы свяжемся с Вами!</span>
								</p>
								<p>
									<span id="order_number_id_h2" style="display:none;">Номер Вашего заказа <span class="order-id-last-order" style="color: #FF5959;font-size: 1.25em;"></span></span>
								</p>
								<p>
									<span>
										<a href="%catalog_path_name/">Продолжить покупки</a>
									</span>
								</p>
							</div>
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%show_js
		<script type="text/javascript" async src="%root/js/last_order.js?ver=%version"></script>
	</body>
</html>
