<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 order-lg-last">
							<ul class="breadcrumbs text" xmlns:v="http://rdf.data-vocabulary.org/#">
								<li typeof="v:Breadcrumb">
									<a href="%root/" rel="v:url" property="v:title">Главная</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									<a href="%catalog_path_name/" rel="v:url" property="v:title">Интернет-магазин</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									%h1
								</li>
							</ul>
							<h1>%h1</h1>
							<a name="top"></a>
							
							<div class="shop-cart cart_order">
								<span class="shop-cart__form shop-cart__form_first js_cart_table_form cart_table_form form ajax">
									<div class="cart_table">
										<div class="shop-cart-block shop-inblock">
											<ul class="shop-cart-list-products shop-table js_shop-cart-list-products">
												<li class="shop-table__row_head shop-table__row">
													<div class="shop-table__cell shop-table__cell_image order-first">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_control order-xl-last">
															<div class="shop-table__cell-content">Удалить</div>
													</div>
													<div class="shop-table__cell shop-table__cell_name d-none d-sm-flex order-sm-first">
														<div class="shop-table__cell-content">Наименование товара</div>
													</div>
													<div class="shop-table__cell shop-table__cell_count d-none d-md-flex order-md-first">
														<div class="shop-table__cell-content">Количество</div>
													</div>
													<div class="shop-table__cell shop-table__cell_price-old d-none d-xl-flex">
														<div class="shop-table__cell-content">Цена без скидки,&nbsp;р</div>
													</div>
													<div class="shop-table__cell shop-table__cell_price d-none d-xl-flex">
														<div class="shop-table__cell-content">Цена,&nbsp;р</div>
													</div>
													<div class="shop-table__cell shop-table__cell_discount d-none d-xl-flex">
														<div class="shop-table__cell-content">Скидка</div>
													</div>
													<div class="shop-table__cell shop-table__cell_sum d-none d-xl-flex">
														<div class="shop-table__cell-content">Сумма,&nbsp;р</div>
													</div>
												</li>
												<li class="shop-table-item shop-table__row shop-table__row_discount" style="display:none;">
													<div class="shop-table__cell shop-table__cell_image d-none d-xl-flex order-first">
														<div class="shop-table__cell-content"></div>
													</div><div class="shop-table__cell shop-table__cell_control d-none d-xl-flex order-xl-last">
															<div class="shop-table__cell-content"></div>
														</div><div class="shop-table__cell shop-table__cell_name order-sm-first">
														<div class="shop-table__cell-content">Скидки</div>
													</div><div class="shop-table__cell shop-table__cell_count d-none d-xl-flex order-md-first">
														<div class="shop-table__cell-content"></div>
													</div><div class="shop-table__cell shop-table__cell_price-old d-none d-xl-flex">
															<div class="shop-table__cell-content"></div>
														</div>
														<div class="shop-table__cell shop-table__cell_price d-none d-xl-flex">
															<div class="shop-table__cell-content"></div>
														</div>
														<div class="shop-table__cell shop-table__cell_discount" title="Скидка">
															<div class="shop-table__cell-content"></div>
														</div><div class="shop-table__cell shop-table__cell_sum" title="Общая скидка">
														<div class="shop-table__cell-content">10%</div>
													</div>
												</li>
												<li class="shop-table-item shop-table__row shop-table__row_total">
													<div class="shop-table__cell shop-table__cell_image d-none d-xl-flex order-first cart-total">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_control d-none d-xl-flex order-xl-last cart-total">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_name order-sm-first cart-total">
														<div class="shop-table__cell-content">Итого за товары</div>
													</div>
													<div class="shop-table__cell shop-table__cell_count order-md-first cart-total" title="Количество">
														<div class="shop-table__cell-content">0</div>
													</div>
													<div class="shop-table__cell shop-table__cell_price-old d-none d-xl-flex cart-total">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_price d-none d-xl-flex cart-total">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_discount d-none d-xl-flex cart-total">
														<div class="shop-table__cell-content"></div>
													</div>
													<div class="shop-table__cell shop-table__cell_sum cart-total" title="Сумма, р">
														<div class="shop-table__cell-content">0</div>
													</div>
												</li>
											</ul>
										</div>
										<!--div class="h2" style="display:none;">Способы доставки</div>
										<div class="shop-cart-delivery-block shop-inblock" style="display:none;">
											<ul class="shop-cart-list-delivery shop-table" onclick="ch_delivery(event)">
												<li class="shop-table__row_head shop-table__row">
													<div class="shop-table__cell shop-table__cell_name">
														<div class="shop-table__cell-content">Наименование</div>
													</div><div class="shop-table__cell shop-table__cell_control order-md-last">
															<div class="shop-table__cell-content">Выбрать</div>
														</div><div class="shop-table__cell shop-table__cell_sum d-none d-md-flex">
														<div class="shop-table__cell-content">Сумма,&nbsp;р</div>
													</div>
												</li>
												<li class="shop-table-item shop-table__row">
													<div class="shop-table__cell shop-table__cell_name">
														<label class="shop-table__cell-content" for="delivery_id_2">
															<div class="shop-table-item__name">Почта России</div><div class="shop-table-item__text">Доставка по всей России небольших посылок<br>Стоимость в зависимости от веса</div></label>
													</div>
													<div class="shop-table__cell shop-table__cell_control order-md-last">
														<div class="shop-table__cell-content">
															<div class="control control_radio control_naked">
																<input name="delivery_id" id="delivery_id_2" value="2" type="radio">
																<label for="delivery_id_2"></label>
															</div>
														</div>
													</div>
													<div class="shop-table__cell shop-table__cell_sum" title="Сумма, р">
														<div class="shop-table__cell-content">0</div>
													</div>
												</li>
												<li class="shop-table-item shop-table__row">
													<div class="shop-table__cell shop-table__cell_name">
														<label class="shop-table__cell-content" for="delivery_id_3">
															<div class="shop-table-item__name">Самовывоз</div><div class="shop-table-item__text">Самовывоз из д.Антоново или можно передать в Андреаполе<br>Стоимость 0 р</div></label>
													</div>
													<div class="shop-table__cell shop-table__cell_control order-md-last">
														<div class="shop-table__cell-content">
															<div class="control control_radio control_naked">
																<input name="delivery_id" id="delivery_id_3" value="3" type="radio">
																<label for="delivery_id_3"></label>
															</div>
														</div>
													</div>
													<div class="shop-table__cell shop-table__cell_sum" title="Сумма, р">
														<div class="shop-table__cell-content">0</div>
													</div>
												</li>
											</ul>
										</div-->
										<div class="shop-total-block">
											<div class="d-sm-flex justify-content-sm-end align-items-sm-center">
												<div class="shop-total-block__title h2">Итого к оплате:</div>
												<div class="shop-total-block__cost"><span id="shop_total_block_cost">0</span>&nbsp;р</div>
											</div>
											<div class="shop-total-block__tax text-sm-right"></div>
										</div>
									</div>
									<div class="errors error" style="display:none"></div>
									<div class="cart_recalc">
										<input value="Пересчитать" style="display: none;" type="submit">
									</div>
								</span>
								<!--div class="row">
									<div class="col-12 col-sm-6">
										<div class="h3">Если Вы оформляли заказ на сайте ранее, просто введите логин и пароль:</div>
										<div class="shop-cart-auth-block shop-inblock">
											<form method="post" action="" class="form ajax">
												<input name="action" value="auth" type="hidden">
												<input name="module" value="registration" type="hidden">
												<input name="form_tag" value="registration_auth" type="hidden">
												<div class="control">
													<label class="label">Имя пользователя<span style="color:red;">*</span>:</label>
													<input name="name" autocomplete="off" type="text">
												</div>
												<div class="control">
													<label class="label">Пароль<span style="color:red;">*</span>:</label>
													<input name="pass" autocomplete="off" type="password">
												</div>
												<div class="form__footer text">
													<button class="button">Войти</button>
												</div>
												<div class="errors error" style="display:none"></div>
											</form>
										</div>
									</div>
									<div class="col-12 col-sm-6">
										<div class="h2">Купон на скидку</div>
										<div class="shop-cart-coupon-block shop-inblock">
											<form method="post" action="" class="js_shop_form shop_form form ajax">
												<input name="action" value="add_coupon" type="hidden">
												<input name="form_tag" value="shop_add_coupon" type="hidden">
												<input name="module" value="shop" type="hidden">
												<div class="row">
													<div class="col-8 col-auto col-sm-12">
														<div class="control">
															<label class="label">Код купона</label>
															<input value="" name="coupon" type="text">
														</div>
													</div>
												</div>
												<button class="button" type="submit">Активировать</button>
												<div class="errors error" style="display:none"></div>
											</form>
										</div>
									</div>
								</div-->
								<span class="shop-cart__form shop-cart__form_second form ajax">
									<div class="h2">Персональные данные</div>
									<div class="shop-cart-personal-information-block shop-inblock">
										<div class="row">
											<div class="control col-12 col-sm-6">
												<label class="label">Ваше имя<span style="color:red;">*</span>:</label>
												<input id="inputNameReg" class="input" name="p1" value="" type="text" oninput="ch_contacts(this,'name')">
												<div class="errors error_p1" style="display:none"></div>
											</div>
											<div class="control col-12 col-sm-6">
														<label class="label">Контактные телефоны (с кодом города)<span style="color:red;">*</span>:</label>
														<input id="inputPhoneReg" class="input" name="p3" value="" type="tel" oninput="ch_contacts(this,'phone')"><div class="errors error_p3" style="display:none"></div>
											</div>
											<div class="control col-12 col-sm-6">
														<label class="label">E-mail:</label>
														<input id="inputEmailReg" class="input" name="p2" value="" type="email" oninput="ch_contacts(this,'email')"><div class="errors error_p2" style="display:none"></div>
											</div>
											<div class="control col-12 col-sm-6">
														<label class="label">Адрес доставки:</label>
														<input id="inputAdressReg" class="input" name="p6" value="" type="text" oninput="ch_contacts(this,'adress')"><div class="errors error_p6" style="display:none"></div>
											</div>
											<div class="control col-12 col-sm-6">
														<label class="label">Комментарии:</label>
														<textarea class="input" name="p10" oninput="ch_contacts(this,'comment')" id="inputCommentReg"></textarea><div class="errors error_p10" style="display:none"></div>
											</div>
										</div>
									</div>
									<!--div class="h2">Выберите способ оплаты</div>
									<div class="shop-cart-payments-block shop-inblock">
										<ul class="shop-cart-list-payments control-group">
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment1" value="1" checked="" type="radio">
												<label for="payment1">Наличными курьеру<div class="control__text">Заказ необходимо оплатить курьеру на руки наличными</div></label>
											</li>
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment3" value="3" type="radio">
												<label for="payment3">WebMoney<div class="control__text">Используйте мгновенную оплату заказа через систему WebMoney. Это ускорит доставку Вашего заказа.</div></label>
											</li>
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment4" value="4" type="radio">
												<label for="payment4">Robokassa<div class="control__text">Robokassa позволяет оплатить заказ одним из удобных для Вас способом.</div></label>
											</li>
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment5" value="5" type="radio">
												<label for="payment5">Банковские платежи<div class="control__text">Распечатайте квитанцию и оплатить в ближайшем отделении банка.</div></label>
											</li>
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment6" value="6" type="radio">
												<label for="payment6">Яндекс.Касса<div class="control__text">Яндекс.Касса — сервис электронных платежей, с ее помощью можно платить с банковских карт, электронными деньгами, наличными и другими способами. За безопасность сервиса отвечают Яндекс.Деньги.</div></label>
											</li>
											<li class="shop-cart-item-payment control control_radio">
												<input name="payment_id" id="payment7" value="7" type="radio">
												<label for="payment7">QIWI<div class="control__text">Оплата мобильной связи, коммунальных услуг, покупок в интернет-магазинах и др. через платежные терминалы, интернет и мобильный телефон.</div></label>
											</li>
										</ul>
									</div-->
									<div class="checkout-cart-button-handler">
										<button onclick="checkout()" class="button" type="submit">Оформить заказ!</button>
										<div class="privacy">Отправляя форму, я даю согласие на <a href="%root/privacy/">обработку персональных данных</a>.</div>
										<div class="errors error" style="display:none"></div>
										<div class="required"><span style="color:red;">*</span> — Поля, обязательные для заполнения</div>
									</div>
								</span>
								<div class="shop-product-related shop-block-products page-block">
									<div id="header-of-full-block-wish" class="h2" style="display:none;">Не забудьте заказать растения из списка Ваших хотелок!</div>
									<ul class="shop-list-products row wishes-list-cart if-unwish-delete">
									</ul>
								</div>							
							</div>							
						</main>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_confirmation_tpl
		%show_js
		<script type="text/javascript" async src="%root/js/cart.js?ver=%version"></script>
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
		<div id="set_one_good_in_list" style="display:none;">
			%one_wish_good
		</div>
		<div id="set_one_good" style="display:none;">
			<li class="shop-table-item shop-table__row d-flex" data-is-good="1">
				<div class="shop-table__cell shop-table__cell_image order-first">
					<div class="shop-table__cell-content">
						<a href="%root/" title="" class="js_img_in_cart_table"></a>
					</div>
				</div>
				<div class="shop-table__cell shop-table__cell_control order-xl-last">
					<div class="shop-table__cell-content">
						<a class="js_cart_remove fas fa-times" href="javascript:void(0)" confirm="Вы действительно хотите удалить товар из корзины?"  onclick="return delete_good_in_cart(this)">
						</a>
					</div>
				</div>
				<div class="shop-table__cell shop-table__cell_name order-sm-first">
					<div class="shop-table__cell-content">
						<div class="shop-table-item__category">
							<a class="js_category_in_cart" href=""></a>
						</div>
						<div class="shop-table-item__name">
							<a class="js_name-good-cart" href="%root/"></a>
						</div>
						<div class="shop-table-item__article">Артикул: <span class="js_article_cart"></span></div>
					</div>
				</div>
				<div class="shop-table__cell shop-table__cell_count order-md-first" title="Количество">
					<div class="shop-table__cell-content">
						<span class="shop-counter js_cart_count">
							<span class="shop-counter__widget">
								<a class="shop-counter__reducer shop-counter__button js_cart_count_minus far fa-minus-square" href="javascript:void(0)" title="Уменьшить" role="button"></a>
								<input class="number js_inp_gray" value="1" min="0" name="editshop12_a2i2i1i6i68_" size="2" type="text" oninput="edit_cart_good_count(this)">
								<a class="shop-counter__adder shop-counter__button js_cart_count_plus far fa-plus-square" href="javascript:void(0)" title="Добавить" role="button"></a>
							</span>
						</span>
					</div>
				</div>
				<div class="shop-table__cell shop-table__cell_price-old" title="Цена без скидки, р">
					<div class="shop-table__cell-content js_discount_price">0</div>
				</div>
				<div class="shop-table__cell shop-table__cell_price" title="Цена, р">
					<div class="shop-table__cell-content js_price_for_one"></div>
				</div>
				<div class="shop-table__cell shop-table__cell_discount" title="Скидка">
					<div class="shop-table__cell-content js_discount"></div>
				</div>
				<div class="shop-table__cell shop-table__cell_sum" title="Сумма, р">
					<div class="shop-table__cell-content js_price-for-result">0</div>
				</div>
			</li>
		</div>
		<div class="modal modal_inCart fade" id="modalNotifyWhenAvailable" tabindex="-1" role="dialog" aria-labelledby="Оповестить когда товар появится в наличии" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
					<div class="h2">Сообщить когда появится</div>
					<div class="control">
						<label class="label">Ваше имя<span style="color:red;">*</span>:</label>
						<input id="notify_when_fio" class="input" type="text" name="fio" value="">
						<div class="errors error_fio" style="display:none"></div>
					</div>
					А также что-то одно из:<br>
					<div class="control">
						<label class="label">Телефон<span style="color:red;">*</span>:</label>
						<input id="notify_when_telefon" class="input" type="text" name="tel" value="">
						<div class="errors error_tel" style="display:none"></div>
					</div>
					или
					<div class="control">
						<label class="label">e-mail для связи<span style="color:red;">*</span>:</label>
						<input id="notify_when_email" class="input" type="text" name="email" value="">
						<div class="errors error_email" style="display:none"></div>
					</div>
					<br>
					<div class="modal__footer">
						<button id="notify_when_button" class="button" data-dismiss="modal" aria-label="Close" onclick="notifyWhenAvailable(this)">Отправить!</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
