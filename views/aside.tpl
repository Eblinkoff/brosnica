<div class="row d-block d-md-flex d-lg-block">
	<nav class="shop-block-navigation navigation navigation_vertical page-block page-block_aside col-12 d-none d-lg-block">
		<div class="shop-block-navigation__box">
			%leftsidebar
		</div>
	</nav>
	%filter_shop
	%news_block
	%articles_block
	<!--div class="subscription-block page-block page-block_aside col-12 order-md-last text-sm-center text-lg-left">
		<div class="subscription-block__box" style="background-image: url(/subscription_7_7.jpg);">
			<div class="subscription-block__content">
				<div class="subscription-block__text">
					<p>Подпишитесь на рассылку и будьте вкурсе важных новостей:</p>
				</div>
				<span class="form ajax">
					<div class="d-sm-flex d-lg-block justify-content-sm-center">
						<div class="control">
							<input class="input" name="mail" type="email" placeholder="Электронная почта">
							<div class="errors error_mail" style="display:none"></div>
						</div>
						<button class="button" type="submit" title="Подписаться">Подписаться</button>
					</div>
					<div class="privacy">Отправляя форму, я даю согласие на <a href="%root/privacy/">обработку персональных данных</a>.</div>
					<div class="errors error" style="display:none"></div>
				</span>
			</div>
		</div>
	</div-->
</div>
