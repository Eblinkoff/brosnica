<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		%shead
	</head>
	<body class="page page_home">
		<div class="page__box">
    		<div class="page__top">
				%header
			</div>
			<div class="page__body">
				<div class="body__container container-fluid">
					<div class="body__row row">
						<main class="page__content col-12 col-lg-9 order-lg-last">
							<ul class="breadcrumbs text" xmlns:v="http://rdf.data-vocabulary.org/#">
								<li typeof="v:Breadcrumb">
									<a href="%root/" rel="v:url" property="v:title">Главная</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									<a href="%catalog_path_name/" rel="v:url" property="v:title">Интернет-магазин</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									<a href="%root/news/" rel="v:url" property="v:title">Новости</a>
								</li>
								<li>—</li>
								<li typeof="v:Breadcrumb">
									%h1
								</li>
							</ul>
							<h1>%h1</h1>
							<div class="blog-post">
								<div class="text">
									<div class="blog-post__date">%date</div>
									<p><span>%category_text</span></p>
									<div class="images">
										<div class="row">
											<div class="col-12 col-sm-4">
												<a href="%img_full_0" data-fancybox="gallery19post"><img src="%img_thumbnail_0" alt="%h1" title="%h1">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</main>
						<aside class="page__aside col-12 col-lg-3 order-lg-first">
							%aside
						</aside>
					</div>
				</div>
			</div>
			<div class="page__bottom">
				%footer
			</div>
		</div>
		<a class="button-up" href="javascript:void(0)">Вверх</a>
		%modal_in_cart
		%show_js
		<script type="text/javascript" async src="%root/js/good.js?ver=%version"></script>
	</body>
</html>
