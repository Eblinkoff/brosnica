<li class="shop-item-product col-12 col-sm-6 col-md-4">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image" href="%this_page_url" style="background-image: url(%img_thumbnail_0);" title="%name"></a>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__name">
				<a href="%this_page_url" style="font-size:140%">%name</a>
			</div>
		</div>
	</div>
</li>
<!--li class="shop-item-category col-12 col-sm-6 col-md-4">
	<a class="shop-item-category__link" href="%this_page_url" style="background-image: url(%img_thumbnail_0);" title="%name">
		<span class="shop-item-category__name">%name</span>
	</a>
</li-->