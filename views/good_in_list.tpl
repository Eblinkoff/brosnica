<li class="shop-item-product col-12 col-sm-6 col-md-4 js_shop" id="shop_item_product_%id">
	<div class="shop-item-product__box">
		<div class="shop-item-product__top">
			<a class="shop-item-product__image js_shop_img" image_id="%image_id" href="%this_page_url" style="background-image: url(%img_thumbnail_0);" title="%name"></a>
			<ul class="shop-stickers">
				%hit
				%new
				%action
			</ul>
			<div class="shop-interactive">
				<i class="shop-interactive__item shop-favorite js_favorite far fa-heart js_shop_wishlist" title="Добавить в избранное" data-addtitle="Добавить в избранное" data-removetitle="Убрать из избранного" data-id="%id"></i>
			</div>
		</div>
		<div class="shop-item-product__details">
			<div class="shop-item-product__article">Артикул: %article</div>
			<div class="shop-item-product__name">
				<a href="%this_page_url">%name</a>
			</div>
			
			<div class="shop-item-product__anons">
				<p class="like-h4">
					%params
				</p>
			</div>
			
			<span class="shop-item-product__offer js_shop_form form ajax">
				%offers
				%buy_buttons
			</span>
			<div class="modal modal_oneClick fade" id="modalOneClick_%id" tabindex="-1" role="dialog" aria-labelledby="Купить в 1 клик" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
						<div class="js_cart_one_click cart_one_click">
							<span class="js_cart_one_click_form cart_one_click_form form ajax">
								<div class="form__header">
									<div class="h2">Купить в 1 клик</div>
								</div>
								<div class="control">
									<label class="label">Ваши ФИО<span style="color:red;">*</span>:</label>
									<input id="one_click_name_%id" class="input" type="text" name="p11" value="">
									<div class="errors error_p11" style="display:none"></div>
								</div>
								<div class="control">
									<label class="label">Email или телефон<span style="color:red;">*</span>:</label>
									<input id="one_click_phone_%id" class="input" type="tel" name="p12" value="">
									<div class="errors error_p12" style="display:none"></div>
								</div>
								<input class="button" type="button" value="Заказать" onclick="buyOneclick(%id)" data-id="%id">
								<div class="privacy text">Отправляя форму, я даю согласие на <a href="%root/privacy/">обработку персональных данных</a>.</div>
								<div class="required"><span style="color:red;">*</span> — Поля, обязательные для заполнения</div>
								<div class="errors error" style="display:none"></div>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>