<div class="modal modal_inCart fade" id="modalInCart" tabindex="-1" role="dialog" aria-labelledby="Товар в корзине" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
			<div class="h2">Товар в корзине</div>
			<div class="modal__footer">
				<a class="button" href="%root/shop/cart/">Оформить заказ</a>
				<button class="button" data-dismiss="modal" aria-label="Close">Продолжить покупки</button>
			</div>
		</div>
	</div>
</div>
<div class="modal modal_inCart fade" id="modalNotifyWhenAvailable" tabindex="-1" role="dialog" aria-labelledby="Оповестить когда товар появится в наличии" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
			<div class="h2">Сообщить когда появится</div>
			<div class="control">
				<label class="label">Ваше имя<span style="color:red;">*</span>:</label>
				<input id="notify_when_fio" class="input" type="text" name="fio" value="">
				<div class="errors error_fio" style="display:none"></div>
			</div>
			А также что-то одно из:<br>
			<div class="control">
				<label class="label">Телефон<span style="color:red;">*</span>:</label>
				<input id="notify_when_telefon" class="input" type="text" name="tel" value="">
				<div class="errors error_tel" style="display:none"></div>
			</div>
			или
			<div class="control">
				<label class="label">e-mail для связи<span style="color:red;">*</span>:</label>
				<input id="notify_when_email" class="input" type="text" name="email" value="">
				<div class="errors error_email" style="display:none"></div>
			</div>
			<br>
			<div class="modal__footer">
				<button id="notify_when_button" class="button" data-dismiss="modal" aria-label="Close" onclick="notifyWhenAvailable(this)">Отправить!</button>
			</div>
		</div>
	</div>
</div>
