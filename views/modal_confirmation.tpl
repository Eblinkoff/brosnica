<div class="modal modal_inCart fade" id="modalConfirmation" tabindex="-1" role="dialog" aria-labelledby="Сообщение" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<a class="modal-close fas fa-times" href="javascript:void(0)" data-dismiss="modal" aria-label="Close"></a>
			<div class="h2">Сообщение</div>
			<div class="modal__footer">
				<button class="button" data-dismiss="modal" aria-label="Close">Хорошо</button>
			</div>
		</div>
	</div>
</div>
